/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.experiments;

import cz.cvut.felk.its.api.structures.EvRequest;
import cz.cvut.felk.its.api.structures.EvResponse;

public class TestResult {
	private EvRequest request;
	private EvResponse response;

	private double fmTimeMillis;
	private double lmTimeMillis;
	private double searchTimeMillis;
	private double totalTimeMillis;

	private double fmNumOfIters;
	private double lmNumOfIters;
	private double searchNumOfIters;

	public TestResult(EvRequest request) {
		this.request = request;
	}

	public EvResponse getResponse() {
		return response;
	}

	public void setResponse(EvResponse response) {
		this.response = response;
	}

	public double getFmTimeMillis() {
		return fmTimeMillis;
	}

	public void setFmTimeMillis(double fmTimeMillis) {
		this.fmTimeMillis = fmTimeMillis;
	}

	public double getLmTimeMillis() {
		return lmTimeMillis;
	}

	public void setLmTimeMillis(double lmTimeMillis) {
		this.lmTimeMillis = lmTimeMillis;
	}

	public double getTotalTimeMillis() {
		return totalTimeMillis;
	}

	public void setTotalTimeMillis(double totalTimeMillis) {
		this.totalTimeMillis = totalTimeMillis;
	}

	public int getNumOfJourneys() {
		return response.plans.size();
	}

	public double getSearchTimeMillis() {
		return searchTimeMillis;
	}

	public void setSearchTimeMillis(double searchTimeMillis) {
		this.searchTimeMillis = searchTimeMillis;
	}

	public EvRequest getRequest() {
		return request;
	}

	public double getFmNumOfIters() {
		return fmNumOfIters;
	}

	public void setFmNumOfIters(double fmNumOfIters) {
		this.fmNumOfIters = fmNumOfIters;
	}

	public double getLmNumOfIters() {
		return lmNumOfIters;
	}

	public void setLmNumOfIters(double lmNumOfIters) {
		this.lmNumOfIters = lmNumOfIters;
	}

	public double getSearchNumOfIters() {
		return searchNumOfIters;
	}

	public void setSearchNumOfIters(double searchNumOfIters) {
		this.searchNumOfIters = searchNumOfIters;
	}
}
