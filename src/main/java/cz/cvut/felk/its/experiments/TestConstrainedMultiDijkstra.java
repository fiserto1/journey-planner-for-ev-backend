/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.experiments;

import cz.agents.basestructures.BoundingBox;
import cz.agents.basestructures.GPSLocation;
import cz.agents.geotools.DistanceUtil;
import cz.agents.geotools.KDTree;
import cz.agents.geotools.Transformer;
import cz.cvut.felk.its.api.structures.*;
import cz.cvut.felk.its.evstructures.chargers.ChargerType;
import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.exceptions.BatteryLowException;
import cz.cvut.felk.its.exceptions.OutOfZoneBounds;
import cz.cvut.felk.its.exceptions.UnreachableDestinationException;
import cz.cvut.felk.its.routing.SpeedUpSettings;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.routing.structures.routers.EvRouter;
import cz.cvut.felk.its.routing.structures.routers.RouterType;
import cz.cvut.felk.its.routing.structures.PlanningInstance;
import cz.cvut.felk.its.routing.structures.graph.FMSearchGraph;
import cz.cvut.felk.its.routing.structures.graph.LMSearchGraph;
import cz.cvut.felk.its.utils.EdgePrecomputer;
import cz.cvut.felk.its.utils.GraphExtender;
import cz.cvut.felk.its.evstructures.*;
import cz.cvut.felk.its.evstructures.graphs.ExtendedGraph;
import cz.cvut.felk.its.evstructures.journey.Journey;
import cz.cvut.felk.its.utils.EVZoneProvider;
import cz.cvut.felk.its.utils.KDTreeProvider;
import cz.cvut.felk.its.utils.TextFilesWorker;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class TestConstrainedMultiDijkstra {

	private final static Logger log = Logger.getLogger(TestConstrainedMultiDijkstra.class);

//	private static EVZoneProvider evZoneProvider;
	private static EVZone<EvNode, EvEdge> zone;
	private static KDTree<GPSLocation> kdTree;
	private static Transformer transformer;

	//oLatE6, oLonE6, dLatE6, dLonE6;
//	private static String DEFAULT_REQ_FILENAME = "requestSet1.csv";
//	private static String DEFAULT_REQ_FILENAME = "selectedGerCitiesODPairs.csv";
	private static String DEFAULT_REQ_FILENAME = "combinedRequestSet.csv";
	private static List<int[]> odPairsE6FromFile;

	//oLatE6, oLonE6, dLatE6, dLonE6;
	private static List<int[]> randomODPairsE6;
	private static final Random rand = new Random(53684);
	private static final int numOfRandomRequests = 95;

	//defined zone shape by more bboxes! better precision to generate point in zoneBBox
	private static Set<BoundingBox> limitedBBoxes;
	private static LocalDateTime departureTime;
	private static RouterType routerType = RouterType.MO_FAST;

	public static void main( String[] args ) {
		init(args);

		//merge ODpairs from file and random odPairs and save them to file
//		saveODPairsToCSVFile("combinedRequestSet.csv", buildCombinedODPairsE6());

//		runTestForPlay();

//		printNumOfSingleEdgesBetweenChargers();
//
		runAllBPTests(odPairsE6FromFile);

		destroy();
	}

	private static void printNumOfSingleEdgesBetweenChargers() {
		int numOfParallelEdges = 0;
		Collection<EvEdge> allEdges = EVZoneProvider.INSTANCE.getZone().getGraph().getAllEdges();
		for (EvEdge allEdge : allEdges) {
			if (allEdge instanceof EvEdgeBetweenChargers) {
				EvEdgeBetweenChargers evEbCh = (EvEdgeBetweenChargers)allEdge;
				numOfParallelEdges += evEbCh.getPaths().size();
			}
		}

		log.info("Num of all parallel edges: " + numOfParallelEdges);

	}

	private static void runAllBPTests(List<int[]> odPairsE6) {

		setDefaulTestSettings();
		TestInstance ts1 = runComputationTimeTestWithRandomInitSoCAndCpH("rnd", odPairsE6);
		ts1.printToFileInLatexLangWithCosts();


		setDefaulTestSettings();
		TestSuite ts12 = runCPHText(odPairsE6, 50);
		ts12.printToFileInLatexLangWithCosts();


		setDefaulTestSettings();
		TestSuite ts2 = runDominanceRelaxTest(odPairsE6, 50, 0);
		ts2.printToFileInLatexLangWithCostsTotalOnly();
//
//
		setDefaulTestSettings();
		TestSuite ts3 = runInitSoCTest(odPairsE6, 0);
		ts3.printToFileInLatexLangWithCosts();
//
//
		setDefaulTestSettings();
		TestSuite ts4 = runChargingPolicyComparisonTest(odPairsE6, 50, 0);
		ts4.printToFileInLatexLangWithCostsTotalOnly();
//
		setDefaulTestSettings();
		TestSuite ts5 = runSpeedUpComparisonTest(odPairsE6.subList(0,1), 50, 0);
		ts5.printToFileInLatexLangWithCosts();

	}

	private static TestSuite runChargingPolicyComparisonTest(List<int[]> odPairsE6, int socInPerc, int centsPerHour) {

		List<TestInstance> tests = new ArrayList<>();

		SpeedUpSettings.DOM_BY_CHARGING = false;

		tests.add(runChargingPolicyTest(true, false, false, 0, odPairsE6, socInPerc, centsPerHour));
		tests.add(runChargingPolicyTest(false, true, false, 0, odPairsE6, socInPerc, centsPerHour));
		tests.add(runChargingPolicyTest(true, true, false, 0, odPairsE6, socInPerc, centsPerHour));
		tests.add(runChargingPolicyTest(true, true, true, 10, odPairsE6, socInPerc, centsPerHour));
		tests.add(runChargingPolicyTest(true, true, true, 20, odPairsE6, socInPerc, centsPerHour));

		return new TestSuite("tsChPolicy", tests);
	}

	private static TestInstance runChargingPolicyTest(boolean minChargSoC, boolean maxChargSoC, boolean altChargSoC,
			int altChargStep, List<int[]> odPairsE6,
			int socInPerc, int centsPerHour) {

		SpeedUpSettings.minChargeableSoc = minChargSoC;
		SpeedUpSettings.maxChargeableSoc = maxChargSoC;
		SpeedUpSettings.alterChargeableSoc = altChargSoC;

		if (altChargSoC) {
			SpeedUpSettings.chargingLimitStepInPerc = altChargStep;
			SpeedUpSettings.minChargingLimitInPerc = altChargStep;
		}

		String testPrefix = String.format("ts%b%b%b%d", minChargSoC, maxChargSoC, altChargSoC, altChargStep);
		return runComputationTimeTest(testPrefix, odPairsE6, socInPerc, centsPerHour, 0);
	}

	private static void setDefaulTestSettings() {

		SpeedUpSettings.BAG_LIMIT = false;
		SpeedUpSettings.DOM_BASIC_APPROACH = true;
		SpeedUpSettings.DOM_RELAX = true;
		SpeedUpSettings.DOM_RELAX_PERC_ENABLED = 0.95;
		SpeedUpSettings.DOM_DELTA_REDUCTION = false;
		SpeedUpSettings.DOM_BY_CHARGING = true;
		SpeedUpSettings.ABORT_CHARGER_CYCLE = false;
		SpeedUpSettings.REDUCE_BY_ELLIPSE = true;
		SpeedUpSettings.EARLY_SEARCH_FINISH = false;

		SpeedUpSettings.minChargeableSoc = true;
		SpeedUpSettings.maxChargeableSoc = true;
		SpeedUpSettings.alterChargeableSoc = true;
		SpeedUpSettings.chargingLimitStepInPerc = 20;
		SpeedUpSettings.minChargingLimitInPerc = 20;
	}

	private static void runTestForPlay() {

		SpeedUpSettings.BAG_LIMIT = false;
		SpeedUpSettings.DOM_BASIC_APPROACH = true;
		SpeedUpSettings.DOM_RELAX = true;
		SpeedUpSettings.DOM_RELAX_PERC_ENABLED = 0.95;
		SpeedUpSettings.DOM_DELTA_REDUCTION = false;
		SpeedUpSettings.DOM_BY_CHARGING = true;
		SpeedUpSettings.ABORT_CHARGER_CYCLE = true;
		SpeedUpSettings.REDUCE_BY_ELLIPSE = true;
		SpeedUpSettings.EARLY_SEARCH_FINISH = false;
//		TestSuite ts1 = runDominanceRelaxTest(odPairsE6FromFile.subList(0,1), 50, 0);
		TestSuite ts1 = runChargingPolicyComparisonTest(odPairsE6FromFile, 50, 0);
		ts1.printToFileInLatexLangWithCosts();

	}


	private static void saveODPairsToCSVFile(String filename, List<int[]> odPairs) {
		List<String> lines = new ArrayList<>();

		for (int[] odPair : odPairs) {
			String line = String.format("%.6f, %.6f, %.6f, %.6f",
					GraphTools.convertFromE6Format(odPair[0]), GraphTools.convertFromE6Format(odPair[1]),
					GraphTools.convertFromE6Format(odPair[2]), GraphTools.convertFromE6Format(odPair[3]));

			lines.add(line);
		}

		try {
			Files.write(Paths.get(filename), lines);
			log.info(String.format("File \"%s\" was saved.", filename));
		} catch (IOException e) {
			log.error(String.format("File \"%s\" was not saved.", filename), e);
		}

	}

	private static List<int[]> buildCombinedODPairsE6() {

		List<int[]> odPairs = new ArrayList<>(odPairsE6FromFile);
		odPairs.addAll(randomODPairsE6);

		return odPairs;
	}

	private static TestSuite runSpeedUpComparisonTest(List<int[]> odPairs, int socInPerc, int centsPerHour) {
		List<TestInstance> tests = new ArrayList<>();
		//
		//domRelax, domDelta, ellipseReduction,
		tests.add(runSpeedUpTest(true, true, true, true, true, odPairs, socInPerc, centsPerHour));
		tests.add(runSpeedUpTest(true, false, true, true, true, odPairs, socInPerc, centsPerHour));
		tests.add(runSpeedUpTest(true, false, false, true, false, odPairs, socInPerc, centsPerHour));
		tests.add(runSpeedUpTest(false, true, false, true, false, odPairs, socInPerc, centsPerHour));
		tests.add(runSpeedUpTest(false, false, true, true, false, odPairs, socInPerc, centsPerHour));
		tests.add(runSpeedUpTest(false, false, false, true, true, odPairs, socInPerc, centsPerHour));
		tests.add(runSpeedUpTest(true, false, true, true, false, odPairs, socInPerc, centsPerHour));
		tests.add(runSpeedUpTest(true, false, false, true, true, odPairs, socInPerc, centsPerHour));
		tests.add(runSpeedUpTest(false, false, true, true, true, odPairs, socInPerc, centsPerHour));
		tests.add(runSpeedUpTest(false, false, false, true, false, odPairs, socInPerc, centsPerHour));

		return new TestSuite("tsSpeedUp", tests);
	}

	private static TestInstance runSpeedUpTest(boolean domRelax, boolean domDelta, boolean domCharge, boolean abortChCycles,
			boolean reduceByEllipse, List<int[]> odPairs, int socInPerc, int centsPerHour) {
		SpeedUpSettings.DOM_RELAX = domRelax;
		SpeedUpSettings.DOM_DELTA_REDUCTION = domDelta;
		SpeedUpSettings.DOM_BY_CHARGING = domCharge;
		SpeedUpSettings.ABORT_CHARGER_CYCLE = abortChCycles;
		SpeedUpSettings.REDUCE_BY_ELLIPSE = reduceByEllipse;

		String testPrefix = String.format("ts%b%b%b%b%b", domRelax, domDelta, domCharge, abortChCycles, reduceByEllipse);
		return runComputationTimeTest(testPrefix, odPairs, socInPerc, centsPerHour, 0);


	}

	private static TestSuite runCPHText(List<int[]> odPairsE6, int socInPerc) {

		String testName = String.format("tsCPH-dom%d-soc%d", (int) (SpeedUpSettings.DOM_RELAX_PERC_ENABLED*100), socInPerc);
		List<TestInstance> tests = new ArrayList<>();

		log.info(String.format("Computation testSuite \"%s\" started.", testName));

		for (int centsPerHour = 0; centsPerHour <= 2000; centsPerHour+=50) {
			TestInstance test = runComputationTimeTest("tsCPH", odPairsE6, socInPerc, centsPerHour,
					centsPerHour);

			tests.add(test);
		}

		return new TestSuite(testName, tests);
	}

	private static TestSuite runInitSoCTest(List<int[]> odPairsE6, int centsPerHour) {

		String testName = String.format("tsSoC-dom%d-cph%d", (int) (SpeedUpSettings.DOM_RELAX_PERC_ENABLED*100), centsPerHour);
		List<TestInstance> tests = new ArrayList<>();

		log.info(String.format("Computation testSuite \"%s\" started.", testName));

		for (int initSoCInPerc = 10; initSoCInPerc <= 100; initSoCInPerc+=10) {
			TestInstance test = runComputationTimeTest("tsSoC", odPairsE6, initSoCInPerc, centsPerHour,
					initSoCInPerc);

			tests.add(test);
		}

		return new TestSuite(testName, tests);
	}

	private static TestSuite runDominanceRelaxTest(List<int[]> odPairsE6, int socInPerc,
			int centsPerHour) {

		String testName = String.format("tsDom-soc%d-cph%d", socInPerc, centsPerHour);
		List<TestInstance> tests = new ArrayList<>();

		log.info(String.format("Computation testSuite \"%s\" started.", testName));

		double dominanceRelaxConst;
		for (int i = 80; i <= 100; i+=2) {
			dominanceRelaxConst = ((double) i)/100;
			SpeedUpSettings.DOM_RELAX_PERC_ENABLED = dominanceRelaxConst;

			TestInstance test = runComputationTimeTest("tsDom", odPairsE6, socInPerc, centsPerHour,
					dominanceRelaxConst);


			tests.add(test);
		}

		return new TestSuite(testName, tests);
	}

	private static TestInstance runComputationTimeTest(List<int[]> odPairsE6, int socInPerc, int centsPerHour) {
		return runComputationTimeTest("", odPairsE6, socInPerc, centsPerHour, Double.MAX_VALUE);
	}

	private static TestInstance runComputationTimeTest(String namePrefix, List<int[]> odPairsE6, int socInPerc,
			int centsPerHour, double testingConst) {

		String testName = String.format("%scomp-time-soc%d-cph%d-domin%d", namePrefix, socInPerc, centsPerHour,
				(int) (SpeedUpSettings.DOM_RELAX_PERC_ENABLED*100));

		log.info(String.format("Computation time test \"%s\" started.", testName));

		List<EvRequest> requestsSame = generateEvRequests(odPairsE6, socInPerc, centsPerHour);

		List<TestResult> testResults  = runComputationTimeTest(requestsSame, routerType);

		return new TestInstance(testName, testingConst, testResults);

	}

	private static TestInstance runComputationTimeTestWithRandomInitSoCAndCpH(String namePrefix, List<int[]> odPairsE6) {

		String testName = String.format("%scomp-time-domin%d", namePrefix,
				(int) (SpeedUpSettings.DOM_RELAX_PERC_ENABLED*100));

		log.info(String.format("Computation time test with random init Soc and CpH \"%s\" started.", testName));

		List<EvRequest> requests = generateEvRequestsWithRandomInitSocAndCpH(odPairsE6);

		List<TestResult> testResults  = runComputationTimeTest(requests, routerType);

		return new TestInstance(testName, 0, testResults);

	}

	private static List<TestResult> runComputationTimeTest(List<EvRequest> requests, RouterType routerType) {

		List<RouterType> routerTypes = Collections.singletonList(routerType);
		List<TestResult> testResults = new ArrayList<>();

		int i = 0;
		for (EvRequest evRequest : requests) {

			TestResult testResult = new TestResult(evRequest);
			log.debug(String.format("Running request %d/%d: %s", i+1, requests.size(), evRequest));
			i++;

			try {
				long startTime = System.currentTimeMillis();

				EvResponse evResponse = retrieveResponse(evRequest, routerTypes, testResult);

				testResult.setTotalTimeMillis(System.currentTimeMillis()-startTime);
				testResult.setResponse(evResponse);

				testResults.add(testResult);


			} catch (BatteryLowException | OutOfZoneBounds | UnreachableDestinationException e) {
				log.error(String.format("Request Failed. %s", evRequest), e);
			}
		}

		return testResults;
	}

	private static EvResponse retrieveResponse(EvRequest evRequest, List<RouterType> routerTypes, TestResult testResult)
			throws BatteryLowException, OutOfZoneBounds, UnreachableDestinationException {

		PlanningInstance planningInstance = buildPlanningInstance(evRequest, testResult);

		List<Plan> plans = new ArrayList<>();
		long startTime, elapsedTime;

		int planId = 0;
		for (RouterType routerType : routerTypes) {

//			startTime = System.currentTimeMillis();
			EvRouter router = routerType.getEvRouter();
			List<Journey> optimalJourneys = router.findOptimalJourneyTest(planningInstance, testResult);

//			testResult.setSearchTimeMillis(System.currentTimeMillis()-startTime);

			for (Journey optimalJourney : optimalJourneys) {
				Plan plan = EvResponseBuilder.buildPlan(planId, planningInstance, optimalJourney);
				plans.add(plan);
				planId++;
			}

//			elapsedTime = System.currentTimeMillis() - startTime;
//			log.info(String.format("Compute %s takes %d ms", routerType, elapsedTime));
		}

		EvResponse evResponse = EvResponseBuilder.buildResponse(planningInstance, plans);

		// TODO: 27-Oct-16 make this never happen!
		if (evResponse == null) {
			log.error(String.format("Initial state of charge '%s' is too low.", planningInstance.getInitSOC()));
			throw new BatteryLowException(planningInstance.getInitSOC());
		}

		return evResponse;
	}

	private static PlanningInstance buildPlanningInstance(EvRequest evRequest, TestResult testResult)
			throws BatteryLowException, OutOfZoneBounds, UnreachableDestinationException {
		BatteryType batteryType = BatteryType.TESLA;
		ChargerType chargerType = ChargerType.SUPERCHARGER;

		ChargingFunction chargingFunction = new ChargingFunction(
				batteryType, chargerType);

//		double DOM_RELAX_PERC = (double) evRequest.getStateOfChargeInPerc()/100;
		int initSOC = (int) (((double) evRequest.getStateOfChargeInPerc()/100) * batteryType.getMaxSoC());

		if (!batteryType.isInBatteryRange(initSOC)) {
			log.error(String.format("Initial state of charge '%s' is too low.", initSOC));
			throw new BatteryLowException(initSOC);
		}

		EVZone<EvNode, EvEdge> zone = EVZoneProvider.INSTANCE.getZone();

		if (!isODInsideZone(zone, evRequest.getOrigin(), evRequest.getDestination())) {
			throw new OutOfZoneBounds(zone.getName(), zone.getBoundingBox());
		}

		GPSLocation originLoc = zone.createGPSLocationInZone(evRequest.getOrigin());
		GPSLocation destinationLoc = zone.createGPSLocationInZone(evRequest.getDestination());

		ExtendedGraph<EvNode, EvEdge> extendedGraph = GraphExtender.buildExtendedGraph(
				zone, originLoc, destinationLoc);

		ExtendedGraph<EvNode, EvEdge> graphFMLM = buildGraphFMLM(extendedGraph, initSOC, zone.getNumOfChargers(),
				testResult);
		//        ExtendedGraph<EvNode, EvEdge> graphFMLM = null;

		double centsPerSecond = ((double)evRequest.getCentsPerHour())/3600;

		return new PlanningInstance(Collections.singletonList(originLoc), Collections.singletonList(destinationLoc),
				zone, extendedGraph, graphFMLM,
				initSOC, batteryType, chargerType, chargingFunction, departureTime, centsPerSecond);
	}

	private static ExtendedGraph<EvNode, EvEdge> buildGraphFMLM(ExtendedGraph<EvNode, EvEdge> extendedGraph, int initSOC,
			int numOfChargers, TestResult testResult)
			throws BatteryLowException, UnreachableDestinationException {

		FMSearchGraph<EvNode, EvEdge> fmSearchGraph = new FMSearchGraph<>(extendedGraph);
		LMSearchGraph<EvNode, EvEdge> lmSearchGraph = new LMSearchGraph<>(extendedGraph);

		Set<Integer> chargerNodeIds = extendedGraph.getChargerNodeIds();

		////////////////////////////////////////
		//FIRST MILE////////////////////////////
		////////////////////////////////////////
		long start = System.currentTimeMillis();
		Map<Integer, EvEdge> fmMultiEdges = EdgePrecomputer.computeFMEdgesTest(numOfChargers,
				fmSearchGraph, extendedGraph, initSOC, departureTime, chargerNodeIds, testResult);

//		testResult.setFmTimeMillis(System.currentTimeMillis() - start);

		// long fmElapsedTime = System.currentTimeMillis() - start;
//		log.info(String.format("Compute First mile takes %d ms num of fmEdges is %d", fmElapsedTime, fmMultiEdges.size()));

		if (fmMultiEdges.isEmpty()) throw new BatteryLowException(initSOC);

		////////////////////////////////////////
		//LAST MILE/////////////////////////////
		////////////////////////////////////////
		start = System.currentTimeMillis();

		Map<Integer, EvEdge> lmMultiEdges = EdgePrecomputer
				.computeLMByBckwEdgesTest(lmSearchGraph, extendedGraph, extendedGraph.getNumOfNodes(),
						chargerNodeIds, testResult);

//		testResult.setFmTimeMillis(System.currentTimeMillis() - start);

//		long lmElapsedTime = System.currentTimeMillis() - start;
//		log.info(String.format("Compute Last mile takes %d ms; num of lmEdges is %d", lmElapsedTime, lmMultiEdges.size()));
		if (lmMultiEdges.isEmpty() && !fmMultiEdges.containsKey(extendedGraph.getGoalNode().id)) throw new UnreachableDestinationException();

		return new ExtendedGraph<>(extendedGraph, extendedGraph.getStartNode(), extendedGraph.getGoalNode(),
				fmMultiEdges, lmMultiEdges);
	}

	private static boolean isODInsideZone(EVZone<EvNode, EvEdge> zone, Coordinates origin, Coordinates destination) {
		return zone.getBoundingBox().inside(origin.getLonE6(), origin.getLatE6())
				&& zone.getBoundingBox().inside(destination.getLonE6(), destination.getLatE6());
	}


	private static List<EvRequest> generateEvRequests(List<int[]> odPairs, int socInPerc, int centsPerHour) {

		List<EvRequest> requests = new ArrayList<>();

		for (int[] odPair : odPairs) {
			requests.add(new EvRequest("test", new Coordinates(odPair[0], odPair[1], 100),
					new Coordinates(odPair[2], odPair[3], 100), socInPerc, centsPerHour));
		}

		return requests;
	}

	private static List<EvRequest> generateEvRequestsWithRandomInitSocAndCpH(List<int[]> odPairs) {

		List<EvRequest> requests = new ArrayList<>();


		int socInPerc;
		int centsPerHour;
		for (int[] odPair : odPairs) {
			socInPerc = ThreadLocalRandom.current().nextInt(40, 100);
			centsPerHour = ThreadLocalRandom.current().nextInt(0, 100);
			requests.add(new EvRequest("test", new Coordinates(odPair[0], odPair[1], 100),
					new Coordinates(odPair[2], odPair[3], 100), socInPerc, centsPerHour));
		}

		return requests;
	}


	private static void init(String[] args) {
//		departureTime = LocalDateTime.now();
		departureTime = LocalDateTime.of(2016, 12, 24, 10, 0, 0);

//		evZoneProvider = EVZoneProvider.INSTANCE;
		zone = EVZoneProvider.INSTANCE.getZone();
		zone.createGPSLocationInZone(new Coordinates(50219095, 8316650));
		kdTree = KDTreeProvider.INSTANCE.getKdTree();

		String requestsFilename = DEFAULT_REQ_FILENAME;

		if (args.length != 0) {
			requestsFilename = args[0];
		}

		loadODPairsFromFile(requestsFilename);

		defineLimitedBBoxes();
		generateRandomODPairs(zone.getBoundingBox(), numOfRandomRequests);

		//first slow request
		runComputationTimeTest("first-slow-req", odPairsE6FromFile.subList(0,2), 80, 50, 80);

	}

	private static void defineLimitedBBoxes() {

		limitedBBoxes = new HashSet<>();

		limitedBBoxes.add(new BoundingBox(7075000, 50931000, 14326000, 54072000));
		limitedBBoxes.add(new BoundingBox(6350000, 49167000, 12568000, 50903000));
		limitedBBoxes.add(new BoundingBox(8108000, 47725000, 13074000, 49167000));
	}

	private static void loadODPairsFromFile(String filename) {

		List<String[]> odPairs = TextFilesWorker
				.loadCSVLinesFromFileInResources(filename, "experiments", ",").collect(Collectors.toList());

		odPairsE6FromFile = new ArrayList<>();

		for (String[] odPair : odPairs) {
			int[] odLatLon = new int[4];

			odLatLon[0] = GraphTools.convertToE6Format(Double.parseDouble(odPair[0]));
			odLatLon[1] = GraphTools.convertToE6Format(Double.parseDouble(odPair[1]));
			odLatLon[2] = GraphTools.convertToE6Format(Double.parseDouble(odPair[2]));
			odLatLon[3] = GraphTools.convertToE6Format(Double.parseDouble(odPair[3]));
			odPairsE6FromFile.add(odLatLon);
		}
	}


	private static void generateRandomODPairs(BoundingBox bbox, int numOfRandomRequests) {

		randomODPairsE6 = new ArrayList<>();

		for (int i = 0; i < numOfRandomRequests; i++) {
			int[] odLatLon = new int[4];

			do {
				//ORIGIN
				do {
					odLatLon[0] = rand.nextInt(bbox.maxLatE6 - bbox.minLatE6) + bbox.minLatE6;
					odLatLon[1] = rand.nextInt(bbox.maxLonE6 - bbox.minLonE6) + bbox.minLonE6;
				} while (!isInLimitedBBoxes(odLatLon[0], odLatLon[1]));

				//DESTINATION
				do {
					odLatLon[2] = rand.nextInt(bbox.maxLatE6 - bbox.minLatE6) + bbox.minLatE6;
					odLatLon[3] = rand.nextInt(bbox.maxLonE6 - bbox.minLonE6) + bbox.minLonE6;
				} while (!isInLimitedBBoxes(odLatLon[0], odLatLon[1]));
			} while (!hasLongerDistanceThan(odLatLon));

			randomODPairsE6.add(odLatLon);
		}
	}

	private static boolean hasLongerDistanceThan(int[] odLatLon) {

		double distance = DistanceUtil.computeGreatCircleDistance(
				GraphTools.convertFromE6Format(odLatLon[0]), GraphTools.convertFromE6Format(odLatLon[1]),
				GraphTools.convertFromE6Format(odLatLon[2]), GraphTools.convertFromE6Format(odLatLon[3]));

		return distance > 20000;
	}

	private static boolean isInLimitedBBoxes(int latE6, int lonE6) {

		for (BoundingBox limitedBBox : limitedBBoxes) {
			if (limitedBBox.inside(lonE6, latE6)) {
				return true;
			}
		}
		return false;

	}

	private static void destroy() {
		EVZoneProvider.INSTANCE.destroyZone();
		org.geotools.referencing.factory.DeferredAuthorityFactory.exit();
		org.geotools.util.WeakCollectionCleaner.DEFAULT.exit();
	}
}


