/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.experiments;

import cz.cvut.felk.its.api.structures.Plan;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestInstance {

	private static final Logger log = Logger.getLogger(TestInstance.class);

	private String testName;
	private double constValue;
	private List<TestResult> results;

	public TestInstance(String testName, double constValue, List<TestResult> results) {
		this.testName = testName;
		this.constValue = constValue;
		this.results = results;
	}

	public String getTestName() {
		return testName;
	}

	public double getConstValue() {
		return constValue;
	}

	public void printToFileInLatexLang() {
		TeXTool teXTool = new TeXTool();
		//HEAD
		teXTool.addHeader(Arrays.asList("#", "FM [ms]", "LM [ms]", "Search [ms]", "Total [ms]", "N"));

		//DATA
		for (int i = 0; i < results.size(); i++) {
			teXTool.addRow(valuesToStringCols(i, results.get(i)));
		}

		//AVG
		teXTool.addRow(computeAvgValuesAsStrings());

		teXTool.write(testName + ".txt");
	}

	public void printToFileInLatexLangWithCosts() {
		TeXTool teXTool = new TeXTool();
		//HEAD
		teXTool.addHeader(Arrays.asList("#", "FM [ms]", "LM [ms]", "Search [ms]", "Total [ms]", "N", "first C", "last C"));

		//DATA
		for (int i = 0; i < results.size(); i++) {
			teXTool.addRow(valuesWithCostsToStringCols(i, results.get(i)));
		}

		//AVG
		teXTool.addRow(computeAvgValuesAsStrings());

		teXTool.write(testName + ".txt");
	}

	public double[] computeAvgValues() {

		int numOfRequests = results.size();

		double[] values = new double[12];
		Arrays.fill(values, 0);

		for (TestResult result : results) {
			values[0] += result.getFmTimeMillis();
			values[1] += result.getFmNumOfIters();
			values[2] += result.getLmTimeMillis();
			values[3] += result.getLmNumOfIters();
			values[4] += result.getSearchTimeMillis();
			values[5] += result.getSearchNumOfIters();
			values[6] += result.getTotalTimeMillis();
			values[7] += result.getNumOfJourneys();
			values[8] += result.getResponse().plans.get(0).travelTime;
			values[9] += result.getResponse().plans.get(0).price;
			values[10] += result.getResponse().plans.get(result.getNumOfJourneys()-1).travelTime;
			values[11] += result.getResponse().plans.get(result.getNumOfJourneys()-1).price;
		}

		for (int i = 0; i < values.length; i++) {
			values[i] = values[i]/numOfRequests;
		}

		return values;

	}
	private List<String> computeAvgValuesAsStrings() {

		double[] doubles = computeAvgValues();

		List<String> cols = new ArrayList<>();

		cols.add(" ");
		cols.add(formatDouble(doubles[0]) + " " + addBrackets(formatDouble(doubles[1])));
		cols.add(formatDouble(doubles[2]) + " " + addBrackets(formatDouble(doubles[3])));
		cols.add(formatDouble(doubles[4]) + " " + addBrackets(formatDouble(doubles[5])));
		cols.add(formatDouble(doubles[6]));
		cols.add(String.valueOf(((int)doubles[7])));
		cols.add(costsToString(doubles[8], doubles[9]));
		cols.add(costsToString(doubles[10], doubles[11]));

		return cols;
	}

	private String addBrackets(String s) {
		return String.format("(%s)", s);
	}

	private List<String> valuesToStringCols(int i, TestResult tr) {

		List<String> cols = new ArrayList<>();

		cols.add(String.valueOf(i+1));
		cols.add(formatDouble(tr.getFmTimeMillis()) + " " + addBrackets(formatDouble(tr.getFmNumOfIters())));
		cols.add(formatDouble(tr.getLmTimeMillis()) + " " + addBrackets(formatDouble(tr.getLmNumOfIters())));
		cols.add(formatDouble(tr.getSearchTimeMillis()) + " " + addBrackets(formatDouble(tr.getSearchNumOfIters())));

		double totalNumOfIters = tr.getFmNumOfIters()+tr.getLmNumOfIters()+tr.getSearchNumOfIters();
		cols.add(formatDouble(tr.getTotalTimeMillis()) + " " + addBrackets(formatDouble(totalNumOfIters)));
		cols.add(String.valueOf(tr.getNumOfJourneys()));

		return cols;
	}

	private List<String> valuesWithCostsToStringCols(int i, TestResult tr) {

		List<String> cols = valuesToStringCols(i, tr);
		cols.add(getFirstCosts(i));
		cols.add(getLastCosts(i));

		return cols;
	}

	private String formatDouble(double value) {

		return String.format("%.0f", value);
	}

	public String getFirstCosts(int i) {
		Plan plan = results.get(i).getResponse().plans.get(0);
		return costsToString(plan);
	}

	public String getLastCosts(int i) {
		List<Plan> plans = results.get(i).getResponse().plans;
		Plan plan = plans.get(plans.size()-1);
		return costsToString(plan);
	}

	private String costsToString(Plan plan) {
		return String.format("(%d, %.2f)", (int)((double)plan.travelTime)/60, ((double)plan.price)/100);
	}

	public static String costsToString(double travelTimeMillis, double priceInCents) {
		return String.format("(%d, %.2f)", (int)(travelTimeMillis)/60, (priceInCents)/100);
	}
}
