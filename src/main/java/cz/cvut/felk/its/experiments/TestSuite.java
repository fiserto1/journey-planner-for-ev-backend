/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.experiments;

import cz.cvut.felk.its.routing.SpeedUpSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestSuite {
	private String testName;
	private List<TestInstance> tests;

	public TestSuite(String testName, List<TestInstance> tests) {
		this.testName = testName;
		this.tests = tests;
	}

	public void printToFileInLatexLang() {
		TeXTool teXTool = new TeXTool();
		//HEAD
		teXTool.addHeader(Arrays.asList("#", "Const", "FM [ms]", "LM [ms]", "Search [ms]", "Total [ms]", "N"));

		//DATA
		for (int i = 0; i < tests.size(); i++) {
			teXTool.addRow(valuesToStringCols(i, tests.get(i), false));
		}

		//AVG
//		teXTool.addRow(computeAvgValuesAsStrings());

		//Print Settings
		teXTool.addRow(Collections.singletonList(SpeedUpSettings.allValuesSettingsToString()));

		teXTool.write(testName + ".txt");
	}

	public void printToFileInLatexLangWithCosts() {
		TeXTool teXTool = new TeXTool();
		//HEAD
		teXTool.addHeader(Arrays.asList("#", "Const", "FM [ms]", "LM [ms]", "Search [ms]", "Total [ms]", "N",
				"1stCosts", "LastCosts"));

		//DATA
		for (int i = 0; i < tests.size(); i++) {
			teXTool.addRow(valuesToStringColsWithCosts(i, tests.get(i), false));
		}

		//AVG
		//		teXTool.addRow(computeAvgValuesAsStrings());

		//Print Settings
		teXTool.addRow(Collections.singletonList(SpeedUpSettings.allValuesSettingsToString()));

		teXTool.write(testName + ".txt");
	}

	public void printToFileInLatexLangWithCostsTotalOnly() {
		TeXTool teXTool = new TeXTool();
		//HEAD
		teXTool.addHeader(Arrays.asList("#", "Const", "Total [ms]", "N", "1stCosts", "LastCosts"));

		//DATA
		for (int i = 0; i < tests.size(); i++) {
			teXTool.addRow(valuesToStringColsWithCosts(i, tests.get(i), true));
		}

		//AVG
		//		teXTool.addRow(computeAvgValuesAsStrings());

		//Print Settings
		teXTool.addRow(Collections.singletonList(SpeedUpSettings.allValuesSettingsToString()));

		teXTool.write(testName + ".txt");
	}

	private List<String> valuesToStringColsWithCosts(int i, TestInstance test, boolean totalOnly) {

		double[] doubles = test.computeAvgValues();

		List<String> cols = new ArrayList<>();

		cols.add(String.valueOf(i+1));
		cols.add(formatDouble(test.getConstValue(), 2));

		if (!totalOnly) {
			cols.add(formatDouble(doubles[0]) + " " + addBrackets(formatDouble(doubles[1])));
			cols.add(formatDouble(doubles[2]) + " " + addBrackets(formatDouble(doubles[3])));
			cols.add(formatDouble(doubles[4]) + " " + addBrackets(formatDouble(doubles[5])));
		}

		cols.add(formatDouble(doubles[6]) + " " + addBrackets(formatDouble(doubles[1] + doubles[3] + doubles[5])));
		cols.add(String.valueOf(((int)doubles[7])));
		cols.add(TestInstance.costsToString(doubles[8], doubles[9]));
		cols.add(TestInstance.costsToString(doubles[10], doubles[11]));

		return cols;
	}

	private List<String> valuesToStringCols(int i, TestInstance test, boolean totalOnly) {

		List<String> strings = valuesToStringColsWithCosts(i, test, totalOnly);

		return strings.subList(0, strings.size()-2);
	}

	private String addBrackets(String s) {

		return String.format("(%s)",s);
	}

	private String formatDouble(double value, int precision) {
		return String.format("%."+precision+"f", value);
	}

	private String formatDouble(double value) {
		return String.format("%.0f", value);
	}
}
