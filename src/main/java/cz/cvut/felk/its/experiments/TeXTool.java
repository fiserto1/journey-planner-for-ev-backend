/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.experiments;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TeXTool {
	private static final Logger log = Logger.getLogger(TeXTool.class);

	private List<String> lines;

	public TeXTool() {
		lines = new ArrayList<>();
	}

	public void write(String filename) {
		try {
			Files.write(Paths.get(filename), lines);
			log.info(String.format("File \"%s\" was saved.", filename));
		} catch (IOException e) {
			log.error(String.format("File \"%s\" was not saved.", filename), e);
		}
	}

	public void addRow(List<String> cols) {

		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < cols.size(); i++) {

			stringBuilder.append(cols.get(i));

			if (i < cols.size()-1) {
				stringBuilder.append(" & ");
			} else {
				stringBuilder.append(" \\\\\n");
			}
		}

		lines.add(stringBuilder.toString());
		addHLine();
	}

	public void addHeader(List<String> cols) {
		addHLine();
		cols = boldText(cols);
		addRow(cols);
	}

	private List<String> boldText(List<String> cols) {
		return cols.stream().map(this::boldText).collect(Collectors.toList());
	}

	private String boldText(String col) {
		return String.format("\\textbf{%s}", col);
	}

	private void addHLine() {

		lines.add("\\hline\n");
	}
}
