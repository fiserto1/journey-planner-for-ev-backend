package cz.cvut.felk.its.api.structures;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by Tomas on 05.01.2016.
 */
@ApiModel(description = "Stopover of the plan. (Origin, Destination or Charger)")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Stopover extends Step implements Serializable {
    public static final String ORIGIN_TYPE = "ORIGIN";
    public static final String DESTINATION_TYPE = "DESTINATION";
    public static final String SUPERCHARGER_TYPE = "SUPERCHARGER";

    private static final long serialVersionUID = -8522625813403848475L;

    @ApiModelProperty(value = "Type of the stopover. (Origin, Destination or Charger")
    @JsonProperty(required = true)
    public final String type;

    @ApiModelProperty(value = "Seconds spent during the charging.")
    @JsonProperty(required = true)
    public final int chargingTime;

    @ApiModelProperty(value = "State of charge before the charging.")
    @JsonProperty(required = true)
    public final int incomingSoC;

    @ApiModelProperty(value = "Money spent for the charging. (euro)")
    @JsonProperty(required = true)
    public final int pricePerWh;

    public Stopover(String type, Coordinates coordinates, int chargingTime, int incomingSoC, int leavingStateOfCharge,
                    int pricePerWh, int travelTimeToNextStep,
                    int distanceToNextStep) {
        super(coordinates, distanceToNextStep, travelTimeToNextStep, leavingStateOfCharge);
        this.type = type;
        this.chargingTime = chargingTime;
        this.incomingSoC = incomingSoC;
        this.pricePerWh = pricePerWh;
    }

    @Override
    public String toString() {
        return "Stopover [" +
                "[" + super.toString() + "], " +
                "type='" + type + '\'' +
                ", chargingTime=" + chargingTime +
                ", incomingSoC=" + incomingSoC +
                ", pricePerWh=" + pricePerWh +
                ']';
    }
}
