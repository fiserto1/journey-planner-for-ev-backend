package cz.cvut.felk.its.api.structures;

import cz.agents.basestructures.GPSLocation;
import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.routing.structures.PlanningInstance;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.RoadType;
import cz.cvut.felk.its.evstructures.graphs.ExtendedGraph;
import cz.cvut.felk.its.evstructures.journey.Journey;
import cz.cvut.felk.its.evstructures.journey.JourneyNode;
import cz.cvut.felk.its.evstructures.journey.JourneySegment;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tomas on 08.12.2015.
 */
public final class EvResponseBuilder {

    private EvResponseBuilder(){}


//    public static EvResponse buildResponse(SearchGraph<EvLabel> graph, Solution[] solutions, Plan moPlan, GPSLocation origin, GPSLocation destination) {
//        if (solutions == null) return null;
//
//        List<Plan> plans = new LinkedList<>();
//        int planId = 0;
//        for (Solution solution : solutions) {
//            if (solution.getRouteNodeIds().isEmpty()) return null;
//
//            List<Step> steps = new ArrayList<>();
//
//            OptimalPathSolution optPathSol = (OptimalPathSolution) solution;
//            List<Stopover> stopovers = new ArrayList<>();
//
//            List<Integer> nodeIds = optPathSol.getRouteNodeIds();
////            EvNode origin = graph.getNode(nodeIds.get(0));
//            int leavingStateOfCharge = solution.statesOfCharge.get(0);
//
//            //todo maybe origin and destination should have coordinates from frontend request
//            int segmentNumber = 0;
//
//            stopovers.add(new Stopover(Stopover.ORIGIN_TYPE, new Coordinates(origin.latE6, origin.lonE6, origin.elevation), 0,
//                    optPathSol.segmentTravelTimesMillis.get(segmentNumber), optPathSol.segmentLengths.get(segmentNumber)));
//            for (int superchargerNodeId : optPathSol.superchargerNodeIds) {
//                segmentNumber++;
//                EvNode node = graph.getNode(superchargerNodeId);
//                Charger supercharger = node.getCharger();
//                stopovers.add(new Stopover(Stopover.SUPERCHARGER_TYPE, new Coordinates(supercharger.location),
//                        optPathSol.supChargingTimes.get(segmentNumber),
//                        optPathSol.statesOfCharge.get(segmentNumber), optPathSol.statesAfterCharge.get(segmentNumber),
//                        optPathSol.chargingPrices.get(segmentNumber), optPathSol.segmentTravelTimesMillis.get(segmentNumber),
//                        optPathSol.segmentLengths.get(segmentNumber)));
//            }
//
//            segmentNumber = 0;
//            int nextSuperchargerStep = -1;
//            if (((OptimalPathSolution) solution).numOfSegments != 1) {
//                nextSuperchargerStep = ((OptimalPathSolution) solution).superchargerStepIndexes.get(segmentNumber);
//            }
//            for (int i = 0; i < nodeIds.size(); i++) {
//                List<Step> stepsBetweenSteps = new LinkedList<>(); //simplified edges
//                EvNode node = graph.getNode(nodeIds.get(i));
//
//                Coordinates coord = new Coordinates(node);
//                Step step;
//                if (i < nodeIds.size() - 1) {
//                    EvNode nextNode = graph.getNode(nodeIds.get(i + 1));
//                    PlanningEdge edge = graph.getEdge(node.id, nextNode.id);
//                    if (edge == null) {
//                        System.out.println("null from " + node.id + " to " + nextNode.id);
//                        System.out.println(nodeIds);
//                    }
//
//                    if (nextSuperchargerStep == i) {
//
//                        leavingStateOfCharge = solution.statesAfterCharge.get(segmentNumber+1);
//                        segmentNumber++;
//                        if (((OptimalPathSolution) solution).numOfSegments-1 > segmentNumber){
//                            nextSuperchargerStep = ((OptimalPathSolution) solution).superchargerStepIndexes.get(segmentNumber);
//                        }
//                    }
////                    step = new Step(coord, ((EvEdge) edge).routePartLengths.get(0), edge.travelTimeMillis, leavingStateOfCharge); //tady
////                    int edgeConsumption = GraphTools.computeConsumptionBetweenTwoPoints(node, nextNode, edge.length);
////                    step = new Step(coord, 0, ((EvEdge) edge).travelTimeMillis, leavingStateOfCharge); //tady
////                    leavingStateOfCharge -= ((EvEdge) edge).getEvWeight(leavingStateOfCharge);
//                    /*
//                    simplified edges START
//                    */
//                    if (edge instanceof EvEdge) {
//                        EvEdge evEdge = (EvEdge) edge;
////                        if (evEdge.viaNodes != null) {
//                            GPSLocation fromLoc = node;
//                            GPSLocation toLoc;
//
//                            if (evEdge.viaNodes != null) {
//                                for (GPSLocation loc: evEdge.viaNodes) {
//                                    toLoc = loc;
//                                    leavingStateOfCharge = inspectStep(fromLoc, toLoc, leavingStateOfCharge, stepsBetweenSteps);
//                                    fromLoc = toLoc;
//                                }
//                            }
//
//                            toLoc = nextNode;
//                            leavingStateOfCharge = inspectStep(fromLoc, toLoc, leavingStateOfCharge, stepsBetweenSteps);
//
////                            int partIndex = 0;
////                            for (Coordinates stepCoords : evEdge.viaNodes) {
////
////                                leavingStateOfCharge -= GraphTools.computeNextPartConsumption(leavingStateOfCharge,
////                                        evEdge.routePartConsumptions.get(partIndex));
////                                Step stepBetweenSteps = new Step(stepCoords, evEdge.routePartLengths.get(partIndex+1),
////                                        -1, leavingStateOfCharge);
////                                stepsBetweenSteps.add(stepBetweenSteps);
////                                partIndex++;
////                            }
////                            leavingStateOfCharge -= GraphTools.computeNextPartConsumption(leavingStateOfCharge,
////                                    evEdge.routePartConsumptions.get(partIndex));
////                        }
//                    }
//                     /*
//                      simplified edges END
//                     */
//
//
//
//                } else {
//                    step = new Step(coord, 0, 0, leavingStateOfCharge);
//                    steps.add(step);
//                    stopovers.add(new Stopover(Stopover.DESTINATION_TYPE, coord, 0, 0, 0));
//                }
//
//                steps.addAll(stepsBetweenSteps);
//            }
//            String type;
//            if (planId == 0) {
//                type = Journey.CHEAPEST_TYPE;
//            } else {
//                type = Journey.FASTEST_TYPE;
//            }
//            plans.add(new Plan(optPathSol.travelTimeMillis, optPathSol.length, optPathSol.consumption, optPathSol.price,
//                    planId, type, steps, stopovers));
//            planId++;
//        }
//
//        if (moPlan != null) plans.add(moPlan);
//
//        Calendar calendar = Calendar.getInstance();
//        Timestamp currentTimestamp = new java.sql.Timestamp(calendar.getTime().getTime());
//        return new EvResponse(currentTimestamp.toString(), plans, EvResponse.STATUS_OK);
//    }
//
    private static int inspectStep(GPSLocation from, GPSLocation to, RoadType roadType, int leavingStateOfCharge, List<Step> stepsBetweenSteps) {

        double partLength = GPSLocationTools.computeDistanceAsDouble(from, to);
        double speedLimitMs = roadType.getSpeedLimitMs();
        double travelTimeMillis = GraphTools.convertToE3Format(partLength/speedLimitMs);
//        leavingStateOfCharge -= GraphTools.computeNextPartConsumption(leavingStateOfCharge,
//                partCons);
        Step stepBetweenSteps = new Step(new Coordinates(from), (int) partLength, (int) travelTimeMillis, leavingStateOfCharge);
        stepsBetweenSteps.add(stepBetweenSteps);
        return leavingStateOfCharge;
    }
//
//    public static Plan buildMOPlan(Graph<EvNode, PlanningEdge> graph, Journey moJourney, ChargingFunction chFunction) {
//
//        int numOfSegments = moJourney.journeySegments.size();
//        List<Step> steps = new ArrayList<>();
//        List<Stopover> stopovers = new ArrayList<>();
//
//
//        //ORIGIN
//        JourneyNode originJNode = moJourney.journeySegments.get(0).from;
//        JourneySegment firstSegment = moJourney.journeySegments.get(0);
//        stopovers.add(buildStopover(graph, Stopover.ORIGIN_TYPE, originJNode, firstSegment, chFunction));
//
//        //USED SUPERCHARGERS AND STEPS
//        int segmentIndex = 1;
//        int stepIndex = 1;
//        for (JourneyNode jNode: moJourney.journeyNodes) {
//            JourneyNode nextJNode;
//            if (stepIndex == moJourney.journeyNodes.size()) {
//                nextJNode = null;
//            } else {
//                nextJNode = moJourney.journeyNodes.get(stepIndex);
//            }
//
//            steps.addAll(buildStepsBetweenJNodes(graph, jNode, nextJNode, steps));
//            stepIndex++;
//
//            if (jNode.isUsedSupercharger) {
//                JourneySegment jSegment = moJourney.journeySegments.get(segmentIndex);
//                stopovers.add(buildStopover(graph, Stopover.SUPERCHARGER_TYPE, jNode, jSegment,chFunction));
//                segmentIndex++;
//            }
//        }
//
//        //DESTINATION
//        JourneyNode destinationJNode = moJourney.journeySegments.get(numOfSegments-1).to;
//        JourneySegment lastSegment = moJourney.journeySegments.get(numOfSegments-1);
//
//        stopovers.add(buildStopover(graph, Stopover.DESTINATION_TYPE, destinationJNode, lastSegment, chFunction));
//
//        return new Plan(moJourney.travelTimeMillis, moJourney.length, moJourney.consumption, moJourney.price, 3,
//                moJourney.journeyType, steps, stopovers);
//    }

    private static List<Step> buildStepsBetweenJNodes(ExtendedGraph<EvNode, EvEdge> graph, JourneyNode jNode, JourneyNode nextJNode) {

        List<Step> stepsBetweenSteps = new LinkedList<>();
        GPSLocation fromLoc = graph.getNode(jNode.nodeId);

        if (nextJNode == null) {
            stepsBetweenSteps.add(new Step(new Coordinates(fromLoc), 0, 0, jNode.stateAfterCharge));
            return stepsBetweenSteps;
        }

        EvEdge edge = graph.getEdge(jNode.nodeId, nextJNode.nodeId);
        GPSLocation toLoc;
        int leavingStateOfCharge = jNode.stateAfterCharge;

		if (edge == null) {
			System.out.println("here");
		}
		List<GPSLocation> viaNodes = edge.viaNodes;
		RoadType roadType = edge.roadType;

		if (viaNodes != null) {
            for (GPSLocation loc: viaNodes) {
				if (loc instanceof EvNode) {
					// TODO: 05-Dec-16 checked :) this is not neccessary because journey does not contain edges between chargers -> keep the code from "else"
					EvNode fromNode = (EvNode) fromLoc;
					EvNode toNode = (EvNode) loc;
					EvEdge miniEdge = graph.getEdge(fromNode.id, toNode.id);
					for (GPSLocation miniLoc: miniEdge.viaNodes) {
						toLoc = miniLoc;
						leavingStateOfCharge = inspectStep(fromLoc, toLoc, miniEdge.roadType, leavingStateOfCharge, stepsBetweenSteps);
						fromLoc = toLoc;
					}
				} else {
					toLoc = loc;
					leavingStateOfCharge = inspectStep(fromLoc, toLoc, roadType, leavingStateOfCharge, stepsBetweenSteps);
					fromLoc = toLoc;
				}
            }
        }

        toLoc = graph.getNode(nextJNode.nodeId);
        leavingStateOfCharge = inspectStep(fromLoc, toLoc, roadType, leavingStateOfCharge, stepsBetweenSteps);


        return stepsBetweenSteps;
    }

    private static Stopover buildStopover(ExtendedGraph<EvNode, EvEdge> graph, JourneyNode jNode,
            int travelTimeToNextStep, int distanceToNextStep) {

        GPSLocation node = graph.getNode(jNode.nodeId);
        int chargingTimeMillis = 0;
        String stopoverType = "";

        if (graph.getStartNode().id == jNode.nodeId) {
            stopoverType = Stopover.ORIGIN_TYPE;
        } else if (graph.getGoalNode().id == jNode.nodeId) {
            stopoverType = Stopover.DESTINATION_TYPE;
        } else if (graph.getChargerNodeIds().contains(jNode.nodeId)) {
            stopoverType = Stopover.SUPERCHARGER_TYPE;
        }

        if (stopoverType.equals(Stopover.SUPERCHARGER_TYPE)) {
            chargingTimeMillis = (int) new ChargingFunction().getChargingTimeMillis(jNode.stateBeforeCharge, jNode.stateAfterCharge);
        }

        return new Stopover(stopoverType, new Coordinates(node), convertFromE3(chargingTimeMillis),
                jNode.stateBeforeCharge, jNode.stateAfterCharge, jNode.pricePerKwh,
                travelTimeToNextStep, distanceToNextStep);
    }

    private static int convertFromE3(int travelTimeMillis) {
        return (int) (travelTimeMillis/1E3);
    }

    private static int convertToE3(int travelTime) {
        return (int) (travelTime*1E3);
    }

    public static EvResponse buildResponse(PlanningInstance planningInstance, List<Plan> plans) {

        String creationTimestamp = LocalDateTime.now().toString();
        if (plans.isEmpty()) {
            return new EvResponse(creationTimestamp, plans, EvResponse.STATUS_FAIL);
        } else {
            return new EvResponse(creationTimestamp, plans, EvResponse.STATUS_OK);
        }
    }

    public static Plan buildPlan(int planId, PlanningInstance planningInstance, Journey journey) {

        List<PlanSegment> planSegments = new ArrayList<>();
        ExtendedGraph<EvNode, EvEdge> graph = planningInstance.getExtendedEvGraph();

        for (JourneySegment journeySegment : journey.journeySegments) {
            planSegments.add(buildPlanSegment(graph, journeySegment));
        }

//        //ORIGIN
//        JourneyNode originJNode = journey.journeySegments.get(0).from;
//        JourneySegment firstSegment = journey.journeySegments.get(0);
//        from = buildStopover(graph, Stopover.ORIGIN_TYPE, originJNode, firstSegment, chFunction);
//
//        //USED SUPERCHARGERS AND STEPS
//        int segmentIndex = 1;
//        int stepIndex = 1;
//        JourneyNode nextJNode;
//
//        for (JourneyNode jNode: journey.journeyNodes) {
//            if (stepIndex == journey.journeyNodes.size()) {
//                nextJNode = null;
//            } else {
//                nextJNode = journey.journeyNodes.get(stepIndex);
//            }
//
//            viaSteps.addAll(buildStepsBetweenJNodes(graph, jNode, nextJNode));
//            stepIndex++;
//
//            if (jNode.isUsedSupercharger) {
//                JourneySegment jSegment = journey.journeySegments.get(segmentIndex);
//                to = buildStopover(graph, Stopover.SUPERCHARGER_TYPE, jNode, jSegment,chFunction);
//                planSegments.add(new PlanSegment(from, to, viaSteps));
//
//                segmentIndex++;
//                viaSteps = new ArrayList<>();
//            }
//        }

//        //DESTINATION
//        JourneyNode destinationJNode = journey.journeySegments.get(numOfSegments-1).to;
//        JourneySegment lastSegment = journey.journeySegments.get(numOfSegments-1);
//
//        to = buildStopover(graph, Stopover.DESTINATION_TYPE, destinationJNode, lastSegment, chFunction);
//        planSegments.add(new PlanSegment(from, to, viaSteps));

        return new Plan(convertFromE3(journey.travelTimeMillis), journey.length, journey.consumption, journey.price,
                planId, journey.journeyType, planSegments);
    }

    private static PlanSegment buildPlanSegment(ExtendedGraph<EvNode, EvEdge> graph, JourneySegment journeySegment) {

        List<Step> viaSteps = new ArrayList<>();

        JourneyNode from = journeySegment.from;
        for (JourneyNode viaNode : journeySegment.viaNodes) {
            List<Step> steps = buildStepsBetweenJNodes(graph, from, viaNode);
            viaSteps.addAll(steps);
            from = viaNode;
        }

        JourneyNode to = journeySegment.to;
        List<Step> steps = buildStepsBetweenJNodes(graph, from, to);
        viaSteps.addAll(steps);

        //VIA STEPS CONTAINS "FROM" node
        Step fromStep = viaSteps.remove(0);

        Stopover fromStopover = buildStopover(graph, journeySegment.from, fromStep.travelTimeToNextStep, fromStep.distanceToNextStep);
        Stopover toStopover = buildStopover(graph, journeySegment.to, 0, 0);
        return new PlanSegment(fromStopover, toStopover, viaSteps);
    }
}
