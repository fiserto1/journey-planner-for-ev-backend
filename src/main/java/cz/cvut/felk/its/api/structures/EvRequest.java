/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.api.structures;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Ev request with origin and destination locations.")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EvRequest implements Serializable {

	private static final long serialVersionUID = -6687101036078331049L;

	/**
	 * Client
	 */
	@ApiModelProperty(value = "Client")
	@JsonProperty(required = true)
	private String client;

	/**
	 * Origin simple location
	 */
	@ApiModelProperty(value = "Origin coordinates")
	@JsonProperty(required = true)
	private Coordinates origin;

	/**
	 * Destination simple location
	 */
	@ApiModelProperty(value = "Destination coordinates")
	@JsonProperty(required = true)
	private Coordinates destination;

	/**
	 * State of charge in percent
	 */
	@ApiModelProperty(value = "State of charge in percent")
	@JsonProperty(required = true)
	private int stateOfChargeInPerc;

	/**
	 * Cost for an hour in cents.
	 */
	@ApiModelProperty(value = "Cost for an hour in cents.")
	@JsonProperty(required = true)
	private int centsPerHour;

	public EvRequest() {
		this("UNKNOWN", null, null, Integer.MAX_VALUE, Integer.MAX_VALUE);
	}

	public EvRequest(String client, Coordinates origin, Coordinates destination, int stateOfChargeInPerc,
			int centsPerHour) {
		this.client = client;
		this.origin = origin;
		this.destination = destination;
		this.stateOfChargeInPerc = stateOfChargeInPerc;
		this.centsPerHour = centsPerHour;
	}

	public String getClient() {
		return client;
	}

	public Coordinates getOrigin() {
		return origin;
	}

	public Coordinates getDestination() {
		return destination;
	}

	public int getStateOfChargeInPerc() {
		return stateOfChargeInPerc;
	}

	@Override
	public String toString() {
		return "EvRequest [" +
				"client='" + client + '\'' +
				", origin=" + origin +
				", destination=" + destination +
				", stateOfChargeInPerc=" + stateOfChargeInPerc +
				", centsPerHour=" + centsPerHour +
				']';
	}

	public int getCentsPerHour() {
		return centsPerHour;
	}
}
