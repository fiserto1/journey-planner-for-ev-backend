package cz.cvut.felk.its.api.structures;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import cz.cvut.felk.its.routing.structures.routers.RouterType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Tomas on 08.12.2015.
 */
@ApiModel(description = "Plan.")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Plan implements Serializable{
    private static final long serialVersionUID = 6104622513671741056L;

    @ApiModelProperty(value = "Total travel time of the plan. (with charging times)")
    @JsonProperty(required = true)
    public final int travelTime;

    @ApiModelProperty(value = "Total length of the plan.")
    @JsonProperty(required = true)
    public final int length;

    @ApiModelProperty(value = "Total consumption of the plan.")
    @JsonProperty(required = true)
    public final int consumption;

    @ApiModelProperty(value = "Total price (in cents) of the plan. (Money spent for charging up the battery.)")
    @JsonProperty(required = true)
    public final int price;

    @ApiModelProperty(value = "Id of the plan.")
    @JsonProperty(required = true)
    public final int planId;

    @ApiModelProperty(value = "Type of the plan. (Describing which criteria was optimized.)")
    @JsonProperty(required = true)
    public final RouterType type;

    @ApiModelProperty(value = "List of plan segments including steps.")
    @JsonProperty(required = true)
    public final List<PlanSegment> segments;

    public Plan(int travelTime, int length, int consumption, int price, int planId, RouterType type, List<PlanSegment> segments) {
        this.travelTime = travelTime;
        this.length = length;
        this.consumption = consumption;
        this.price = price;
        this.planId = planId;
        this.type = type;
        this.segments = segments;
    }

    @Override
    public String toString() {
        return "Plan [" +
                "travelTime=" + travelTime +
                ", length=" + length +
                ", consumption=" + consumption +
                ", price=" + price +
                ", planId=" + planId +
                ", type=" + type +
                ", segments=" + segments +
                ']';
    }
}
