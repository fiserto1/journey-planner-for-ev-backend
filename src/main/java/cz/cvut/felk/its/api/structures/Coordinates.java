/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.api.structures;

/**
 * Simple Location containing only GPS location
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import cz.agents.basestructures.GPSLocation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Simple Location containing GPS location E6 and elevation in meters")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Coordinates {

	/**
	 * Latitude
	 */
	@ApiModelProperty(value = "Latitude E6")
	@JsonProperty(required = true)
	protected final int latE6;

	/**
	 * Longitude
	 */
	@ApiModelProperty(value = "Longitude E6")
	@JsonProperty(required = true)
	protected final int lonE6;

	@ApiModelProperty(value = "Elevation in meters of the coordinates. Height above the sea level.")
	@JsonProperty(required = false)
	public final Integer elevation;

	protected Coordinates() {
		this(Integer.MAX_VALUE, Integer.MAX_VALUE, null);
	}

	public Coordinates(int latE6, int lonE6) {
		this(latE6, lonE6, null);
	}

	public Coordinates(GPSLocation location) {
		this(location.latE6, location.lonE6, location.elevation);
	}

	public Coordinates(int latE6, int lonE6, Integer elevation) {
		this.latE6 = latE6;
		this.lonE6 = lonE6;
		this.elevation = elevation;
	}

	public int getLatE6() {
		return latE6;
	}


	public int getLonE6() {
		return lonE6;
	}

	@JsonIgnore
	public double getLat() {
		return (double) latE6/1E6;
	}

	@JsonIgnore
	public double getLon() {
		return (double) lonE6/1E6;
	}

	public Integer getElevation() {
		return elevation;
	}

	@Override
	public String toString() {
		return "Coordinates [" +
				"latE6=" + latE6 +
				", lonE6=" + lonE6 +
				", elevation=" + elevation +
				']';
	}
}
