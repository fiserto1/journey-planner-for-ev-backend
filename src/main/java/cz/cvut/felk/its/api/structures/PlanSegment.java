/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.api.structures;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(description = "Plan segment.")
public class PlanSegment {

	@ApiModelProperty(value = "From stopover - Origin,Destination or charger")
	@JsonProperty(required = true)
	public final Stopover from;

	@ApiModelProperty(value = "To stopover - Origin,Destination or charger")
	@JsonProperty(required = true)
	public final Stopover to;

	@ApiModelProperty(value = "Steps between from and to stopovers.")
	@JsonProperty(required = true)
	public final List<Step> viaSteps;

	public PlanSegment(Stopover from, Stopover to, List<Step> viaSteps) {
		this.from = from;
		this.to = to;
		this.viaSteps = viaSteps;
	}

	@Override
	public String toString() {
		return "PlanSegment [" +
				"from=" + from +
				", to=" + to +
				", viaSteps=" + viaSteps +
				']';
	}
}
