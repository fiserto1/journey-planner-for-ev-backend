/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.api;

import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.evstructures.EVZone;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.utils.EVZoneProvider;
import cz.cvut.felk.its.utils.KDTreeProvider;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class EvRouterServletContextListener implements ServletContextListener {

	private final Logger log = Logger.getLogger(EvRouterServletContextListener.class);
	private EVZoneProvider evZoneProvider;

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		log.info("Call contextInitialized(...) method.");
		evZoneProvider = EVZoneProvider.INSTANCE;
		EVZone<EvNode, EvEdge> zone = evZoneProvider.getZone();

		// creating location for first time last longer ->
		// prevent loading instance with first request
		GPSLocationTools.createGPSLocation(52.019121, 13.375854, 100, zone.srid);


		KDTreeProvider kdTreeProvider = KDTreeProvider.INSTANCE;
	// 	TODO: 14-Apr-16 create thread which will load prices every 5-10 minutes
//		ChargingPriceRefresher chargingPriceRefresher = ChargingPriceRefresher.INSTANCE;
//		chargingPriceRefresher.refreshPrices(evZoneProvider.getZone().getGraph());
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		log.info("Call contextDestroyed(...) method.");

		evZoneProvider.destroyZone();
		org.geotools.referencing.factory.DeferredAuthorityFactory.exit();
		org.geotools.util.WeakCollectionCleaner.DEFAULT.exit();
	}
}
