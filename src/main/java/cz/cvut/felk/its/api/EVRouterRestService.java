package cz.cvut.felk.its.api;

import cz.agents.basestructures.GPSLocation;
import cz.cvut.felk.its.api.structures.*;
import cz.cvut.felk.its.evstructures.chargers.ChargerType;
import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.exceptions.BatteryLowException;
import cz.cvut.felk.its.exceptions.OutOfZoneBounds;
import cz.cvut.felk.its.routing.structures.routers.EvRouter;
import cz.cvut.felk.its.routing.structures.routers.RouterType;
import cz.cvut.felk.its.routing.structures.PlanningInstance;
import cz.cvut.felk.its.routing.structures.graph.FMSearchGraph;
import cz.cvut.felk.its.routing.structures.graph.LMSearchGraph;
import cz.cvut.felk.its.utils.EdgePrecomputer;
import cz.cvut.felk.its.utils.GraphExtender;
import cz.cvut.felk.its.evstructures.*;
import cz.cvut.felk.its.evstructures.graphs.ExtendedGraph;
import cz.cvut.felk.its.evstructures.journey.Journey;
import cz.cvut.felk.its.utils.EVZoneProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by Tomas on 08.12.2015.
 */
@Path("/v1")
@Api(value = "/v1/journeys", description = "API for EV routing.")
public class EvRouterRestService extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private final Logger log = Logger.getLogger(EvRouterRestService.class);

    private final double dominanceRelaxConst = 0.9;


    @POST
    @Path("/journeys")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Request EV journeys.")
    public Response getRoutes(
            @ApiParam(value = "Search request for journeys based on given parameters") EvRequest evRequest,
            @QueryParam("routerTypes") //value = "Array of router types to use for routing")
                @DefaultValue("MO_FAST") List<RouterType> routerTypes) {

        long startTime = System.currentTimeMillis();

        log.info(String.format("[POST] /journeys?routerTypes=%s", routerTypes));
//        log.debug(String.format("Request: %s", evRequest));

        try {
            EvResponse evResponse = retrieveResponse(evRequest, routerTypes);

            logRuntime(startTime);
//			log.debug(String.format("EvResponse: %s", evResponse));
            return Response.ok(evResponse, MediaType.APPLICATION_JSON).build();

        } catch (OutOfZoneBounds e) {
            log.debug(String.format("Request: %s", evRequest));
            log.error(e.getMessage());
            logRuntime(startTime);

            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        } catch (BatteryLowException e) {
            log.debug(String.format("Request: %s", evRequest));
            log.error(e.getMessage());
            logRuntime(startTime);

            return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/chargers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Request for chargers with actual prices")
    public Response getChargersInfo() {

        long startTime = System.currentTimeMillis();

        log.info(String.format("[GET] /chargers?"));

        return Response.status(Response.Status.NOT_IMPLEMENTED).build();
    }

    private EvResponse retrieveResponse(EvRequest evRequest, List<RouterType> routerTypes)
            throws BatteryLowException, OutOfZoneBounds {

        PlanningInstance planningInstance = buildPlanningInstance(evRequest);

        List<Plan> plans = new ArrayList<>();
        long start, elapsedTime;

        int planId = 0;
        for (RouterType routerType : routerTypes) {
            start = System.currentTimeMillis();
            EvRouter router = routerType.getEvRouter();
            List<Journey> optimalJourneys = router.findOptimalJourney(planningInstance);

            elapsedTime = System.currentTimeMillis() - start;
            log.info(String.format("Compute %s takes %d ms, %d journeys found.", routerType, elapsedTime, optimalJourneys.size()));

            for (Journey optimalJourney : optimalJourneys) {
                //				log.debug("Journey: " + optimalJourney);
//                log.debug("Building plan started.");
                Plan plan = EvResponseBuilder.buildPlan(planId, planningInstance, optimalJourney);
                plans.add(plan);
                planId++;
            }

        }

        EvResponse evResponse = EvResponseBuilder.buildResponse(planningInstance, plans);

        // TODO: 27-Oct-16 make this never happen!
        if (evResponse == null) {
            throw new BatteryLowException(planningInstance.getInitSOC());
        }

        return evResponse;
    }

    private PlanningInstance buildPlanningInstance(EvRequest evRequest) throws BatteryLowException, OutOfZoneBounds {
        BatteryType batteryType = BatteryType.TESLA;
        ChargerType chargerType = ChargerType.SUPERCHARGER;

        ChargingFunction chargingFunction = new ChargingFunction(
                batteryType, chargerType);

        LocalDateTime startDateTime = LocalDateTime.now();
        int initSOC = (int) (((double) evRequest.getStateOfChargeInPerc()/100) * batteryType.getMaxSoC());

        if (!batteryType.isInBatteryRange(initSOC)) {
            log.error(String.format("Initial state of charge '%s' is too low.", initSOC));
            throw new BatteryLowException(initSOC);
        }

        EVZone<EvNode, EvEdge> zone = EVZoneProvider.INSTANCE.getZone();

        if (!isODInsideZone(zone, evRequest.getOrigin(), evRequest.getDestination())) {
            throw new OutOfZoneBounds(zone.getName(), zone.getBoundingBox());
        }

        GPSLocation originLoc = zone.createGPSLocationInZone(evRequest.getOrigin());
        GPSLocation destinationLoc = zone.createGPSLocationInZone(evRequest.getDestination());

        ExtendedGraph<EvNode, EvEdge> extendedGraph = GraphExtender.buildExtendedGraph(
                zone, originLoc, destinationLoc);

        ExtendedGraph<EvNode, EvEdge> graphFMLM = buildGraphFMLM(extendedGraph, initSOC,startDateTime, zone.getNumOfChargers());
//        ExtendedGraph<EvNode, EvEdge> graphFMLM = null;

        double centsPerSecond = ((double)evRequest.getCentsPerHour())/3600;

        return new PlanningInstance(Collections.singletonList(originLoc), Collections.singletonList(destinationLoc),
                zone, extendedGraph, graphFMLM,
                initSOC, batteryType, chargerType, chargingFunction, startDateTime, centsPerSecond);
    }

    private ExtendedGraph<EvNode, EvEdge> buildGraphFMLM(ExtendedGraph<EvNode, EvEdge> extendedGraph, int initSOC,
            LocalDateTime startDateTime, int numOfChargers) throws BatteryLowException {

        FMSearchGraph<EvNode, EvEdge> fmSearchGraph = new FMSearchGraph<>(extendedGraph);
        LMSearchGraph<EvNode, EvEdge> lmSearchGraph = new LMSearchGraph<>(extendedGraph);

        Set<Integer> chargerNodeIds = extendedGraph.getChargerNodeIds();

        //FIRST MILE/////////////////////////////
        long start = System.currentTimeMillis();
        Map<Integer, EvEdge> fmMultiEdges = EdgePrecomputer.computeFMEdges(numOfChargers,
                fmSearchGraph, extendedGraph, initSOC, startDateTime, chargerNodeIds);
        long fmElapsedTime = System.currentTimeMillis() - start;
        log.info(String.format("Compute First mile takes %d ms num of fmEdges is %d", fmElapsedTime, fmMultiEdges.size()));
        if (fmMultiEdges.isEmpty()) return null;

        //LAST MILE/////////////////////////////
        start = System.currentTimeMillis();
        Map<Integer, EvEdge> lmMultiEdges = EdgePrecomputer
                .computeLMByBckwEdges(lmSearchGraph, extendedGraph, extendedGraph.getNumOfNodes(), chargerNodeIds);
        long lmElapsedTime = System.currentTimeMillis() - start;
        log.info(String.format("Compute Last mile takes %d ms; num of lmEdges is %d", lmElapsedTime, lmMultiEdges.size()));
        if (lmMultiEdges.isEmpty() && !fmMultiEdges.containsKey(extendedGraph.getGoalNode().getId())) return null;

        return new ExtendedGraph<>(extendedGraph, extendedGraph.getStartNode(), extendedGraph.getGoalNode(),
                fmMultiEdges, lmMultiEdges);
    }

    private boolean isODInsideZone(EVZone<EvNode, EvEdge> zone, Coordinates origin, Coordinates destination) {
        return zone.getBoundingBox().inside(origin.getLonE6(), origin.getLatE6())
                && zone.getBoundingBox().inside(destination.getLonE6(), destination.getLatE6());
    }

    private void logRuntime(long start) {
        long elapsedTime = System.currentTimeMillis() - start;
        log.info(String.format("Sending response, total time: %d ms", elapsedTime));
//        log.info("--------------------------------------------------------");
    }
}
