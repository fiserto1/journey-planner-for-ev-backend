package cz.cvut.felk.its.api.structures;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Tomas on 08.12.2015.
 */
@ApiModel(description = "Step of the plan.")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Step implements Serializable{
    private static final long serialVersionUID = 478439046305060058L;

    @ApiModelProperty(value = "Coordinates of the step.")
    @JsonProperty(required = true)
    public final Coordinates coordinates;

    @ApiModelProperty(value = "Distance to next step.")
    @JsonProperty(required = true)
    public final int distanceToNextStep;

    @ApiModelProperty(value = "Travel time to the next step. (without charging times (from leavingSoC to incomingSoC of the next step))")
    @JsonProperty(required = true)
    public final int travelTimeToNextStep;

    @ApiModelProperty(value = "State of charge while leaving the step.")
    @JsonProperty(required = true)
    public final int leavingStateOfCharge;

    public Step(Coordinates coordinates, int distanceToNextStep, int travelTimeToNextStep, int leavingStateOfCharge) {
        this.coordinates = coordinates;
        this.distanceToNextStep = distanceToNextStep;
        this.travelTimeToNextStep = travelTimeToNextStep;
        this.leavingStateOfCharge = leavingStateOfCharge;
    }

    @Override
    public String toString() {
        return "Step{" +
                "coordinates=" + coordinates +
                ", distanceToNextStep=" + distanceToNextStep +
                ", travelTimeToNextStep=" + travelTimeToNextStep +
                ", leavingStateOfCharge=" + leavingStateOfCharge +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Step step = (Step) o;
        return distanceToNextStep == step.distanceToNextStep &&
                travelTimeToNextStep == step.travelTimeToNextStep &&
                leavingStateOfCharge == step.leavingStateOfCharge &&
                Objects.equals(coordinates, step.coordinates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinates, distanceToNextStep, travelTimeToNextStep, leavingStateOfCharge);
    }
}
