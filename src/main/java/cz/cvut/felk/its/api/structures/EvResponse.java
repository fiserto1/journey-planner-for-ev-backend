package cz.cvut.felk.its.api.structures;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Created by Tomas on 08.12.2015.
 */
@ApiModel(description = "Ev response with computed plans.")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EvResponse implements Serializable{

    private static final long serialVersionUID = -7304928682633419704L;
    public static final int STATUS_OK = 0;
    public static final int STATUS_FAIL = -1;

    @ApiModelProperty(value = "Creation timestamp.")
    @JsonProperty(required = true)
    public final String creationTimestamp;

    @ApiModelProperty(value = "Plans.")
    @JsonProperty(required = true)
    public final List<Plan> plans;

    @ApiModelProperty(value = "Status.")
    @JsonProperty(required = true)
    public final int status;

    public EvResponse(String creationTimestamp, List<Plan> plans, int status) {
        this.creationTimestamp = creationTimestamp;
        this.plans = plans;
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EvResponse that = (EvResponse) o;
        return status == that.status &&
                Objects.equals(creationTimestamp, that.creationTimestamp) &&
                Objects.equals(plans, that.plans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creationTimestamp, plans, status);
    }

    @Override
    public String toString() {
        return "EvResponse{" +
                "creationTimestamp='" + creationTimestamp + '\'' +
                ", plans=" + plans +
                ", status=" + status +
                '}';
    }
}
