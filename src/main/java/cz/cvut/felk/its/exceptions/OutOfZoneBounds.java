/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.exceptions;

import cz.agents.basestructures.BoundingBox;

public class OutOfZoneBounds extends Exception {
	private static final long serialVersionUID = 3980208537406716693L;

	public OutOfZoneBounds(String name, BoundingBox bbox) {
		super("Origin or Destination is out of " + name + " zone bounds: " +
				String.format("bbox=[min(%d, %d), max(%d, %d)]",
						bbox.minLatE6, bbox.minLonE6, bbox.maxLatE6, bbox.maxLonE6));
	}
}
