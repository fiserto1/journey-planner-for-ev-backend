/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.exceptions;

public class UnreachableDestinationException extends Exception {

	private static final long serialVersionUID = 7443194215614180641L;

	public UnreachableDestinationException() {
		super("Destination is not reachable from any charger.");
	}
}
