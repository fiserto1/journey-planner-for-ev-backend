/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.exceptions;

import java.io.FileNotFoundException;

public class GraphNotFoundException extends FileNotFoundException {
	private static final long serialVersionUID = -1284143255790857837L;

	public GraphNotFoundException(String graphFilename) {
		super("File with deserialized graph not found: " + graphFilename);
	}
}
