/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.exceptions;

import java.io.FileNotFoundException;

public class ZoneNotFoundException extends FileNotFoundException {

	private static final long serialVersionUID = -6403891473978359474L;

	public ZoneNotFoundException(String zoneFilename) {
		super("File with properties not found: " + zoneFilename);
	}

}
