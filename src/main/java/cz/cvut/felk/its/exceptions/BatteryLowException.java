/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.exceptions;

public class BatteryLowException extends Exception{
	private static final long serialVersionUID = 6978917122652375585L;

	public BatteryLowException(int initSoC) {
		super(String.format("Initial Battery state of charge (%s) is too low. ", initSoC));
	}
}
