package cz.cvut.felk.its;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.agents.basestructures.BoundingBox;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by Tomas on 08-Feb-16.
 */
@Deprecated
public class ServletTest {

    //53684
    private static final Random rand = new Random(53684);
    private static final int numOfRequests = 1000;
    private static final BoundingBox boundingBox = new BoundingBox(6866241, 47270211, 14542051, 53058141);

    private static int[] initSoCsInPerc = new int[numOfRequests];
    private static int[][] originLatLons = new int[2][numOfRequests];
    private static int[][] destLatLons = new int[2][numOfRequests];
//    private static long[] executionTimes = new long[numOfRequests];
//    private static long[] travelTimes = new long[numOfRequests];
//    private static double avgExecutionTime = 0;

    private static double[][] avgResults = new double[7][4];
    private static int[] nullCounters = new int[7];

    private static ServletSimulation servletSimulation;

    private static HttpClient client;
    private static ResponseHandler<String> responseHandler;
    private static HttpPost request;


    private static double optTravelTime=0;
    private static double optimalPrice=0;
    private static double optimalPriceWithTime=0;
    private static double fm_comp_time=0;
    private static double lm_comp_time=0;
    private static double timeSearch_comp_time=0;
    private static double priceSearch_comp_time=0;
    private static double rTr_comp_time=0;
    private static int comp_time_counter=0;
    private static int rtr_counter=0;

    private static StringBuilder totalOutput = new StringBuilder();

    public static void addResults(double fm, double lm, double time_fast, double time_price, double travelTime, double price, double priceWithTime){
        fm_comp_time += fm;
        lm_comp_time += lm;
        timeSearch_comp_time += time_fast;
        priceSearch_comp_time += time_price;
        optTravelTime += travelTime/1000;
        optimalPrice += price;
        optimalPriceWithTime += priceWithTime;
        comp_time_counter++;
    }

    public static void addRtR(double rTr) {
        rTr_comp_time += rTr;
        rtr_counter++;
    }

    public static void main(String[] args) {
        initSoCsInPerc = new int[numOfRequests];
//        originLatLons = new int[2][numOfRequests];
//        destLatLons = new int[2][numOfRequests];
//        executionTimes = new long[numOfRequests];
//        travelTimes = new long[numOfRequests];
//        avgExecutionTime = 0;
//        avgResults = new double[7][4];
//        nullCounters = new int[7];
        servletSimulation = new ServletSimulation();
//        servletSimulation.init("serialized/GE-primary-bytns-prec-graph-ultra.ser");
//        servletSimulation.init("GE-tertiary-bytns-prec-graph-ultra.ser");
        servletSimulation.init("serialized/GE-bytns-to-trunk-ultra.ser");

        generateRequests();

//        connectToOnlineServlet();

        //100 Tests to warm up
        int oLat = originLatLons[0][0];
        int oLon = originLatLons[1][0];
        int dLat = destLatLons[0][0];
        int dLon = destLatLons[1][0];
        testOfflineServlet(oLat, oLon, dLat, dLon, 100, 1);
//        for (int i = 0; i < 100; i++) {
//            testOfflineServlet(oLat, oLon, dLat, dLon, 100, 1);
//        }
//////
//        saveResultToString2(-1);
//
//        testBut();
//        testSOC();


//        System.out.println(totalOutput.toString());



    }
    private static void testBut() {
        int oLat, oLon, dLat,dLon;
        totalOutput.append("Testing button primary: 100perc init SoC, 0.6-1.0 po 0.02\n");
//        step = 0.02;
//        fmLMReduction = 0.6;
        int initSocPerc = 100;
//        double[] reductions = {0, 0.2,0.5, 0.7, 0.9, 1};
        double[] reductions = {0.6, 0.62,0.64, 0.66, 0.68, 0.70, 0.72, 0.74, 0.76, 0.78, 0.80, 0.82, 0.84, 0.86, 0.88,
                0.90, 0.92, 0.94, 0.96, 0.98, 1};

        for (int i = 0; i < 21; i++) {
            System.out.println(reductions[i]);
            for (int j = 0; j < numOfRequests; j++) {
                oLat = originLatLons[0][j];
                oLon = originLatLons[1][j];
                dLat = destLatLons[0][j];
                dLon = destLatLons[1][j];
                testOfflineServlet(oLat, oLon, dLat, dLon, initSocPerc, reductions[i]);
            }
            saveResultToString2(reductions[i]);
        }
    }

    private static void testSOC() {
        int oLat, oLon, dLat,dLon;
        totalOutput.append("Testing primary initSoC: 10-100perc step 10; constant 1.0\n");
        double step = 5;
        int initSocPerc = 10;
        double fmLMReduction = 1;
        for (int i = 0; i < 19; i++) {
            System.out.println(initSocPerc);
            for (int j = 0; j < numOfRequests; j++) {
                oLat = originLatLons[0][j];
                oLon = originLatLons[1][j];
                dLat = destLatLons[0][j];
                dLon = destLatLons[1][j];
                testOfflineServlet(oLat, oLon, dLat, dLon, initSocPerc, fmLMReduction);
            }
            saveResultToString(initSocPerc);
            initSocPerc += step;
        }
    }

    private static void saveResultToString(int initSocInPerc) {
        int succResp = comp_time_counter;
        totalOutput.append(String.format("%d & %d & %.2f & %.2f & %.2f & %.2f & %.2f\\\\\n", initSocInPerc, succResp,
                fm_comp_time/succResp, lm_comp_time/succResp,
                timeSearch_comp_time/succResp, priceSearch_comp_time/succResp, rTr_comp_time/succResp));
        totalOutput.append("\\hline\n");
        if (rtr_counter != comp_time_counter) {
            System.out.println("Failed.");
        }
        fm_comp_time = 0;
        lm_comp_time = 0;
        timeSearch_comp_time = 0;
        priceSearch_comp_time = 0;
        rTr_comp_time = 0;
        comp_time_counter = 0;
        rtr_counter = 0;
        optTravelTime = 0;
        optimalPrice = 0;
        optimalPriceWithTime = 0;
    }

    private static void saveResultToString2(double eps) {
        int succResp = comp_time_counter;
        totalOutput.append(String.format("%.2f & %d & %.2f & %.2f & %.2f & %.2f & %.2f & %.2f & %.2f & %.2f\\\\\n", eps, succResp,
                fm_comp_time/succResp, lm_comp_time/succResp,
                timeSearch_comp_time/succResp, priceSearch_comp_time/succResp, rTr_comp_time/succResp,
                (optTravelTime)/succResp, optimalPrice/succResp, optimalPriceWithTime/succResp));
        totalOutput.append("\\hline\n");

        fm_comp_time = 0;
        lm_comp_time = 0;
        timeSearch_comp_time = 0;
        priceSearch_comp_time = 0;
        rTr_comp_time = 0;
        comp_time_counter = 0;
        rtr_counter = 0;
        optTravelTime = 0;
        optimalPrice = 0;
        optimalPriceWithTime = 0;
    }

    private static void generateRequests() {

        /*BERLIN -> MUNCHEN*/
        originLatLons[0][0] = 52506191;
        originLatLons[1][0] = 13392334;
        destLatLons[0][0] = 48125768;
        destLatLons[1][0] = 11557617;
//        initSoCsInPerc[0] = 386; //Tesla Model S 85D

        /*FRANKFURT -> MUNCHEN*/
        originLatLons[0][1] = 50095917;
        originLatLons[1][1] = 8640747;
        destLatLons[0][1] = 48125768;
        destLatLons[1][1] = 11557617;
//        initSoCsInPerc[1] = 140; // FIAT 500e

        /*KOLN -> BERLIN*/
        originLatLons[0][2] = 50986099;
        originLatLons[1][2] = 6987305;
        destLatLons[0][2] = 52506191;
        destLatLons[1][2] = 13392334;
//        initSoCsInPerc[2] = 150; //KIA Soul EV

        /*KOLN -> HAMBURG*/
        originLatLons[0][3] = 50986099;
        originLatLons[1][3] = 6987305;
        destLatLons[0][3] = 53507651;
        destLatLons[1][3] = 9975586;
//        initSoCsInPerc[3] = 140; // FIAT 500e


        /*FRANKFURT -> Stuttgart*/
        originLatLons[0][4] = 50162824;
        originLatLons[1][4] = 8679199;
        destLatLons[0][4] = 48835797;
        destLatLons[1][4] = 9173584;
//        initSoCsInPerc[4] = 135; //NISSAN LEAF

        //initSoCsInPerc[4] = 130; //BMW i3 EC
        /*Munchen -> Hamburg*/
        originLatLons[0][5] = 48122101;
        originLatLons[1][5] = 11590576;
        destLatLons[0][5] = 53507651;
        destLatLons[1][5] = 9975586;

//        initSoCsInPerc[0] = 10;
        initSoCsInPerc[0] = 10;
        initSoCsInPerc[1] = 20;
        initSoCsInPerc[2] = 30;
        initSoCsInPerc[3] = 40;
        initSoCsInPerc[4] = 50;
        initSoCsInPerc[5] = 60;
        //            initSoCsInPerc[7] = 500;
//            initSoCsInPerc[8] = 100000000;

        for (int i = 6; i < numOfRequests; i++) {
            // TODO: 11-Feb-16 different range intevals
//            initSoCsInPerc[i] = rand.nextInt(100-1) + 1;

            originLatLons[0][i] = rand.nextInt(boundingBox.maxLatE6 - boundingBox.minLatE6) + boundingBox.minLatE6;
            originLatLons[1][i] = rand.nextInt(boundingBox.maxLonE6 - boundingBox.minLonE6) + boundingBox.minLonE6;

            destLatLons[0][i] = rand.nextInt(boundingBox.maxLatE6 - boundingBox.minLatE6) + boundingBox.minLatE6;
            destLatLons[1][i] = rand.nextInt(boundingBox.maxLonE6 - boundingBox.minLonE6) + boundingBox.minLonE6;
        }
    }

    private static void testOfflineServlet(int oLat, int oLon, int dLat, int dLon, int initSoCInPerc, double reduceConst) {
//        long start = System.currentTimeMillis();
//        System.out.println("Request: " + oLat + ", " + oLon + "; "+ dLat + ", " + dLon + "; "+ initSoCInPerc + "; " + reduceConst);
//        EvResponse response = servletSimulation.doPost(oLat, oLon, dLat, dLon, initSoCInPerc, reduceConst);
//        System.out.println(response);
//        long rTr =  System.currentTimeMillis() - start;
//        if (response.status == EvResponse.STATUS_FAIL) return;

//        System.out.println("Price TT: " + response.plans.get(1).travelTimeMillis);
//        System.out.println("Fast TT : " + response.plans.get(0).travelTimeMillis);

    }

    private static void printResults() {
        // TODO: 11-Feb-16 round numbers and convert units
        System.out.println(Arrays.toString(nullCounters));
        System.out.println(Arrays.deepToString(avgResults));
        for (int i = 0; i < 7; i++) {
            int numOfCorrectResponses = numOfRequests - nullCounters[i];
//                System.out.println(initSoCsInPerc[i] + " & " + avgResults[i][0]/numOfCorrectResponses + " & " +
//                        avgResults[i][1]/numOfCorrectResponses + " & " + avgResults[i][2]/numOfCorrectResponses +
//                        " & " + avgResults[i][3]/numOfCorrectResponses + " \\\\");

            System.out.printf("%d & %.2f & %.2f & %.2f & %.2f \\\\", initSoCsInPerc[i],
                    avgResults[i][0]/numOfCorrectResponses, (avgResults[i][1]/1000)/numOfCorrectResponses,
                    avgResults[i][2]/numOfCorrectResponses, (avgResults[i][3]/60)/numOfCorrectResponses);
            System.out.println("\\hline");
        }
    }

    private static boolean connectToOnlineServlet() {
        client = HttpClientBuilder.create().build();

        try {
            responseHandler = new BasicResponseHandler();
            request = new HttpPost("http://localhost:8080/jsonservlet");
            request.addHeader("content-type", "application/json");
            return true;
        } catch (Exception ex) {
            // handle exception here
            System.out.println(Arrays.toString(ex.getStackTrace()));
            return false;
        }

    }



    private static void testOnlineServlet(int oLat, int oLon, int dLat, int dLon, int i, int j) throws IOException {


        StringEntity params =new StringEntity("{\"client\":\"journey-planner-for-EV\"," +
                "\"origin\":{\"type\":\"ORIGIN\"," +
                "\"latE6\":" + oLat  + ",\"lonE6\":" + oLon + "}," +
                "\"destination\":{\"type\":\"DESTINATION\"," +
                "\"latE6\":" + dLat + ",\"lonE6\":" + dLon + "}," +
                "\"rangeInKm\":" + initSoCsInPerc[j] + "} ");

        request.setEntity(params);
        long start = System.currentTimeMillis();
        String response = client.execute(request, responseHandler);
        long rTr =  System.currentTimeMillis() - start;

        ObjectMapper mapper = new ObjectMapper();
        if (response.equals("null")) {
            nullCounters[j]++;
            return;
        }
        JsonNode requestJNode = mapper.readValue(response, JsonNode.class);
        JsonNode planJNode = requestJNode.get("plans").get(0);
//                String client = clientJNode.asText();

        int travelTime = planJNode.get("travelTimeMillis").asInt();
        int length = planJNode.get("length").asInt();
        int numOfSup = planJNode.get("stopovers").size() - 2;

//                    System.out.println(length + "," + travelTimeMillis);
        avgResults[j][0] += rTr;
        avgResults[j][1] += length;
        avgResults[j][2] += numOfSup;
        avgResults[j][3] += travelTime;
//                    System.out.println(initSoCsInPerc[j] + " & " + rTr + " & " + length + " & " + numOfSup + " & " + travelTimeMillis + " \\\\");



//            for (int i = 0; i < numOfRequests; i++) {
//
//                StringEntity params =new StringEntity("{\"client\":\"journey-planner-for-EV\"," +
//                        "\"origin\":{\"type\":\"ORIGIN\"," +
//                        "\"latE6\":" + originLatLons[0][i]  + ",\"lonE6\":" + originLatLons[1][i] + "}," +
//                        "\"destination\":{\"type\":\"DESTINATION\"," +
//                        "\"latE6\":" + destLatLons[0][i] + ",\"lonE6\":" + destLatLons[1][i] + "}," +
//                        "\"rangeInKm\":" + initSoCsInPerc[i] + "} ");
//
//                request.setEntity(params);
//                System.out.println(i);
//                long start = System.currentTimeMillis();
//                String response = client.execute(request, responseHandler);
//                executionTimes[i] =  System.currentTimeMillis() - start;
//                avgExecutionTime += ((double) executionTimes[i])/numOfRequests;
//
////                System.out.println("execution time: " + executionTimes[0] + "ms");
////
////                System.out.println(response);
//
//                ObjectMapper mapper = new ObjectMapper();
//                if (response.equals("null")) continue;
//                JsonNode requestJNode = mapper.readValue(response, JsonNode.class);
//                JsonNode planJNode = requestJNode.get("plans").get(0);
////                String client = clientJNode.asText();
//
//                int travelTimeMillis = planJNode.get("travelTimeMillis").asInt();
//                travelTimes[i] = travelTimeMillis;
//
//            }
//
//
//            System.out.println(Arrays.toString(travelTimes));
//            System.out.println(Arrays.toString(executionTimes));
//            System.out.println(avgExecutionTime);

    }
}
