package cz.cvut.felk.its.evstructures.chargers;

import cz.agents.basestructures.GPSLocation;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * Created by Tomas on 05.11.2015.
 */
public class Supercharger implements Charger, Serializable {

    public static final int NUM_OF_HOURS = 24;
    private static final long serialVersionUID = 6149056652146525318L;

    /*
    compulsory parameters
    */
    public final int sourceId;
    public final String name;
    public final GPSLocation location;
    public final String status;

    private Integer[] pricesDuringDay;// price per kWh in cents
    /*
    other parameters
     */
    public final String streetAddress;
    public final String city;
    public final String state;
    public final int zip;
    public final String country;
    public final int numberOfStalls;
    public final int elevationInMeters;
    public final String openDate;

    public Supercharger(int sourceId, String name, String streetAddress, String city, String state, int zip, String country,
                        int numberOfStalls, GPSLocation location, int elevationInMeters, String status,
                        String openDate) {
        this.sourceId = sourceId;
        this.name = name;
        this.streetAddress = streetAddress;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.country = country;
        this.numberOfStalls = numberOfStalls;
        this.location = location;
        this.elevationInMeters = elevationInMeters;
        this.status = status;
        this.openDate = openDate;
        this.pricesDuringDay = null;
    }

    public Supercharger(int sourceId, String name, GPSLocation location, String status) {
        this(sourceId, name, "", "", "", -1, "", -1, location, -1, status, "");
    }

    @Override
    public void setPricesDuringDay(Integer[] pricesDuringDay) {
        this.pricesDuringDay = pricesDuringDay;
    }

    @Override
    public int getPricePerkWh(LocalDateTime visitationDateTime) {
        if (visitationDateTime == null) return 0;

        int hour = visitationDateTime.getHour();
        return pricesDuringDay[hour];
    }

    @Override
    public String toString() {
        return "Supercharger [" +
                "sourceId=" + sourceId +
                ", name='" + name + '\'' +
                ", location=" + location +
                ", status='" + status + '\'' +
                ", pricesDuringDay=" + Arrays.toString(pricesDuringDay) +
                ", streetAddress='" + streetAddress + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zip=" + zip +
                ", country='" + country + '\'' +
                ", numberOfStalls=" + numberOfStalls +
                ", elevationInMeters=" + elevationInMeters +
                ", openDate='" + openDate + '\'' +
                ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Supercharger))
            return false;

        Supercharger that = (Supercharger) o;

        if (sourceId != that.sourceId)
            return false;
        if (zip != that.zip)
            return false;
        if (numberOfStalls != that.numberOfStalls)
            return false;
        if (elevationInMeters != that.elevationInMeters)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null)
            return false;
        if (location != null ? !location.equals(that.location) : that.location != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null)
            return false;
        if (streetAddress != null ? !streetAddress.equals(that.streetAddress) : that.streetAddress != null)
            return false;
        if (city != null ? !city.equals(that.city) : that.city != null)
            return false;
        if (state != null ? !state.equals(that.state) : that.state != null)
            return false;
        if (country != null ? !country.equals(that.country) : that.country != null)
            return false;
        return openDate != null ? openDate.equals(that.openDate) : that.openDate == null;

    }

    @Override
    public int hashCode() {
        int result = sourceId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (streetAddress != null ? streetAddress.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + zip;
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + numberOfStalls;
        result = 31 * result + elevationInMeters;
        result = 31 * result + (openDate != null ? openDate.hashCode() : 0);
        return result;
    }
}
