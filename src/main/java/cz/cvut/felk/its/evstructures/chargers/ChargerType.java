/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures.chargers;

import java.util.Arrays;
import java.util.List;

public enum ChargerType {
	//threshold[0]-charging time in seconds; threshold[1]-battery SoC in perc
	SUPERCHARGER(Arrays.asList(
			new int[]{2400, 80} ,
			new int[]{4500, 100}));

	private List<int[]> chargingThresholds;

	ChargerType(List<int[]> chargingThresholds) {
		this.chargingThresholds = chargingThresholds;
	}

	public List<int[]> getChargingThresholds() {
		return chargingThresholds;
	}
}
