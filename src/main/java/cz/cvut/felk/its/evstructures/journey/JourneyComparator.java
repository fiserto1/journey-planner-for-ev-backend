/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures.journey;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JourneyComparator {

	/**
	 * j1,j2 and j2,j1 will return different results
	 * @param j1
	 * @param j2
	 * @return
	 */
	public static boolean areSimilar(Journey j1, Journey j2, double similarityPerc) {
		List<JourneySegment> j1Segments = j1.journeySegments;
		List<JourneySegment> j2Segments = j2.journeySegments;

		//if segments have same from-to then equals
		if (!j1Segments.equals(j2Segments)) return false;

		int counterSameVias = 0;
		int counterAllVias = 0;

		for (int i = 0; i < j1Segments.size(); i++) {
			List<JourneyNode> j1SegVias = j1Segments.get(i).viaNodes;
			List<JourneyNode> j2SegVias = j2Segments.get(i).viaNodes;
			Set<JourneyNode> j2SegViasSet = new HashSet<>(j2SegVias);

			counterAllVias += j1SegVias.size();

			for (JourneyNode j1SegVia : j1SegVias) {
				if (j2SegViasSet.contains(j1SegVia)) {
					//if JNodes have same id then equals
					counterSameVias++;
				}
			}

		}
		double perc = ((double) counterSameVias/counterAllVias)*100;

		return perc >= similarityPerc;
	}

	public static boolean areSimilar(Journey j1, Journey j2) {
		return areSimilar(j1, j2, 80);
	}
}
