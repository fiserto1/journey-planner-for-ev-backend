package cz.cvut.felk.its.evstructures;

import cz.cvut.felk.its.evstructures.chargers.ChargingModel;

import java.io.Serializable;

/**
 * Created by Tomas on 21-Jul-16.
 */
public class ConsumptionProfile implements Serializable, Comparable<ConsumptionProfile> {

	private static final long serialVersionUID = 2329438440142148607L;

	public final int inSoc; //the minimum SoC required to traverse path P
    public final int outSoc; //the maximum possible SoC after traversing path P
    public final int cost; //energy consumption costs of the Path

    public ConsumptionProfile(int inSoc, int outSoc, int cost) {
        this.inSoc = inSoc;
        this.outSoc = outSoc;
        this.cost = cost;
    }

	public ConsumptionProfile() {

		this(ChargingModel.MIN_STATE_OF_CHARGE, ChargingModel.MAX_STATE_OF_CHARGE-ChargingModel.MIN_STATE_OF_CHARGE, 0);
	}

	public double getConsumption(double stateOfCharge) {
        if (stateOfCharge < inSoc) {
            return Integer.MAX_VALUE;
        } else if (stateOfCharge - cost > outSoc) {
            return stateOfCharge - outSoc;
        } else {
            return cost;
        }
    }

    public double computeSocInB(double stateOfChargeInA) {
//        if (consProfile == null) return stateOfChargeInA;
        return stateOfChargeInA - getConsumption(stateOfChargeInA);
    }

	public boolean isDominatedBy(ConsumptionProfile nextConsProfile) {
		return isDominatedBy(nextConsProfile, 1);
	}

    public boolean isDominatedBy(ConsumptionProfile nextConsProfile, double dominanceRelaxConst) {
		if (nextConsProfile == null) {
			return false;
		}

		return inSoc >= nextConsProfile.inSoc*dominanceRelaxConst
				&& outSoc*dominanceRelaxConst <= nextConsProfile.outSoc
				&& cost >= nextConsProfile.cost //THIS CANNOT BE RELAXED - VIOLATES SOLUTION OPTIMALITY
				&& !this.equals(nextConsProfile);

    }

	@Deprecated
	public boolean isDominatedByOld(ConsumptionProfile nextConsProfile) {
		//        if (nextConsProfile == null) return false; // TODO: 22-Aug-16 check this and think about it

		double thisConsProfileConsumption, nextConsProfileConsumption;
		//// TODO: 24-Nov-16 this is used almost everytime.... try to simplify this
		for (int i=ChargingModel.MIN_STATE_OF_CHARGE; i < ChargingModel.MAX_STATE_OF_CHARGE; i++) {
			thisConsProfileConsumption = getConsumption(i);
			nextConsProfileConsumption = nextConsProfile.getConsumption(i);
			if (thisConsProfileConsumption < nextConsProfileConsumption){
				return false;
			} else if (thisConsProfileConsumption == nextConsProfileConsumption
					&& thisConsProfileConsumption!= Integer.MAX_VALUE) {
				return false;
			}
		}

		return true;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ConsumptionProfile))
            return false;

        ConsumptionProfile that = (ConsumptionProfile) o;

        if (inSoc != that.inSoc)
            return false;
        if (outSoc != that.outSoc)
            return false;
        return cost == that.cost;

    }

    @Override
    public int hashCode() {
        int result = inSoc;
        result = 31 * result + outSoc;
        result = 31 * result + cost;
        return result;
    }

    @Override
    public String toString() {
        return "ConsumptionProfile [" +
                "inSoc=" + inSoc +
                ", outSoc=" + outSoc +
                ", costs=" + cost +
                ']';
    }

    @Override
    public int compareTo(ConsumptionProfile o) {
		if (this.isDominatedBy(o)) {
			return 1;
		} else if (o.isDominatedBy(this)) {
			return -1;
		} else {
			return 0;
		}
    }
}
