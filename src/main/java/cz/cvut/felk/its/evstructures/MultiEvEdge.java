package cz.cvut.felk.its.evstructures;

import cz.cvut.felk.its.evstructures.chargers.ChargingModel;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Tomas on 11-Mar-16.
 */
@Deprecated
public class MultiEvEdge<TEdge extends EvEdge> extends EvEdge implements Serializable {
    private static final long serialVersionUID = 7566836489379201708L;
    public final List<TEdge> edges;
    public final int[] minConsFunc; //index matches to incomingSoC, value matches to index of charger edges
//    public final int[] statesToCharge;

    public MultiEvEdge(int fromId, int toId, int length, List<TEdge> edges, int[] minConsFunc) {
        super(fromId, toId, length, null, 0, null, null);
        this.edges = edges;
        this.minConsFunc = minConsFunc;
//        this.statesToCharge = statesToCharge;
    }

//    public int getWeight(int incomingSoC) {
//        int indexSoC = getIndexFromSoC(incomingSoC);
//        if (indexSoC < 0) return Integer.MAX_VALUE;
//        int edgeIndex = minConsFunc[indexSoC];
//        if (edgeIndex < 0) return Integer.MAX_VALUE;
//        return edges.get(edgeIndex).getWeight();
//    }

    public TEdge getBestEdge(int stateOfCharge) {
        int indexSoC = getIndexFromSoC(stateOfCharge);
        if (indexSoC < 0) return null;
        int edgeIndex = minConsFunc[indexSoC];
        if (edgeIndex < 0) return null;
        return edges.get(edgeIndex);
    }

    private int getIndexFromSoC(int stateOfCharge) {
        int i = 0;
        while (i < ChargingModel.STATES_TO_CHARGE.length && ChargingModel.STATES_TO_CHARGE[i] <= stateOfCharge) {
            i++;
        }
        return i-1;
    }

    @Override
    public int getWeight() {
        return 0;
    }

    //do not create EQUALS AND HASHCODE!!! multiedges are in sets, and can have two (from,to,length) edges in one graph.

    @Override
    public String toString() {
        return "MultiEvEdge{" +
                "edges=" + edges +
                ", minConsFunc=" + Arrays.toString(minConsFunc) +
                "} " + super.toString();
    }
}
