package cz.cvut.felk.its.evstructures;

import cz.agents.basestructures.Edge;

import java.io.Serializable;

/**
 * Created by Tomas on 03.12.2015.
 */
public abstract class PlanningEdge extends Edge implements Serializable {

    private static final long serialVersionUID = 1121842902413557704L;

    public PlanningEdge(int fromId, int toId, int length) {
        super(fromId, toId, length);
    }
//    public final int travelTimeMillis;
//    public final int consumption;


    public abstract int getWeight();


}
