package cz.cvut.felk.its.evstructures;

import cz.agents.basestructures.GPSLocation;

import java.util.Objects;

/**
 * Created by Tomas on 10-May-16.
 */
@Deprecated
public class ViaNode extends GPSLocation {
    public final int edgeId;

    public ViaNode(GPSLocation loc, int edgeId) {
        super(loc.latE6, loc.lonE6, loc.latProjected, loc.lonProjected, loc.elevation);
        this.edgeId = edgeId;
    }

//    public ViaNode(int latE6, int lonE6, int latProjected, int lonProjected, int elevation, Set<Integer> edgeIds) {
//        super(latE6, lonE6, latProjected, lonProjected, elevation);
//        this.edgeIds = edgeIds;
//    }
//
//    public ViaNode(int latE6, int lonE6, int latProjected, int lonProjected, Set<Integer> edgeIds) {
//        super(latE6, lonE6, latProjected, lonProjected);
//        this.edgeIds = edgeIds;
//    }
//
//    public ViaNode(double latE6, double lonE6, int latProjected, int lonProjected, int elevation, Set<Integer> edgeIds) {
//        super(latE6, lonE6, latProjected, lonProjected, elevation);
//        this.edgeIds = edgeIds;
//    }
//
//    public ViaNode(double latE6, double lonE6, int latProjected, int lonProjected, Set<Integer> edgeIds) {
//        super(latE6, lonE6, latProjected, lonProjected);
//        this.edgeIds = edgeIds;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ViaNode that = (ViaNode) o;
        return edgeId == that.edgeId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), edgeId);
    }

    @Override
    public String toString() {
        return "ViaNode{" +
                "edgeId=" + edgeId +
                "} " + super.toString();
    }
}
