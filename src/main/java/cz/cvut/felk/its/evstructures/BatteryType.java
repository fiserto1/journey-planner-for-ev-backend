/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures;

public enum BatteryType {
	TESLA("Tesla", 500, 85000);

	private final String name;
	private final int minSoC;
	private final int maxSoC;

	BatteryType(String name, int minSoC, int maxSoC){
		this.name = name;
		this.minSoC = minSoC;
		this.maxSoC = maxSoC;
	}

	public int getMinSoC() {
		return minSoC;
	}

	public int getMaxSoC() {
		return maxSoC;
	}

	public boolean isInBatteryRange(int stateOfCharge) {
		return stateOfCharge <= maxSoC && stateOfCharge >= minSoC;
	}
}
