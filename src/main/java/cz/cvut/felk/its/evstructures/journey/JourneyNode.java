/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures.journey;

public class JourneyNode {
//	public final boolean isUsedSupercharger;
	public final int nodeId;
	public final int stateBeforeCharge;
	public final int stateAfterCharge;
	public final int pricePerKwh;

	public JourneyNode(int nodeId, int stateBeforeCharge, int stateAfterCharge,
			int pricePerKwh) {
//		this.isUsedSupercharger = isUsedSupercharger;
		this.nodeId = nodeId;
		this.stateBeforeCharge = stateBeforeCharge;
		this.stateAfterCharge = stateAfterCharge;
		this.pricePerKwh = pricePerKwh;
	}
	
//	public double getPriceForCharging() {
//		return pricePerKwh *((double) (stateAfterCharge - incomingSoC))/1000;
//	}

	@Override
	public String toString() {
		return "JourneyNode [" +
				"nodeId=" + nodeId +
				", stateBeforeCharge=" + stateBeforeCharge +
				", stateAfterCharge=" + stateAfterCharge +
				", pricePerKwh=" + pricePerKwh +
				']';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof JourneyNode))
			return false;

		JourneyNode that = (JourneyNode) o;

//		if (nodeId != that.nodeId)
//			return false;
//		if (stateBeforeCharge != that.stateBeforeCharge)
//			return false;
//		if (stateAfterCharge != that.stateAfterCharge)
//			return false;
//		return pricePerKwh == that.pricePerKwh;
		return nodeId == that.nodeId;

	}

	@Override
	public int hashCode() {
		int result = nodeId;
//		result = 31 * result + stateBeforeCharge;
//		result = 31 * result + stateAfterCharge;
//		result = 31 * result + pricePerKwh;
		return result;
	}
}
