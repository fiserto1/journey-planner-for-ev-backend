/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures.journey;

import cz.cvut.felk.its.routing.structures.routers.RouterType;

import java.util.ArrayList;
import java.util.List;

public class Journey {

	public final int startNodeId;
	public final int goalNodeId;
//	public final List<JourneyNode> journeyNodes;

	public final List<JourneySegment> journeySegments;

	public final int length;
	public final int travelTimeMillis;
	public final int consumption;
	public final int price; //Price in cents
//	public final int costs;
	public final RouterType journeyType;

	public Journey(int startNodeId, int goalNodeId, RouterType journeyType) {
		this(startNodeId, goalNodeId, new ArrayList<>(), Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE,
				Integer.MAX_VALUE, journeyType);
	}

	public Journey(int startNodeId, int goalNodeId, List<JourneySegment> journeySegments, int length,
			int travelTimeMillis, int consumption, int price, RouterType journeyType) {
		this.startNodeId = startNodeId;
		this.goalNodeId = goalNodeId;
		this.journeySegments = journeySegments;
		this.length = length;
		this.travelTimeMillis = travelTimeMillis;
		this.consumption = consumption;
		this.price = price;
		this.journeyType = journeyType;
	}

	//public final List<Integer> superchargerNodeIds;

	//public final List<Integer> superchargerStepIndexes; //indexes of step, where supercharger is used!!

	@Override
	public String toString() {
		return "Journey [" +
				"startNodeId=" + startNodeId +
				", goalNodeId=" + goalNodeId +
				", journeySegments=" + journeySegments +
				", length=" + length +
				", travelTimeMillis=" + travelTimeMillis +
				", consumption=" + consumption +
				", price=" + price +
				", journeyType=" + journeyType +
				']';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Journey))
			return false;

		Journey journey = (Journey) o;

		if (startNodeId != journey.startNodeId)
			return false;
		if (goalNodeId != journey.goalNodeId)
			return false;
		if (length != journey.length)
			return false;
		if (travelTimeMillis != journey.travelTimeMillis)
			return false;
		if (consumption != journey.consumption)
			return false;
		if (price != journey.price)
			return false;
		if (journeySegments != null ?
				!journeySegments.equals(journey.journeySegments) :
				journey.journeySegments != null)
			return false;
		return journeyType == journey.journeyType;

	}

	@Override
	public int hashCode() {
		int result = startNodeId;
		result = 31 * result + goalNodeId;
		result = 31 * result + (journeySegments != null ? journeySegments.hashCode() : 0);
		result = 31 * result + length;
		result = 31 * result + travelTimeMillis;
		result = 31 * result + consumption;
		result = 31 * result + price;
		result = 31 * result + (journeyType != null ? journeyType.hashCode() : 0);
		return result;
	}
}
