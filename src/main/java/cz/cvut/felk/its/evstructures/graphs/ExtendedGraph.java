/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures.graphs;

import cz.agents.basestructures.GraphStructure;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.PlanningEdge;

import java.util.*;
import java.util.stream.Collectors;

public class ExtendedGraph<TNode extends EvNode, TEdge extends PlanningEdge>
		implements GraphStructure<TNode, TEdge> {

	private GraphStructure<TNode, TEdge> graph;
	private final TNode startNode;
	private final TNode goalNode;
	private Map<Integer, TEdge> startEdges; //key - edge.toId
	private Map<Integer, TEdge> goalEdges; //key - edge.fromId
//	private Map<Integer, TNode> additionalNodes;
	Set<Integer> chargerNodeIds;

	public ExtendedGraph(GraphStructure<TNode, TEdge> graph, TNode startNode, TNode goalNode,
			Map<Integer, TEdge> startEdges, Map<Integer, TEdge> goalEdges) {
		this.graph = graph;
		this.startNode = startNode;
		this.goalNode = goalNode;
		this.startEdges = startEdges;
		this.goalEdges = goalEdges;
		this.chargerNodeIds = graph.getAllNodes().stream()
				.filter(EvNode::isCharger)
				.map(node -> node.id)
				.collect(Collectors.toSet());
	}

	public Set<Integer> getChargerNodeIds() {
		return chargerNodeIds;
	}

	@Override
	public boolean containsNode(TNode tNode) {
		return containsNode(tNode.id);
	}

	@Override
	public boolean containsNode(int i) {
		return graph.containsNode(i) || i == startNode.id || i == goalNode.id;
	}

	@Override
	public TNode getNode(int i) {
		if (i == startNode.id) {
			return startNode;
		} else if (i == goalNode.id)  {
			return goalNode;
		} else if (graph.containsNode(i)) {
			return graph.getNode(i);
		} else {
			return null;
		}
	}

	@Override
	public boolean containsEdge(TEdge tEdge) {
		return containsEdge(tEdge.fromId, tEdge.toId);
	}

	@Override
	public boolean containsEdge(int i, int i1) {
		return (i1 == goalNode.id && goalEdges.containsKey(i))
				|| (i == startNode.id && startEdges.containsKey(i1)) || graph.containsEdge(i, i1);
	}

	@Override
	public TEdge getEdge(int from, int to) {
		 if (from == startNode.id) {
			return startEdges.get(to);
		} else if (to == goalNode.id) {
			return goalEdges.get(from);
		} else if (graph.containsEdge(from, to)) {
			return graph.getEdge(from, to);
		} else {
			return null;
		}
	}

	@Override
	public List<TEdge> getInEdges(TNode tNode) {
		return getInEdges(tNode.id);
	}

	@Override
	public List<TEdge> getInEdges(int i) {
		if (i == goalNode.id) {
			return new ArrayList<>(goalEdges.values());
		} else if (i == startNode.id) {
			return new ArrayList<>();
		} else if (!startEdges.containsKey(i)){
			return graph.getInEdges(i);
		} else {
			List<TEdge> inEdges = new ArrayList<>(graph.getInEdges(i));
			inEdges.add(startEdges.get(i));
			return inEdges;
		}
	}

	@Override
	public List<TEdge> getOutEdges(TNode tNode) {
		return getOutEdges(tNode.id);
	}

	@Override
	public List<TEdge> getOutEdges(int i) {
		if (i == startNode.id) {
			return new ArrayList<>(startEdges.values());
		} else if (i == goalNode.id) {
			return new ArrayList<>();
		} else if (!goalEdges.containsKey(i)){
			return graph.getOutEdges(i);
		} else {
			List<TEdge> outEdges = new ArrayList<>(graph.getOutEdges(i));
			outEdges.add(goalEdges.get(i));
			return outEdges;
		}
	}

	public int getNumOfNodes() {
		return graph.getAllNodes().size() + 2;
	}

	@Override
	public Collection<TNode> getAllNodes() {

		Collection<TNode> allNodes = new ArrayList<>(graph.getAllNodes());
		allNodes.add(startNode);
		allNodes.add(goalNode);

		return allNodes;
	}

	@Override
	public Collection<TEdge> getAllEdges() {
		Collection<TEdge> allEdges = new ArrayList<>(graph.getAllEdges());
		allEdges.addAll(startEdges.values());
		allEdges.addAll(goalEdges.values());
		return allEdges;
	}

	public TNode getStartNode() {
		return startNode;
	}

	public TNode getGoalNode() {
		return goalNode;
	}
}
