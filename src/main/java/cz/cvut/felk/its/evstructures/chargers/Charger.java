/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures.chargers;

import java.time.LocalDateTime;

public interface Charger {
	void setPricesDuringDay(Integer[] pricesDuringDay);
	/**
	 * Price per kWh in cents
	 * @return
	 */
	int getPricePerkWh(LocalDateTime visitationDateTime);
}
