/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures;

import cz.agents.basestructures.*;
import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.api.structures.Coordinates;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class EVZone<TNode extends Node, TEdge extends Edge> extends Zone<TNode, TEdge> implements Serializable {
	private static final long serialVersionUID = 4470798717795964226L;

	private Map<GPSLocation, Collection<EvEdge>> viaNodes;
	private int numOfChargers;

	public EVZone(String name, int srid, GraphStructure<TNode, TEdge> graph, BoundingBox boundingBox,
			Map<GPSLocation, Collection<EvEdge>> viaNodes, int numOfChargers) {
		super(name, srid, graph, boundingBox);
		this.viaNodes = viaNodes;
		this.numOfChargers = numOfChargers;
	}

	public Map<GPSLocation, Collection<EvEdge>> getViaNodes() {
		return viaNodes;
	}

	public int getNumOfChargers() {
		return numOfChargers;
	}

	public GPSLocation createGPSLocationInZone(Coordinates loc) {

		return GPSLocationTools.createGPSLocation(loc.getLat(), loc.getLon(), 100, srid);
	}

	@Override
	public String toString() {
		return "EVZone [" +
				"name=" + name +
				", srid=" + srid +
				", graph=[" + graph.getAllNodes().size() + " nodes, " +graph.getAllEdges().size() + " edges]" +
				", numOfViaNodes=" + viaNodes.size() +
				", numOfChargers=" + numOfChargers +
				", bbox=[min(" + boundingBox.minLatE6 + ", " + boundingBox.minLonE6
				+ "), max("+ boundingBox.maxLatE6 + ", " + boundingBox.maxLonE6 + ")]"+
				']';
	}
}
