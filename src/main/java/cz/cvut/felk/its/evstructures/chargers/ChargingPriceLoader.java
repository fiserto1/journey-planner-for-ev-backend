package cz.cvut.felk.its.evstructures.chargers;

import cz.agents.basestructures.GraphStructure;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.PlanningEdge;

/**
 * Created by Tomas on 14-Apr-16.
 */
public interface ChargingPriceLoader {
    <TNode extends EvNode, TEdge extends PlanningEdge> boolean loadPrices(GraphStructure<TNode, TEdge> graph);
}
