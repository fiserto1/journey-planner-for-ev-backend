package cz.cvut.felk.its.evstructures.chargers;

import java.util.Objects;

/**
 * Created by Tomas on 21-Mar-16.
 */
public class ChargingModel {
    public final static int CONSTANT_CHARGING_TIME_MILLIS = 30 * 60000;

//    public final static int[] STATES_TO_CHARGE = {1000, 20000, 35000, 50000, 70000, 85000};//max 10000
    public final static int[] STATES_TO_CHARGE = {1000, 20000, 35000, 50000, 70000, 85000};//max 10000
//    public final static int[] STATES_TO_CHARGE = {1, 4, 5, 8, 10, 20};//max 10000
    public static final int MIN_STATE_OF_CHARGE = 500;
    public static final int MAX_STATE_OF_CHARGE = 85000; //85000 default:10000

    public static final double TIME_COST_CONSTANT = 0.01;

    public final double SHIFT_IN_PERC = 80; //e.g. 80 percent from maxState
    public final double TIME_IN_MIN_SOC = 0;
    public final double TIME_IN_SHIFT = 40 * 60;
    public final double TIME_IN_MAX_SOC = 75 * 60;
    public final double stateOfChargeInShift; //real value computed as part of maxStateOfCharge
    public final int maxStateOfCharge;
    public final int minStateOfCharge;

    public final double firstPartCoeff; // coefficient "a" of first linear function y=ax
    public final double secondPartCoeff; //coefficient "b" of second linear function y=bx + c
    public final double firstPartConst;
    public final double secondPartConst;

    public ChargingModel() {
        this(MIN_STATE_OF_CHARGE, MAX_STATE_OF_CHARGE);
    }

    public ChargingModel(int minStateOfCharge, int maxStateOfCharge) {
        this.minStateOfCharge = minStateOfCharge;
        this.maxStateOfCharge = maxStateOfCharge;
        this.stateOfChargeInShift = (int) (((double) (this.SHIFT_IN_PERC)/100)*maxStateOfCharge);
        this.firstPartCoeff = computeCoefficient(TIME_IN_MIN_SOC, TIME_IN_SHIFT, minStateOfCharge, stateOfChargeInShift);
        this.firstPartConst = computeConstant(TIME_IN_MIN_SOC, TIME_IN_SHIFT, minStateOfCharge, stateOfChargeInShift);
        this.secondPartCoeff = computeCoefficient(TIME_IN_SHIFT, TIME_IN_MAX_SOC, stateOfChargeInShift, maxStateOfCharge);
        this.secondPartConst = computeConstant(TIME_IN_SHIFT, TIME_IN_MAX_SOC, stateOfChargeInShift, maxStateOfCharge);
    }

    @Deprecated
    public static boolean isInBatteryRangeStatic(int stateOfCharge) {
        return stateOfCharge <= MAX_STATE_OF_CHARGE && stateOfCharge >= MIN_STATE_OF_CHARGE;
    }

    @Deprecated
    public boolean isInBatteryRange(int stateOfCharge) {
        return stateOfCharge <= maxStateOfCharge && stateOfCharge >= minStateOfCharge;
    }

    private double computeCoefficient(double tMin, double tMax, double minSoC,  double maxSoC) {
        return (tMax-tMin)/(maxSoC-minSoC);
    }

    private double computeConstant(double tMin, double tMax, double minSoC,  double maxSoC) {
        return tMin - minSoC*(tMax-tMin)/(maxSoC-minSoC);
    }

    public static int getStCIdFromSoC(int stateOfCharge) {
        int stateToChargeId = 0;
        while (stateToChargeId < ChargingModel.STATES_TO_CHARGE.length
                && ChargingModel.STATES_TO_CHARGE[stateToChargeId] <= stateOfCharge) {
            stateToChargeId++;
        }
        return stateToChargeId-1;
    }

    /**
     * Compute time spent during charging.
     * @param stateOfCharge
     * @param stateAfterCharge
     * @return charging time in seconds
     */
    public int computeChargingTime(int stateOfCharge, double stateAfterCharge) {
        //stateAfterCharge or energyToCharge ?? and..

//        if (incomingSoC < minStateOfCharge || incomingSoC > maxStateOfCharge
//                  || stateAfterCharge > maxStateOfCharge ) return Integer.MAX_VALUE;

        if (stateAfterCharge < stateOfCharge) return Integer.MAX_VALUE;

        double t1, t2;
        if (stateOfCharge < stateOfChargeInShift) {
            t1 = firstPartCoeff*stateOfCharge + firstPartConst;
        } else {
            t1 = secondPartCoeff*stateOfCharge + secondPartConst;
        }
        if (stateAfterCharge < stateOfChargeInShift) {
            t2 = firstPartCoeff*stateAfterCharge + firstPartConst;
        } else {
            t2 = secondPartCoeff*stateAfterCharge + secondPartConst;
        }

        return (int) (t2 - t1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChargingModel that = (ChargingModel) o;
        return Double.compare(that.SHIFT_IN_PERC, SHIFT_IN_PERC) == 0 &&
                Double.compare(that.TIME_IN_MIN_SOC, TIME_IN_MIN_SOC) == 0 &&
                Double.compare(that.TIME_IN_SHIFT, TIME_IN_SHIFT) == 0 &&
                Double.compare(that.TIME_IN_MAX_SOC, TIME_IN_MAX_SOC) == 0 &&
                Double.compare(that.stateOfChargeInShift, stateOfChargeInShift) == 0 &&
                maxStateOfCharge == that.maxStateOfCharge &&
                minStateOfCharge == that.minStateOfCharge &&
                Double.compare(that.firstPartCoeff, firstPartCoeff) == 0 &&
                Double.compare(that.secondPartCoeff, secondPartCoeff) == 0 &&
                Double.compare(that.firstPartConst, firstPartConst) == 0 &&
                Double.compare(that.secondPartConst, secondPartConst) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(SHIFT_IN_PERC, TIME_IN_MIN_SOC, TIME_IN_SHIFT, TIME_IN_MAX_SOC, stateOfChargeInShift, maxStateOfCharge, minStateOfCharge, firstPartCoeff, secondPartCoeff, firstPartConst, secondPartConst);
    }
}
