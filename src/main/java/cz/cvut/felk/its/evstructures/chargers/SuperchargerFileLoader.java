package cz.cvut.felk.its.evstructures.chargers;

import cz.agents.basestructures.GPSLocation;
import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.utils.TextFilesWorker;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Tomas on 06.11.2015.
 */
public class SuperchargerFileLoader {

    Logger log = Logger.getLogger(SuperchargerFileLoader.class);

    private static final String DELIMITER = ";";
    public final int NUM_OF_SCHARGER_PARAMS = 11;

    public ArrayList<Supercharger> parseSuperchargers(String filename, String folder, int srid){

        ArrayList<Supercharger> superchargers = new ArrayList<>();

        List<String[]> csvLines = TextFilesWorker.loadCSVLinesFromFileInResources(filename, folder, DELIMITER)
                .collect(Collectors.toList());

        //HEADER
        String[] header = csvLines.get(0);
        if (header.length != NUM_OF_SCHARGER_PARAMS){
            log.error("Wrong number of columns: "+ NUM_OF_SCHARGER_PARAMS + " vs. " + header.length);
            return null;
        }

        //BODY
        int id = -1;
        for (String[] line: csvLines.subList(0, csvLines.size())) {

            Supercharger supercharger = parseSuperchargerFromLine(id, line, srid);
            if (supercharger == null) continue;
            superchargers.add(supercharger);
            id--;
        }

        log.info(String.format("%d Superchargers loaded.", (-1)*(id+1)));
        return superchargers;
    }

    private Supercharger parseSuperchargerFromLine(int id, String[] chargerParams, int srid) {
        int zip = -1;
        int numberOfStalls = -1;
        int elevation = -1;

        if(!chargerParams[9].equals("Open")) return null; //closed
        String[] gps = chargerParams[7].split(",");
        double lat = Double.parseDouble(gps[0]);
        double lon = Double.parseDouble(gps[1]);
        GPSLocation location = GPSLocationTools.createGPSLocation(lat, lon, 0, srid);
        if(!chargerParams[4].equals("")) zip = Integer.parseInt(chargerParams[4]);
        if(!chargerParams[6].equals("")) numberOfStalls = Integer.parseInt(chargerParams[6]);
        if(!chargerParams[8].equals("")) elevation = Integer.parseInt(chargerParams[6]);

        return new Supercharger(id, chargerParams[0], chargerParams[1], chargerParams[2], chargerParams[3], zip,
                chargerParams[5], numberOfStalls, location, elevation, chargerParams[9], chargerParams[10]);
    }


}
