package cz.cvut.felk.its.evstructures.chargers;

import cz.agents.basestructures.GraphStructure;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.PlanningEdge;
import cz.cvut.felk.its.utils.TextFilesWorker;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Tomas on 14-Apr-16.
 */
public class ChargingPriceFileLoader implements ChargingPriceLoader {

    Logger log = Logger.getLogger(ChargingPriceFileLoader.class);

    private static final int NUM_OF_FILES = 5;
    private static final String SEPARATOR = ",";

    @Override
    public <TNode extends EvNode, TEdge extends PlanningEdge>
    boolean loadPrices(GraphStructure<TNode, TEdge> graph) {

//        Random rnd = new Random(47);
        Random rnd = new Random();
        int fileIndex = rnd.nextInt(NUM_OF_FILES) + 1;
//        return loadPricesFromFile("charging-prices-" + fileIndex + ".csv", "prices", graph);
        return loadPricesFromFile("charging-prices.csv", "prices", graph);
    }

    private <TNode extends EvNode, TEdge extends PlanningEdge>
    boolean loadPricesFromFile(String filename, String folder, GraphStructure<TNode, TEdge> graph) {
        List<String[]> csvLines = TextFilesWorker.loadCSVLinesFromFileInResources(filename, folder, SEPARATOR)
                .collect(Collectors.toList());

        String[] columns = csvLines.get(0);

        //HEADER
        if (columns.length != Supercharger.NUM_OF_HOURS){
            log.error("Wrong number of columns.");
            return false;
        }

        Collection<TNode> superchargers = graph.getAllNodes().stream()
                .filter(TNode::isCharger)
                .collect(Collectors.toList());

        //BODY
        int counter = 0;
        for (TNode sChargerNode : superchargers) {
            counter++;
            if (csvLines.size() == counter) {
                log.error("Wrong number of rows. " + filename + " " + counter);
                return false;
            }
            columns = csvLines.get(counter);

            Integer[] pricesDuringDay = Arrays.stream(columns).map(Integer::parseInt).toArray(Integer[]::new);

            if (pricesDuringDay.length != Supercharger.NUM_OF_HOURS) {
                log.error("Wrong number of columns.");
                return false;
            }

            sChargerNode.getCharger().setPricesDuringDay(pricesDuringDay);
        }

        log.info("Prices were loaded from file: " + filename);
        return true;
    }
}
