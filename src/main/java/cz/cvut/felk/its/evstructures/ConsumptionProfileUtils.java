package cz.cvut.felk.its.evstructures;

import cz.agents.basestructures.GPSLocation;
import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.routing.SpeedUpSettings;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.EwLabel;
import cz.cvut.felk.its.zonebuilder.tns.TnsEdge;
import cz.cvut.felk.its.zonebuilder.tns.TnsGraph;
import cz.cvut.felk.its.zonebuilder.tns.TnsNode;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tomas on 21-Jul-16.
 */
public class ConsumptionProfileUtils {

    private static final Logger log = Logger.getLogger(ConsumptionProfileUtils.class);

    private static final double LAMBDA = 2; //uphill constant // 1 //Karlsruhe: 1
    private static final double KAPPA = 0.2; //length constant //0.1 || 0.15 //Karlsruhe: 0.02
    private static final double ALPHA = 1.5; //downhill constant 0.5 //Karlsruhe: 0.25
    //0.5 alpha causes that we have none negative edges.


    public static ConsumptionProfile linkConsumptionProfiles(ConsumptionProfile cpA, ConsumptionProfile cpB) {
        if (cpA == null) return cpB;
		if (cpB == null) return cpA;

        int inSocAB = Math.max(cpA.inSoc, cpA.cost + cpB.inSoc);
        int outSocAB = Math.min(cpB.outSoc, cpA.outSoc - cpB.cost);
        int costAB = Math.max(cpA.cost + cpB.cost, cpA.inSoc - cpB.outSoc);
        return new ConsumptionProfile(inSocAB, outSocAB, costAB);
    }

    @Deprecated //moved to method in class ConsumptionProfile
    public static double computeSocInB(ConsumptionProfile consProfile, double stateOfChargeInA) {
        if (consProfile == null) return stateOfChargeInA;
        return stateOfChargeInA - consProfile.getConsumption(stateOfChargeInA);
    }

//    public static <TEdge extends PlanningEdge> ConsumptionProfile createConsumptionProfile(TEdge edge) {
//        return computeConsumptionProfileBetweenTwoLocs(from, to);
//        int costs = ((EvEdge) edge).consumption;
//        int inSoc = Math.max(ChargingModel.MIN_STATE_OF_CHARGE, ChargingModel.MIN_STATE_OF_CHARGE + costs); // (ChargingModel.MIN_STATE_OF_CHARGE + costs) or (costs) ????
//        int outSoc = Math.min(ChargingModel.MAX_STATE_OF_CHARGE, ChargingModel.MAX_STATE_OF_CHARGE - costs);
//
//        return new ConsumptionProfile(inSoc, outSoc, costs);
//    }


    public static <TLoc extends GPSLocation> ConsumptionProfile computeConsumptionProfileBetweenTwoLocs(
            TLoc from, TLoc to) {

		return computeConsumptionProfileBetweenTwoLocs(from, to, GPSLocationTools.computeDistanceAsDouble(from, to));
    }

    public static <TLoc extends GPSLocation> ConsumptionProfile computeConsumptionProfileBetweenTwoLocs(
            TLoc from, TLoc to, double length) {
		int consumption = computeConsumptionBetweenTwoPoints(from, to, length);

		if (consumption > ChargingModel.MAX_STATE_OF_CHARGE) {
			//path is not feasible
			return null;
		}

		int cost = Math.max(consumption, -1*ChargingModel.MAX_STATE_OF_CHARGE);

        int inSoc = Math.max(ChargingModel.MIN_STATE_OF_CHARGE, ChargingModel.MIN_STATE_OF_CHARGE + cost); // (ChargingModel.MIN_STATE_OF_CHARGE + costs) or (costs) ????
        int outSoc = Math.min(ChargingModel.MAX_STATE_OF_CHARGE, ChargingModel.MAX_STATE_OF_CHARGE - cost);

        return new ConsumptionProfile(inSoc, outSoc, cost);
    }

    public static ConsumptionProfile computeConsumptionProfile(TnsEdge edge, TnsGraph tnsGraph) {
        TnsNode fromNode = tnsGraph.getNode(edge.fromId);
        ConsumptionProfile totalConsumptionProfile = null, consumptionProfile;

        TnsNode toNode;
        for (TnsNode tnsNode : edge.getVia()) {
            consumptionProfile = computeConsumptionProfileBetweenTwoLocs(fromNode, tnsNode);
			if (consumptionProfile == null) {
				return null;
			}
            totalConsumptionProfile = linkConsumptionProfiles(totalConsumptionProfile, consumptionProfile);
			fromNode = tnsNode;
        }

        toNode = tnsGraph.getNode(edge.toId);
        consumptionProfile = computeConsumptionProfileBetweenTwoLocs(fromNode, toNode);
		if (consumptionProfile == null) {
			return null;
		}
		totalConsumptionProfile = linkConsumptionProfiles(totalConsumptionProfile, consumptionProfile);
        return totalConsumptionProfile;
    }

    public static int computeEdgeConsumption(TnsEdge edge, TnsGraph tnsGraph) {

        TnsNode fromNode = tnsGraph.getNode(edge.fromId);
        int totalConsumption = 0, consumption;

        TnsNode toNode;
        for (TnsNode tnsNode : edge.getVia()) {
            consumption = computeConsumptionBetweenTwoPoints(fromNode, tnsNode);
            totalConsumption += consumption;
            fromNode = tnsNode;

        }
        toNode = tnsGraph.getNode(edge.toId);
        consumption = computeConsumptionBetweenTwoPoints(fromNode, toNode);
        totalConsumption += consumption;
        return totalConsumption;
    }

    public static int computeConsumptionBetweenTwoPoints(GPSLocation from, GPSLocation to) {
        return computeConsumptionBetweenTwoPoints(from, to, GPSLocationTools.computeDistanceAsDouble(from, to));
    }

    public static int computeConsumptionBetweenTwoPoints(GPSLocation from, GPSLocation to, double length) {
        if (length == 0) return 0;
        double elevationChange = (to.elevation - from.elevation);

        if (elevationChange < 0) {
            //downhill - recharging
            return (int) (KAPPA*length + ALPHA*elevationChange);
        } else {
            //uphill
            return (int) (KAPPA*length + LAMBDA*elevationChange);
        }
    }

	/**
     *
     * @param fNode
     * @param <FNode>
     * @return pairs of <tt, soc>; travel time and state of charge in fNode recomputed after charging in lastSeenCharger
     */
	public static <FNode extends EvLabel> List<double[]> computePossibleChargingPairs(FNode fNode, ChargingFunction chFunction, BatteryType battery) {

//		if (fNode.lastSeenChargerSoC >= fNode.consProfile.inSoc) return null; // TODO: 10-Dec-16 try to turn this off!!

		List<double[]> possibleChargingPairs = new LinkedList<>();

		double shiftRight = fNode.travelTimeMillis; // travel time in fNode without charging at lastSeenCharger
		double shiftLeft = chFunction.getChargingTimeMillisFromMinimum(
				fNode.lastSeenChargerSoC); // charging time from minimum that we wont spend, because we arrived with some energy already

		double maxChargableSoc =
				fNode.consProfile.cost + fNode.consProfile.outSoc; // the max SoC that pays off to charge

		//		double minChargableSoc = fNode.consProfile.inSoc;
		//+500 means that at least 500wh is charged
		double minChargableSoc = Math.max(fNode.consProfile.inSoc,
				Math.min(fNode.lastSeenChargerSoC+SpeedUpSettings.minChargingAmountInWh, maxChargableSoc));


		double[] chPair;
		if (SpeedUpSettings.minChargeableSoc) {
			chPair = computeChargingPair(chFunction, fNode.consProfile, minChargableSoc, shiftLeft, shiftRight);
			possibleChargingPairs.add(chPair);
		}

		if (minChargableSoc != maxChargableSoc) {

			if (SpeedUpSettings.alterChargeableSoc) {
				for (int i = SpeedUpSettings.minChargingLimitInPerc; i <= 100; i+=SpeedUpSettings.chargingLimitStepInPerc) {
					double anotherStateToCharge =  ((double)battery.getMinSoC()*i)/100;
					if (anotherStateToCharge > minChargableSoc && anotherStateToCharge < maxChargableSoc) {

						chPair = computeChargingPair(chFunction, fNode.consProfile, anotherStateToCharge, shiftLeft, shiftRight);
						possibleChargingPairs.add(chPair);
					}
				}
			}

			if (SpeedUpSettings.maxChargeableSoc) {
				chPair = computeChargingPair(chFunction, fNode.consProfile, maxChargableSoc, shiftLeft, shiftRight);
				possibleChargingPairs.add(chPair);
			}
		}



//
//		double[] firstPossibleChPair = computeFirstPossibleChargingPair(fNode, chFunction, shiftLeft, shiftRight);
//		if (firstPossibleChPair != null) {
//			possibleChargingPairs.add(firstPossibleChPair);
//		}
//		//			log.debug("t1:" + t1);
//		//			log.debug("soc1:" + soc1);
//
//		double[] lastPossibleChPair = computeLastPossibleChargingPair(fNode, chFunction, maxChargableSoc, shiftLeft,
//				shiftRight);
//		if (lastPossibleChPair != null) {
//			possibleChargingPairs.add(lastPossibleChPair);
//		}



//		if ((t2 != t1 && soc2 == soc1) || (t2 == t1 && soc2 != soc1)) {
//			log.error("ITS NOT POSSIBLE: inspect it.");
//		}

//		if (((int) firstPossibleChPair[0]) + 1 < fNode.travelTimeMillis
//				|| (lastPossibleChPair != null && ((int) lastPossibleChPair[0]) + 1 < fNode.travelTimeMillis)) {
//			log.error("WHAT THE HEELLL?: inspect this");
//		}

		return possibleChargingPairs;
	}

	public static <FNode extends EvLabel> double[] computeChargingPair(FNode fNode, ChargingFunction chFunction, ConsumptionProfile consProfile, double stateToCharge) {
		double shiftRight = fNode.travelTimeMillis; // travel time in fNode without charging at lastSeenCharger
		double shiftLeft = chFunction.getChargingTimeMillisFromMinimum(
				fNode.lastSeenChargerSoC);
		return computeChargingPair(chFunction, fNode.consProfile, stateToCharge, shiftLeft, shiftRight);
	}

	private static <FNode extends EvLabel> double[] computeLastPossibleChargingPair(FNode fNode,
			ChargingFunction chFunction, double maxChargableSoc, double shiftLeft, double shiftRight) {

		if (maxChargableSoc < fNode.consProfile.inSoc) {
			log.debug(String.format("HELLL NOOO (maxChargableSoc < fNode.consProfile.inSoc) %f and %d", maxChargableSoc, fNode.consProfile.inSoc));
		}

		if (maxChargableSoc != fNode.consProfile.inSoc) {
			//        double t2 = chModel.computeChargingTime(ChargingModel.MIN_STATE_OF_CHARGE, consProfileSlope) + shiftRight - shiftLeft;
//			double t2 = chFunction.getChargingTimeMillisFromMinimum(maxChargableSoc) + shiftRight - shiftLeft;
//			double soc2 = fNode.consProfile.computeSocInB(maxChargableSoc);// + ChargingModel.MIN_STATE_OF_CHARGE;

			//				log.debug("t2:" + t2);
			//				log.debug("soc2:" + soc2);
			//			return new double[]{t2, soc2};
			return computeChargingPair(chFunction, fNode.consProfile, maxChargableSoc, shiftLeft, shiftRight);


		} else {
			return null;
		}
	}

	private static <FNode extends EvLabel> double[] computeFirstPossibleChargingPair(FNode fNode,
			ChargingFunction chFunction, double shiftLeft, double shiftRight) {

		double t1, soc1;
		if (fNode.lastSeenChargerSoC > fNode.consProfile.inSoc) {
			//arrived with more energy than needed to pass next edge
			return null;
			// TODO: 25-Nov-16 this means we are not charging... it is unwanted state
//			t1 = chFunction.getChargingTimeMillisFromMinimum(fNode.lastSeenChargerSoC) + shiftRight - shiftLeft;
//			soc1 = fNode.consProfile.computeSocInB(fNode.lastSeenChargerSoC);// + ChargingModel.MIN_STATE_OF_CHARGE;
//			return new double[]{t1, soc1};
		} else {
			return computeChargingPair(chFunction, fNode.consProfile, fNode.consProfile.inSoc, shiftLeft, shiftRight);
//			t1 = chFunction.getChargingTimeMillisFromMinimum(fNode.consProfile.inSoc) + shiftRight - shiftLeft; //also getChargingTime(shiftLeft <state before charge>, fnode.consProfile.inSoc<state after charge> ) + shiftRight<tt without charging time>
//			soc1 = fNode.consProfile.computeSocInB(fNode.consProfile.inSoc);// + ChargingModel.MIN_STATE_OF_CHARGE;
//			return new double[]{t1, soc1};
		}
	}

	private static double[] computeChargingPair(ChargingFunction chFunction, ConsumptionProfile consProfile, double stateToCharge,
			double shiftLeft, double shiftRight) {

		double t1 = chFunction.getChargingTimeMillisFromMinimum(stateToCharge) + shiftRight - shiftLeft; //also getChargingTime(shiftLeft <state before charge>, fnode.consProfile.inSoc<state after charge> ) + shiftRight<tt without charging time>
		double soc1 = consProfile.computeSocInB(stateToCharge);// + ChargingModel.MIN_STATE_OF_CHARGE;
		return new double[]{t1, soc1};
	}

	public static <FNode extends EvLabel> int retrieveSoCInA(FNode fNode, double socInB, BatteryType battery) {
		double maxChargeableSoc =
				fNode.consProfile.cost + fNode.consProfile.outSoc; // the max SoC that pays off to charge

		//+500 means that at least 500wh is charged
		double minChargeableSoc = Math.max(fNode.consProfile.inSoc,
				Math.min(fNode.lastSeenChargerSoC+500, maxChargeableSoc));


		double possibleSocInB = fNode.consProfile.computeSocInB(minChargeableSoc);

		if (possibleSocInB == socInB) {
			return (int) minChargeableSoc;
		}
		if (minChargeableSoc != maxChargeableSoc) {
			for (int i = 2; i <= 10; i+=2) {
				double anotherStateToCharge =  ((double)battery.getMinSoC()*i)/10;
				if (anotherStateToCharge > minChargeableSoc && anotherStateToCharge < maxChargeableSoc) {

					possibleSocInB = fNode.consProfile.computeSocInB(anotherStateToCharge);

					if (possibleSocInB == socInB) {
						return (int) anotherStateToCharge;
					}
				}
			}

			possibleSocInB = fNode.consProfile.computeSocInB(maxChargeableSoc);

			if (possibleSocInB == socInB) {
				return (int) maxChargeableSoc;
			}
		}

//		if (soc1 == socInB) {
//			if (fNode.lastSeenChargerSoC > fNode.consProfile.inSoc){
//				log.debug("no no no.");
//				double soc2 = fNode.consProfile.computeSocInB(maxStateAfterCharge);
//			}
//
//			return minStateAfterCharge;
//		}

		return -1;

	}



	public static List<Double> computePossibleChargeableSoCs(EwLabel currFNode, ConsumptionProfile consProfile,
			BatteryType battery) {

		List<Double> chargeableSocs = new ArrayList<>();

		double maxChargableSoc =
				consProfile.cost + consProfile.outSoc; // the max SoC that pays off to charge

		//+500 means that at least 500wh is charged
		double minChargableSoc = Math.max(consProfile.inSoc,
				Math.min(currFNode.inSoC+SpeedUpSettings.minChargingAmountInWh, maxChargableSoc));


		double[] chPair;
		if (SpeedUpSettings.minChargeableSoc) {
			chargeableSocs.add(minChargableSoc);
		}

		if (minChargableSoc != maxChargableSoc) {

			if (SpeedUpSettings.alterChargeableSoc) {
				for (int i = SpeedUpSettings.minChargingLimitInPerc; i <= 100; i+=SpeedUpSettings.chargingLimitStepInPerc) {
					double anotherStateToCharge =  ((double)battery.getMinSoC()*i)/100;
					if (anotherStateToCharge > minChargableSoc && anotherStateToCharge < maxChargableSoc) {
						chargeableSocs.add(anotherStateToCharge);
					}
				}
			}

			if (SpeedUpSettings.maxChargeableSoc) {
				chargeableSocs.add(maxChargableSoc);
			}
		}
		return chargeableSocs;
	}
}
