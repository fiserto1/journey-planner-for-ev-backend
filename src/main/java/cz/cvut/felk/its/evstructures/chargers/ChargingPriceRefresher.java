/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures.chargers;

import cz.agents.basestructures.GraphStructure;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;

public enum ChargingPriceRefresher {
	INSTANCE;

	private static final long PRICE_LOAD_INTERVAL = 60000;

	private long lastTimePriceLoaded;
	private ChargingPriceFileLoader chargingPriceFileLoader;

	ChargingPriceRefresher() {
		this.lastTimePriceLoaded = System.currentTimeMillis() - PRICE_LOAD_INTERVAL;
		this.chargingPriceFileLoader = new ChargingPriceFileLoader();
	}

	public void refreshPrices(GraphStructure<EvNode, EvEdge> deserializedSuperGraph) {

		long timeDiff = System.currentTimeMillis() - lastTimePriceLoaded;

		if (timeDiff > PRICE_LOAD_INTERVAL) {
			chargingPriceFileLoader.loadPrices(deserializedSuperGraph);
			lastTimePriceLoaded = System.currentTimeMillis();
		}
	}
}
