package cz.cvut.felk.its.evstructures.chargers;

import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.evstructures.BatteryType;

import java.util.List;

/**
 * Created by Tomas on 01-Aug-16.
 */
public class ChargingFunction extends ChargingModel {

    public final double[][] supportVectors;
    //[][0] -> charging time
    //[][1] -> state After charge
    //including the start and the end of the curve

    public ChargingFunction() {
		this(BatteryType.TESLA, ChargerType.SUPERCHARGER);
        //        double[][] supportVectors = {{1,2}, {2,3}, {5,4}};
        //        ChargingFunction chFunction = new ChargingFunction(0, 4, supportVectors);
    }

    public ChargingFunction(BatteryType batteryType, ChargerType chargerType) {
        super(batteryType.getMinSoC(), batteryType.getMaxSoC());
        this.supportVectors = computeSupportVectors(batteryType, chargerType);
    }

    private double[][] computeSupportVectors(BatteryType batteryType, ChargerType chargerType) {
        List<int[]> chargingThresholds = chargerType.getChargingThresholds();

        double[][] supportVectors = new double[chargingThresholds.size()+1][2];

        supportVectors[0] = new double[]{0, batteryType.getMinSoC()};

        int i = 1;
		for (int[] chargingThreshold : chargingThresholds) {

			supportVectors[i] = new double[]{
					GraphTools.convertToE3Format(chargingThreshold[0]),
					computeSoCToCharge(chargingThreshold[1], batteryType)};
			i++;
		}

		return supportVectors;
    }

	private double computeSoCToCharge(double socInPerc, BatteryType batteryType) {
		return (socInPerc/100)*batteryType.getMaxSoC();
	}

	/*
	NOT TESTED YET
	 */
    public double getStateAfterCharge(double chargingTime) {

        int i = 0;
        while(chargingTime >= supportVectors[i][0] && i < supportVectors.length-1) {
            i++;
        }

        double timeSV2 = supportVectors[i][0];
        double socSV2= supportVectors[i][1];

        double timeSV1 = supportVectors[i-1][0];
        double socSV1= supportVectors[i-1][1];

        return ((chargingTime - timeSV1) * (socSV2 - socSV1)) / (timeSV2 - timeSV1) + socSV1;
    }

    public double getChargingTimeMillisFromMinimum(double stateOfCharge) {
//        if (incomingSoC >= ChargingModel.MAX_STATE_OF_CHARGE) return supportVectors[supportVectors.length-1][0];
        if (stateOfCharge > ChargingModel.MAX_STATE_OF_CHARGE || stateOfCharge < ChargingModel.MIN_STATE_OF_CHARGE) {
            throw new IllegalArgumentException("State of charge must be in battery range. Argument value: " + stateOfCharge);
        }

        int i = 0;
        while(i < supportVectors.length-1 && stateOfCharge >= supportVectors[i][1]) {
            i++;
        }

        double timeSV2 = supportVectors[i][0];
        double socSV2= supportVectors[i][1];

        double timeSV1 = supportVectors[i-1][0];
        double socSV1= supportVectors[i-1][1];

        return ((stateOfCharge - socSV1) * (timeSV2 - timeSV1)) / (socSV2 - socSV1) + timeSV1;
    }

    public double getChargingTimeMillis(double stateBeforeCharge, double stateAfterCharge) {
        return getChargingTimeMillisFromMinimum(stateAfterCharge) - getChargingTimeMillisFromMinimum(stateBeforeCharge);
    }


}
