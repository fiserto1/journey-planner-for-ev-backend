/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EvEdgeBetweenChargers extends EvEdge implements Serializable {

	private static final long serialVersionUID = -3321449817433346767L;

	private final List<EvEdge> paths;
//	private final List<EvEdge> reversedOrderPaths;

	public EvEdgeBetweenChargers(int fromId, int toId, RoadType roadType, int travelTimeMillis,
			ConsumptionProfile consumptionProfile, List<EvEdge> paths) {
		super(fromId, toId, 0, roadType, travelTimeMillis, consumptionProfile, null);
		this.paths = paths;
//		this.reversedOrderPaths = new ArrayList<>(paths);
//		Collections.reverse(this.reversedOrderPaths);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof EvEdgeBetweenChargers))
			return false;
		if (!super.equals(o))
			return false;

		EvEdgeBetweenChargers that = (EvEdgeBetweenChargers) o;

		return paths != null ? paths.equals(that.paths) : that.paths == null;

	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (paths != null ? paths.hashCode() : 0);
		return result;
	}

	public List<EvEdge> getPaths() {
		return paths;
	}

	public List<EvEdge> getPathsInReversedOrder() {
		List<EvEdge> reversedOrderPaths = new ArrayList<>(paths);
		Collections.reverse(reversedOrderPaths);
		return reversedOrderPaths;
	}
}
