/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures.journey;

import java.util.List;

public class JourneySegment {
	public final JourneyNode from;
	public final JourneyNode to;
//	public final int length;
	//todo dont forget, travelTimeMillis is with charging time
//	public final int travelTimeMillis;
	public final List<JourneyNode> viaNodes;

	public JourneySegment(JourneyNode from, JourneyNode to,
			List<JourneyNode> viaNodes) {
		this.from = from;
		this.to = to;
//		this.length = length;
//		this.travelTimeMillis = travelTimeMillis;
		this.viaNodes = viaNodes;
	}

	@Override
	public String toString() {
		return "JourneySegment [" +
				"from=" + from +
				", to=" + to +
				", viaNodes=" + viaNodes +
				']';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof JourneySegment))
			return false;

		JourneySegment that = (JourneySegment) o;

		if (from != null ? !from.equals(that.from) : that.from != null)
			return false;
//		if (to != null ? !to.equals(that.to) : that.to != null)
//			return false;
//		return viaNodes != null ? viaNodes.equals(that.viaNodes) : that.viaNodes == null;
		return to != null ? to.equals(that.to) : that.to == null;

	}

	@Override
	public int hashCode() {
		int result = from != null ? from.hashCode() : 0;
		result = 31 * result + (to != null ? to.hashCode() : 0);
//		result = 31 * result + (viaNodes != null ? viaNodes.hashCode() : 0);
		return result;
	}
}
