/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.evstructures;

import java.io.Serializable;

public enum RoadType implements Serializable {

	//	public static final double MOTORWAY_RECOMMENDED_SPEED = 130 / 3.6;
//	public static final double MOTORWAY_LINK_SPEED_LIMIT = 50 / 3.6;
//	public static final double OUTSIDE_URBAN_SPEED_LIMIT = 100 / 3.6;
//	public static final double PRIMARY_SPEED_LIMIT = 50 /3.6;
//	public static final double SECONDARY_SPEED_LIMIT = 70 /3.6;
//	public static final double TERTIARY_SPEED_LIMIT = 50 /3.6;
//	public static final double LINK_SPEED_LIMIT = 50 /3.6;
//	public static final double URBAN_SPEED_LIMIT = 50 /3.6;
//	public static final double PARKING_SPEED_LIMIT = 200 /3.6; //10 // TODO: 20-Sep-16 this is for testing on long parking edges in my-graph.ser - assign small edge travel time



	MOTORWAY(100 / 3.6), MOTORWAY_LINK(40 / 3.6), TRUNK(70 / 3.6), TRUNK_LINK(40 /3.6), PRIMARY(60 /3.6),
	PRIMARY_LINK(40 /3.6), SECONDARY(60 /3.6), SECONDARY_LINK(40 /3.6),
	TERTIARY(50 /3.6), TERTIARY_LINK(40 /3.6), SUPERCHARGER_LINK_IN(10 /3.6), SUPERCHARGER_LINK_OUT(10 /3.6),
	UNKNOWN(Double.MAX_VALUE), FIRST_MILE(Double.MAX_VALUE), LAST_MILE(Double.MAX_VALUE),
	PRECOMPUTED(Double.MAX_VALUE); //precomputed = between superchargers


	private final double speedLimitMs;
	private static final long serialVersionUID = -6395796523932892312L;


	RoadType(double speedLimitMs) {
		this.speedLimitMs = speedLimitMs;
	}

	public double getSpeedLimitMs() {
		return speedLimitMs;

//		//TODO get another tags to set appropriate speedlimit
//		if (elementTags.containsKey("supercharger")) {
//			String highwayVal = elementTags.get("supercharger");
//			switch (highwayVal) {
//			case "link_in":
//				return PARKING_SPEED_LIMIT;
//			case "link_out":
//				return PARKING_SPEED_LIMIT;
//			}
//		} else if (elementTags.containsKey("highway")) {
//			String highwayVal = elementTags.get("highway");
//			switch (highwayVal) {
//			case "motorway":
//				return MOTORWAY_RECOMMENDED_SPEED;
//			case "motorway_link":
//				return MOTORWAY_LINK_SPEED_LIMIT;
//			case "trunk":
//				return OUTSIDE_URBAN_SPEED_LIMIT;
//			case "primary":
//				return PRIMARY_SPEED_LIMIT;
//			case "secondary":
//				return SECONDARY_SPEED_LIMIT;
//			case "tertiary":
//				return TERTIARY_SPEED_LIMIT;
//			default:
//				//trunk, primary, secondary, tertiary links
//				return LINK_SPEED_LIMIT;
//			}
//		}
//		return SECONDARY_SPEED_LIMIT;
	}
}
