package cz.cvut.felk.its.evstructures;

import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.Node;
import cz.cvut.felk.its.evstructures.chargers.Charger;

import java.io.Serializable;

/**
 * Created by Tomas on 18.11.2015.
 */
public class EvNode extends Node implements Serializable{

    private static final long serialVersionUID = -473098667841137571L;
    private Charger charger;

    public EvNode(int id, long osmId, GPSLocation location, Charger charger) {
        this(id, osmId, location.latE6, location.lonE6, location.latProjected, location.lonProjected,
                location.elevation, charger);
    }

    public EvNode(int id, long osmId, GPSLocation location) {
        this(id, osmId, location.latE6, location.lonE6, location.latProjected, location.lonProjected, location.elevation);
    }

    public EvNode(int id, long osmId, int latE6, int lonE6, int latProjected, int lonProjected, int elevation) {
        this(id, osmId, latE6, lonE6, latProjected, lonProjected, elevation, null);
    }
    public EvNode(int id, long osmId, double lat, double lon, int latProjected, int lonProjected, int elevation) {
        super(id, osmId, lat, lon, latProjected, lonProjected, elevation);
        this.charger = null;
    }

    public EvNode(int id, long osmId, int latE6, int lonE6, int latProjected, int lonProjected,
                  int elevation, Charger charger) {
        super(id, osmId, latE6, lonE6, latProjected, lonProjected, elevation);
        this.charger = charger;
    }

    //for testing
    public EvNode(int nodeId) {
        super(nodeId, Long.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    public Charger getCharger() {
        return charger;
    }

    public boolean isCharger() {
        return charger != null;
    }

    public void setCharger(Charger charger) {
        this.charger = charger;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof EvNode))
            return false;
        if (!super.equals(o))
            return false;

        EvNode evNode = (EvNode) o;

        return charger != null ? charger.equals(evNode.charger) : evNode.charger == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (charger != null ? charger.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EvNode{" +
                "charger=" + charger +
                "} " + super.toString();
    }
}
