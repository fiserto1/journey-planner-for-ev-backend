package cz.cvut.felk.its.evstructures;

import cz.agents.basestructures.GPSLocation;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Tomas on 08-Mar-16.
 */
public class EvEdge extends PlanningEdge implements Serializable {

	private static final long serialVersionUID = -6395590923932012312L;

	public final int travelTimeMillis;
    public final ConsumptionProfile consumptionProfile;
    public final RoadType roadType;
    public final List<GPSLocation> viaNodes; // for simplified road edges only (without fromId and toId)

    /**
     * @param fromId
     * @param toId
     * @param length     in meters
     * @param roadType
     * @param travelTimeMillis in seconds
     * @param consumptionProfile
     * @param viaNodes
     */
    public EvEdge(int fromId, int toId, int length, RoadType roadType, int travelTimeMillis,
            ConsumptionProfile consumptionProfile, List<GPSLocation> viaNodes) {
        super(fromId, toId, length);
        this.roadType = roadType;
        this.travelTimeMillis = travelTimeMillis;
        this.consumptionProfile = consumptionProfile;
        this.viaNodes = viaNodes;
    }

    public EvEdge(int fromId, int toId, int length, RoadType roadType, int travelTimeMillis,
            List<GPSLocation> viaNodes) {
        this(fromId, toId, length, roadType, travelTimeMillis, null, viaNodes);
    }

    public EvEdge(int fromId, int toId, EvEdge oldEdge) {
        this(fromId, toId, oldEdge.length, oldEdge.roadType, oldEdge.travelTimeMillis,
                oldEdge.consumptionProfile, oldEdge.viaNodes);
    }

    public EvEdge(int fromId, int toId, int length, RoadType roadType, int travelTimeMillis, ConsumptionProfile consumptionProfile) {
        this(fromId, toId, length, roadType, travelTimeMillis, consumptionProfile, null);
    }

    public EvEdge(int fromId, int toId, int length, RoadType roadType, int travelTimeMillis) {
        this(fromId, toId, length, roadType, travelTimeMillis, null, null);
    }

    public EvEdge(int fromId, int toId, int length, int travelTimeMillis, ConsumptionProfile consumptionProfile) {
        this(fromId, toId, length, RoadType.PRIMARY, travelTimeMillis, consumptionProfile);
    }

    @Deprecated
    public int consFunction(int soCInFromNode){
        //TODO implement [EvEdge:consFunction]
        throw new UnsupportedOperationException ("[EvEdge:consFunction] has not been implemented yet");
//        return GraphTools.computeNextPartConsumption(soCInFromNode, consumptionProfile.costs);
//        int condition = soCInFromNode - consumption;
//        if (condition < ChargingModel.MIN_STATE_OF_CHARGE) {
//            return Integer.MAX_VALUE;
//        } else if (condition > ChargingModel.MAX_STATE_OF_CHARGE) {
//            return soCInFromNode - ChargingModel.MAX_STATE_OF_CHARGE;
//        } else {
//            //in charging interval
//            return consumption;
//        }
    }

    @Deprecated
    public int getNextSOC(int soCInFromNode){
        int consWeight = consFunction(soCInFromNode);
        if (consWeight != Integer.MAX_VALUE) {
            return soCInFromNode - consWeight;
        }
        return Integer.MAX_VALUE;
    }

    @Deprecated
    public int getPrevSOC(int toNodeSOCIn, int pathMinSoC){
        int consWeight = backwardConsFunction(toNodeSOCIn, pathMinSoC);
        if (consWeight != Integer.MAX_VALUE) {
            return toNodeSOCIn + consWeight;
        }
        return Integer.MAX_VALUE;
    }

    @Deprecated
    public int backwardConsFunction(int toNodeSoCIn, int pathMinSoC){
        int condition = (toNodeSoCIn + consumptionProfile.cost) - pathMinSoC;
        //if condition > 0 &&
        if (condition > (ChargingModel.MAX_STATE_OF_CHARGE-ChargingModel.MIN_STATE_OF_CHARGE)) {
            return Integer.MAX_VALUE;
        } else {
            return consumptionProfile.cost;
        }
    }

    @Override
    public int getWeight() {
        return travelTimeMillis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof EvEdge))
            return false;
        if (!super.equals(o))
            return false;

        EvEdge evEdge = (EvEdge) o;

        if (travelTimeMillis != evEdge.travelTimeMillis)
            return false;
        if (consumptionProfile != null ?
                !consumptionProfile.equals(evEdge.consumptionProfile) :
                evEdge.consumptionProfile != null)
            return false;
        if (roadType != evEdge.roadType)
            return false;
        return viaNodes != null ? viaNodes.equals(evEdge.viaNodes) : evEdge.viaNodes == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + travelTimeMillis;
        result = 31 * result + (consumptionProfile != null ? consumptionProfile.hashCode() : 0);
        result = 31 * result + (roadType != null ? roadType.hashCode() : 0);
        result = 31 * result + (viaNodes != null ? viaNodes.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EvEdge [" +
                "[" + super.toString() + "], " +
                "travelTimeMillis=" + travelTimeMillis +
                ", consumptionProfile=" + consumptionProfile +
                ", roadType=" + roadType +
                ", viaNodes=" + viaNodes +
                ']';
    }
}
