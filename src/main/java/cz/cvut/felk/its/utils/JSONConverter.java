package cz.cvut.felk.its.utils;

import cz.agents.basestructures.*;
import cz.cvut.felk.its.evstructures.*;
import cz.cvut.felk.its.evstructures.graphs.ExtendedGraph;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Tomas on 19.11.2015.
 */
public class JSONConverter {

    public <TNode extends Node, TEdge extends PlanningEdge> void convertNodeIdsToJSON(
            String filepath, List<Integer> routeNodeIds, Graph<TNode, TEdge> graph) {

        JSONObject featureCollection = new JSONObject();
        featureCollection.put("type", "FeatureCollection");
        JSONArray featureList = new JSONArray();

        featureList.put(convertEdgeToFeature(null, graph, false, routeNodeIds));
        featureCollection.put("features", featureList);

        try (FileWriter wr = new FileWriter(filepath)) {
            wr.write(featureCollection.toString());
			System.out.println("Nodes were parsed to geojson: " + filepath);
		} catch (IOException e) {
            e.printStackTrace();
        }
    }

    public <TNode extends Node, TEdge extends PlanningEdge> void convertGraphEdgesToJSON(
            String fileName, GraphStructure<TNode, TEdge> graph, boolean withSteps) {
        convertEdgesToJSON(fileName, graph, graph.getAllEdges(), withSteps);
    }

    public <TNode extends Node, TEdge extends PlanningEdge> void convertEdgesToJSON(
            String filepath, GraphStructure<TNode, TEdge> graph, Collection<TEdge> edges, boolean withSteps) {

        /*
        withSteps means if it is edge with field viaNodes
         */
        JSONObject featureCollection = new JSONObject();
        featureCollection.put("type", "FeatureCollection");
        JSONArray featureList = new JSONArray();
        for (TEdge edge : edges) {
            featureList.put(convertEdgeToFeature(edge, graph, withSteps, null));
        }
        featureCollection.put("features", featureList);

        try (FileWriter wr = new FileWriter(filepath)) {
            wr.write(featureCollection.toString());
			System.out.println("Edges were parsed to geojson: " + filepath);
		} catch (IOException e) {
            e.printStackTrace();
        }
    }





    /**
     *
     * @param edge
     * @param graph
     * @param withSteps means if it is Edge with field viaNodes
     * @param ownEdge list of nodeIds
     * @param <TNode>
     * @param <TEdge>
     * @return
     */
    private <TNode extends Node, TEdge extends PlanningEdge> JSONObject convertEdgeToFeature(
            TEdge edge, GraphStructure<TNode, TEdge> graph, boolean withSteps, List<Integer> ownEdge) {
        JSONObject feature = new JSONObject();
        feature.put("type", "Feature");
        JSONObject lineString = new JSONObject();
        lineString.put("type", "LineString");
        JSONObject properties = new JSONObject();
        JSONArray coord = new JSONArray();
        if (ownEdge != null) {
            for (int nodeId: ownEdge) {
                coord.put(convertLocationToCoordArray(graph.getNode(nodeId)));
            }
        } else if (withSteps && (edge instanceof EvEdge)) {
            EvEdge evEdge = (EvEdge) edge;
            coord.put(convertLocationToCoordArray(graph.getNode(edge.fromId)));
            if (evEdge.viaNodes != null) {
                for (GPSLocation loc: evEdge.viaNodes) {
                    coord.put(convertLocationToCoordArray(loc));
                }
            }
            coord.put(convertLocationToCoordArray(graph.getNode(edge.toId)));

        } else {
            coord.put(convertLocationToCoordArray(graph.getNode(edge.fromId)));
            coord.put(convertLocationToCoordArray(graph.getNode(edge.toId)));
        }
        lineString.put("coordinates", coord);
        feature.put("geometry", lineString);
//        properties.put("from_id", edge.fromId);
//        properties.put("to_id", edge.toId);
//        properties.put("length_m", edge.length);
        if (edge instanceof EvEdge) {
            EvEdge evEdge = (EvEdge) edge;
//            properties.put("edgeId", evEdge.edgeId);
            properties.put("from", evEdge.fromId);
            properties.put("to", evEdge.toId);
//            properties.put("consumption", evEdge.consumptionProfile);
            properties.put("travelTime_s", evEdge.travelTimeMillis);
            properties.put("length", evEdge.length);
            properties.put("roadType", evEdge.roadType);

//            if (edge instanceof EvEdgeBetweenChargers) {
//                properties.put("type", "edge_between_chargers");
//            } else {
//                properties.put("type", "road_edge");
//            }
			ConsumptionProfile cProfile = evEdge.consumptionProfile;
            if (cProfile != null) {
                properties.put("CP-inSoc", cProfile.inSoc);
                properties.put("CP-outSoc", cProfile.outSoc);
                properties.put("CP-costs", cProfile.cost);
            }
		} else {
            properties.put("type", "supercharger_edge");
        }

        feature.put("properties", properties);
        return feature;
    }

    private <TLoc extends GPSLocation> JSONArray convertLocationToCoordArray(TLoc location) {
        if (location==null) return null;
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        //swapped latE6 lonE6 -> lonE6 latE6
        return new JSONArray("[" + lon + "," + lat + "]");
    }

    public <TLoc extends GPSLocation> void convertLocationsToJSON(String filepath, Collection<TLoc> locations)
            throws IOException {
        JSONObject featureCollection = new JSONObject();
        featureCollection.put("type", "FeatureCollection");
        JSONArray featureList = new JSONArray();
        for (TLoc node: locations) {
            if (node == null) continue;
            featureList.put(convertLocationToFeature(node));
        }

        featureCollection.put("features", featureList);

        try (FileWriter wr = new FileWriter(filepath)) {
            wr.write(featureCollection.toString());
			System.out.println("Locations were parsed to geojson: " + filepath);
		} catch (IOException e) {
            e.printStackTrace();
        }
    }

    public <TNode extends Node, TEdge extends Edge> void convertNodesToJSON(String filepath, GraphStructure<TNode, TEdge> graph)
            throws IOException {
        convertLocationsToJSON(filepath, graph.getAllNodes());
    }

    private <TLoc extends GPSLocation> JSONObject convertLocationToFeature(TLoc loc) {
        JSONObject feature = new JSONObject();
        JSONObject point = new JSONObject();
        JSONObject properties = new JSONObject();
        point.put("type", "Point");
        JSONArray coord = convertLocationToCoordArray(loc);
        point.put("coordinates", coord);
        feature.put("type", "Feature");
        feature.put("geometry", point);
        if (loc instanceof Node) {
            Node node = (Node) loc;
            properties.put("node_id", node.id);
            properties.put("osm_id", node.sourceId);
            if (node instanceof EvNode ) {
                EvNode evNode = (EvNode) node;
                if (evNode.isCharger()) properties.put("supercharger", "yes");
            }
        }

        feature.put("properties", properties);
        return feature;
    }

    public <TNode extends Node, TEdge extends Edge> void convertLocationsToJSON(String filepath, Graph<TNode, TEdge> graph)
            throws IOException {
        //add Nodes
        Collection<GPSLocation> locations = new HashSet<>(graph.getAllNodes());

        //add Locations between nodes
        graph.getAllEdges().stream().filter(edge -> edge instanceof EvEdge).forEach(edge -> {
            EvEdge evEdge = (EvEdge) edge;
            if (evEdge.viaNodes != null) {
                locations.addAll(evEdge.viaNodes);
            }
        });

        convertLocationsToJSON(filepath, locations);
    }

	public void convertFMLMGraphEdgesToJSON(String filepath, ExtendedGraph<EvNode, EvEdge> graphFMLM) {

		Set<EvEdge> edgesBetweenChargers = graphFMLM.getAllEdges().stream().filter(e -> e instanceof EvEdgeBetweenChargers)
				.collect(Collectors.toSet());

		Collection<EvEdge> fastestEdges = new HashSet<>();

		for (EvEdge edge : edgesBetweenChargers) {
			EvEdgeBetweenChargers edgeBetweenChargers = (EvEdgeBetweenChargers) edge;
			EvEdge evEdge = edgeBetweenChargers.getPaths().get(0);
			fastestEdges.add(evEdge);
		}
		convertEdgesToJSON(filepath, graphFMLM, fastestEdges, true);

	}
}
