/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.utils;

import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.GraphStructure;
import cz.agents.basestructures.Node;
import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.evstructures.*;
import cz.cvut.felk.its.evstructures.graphs.ExtendedGraph;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.zonebuilder.tns.util.TnsGraphUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GraphExtender {

	public static int START_ID = -1;
	public static int GOAL_ID = -2;

//	public static double NN_MAX_DISTANCE = 2000;
	public static double NN_MAX_DISTANCE = Double.MAX_VALUE;

	public static ExtendedGraph<EvNode, EvEdge> buildExtendedGraph(EVZone<EvNode, EvEdge> zone,
			GPSLocation originLoc, GPSLocation destinationLoc) {

		KDTreeProvider kdTreeProvider = KDTreeProvider.INSTANCE;

		List<GPSLocation> originNNs = kdTreeProvider
				.getNNearestNodesWithMaxDistance(originLoc, NN_MAX_DISTANCE);
		List<GPSLocation> destinationNNs = kdTreeProvider
				.getNNearestNodesWithMaxDistance(destinationLoc, NN_MAX_DISTANCE);

		if (originNNs == null || destinationNNs == null) return null;

		EvNode startNode = new EvNode(START_ID, START_ID, originLoc);
		EvNode goalNode = new EvNode(GOAL_ID, GOAL_ID, destinationLoc);

		//start edges from start to nearest nodes
		Map<Integer, EvEdge> startEdges = buildStartEdges(startNode, originNNs, zone);

		//goal edges from nearest nodes to goal
		Map<Integer, EvEdge> goalEdges = buildGoalEdges(goalNode, destinationNNs, zone);

		return new ExtendedGraph<>(zone.getGraph(), startNode, goalNode, startEdges, goalEdges);
	}

	private static EvEdge buildEvEdge(Node fromNode, Node toNode, List<GPSLocation> viaNodes) {
		double length = GPSLocationTools.computeDistanceAsDouble(fromNode, toNode);
		double travelTimeMillis = GraphTools.convertToE3Format(length/ TnsGraphUtils.URBAN_SPEED_LIMIT);
		ConsumptionProfile consumptionProfile =
				ConsumptionProfileUtils.computeConsumptionProfileBetweenTwoLocs(fromNode, toNode);

		if (consumptionProfile == null) {
			return null;
		}

		// TODO: 11-May-16 compute RoadType
		return new EvEdge(fromNode.id, toNode.id, (int)length, RoadType.UNKNOWN, (int)travelTimeMillis, consumptionProfile,
				viaNodes);
	}

	private static HashMap<Integer, EvEdge> buildStartEdges(EvNode startNode, List<GPSLocation> origins,
			EVZone<EvNode, EvEdge> zone) {

		Map<GPSLocation, Collection<EvEdge>> mappedViaNodes = zone.getViaNodes();
		GraphStructure<EvNode, EvEdge> graph = zone.getGraph();
		HashMap<Integer, EvEdge> startEdges = new HashMap<>();
		EvNode toNode;
		Collection<EvEdge> evEdges;
		EvEdge newEvEdge;
		List<GPSLocation> viaNodes;

		// TODO: 28-Oct-16 check if iterate from the nearest to the least nearest node
		//if we iterate from the nearest to the least nearest node then keep the first added edge only!
		for (GPSLocation nearestNode: origins) {

			evEdges = mappedViaNodes.get(nearestNode);

			if(evEdges == null) {
				//NNearest node is EvNode
				toNode = (EvNode) nearestNode;
				newEvEdge = buildEvEdge(startNode, toNode, null);
				if (newEvEdge != null && !startEdges.containsKey(newEvEdge.toId)) {
					startEdges.put(newEvEdge.toId, newEvEdge);
				}
			} else {
				//NNearest node is viaNode
				for (EvEdge evEdge : evEdges) {
					viaNodes = evEdge.viaNodes;
					int nnIndex = viaNodes.indexOf(nearestNode);
					viaNodes = viaNodes.subList(nnIndex, viaNodes.size());
					toNode = graph.getNode(evEdge.toId);
					newEvEdge = buildEvEdge(startNode, toNode, viaNodes);
					if (newEvEdge != null && !startEdges.containsKey(newEvEdge.toId)) {
						startEdges.put(newEvEdge.toId, newEvEdge);
					}
				}
			}
		}

		return startEdges;
	}

	private static HashMap<Integer, EvEdge> buildGoalEdges(EvNode goalNode, List<GPSLocation> destinations,
			EVZone<EvNode, EvEdge> zone) {

		Map<GPSLocation, Collection<EvEdge>> mappedViaNodes = zone.getViaNodes();
		GraphStructure<EvNode, EvEdge> graph = zone.getGraph();
		HashMap<Integer, EvEdge> goalEdges = new HashMap<>();
		EvNode fromNode;
		Collection<EvEdge> evEdges;
		EvEdge newEvEdge;
		List<GPSLocation> viaNodes;

		// TODO: 28-Oct-16 check if iterate from the nearest to the least nearest node
		//if we iterate from the nearest to the least nearest node then keep the first added edge only!
		for (GPSLocation nearestNode: destinations) {

			evEdges = mappedViaNodes.get(nearestNode);

			if(evEdges == null) {
				//NNearest node is EvNode
				fromNode = (EvNode) nearestNode;
				newEvEdge = buildEvEdge(fromNode, goalNode, null);////zde
				if (newEvEdge != null && !goalEdges.containsKey(newEvEdge.fromId)) {////zde
					goalEdges.put(newEvEdge.fromId, newEvEdge);////zde
				}
			} else {
				//NNearest node is viaNode
				for (EvEdge evEdge : evEdges) {
					viaNodes = evEdge.viaNodes;
					int nnIndex = viaNodes.indexOf(nearestNode);
					viaNodes = viaNodes.subList(0, nnIndex);///zde
					fromNode = graph.getNode(evEdge.fromId);///zde
					newEvEdge = buildEvEdge(fromNode, goalNode, viaNodes);///zde
					if (newEvEdge != null && !goalEdges.containsKey(newEvEdge.fromId)) {///zde
						goalEdges.put(newEvEdge.fromId, newEvEdge);///zde
					}
				}
			}
		}

		return goalEdges;
	}

}
