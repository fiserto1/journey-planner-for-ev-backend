/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.utils;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Stream;

public class TextFilesWorker {

	public static Stream<String[]> loadCSVLinesFromFile(String textFileName, String folder, String delimiter) {

		try {
			return loadLinesFromFile(textFileName, folder).map(l -> l.split(delimiter));
		} catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Stream<String> loadLinesFromFile(String textFileName, String folder) {

		Path file = Paths.get(folder + "/" + textFileName);

		try {
			return Files.lines(file, Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Map<String, Stream<String>> loadLinesFromAllFiles(String folder, String sufix) {

		Map<String, Stream<String>> streamByName = new HashMap<>();

		File file = new File(folder);
		for (File f : file.listFiles()) {

			if (f.getName().endsWith(sufix)) {
				streamByName.put(f.getName(), loadLinesFromFile(f.getName(), folder));
			}
		}

		return streamByName;
	}

	public static Stream<String[]> loadCSVLinesFromFileInResources(String textFileName, String folder,
			String delimiter) {

		try {
			return loadLinesFromFileInResources(textFileName, folder).map(l -> l.split(delimiter));
		} catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Properties loadPropertiesFromResources(String textFileName, String folder) throws IOException {

		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		InputStream resourceAsStream = contextClassLoader.getResourceAsStream(folder + "/" + textFileName);
		Properties properties = new Properties();
		properties.load(resourceAsStream);

		resourceAsStream.close();
		return properties;
	}

	public static Stream<String> loadLinesFromFileInResources(String textFileName, String folder) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
//		ClassLoader contextClassLoader = TextFilesWorker.class.getClassLoader();
		InputStream inputStream = contextClassLoader.getResourceAsStream(folder + "/" + textFileName);
		return toStringStream(inputStream);

	}

	public static Map<String, InputStream> loadLinesFromAllFilesInResources(String folder, String suffix) {
		return new TextFilesWorker().loadLinesFromAllFilesInResourcesNonSt(folder, suffix);
	}

	public Map<String, InputStream> loadLinesFromAllFilesInResourcesNonSt(String folder, String suffix) {

		Map<String, InputStream> streamByName = new HashMap<>();
		//		String innerFolder = folder + "/";

		//		final Collection<InputStream> streams = new ArrayList<>();
		// final File jarFile = new
		// File(CycleZoneProvider.class.getProtectionDomain()
		// .getCodeSource().getLocation().getPath());

		// Loads resource directory
		//		final File jarFile = new File(TextFilesWorker.class.getResource(innerFolder).getPath());
		//		System.out.println(jarFile);
		//		System.out.println(jarFile.exists());
		//		System.out.println(jarFile.isFile());
		//		System.out.println(jarFile.isDirectory());
		//		System.out.println(jarFile.getName());
		//
		//		final File jarFile2 = new File(TextFilesWorker.class.getResource("/").getPath());
		//		System.out.println(jarFile2);
		//		System.out.println(jarFile2.exists());
		//		System.out.println(jarFile2.isFile());
		//		System.out.println(jarFile2.isDirectory());
		//		System.out.println(jarFile2.getName());

		final File jarFile = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());

		try {

			if (jarFile.isFile()) {  // Run with JAR file
				final JarFile jar = new JarFile(jarFile);
				final Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
				while (entries.hasMoreElements()) {
					final String name = entries.nextElement().getName();
					if (name.startsWith(folder)) { //filter according to the path
						if (name.matches(".*." + suffix)) {
							String fileName = name.substring(name.lastIndexOf('/') + 1);
							streamByName.put(fileName,
									getClass().getClassLoader().getResourceAsStream(name));
						}

					}
				}
				jar.close();
			} else { // Run with IDE
				final URL url = TextFilesWorker.class.getResource("/" + folder);
				if (url != null) {
					try {
						final File apps = new File(url.toURI());
						for (File app : apps.listFiles()) {
							if (app.getName().matches(".*." + suffix)) {
								streamByName.put(app.getName(), new FileInputStream(app));
							}
						}
					} catch (URISyntaxException ex) {
						// never happens
					}
				}
			}
			//
			//
			//
			//			if (jarFile2.isFile()) { // Run with JAR file
			//				final JarFile jar;
			//
			//				jar = new JarFile(jarFile2);
			//				// gives ALL entries in jar
			//				final Enumeration<JarEntry> entries = jar.entries();
			//
			//				while (entries.hasMoreElements()) {
			//					final String name = entries.nextElement().getName();
			//
			//					// filter only serialized city data (files with suffix ser)
			//					if (name.matches(".*." + suffix)) {
			//						//					log.info("Loading zone file: " + name);
			//						streamByName.put(name,
			//								toStringStream(TextFilesWorker.class.getResourceAsStream(innerFolder + name)));
			//					}
			//				}
			//
			//				jar.close();
			//
			//			} else { // Run in IDE
			//				for (File file : jarFile2.listFiles()) {
			//					// filter only serialized city data (files with suffix ser)
			//					if (file.getName().matches(".*." + suffix)) {
			//						//					log.info("Loading zone file: " + file.getName());
			//						streamByName.put(file.getName(), toStringStream(
			//								TextFilesWorker.class.getResourceAsStream(innerFolder + file.getName())));
			//					}
			//
			//				}
			//
			//			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return streamByName;
	}

	public static Map<String, Stream<String>> loadLinesFromAllFilesInResourcesToStream(String folder, String suffix) {
		return new TextFilesWorker().loadLinesFromAllFilesInResourcesToStreamNonSta(folder, suffix);
	}

	public Map<String, Stream<String>> loadLinesFromAllFilesInResourcesToStreamNonSta(String folder, String suffix) {

		Map<String, Stream<String>> streamByName = new HashMap<>();
		//		String innerFolder = folder + "/";

		//		final Collection<InputStream> streams = new ArrayList<>();
		// final File jarFile = new
		// File(CycleZoneProvider.class.getProtectionDomain()
		// .getCodeSource().getLocation().getPath());

		// Loads resource directory
		//		final File jarFile = new File(TextFilesWorker.class.getResource(innerFolder).getPath());
		//		System.out.println(jarFile);
		//		System.out.println(jarFile.exists());
		//		System.out.println(jarFile.isFile());
		//		System.out.println(jarFile.isDirectory());
		//		System.out.println(jarFile.getName());
		//
		//		final File jarFile2 = new File(TextFilesWorker.class.getResource("/").getPath());
		//		System.out.println(jarFile2);
		//		System.out.println(jarFile2.exists());
		//		System.out.println(jarFile2.isFile());
		//		System.out.println(jarFile2.isDirectory());
		//		System.out.println(jarFile2.getName());

		final File jarFile = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());

		try {

			if (jarFile.isFile()) {  // Run with JAR file
				final JarFile jar = new JarFile(jarFile);
				final Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
				while (entries.hasMoreElements()) {
					final String name = entries.nextElement().getName();
					if (name.startsWith(folder)) { //filter according to the path
						if (name.matches(".*." + suffix)) {
							String fileName = name.substring(name.lastIndexOf('/') + 1);
							streamByName.put(fileName,
									toStringStream(getClass().getClassLoader().getResourceAsStream(name)));
						}

					}
				}
				jar.close();
			} else { // Run with IDE
				final URL url = TextFilesWorker.class.getResource("/" + folder);
				if (url != null) {
					try {
						final File apps = new File(url.toURI());
						for (File app : apps.listFiles()) {
							if (app.getName().matches(".*." + suffix)) {
								streamByName.put(app.getName(), toStringStream(new FileInputStream(app)));
							}
						}
					} catch (URISyntaxException ex) {
						// never happens
					}
				}
			}
			//
			//
			//
			//			if (jarFile2.isFile()) { // Run with JAR file
			//				final JarFile jar;
			//
			//				jar = new JarFile(jarFile2);
			//				// gives ALL entries in jar
			//				final Enumeration<JarEntry> entries = jar.entries();
			//
			//				while (entries.hasMoreElements()) {
			//					final String name = entries.nextElement().getName();
			//
			//					// filter only serialized city data (files with suffix ser)
			//					if (name.matches(".*." + suffix)) {
			//						//					log.info("Loading zone file: " + name);
			//						streamByName.put(name,
			//								toStringStream(TextFilesWorker.class.getResourceAsStream(innerFolder + name)));
			//					}
			//				}
			//
			//				jar.close();
			//
			//			} else { // Run in IDE
			//				for (File file : jarFile2.listFiles()) {
			//					// filter only serialized city data (files with suffix ser)
			//					if (file.getName().matches(".*." + suffix)) {
			//						//					log.info("Loading zone file: " + file.getName());
			//						streamByName.put(file.getName(), toStringStream(
			//								TextFilesWorker.class.getResourceAsStream(innerFolder + file.getName())));
			//					}
			//
			//				}
			//
			//			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return streamByName;
	}



	private static Stream<String> toStringStream(InputStream is) {
		return new BufferedReader(new InputStreamReader(is)).lines();
	}

}
