package cz.cvut.felk.its.utils;

import cz.agents.basestructures.Graph;
import cz.cvut.felk.its.exceptions.GraphNotFoundException;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.PlanningEdge;
import org.apache.log4j.Logger;

import java.io.*;

/**
 * Created by Tomas on 06.12.2015.
 */
public class SerializeUtil {

    private static final Logger log = Logger.getLogger(SerializeUtil.class);

    @SuppressWarnings("unchecked")
    public static Graph<EvNode, PlanningEdge> deserializeGraph(String filename) {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))) {
            return (Graph<EvNode, PlanningEdge>) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static Graph<EvNode, EvEdge> deserializeGraphFromResources(String graphFilename)
            throws GraphNotFoundException {

        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try (
				InputStream resourceAsStream = contextClassLoader.getResourceAsStream(graphFilename);
				ObjectInputStream in = new ObjectInputStream(resourceAsStream)) {
            return (Graph<EvNode, EvEdge>) in.readObject();
//            return (Graph<EvNode, EvEdge>) in.readUnshared();
        } catch (IOException | ClassNotFoundException e) {
            log.error("File with deserialized graph not found: " + graphFilename);
            throw new GraphNotFoundException(graphFilename);
        }
    }

    public static Graph<EvNode, EvEdge> deserializeEvGraph(String filename) {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))) {
            return (Graph<EvNode, EvEdge>) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean serializeGraph(Graph<EvNode, PlanningEdge> graph, String filename) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))){
            out.writeObject(graph);
            System.out.println("Graph was serialized to " + filename);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static <O extends Object> boolean serializeObject(O obj, String filename) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))){
            out.writeObject(obj);
//            out.writeUnshared(obj);
            System.out.println("Object serialized to " + filename);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static <O> O deserializeObject(String filename, O clazz) {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))) {
            return (O) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int[] deserializePotentials(String filename) {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))) {
            return (int[]) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
