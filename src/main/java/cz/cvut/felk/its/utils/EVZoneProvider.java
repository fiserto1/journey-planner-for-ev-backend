/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.utils;

import cz.agents.basestructures.BoundingBox;
import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.Graph;
import cz.agents.basestructures.GraphStructure;
import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.evstructures.chargers.ChargingPriceRefresher;
import cz.cvut.felk.its.exceptions.GraphNotFoundException;
import cz.cvut.felk.its.exceptions.ZoneNotFoundException;
import cz.cvut.felk.its.evstructures.EVZone;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvEdgeBetweenChargers;
import cz.cvut.felk.its.evstructures.EvNode;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.*;

public enum EVZoneProvider {
	INSTANCE;

	private static final int MIN_DIST_BETWEEN_VIANODES = 200;

	private static final String ZONE_FILENAME = "ge.properties";
	private static final String ZONES_PROPERTIES_FOLDER_NAME = "zones";
	private final Logger log = Logger.getLogger(EVZoneProvider.class);

	private EVZone<EvNode, EvEdge> zone;

	EVZoneProvider() {
		log.info("Loading zone started.");

		try {
			this.zone = loadZoneFromPropertiesFile(ZONE_FILENAME, ZONES_PROPERTIES_FOLDER_NAME);
			log.info("Zone loaded successfully: " + zone);
		} catch (GraphNotFoundException | ZoneNotFoundException e) {
			e.printStackTrace();
		}

	}

	private EVZone<EvNode, EvEdge> loadZoneFromPropertiesFile(String zoneFilename, String zonesPropertiesFolderName) throws ZoneNotFoundException,
			GraphNotFoundException {

		Properties properties;
		try {
			properties = TextFilesWorker.loadPropertiesFromResources(zoneFilename, zonesPropertiesFolderName);
		} catch (IOException e) {
			log.error("File with properties not found: " + zoneFilename);
			throw new ZoneNotFoundException(zoneFilename);
		}

		String zoneName = properties.getProperty("name");
		String graphFilename = properties.getProperty("graph");
		int srid = Integer.parseInt(properties.getProperty("srid"));
		int minLatE6 = (int) (Double.parseDouble(properties.getProperty("minLat"))*1E6);
		int minLonE6 = (int) (Double.parseDouble(properties.getProperty("minLon"))*1E6);
		int maxLatE6 = (int) (Double.parseDouble(properties.getProperty("maxLat"))*1E6);
		int maxLonE6 = (int) (Double.parseDouble(properties.getProperty("maxLon"))*1E6);

		Graph<EvNode, EvEdge> deserializedGraph = SerializeUtil
				.deserializeGraphFromResources(graphFilename);

		BoundingBox bBox = new BoundingBox(minLonE6, minLatE6, maxLonE6, maxLatE6);

//		Map<GPSLocation, Collection<EvEdge>> viaNodes = mapEdgesOnViaNodes(deserializedGraph);
		Map<GPSLocation, Collection<EvEdge>> viaNodes = mapEdgesOnViaNodesReduced(deserializedGraph);
		countViaNodesWithMappedEdges(viaNodes);

//		viaNodes = filterViaNodes(viaNodes, deserializedGraph);

		int numOfChargers = (int) deserializedGraph.getAllNodes().stream().filter(EvNode::isCharger).count();

		return new EVZone<>(zoneName, srid, deserializedGraph, bBox, viaNodes, numOfChargers);
	}

	private void countViaNodesWithMappedEdges(Map<GPSLocation, Collection<EvEdge>> viaNodes) {

		int moreEdgesCounter = 0, oneEdgeCounter = 0, twoEdgeCounter = 0;

		for (Collection<EvEdge> evEdges : viaNodes.values()) {
			if (evEdges.size() == 1) {
				oneEdgeCounter++;
			} else if (evEdges.size() == 2) {
				twoEdgeCounter++;
			} else {
				moreEdgesCounter++;
			}
		}
		log.info("more Edges: " + moreEdgesCounter + " two edge: " + twoEdgeCounter + " one edge: " + oneEdgeCounter);
	}

	private Map<GPSLocation, Collection<EvEdge>> filterViaNodes(Map<GPSLocation, Collection<EvEdge>> viaNodes,
			Graph<EvNode, EvEdge> graph) {
		// TODO: 09-Dec-16 filter by 1km

		Set<GPSLocation> viaNodesToKeep = new HashSet<>();

		for (Collection<EvEdge> evEdges : viaNodes.values()) {

			for (EvEdge evEdge : evEdges) {
				int length = 0;
				GPSLocation fromNode = graph.getNode(evEdge.fromId);
				List<GPSLocation> viaNodes1 = evEdge.viaNodes;
				for (GPSLocation gpsLocation : viaNodes1) {
					int partLenght = GPSLocationTools.computeDistance(fromNode, gpsLocation);
					length += partLenght;
					if (length > MIN_DIST_BETWEEN_VIANODES) {
						length = 0;
						viaNodesToKeep.add(gpsLocation);
					}
					fromNode = gpsLocation;
				}
				break;
			}
		}

		viaNodes.entrySet().removeIf(e -> !viaNodesToKeep.contains(e.getKey()));

		return viaNodes;
	}

	private Map<GPSLocation, Collection<EvEdge>> mapEdgesOnViaNodes(GraphStructure<EvNode, EvEdge> graph) {
		Map<GPSLocation, Collection<EvEdge>> contractedNodes = new HashMap<>();

		for (EvEdge edge : graph.getAllEdges()) {
			if (edge instanceof EvEdgeBetweenChargers) continue;

			for (GPSLocation viaNode : edge.viaNodes) {

				Collection<EvEdge> contractedNode = contractedNodes.get(viaNode);

				if (contractedNode == null) {
					contractedNode = new HashSet<>();
					contractedNodes.put(viaNode, contractedNode);
				}

				contractedNode.add(edge);
			}
		}

		return contractedNodes;
	}

	private Map<GPSLocation, Collection<EvEdge>> mapEdgesOnViaNodesReduced(GraphStructure<EvNode, EvEdge> graph) {
		Map<GPSLocation, Collection<EvEdge>> contractedNodes = new HashMap<>();

//		Set<EvEdge> edgesToSkip = new HashSet<>();
//
//		for (EvEdge edge : graph.getAllEdges()) {
//			if (edge instanceof EvEdgeBetweenChargers) continue;
//
//			if (edgesToSkip.contains(edge)) {
//				//avoid inspecting exact opposite edge
//				continue;
//			}
//
//			int length = 0;
//
//			List<GPSLocation> edgeViaNodes = new ArrayList<>(edge.viaNodes);
//
//			GPSLocation fromNode = graph.getNode(edge.fromId);
//			for (GPSLocation viaNode : edgeViaNodes) {
//				if (contractedNodes.containsKey(viaNode)) {
////					log.info("ViaNode is mapped already.");
//					length = 0;
//					fromNode = viaNode;
//					continue;
//				}
//
//				int partLenght = GPSLocationTools.computeDistance(fromNode, viaNode);
//				length += partLenght;
//
//				if (length > MIN_DIST_BETWEEN_VIANODES) {
//					length = 0;
//					Collection<EvEdge> edges = new HashSet<>();
//					edges.add(edge);
//
//					if (graph.containsEdge(edge.toId, edge.fromId)) {
//
//						EvEdge oppositeEdge = graph.getEdge(edge.toId, edge.fromId);
//						Collections.reverse(edgeViaNodes);
//
//						if (edgeViaNodes.equals(oppositeEdge.viaNodes)) {
//							//add oppositeEdge
//							edges.add(oppositeEdge);
//							edgesToSkip.add(oppositeEdge);
//						} else if (oppositeEdge.viaNodes.contains(viaNode)) {
//							edges.add(oppositeEdge);
////							log.debug("Opposite edge is different, but has at least one same viaNode.");
//						} else {
////							log.debug("Opposite edge is different.");
//						}
//					}
//
//					if (contractedNodes.containsKey(viaNode)) {
//						Collection<EvEdge> mappedEdges = contractedNodes.get(viaNode);
//						mappedEdges.addAll(edges);
//					} else {
//						contractedNodes.put(viaNode, edges);
//					}
//				}
//
//				fromNode = viaNode;
//			}
//		}
//
//


		return contractedNodes;
	}

	public EVZone<EvNode, EvEdge> getZone() {
		ChargingPriceRefresher.INSTANCE.refreshPrices(zone.getGraph());
		return zone;
	}

	public void destroyZone() {
		zone = null;
	}


}
