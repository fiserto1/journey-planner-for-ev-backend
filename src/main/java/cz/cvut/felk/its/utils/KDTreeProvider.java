/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.utils;

import cz.agents.basestructures.GPSLocation;
import cz.agents.geotools.GPSLocationKDTreeResolver;
import cz.agents.geotools.KDTree;
import cz.cvut.felk.its.api.structures.Coordinates;
import cz.cvut.felk.its.evstructures.EVZone;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.List;

public enum KDTreeProvider {
	INSTANCE;

	private final Logger log = Logger.getLogger(KDTreeProvider.class);
	private KDTree<GPSLocation> kdTree;
	EVZoneProvider evZoneProvider;

	KDTreeProvider() {
		log.info("KDTree initialization started.");

		this.kdTree = new KDTree<>(2, new GPSLocationKDTreeResolver<>(), -1);
		evZoneProvider = EVZoneProvider.INSTANCE;
		EVZone<EvNode, EvEdge> evZone = evZoneProvider.getZone();
		Collection<EvNode> nodes = evZone.getGraph().getAllNodes();

		nodes.forEach(kdTree::insert);
		evZone.getViaNodes().keySet().forEach(kdTree::insert);

		log.info("KDTree initialized successfully.");
	}

	public KDTree<GPSLocation> getKdTree() {
		return kdTree;
	}

	@Deprecated
	public List<GPSLocation> getNNearestNodesWithMaxDistance(Coordinates loc) {

		GPSLocation gpsLocation = evZoneProvider.getZone().createGPSLocationInZone(loc);

		return kdTree.getNNearestNodesWithMaxDistance(GPSLocationKDTreeResolver.getCoords(gpsLocation), 5, 2000);

	}

	public List<GPSLocation> getNNearestNodesWithMaxDistance(GPSLocation loc, double maxDistance) {

		return kdTree.getNNearestNodesWithMaxDistance(GPSLocationKDTreeResolver.getCoords(loc), 5, maxDistance);

	}
}
