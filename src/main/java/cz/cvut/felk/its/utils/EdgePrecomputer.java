package cz.cvut.felk.its.utils;

import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.GraphStructure;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.exceptions.BatteryLowException;
import cz.cvut.felk.its.experiments.TestResult;
import cz.cvut.felk.its.routing.alg.MulticriteriaDijkstra;
import cz.cvut.felk.its.routing.structures.goalchecker.FMGoalChecker;
import cz.cvut.felk.its.routing.structures.goalchecker.GoalChecker;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvEdgeBetweenChargers;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.RoadType;
import cz.cvut.felk.its.zonebuilder.tns.util.MultiEdgeBuilder;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by Tomas on 23-Mar-16.
 */
public class EdgePrecomputer {

    public static int PRECOMPUTED_ID = -3;

    private static final Logger log = Logger.getLogger(EdgePrecomputer.class);

    private static int convertToE3Format(double travelTime) {
        return (int) Math.round(travelTime * 1E3);
    }

	public static Map<Integer, EvEdge> computeFMEdges(int numOfChargers, SearchGraph<EvLabel> graph,
			GraphStructure<EvNode, EvEdge> extendedGraph, int initSoC, LocalDateTime startDateTime, Set<Integer> chargerNodeIds)
			throws BatteryLowException {
		return computeFMEdgesTest(numOfChargers, graph, extendedGraph, initSoC, startDateTime, chargerNodeIds, null);
	}

    public static Map<Integer, EvEdge> computeFMEdgesTest(int numOfChargers, SearchGraph<EvLabel> graph,
			GraphStructure<EvNode, EvEdge> extendedGraph, int initSoC, LocalDateTime startDateTime, Set<Integer> chargerNodeIds, TestResult testResult)
			throws BatteryLowException {

        MultiEdgeBuilder<EvEdge> multiEdgeBuilder = new MultiEdgeBuilder<>();

        Set<Integer> goalNodeIds = new HashSet<>(chargerNodeIds);
        goalNodeIds.add(GraphExtender.GOAL_ID);
        GoalChecker<EvLabel> fmGoalChecker = new FMGoalChecker<>(goalNodeIds);

        EvLabel startNode = new EvLabel(GraphExtender.START_ID, initSoC);

        MulticriteriaDijkstra<EvLabel> multiDijktra = new MulticriteriaDijkstra<>(graph,
                Collections.singleton(startNode), fmGoalChecker, 0, false, false, startDateTime);

		long startTime = System.currentTimeMillis();

		multiDijktra.run();

//		Map<Integer, List<Path<EvLabel>>> searchSpace = multiDijktra.getSearchSpace();
//		Collection<GPSLocation> nodes = searchSpace.keySet().stream().map(extendedGraph::getNode)
//				.collect(Collectors.toCollection(ArrayList::new));
//
//		try {
//			new JSONConverter().convertLocationsToJSON("searchSpace.geojson", nodes);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

		if (testResult != null) {
			testResult.setFmTimeMillis(System.currentTimeMillis()-startTime);
			testResult.setFmNumOfIters(multiDijktra.getNumOfIters());
		}

		Map<Integer, List<Path<EvLabel>>> paths = multiDijktra.getPaths();

		// TODO: 24-Apr-16 if goal is reached in FM return solution immediately
		if (paths.isEmpty()) throw new BatteryLowException(initSoC);

        long start = System.currentTimeMillis();

        Map<Integer, EvEdge> fmEdges = new HashMap<>();
        for (Integer chargerOrGoalNodeId : paths.keySet()) {

            List<Path<EvLabel>> paths1 = paths.get(chargerOrGoalNodeId);

			List<EvEdge> parallelEdges = transformPathsToEvEdges(paths1, RoadType.FIRST_MILE, extendedGraph);

			EvEdgeBetweenChargers evEdgeBetweenChargers = new EvEdgeBetweenChargers(GraphExtender.START_ID,
                    chargerOrGoalNodeId, RoadType.FIRST_MILE, 0, null, parallelEdges);
            fmEdges.put(chargerOrGoalNodeId, evEdgeBetweenChargers);
        }

//        for (Path<EvLabel> sol: paths) {
//
//            int chargerOrGoalNodeId = sol.getNodes().get(sol.getNodes().size()-1).nodeId;
//
//            if (startEdges.containsKey(chargerOrGoalNodeId)) {
//
//            } else {
//
//            }
//            PrecomputedEvEdge edge = new PrecomputedEvEdge(sol.startNodeId, sol.goalNodeId, sol.length,
//                    RoadType.FIRST_MILE, sol.travelTimeMillis,
//                    sol.consumptionProfile, sol.getRouteNodeIds());
//
//            int stateToChargeId = ChargingModel.getStCIdFromSoC(sol.statesOfCharge.get(0));
//            multiEdgeBuilder.addEvEdge(edge, stateToChargeId); // TODO: 11-May-16 need to fill all STC in minCostFuncs
//        }
        long elapsedTime = System.currentTimeMillis() - start;
//        log.debug("FM parsing from solution " + elapsedTime + "ms");
//        return multiEdgeBuilder.createFMMultiEdges();
        return fmEdges;
    }

	public static Map<Integer, EvEdge> computeLMByBckwEdges(SearchGraph<EvLabel> graph,
			GraphStructure<EvNode, EvEdge> extendedGraph, int numOfNodes,
			Set<Integer> chargerNodeIds) {
		return computeLMByBckwEdgesTest(graph, extendedGraph, numOfNodes, chargerNodeIds, null);
	}

    public static Map<Integer, EvEdge> computeLMByBckwEdgesTest(SearchGraph<EvLabel> graph,
			GraphStructure<EvNode, EvEdge> extendedGraph, int numOfNodes,
			Set<Integer> chargerNodeIds, TestResult testResult) {

        MultiEdgeBuilder<EvEdge> multiEdgeBuilder = new MultiEdgeBuilder<>();
//        GoalChecker<EvLabel> goalChecker = new ChargersAreGoalsBackwardGC<>(
//				numOfNodes, GraphExtender.START_ID, chargerNodeIds);

        GoalChecker<EvLabel> lmGoalChecker = new FMGoalChecker<>(chargerNodeIds);

        EvLabel goalNode = new EvLabel(GraphExtender.GOAL_ID, ChargingModel.MAX_STATE_OF_CHARGE);

        MulticriteriaDijkstra<EvLabel> multiDijktra = new MulticriteriaDijkstra<>(graph,
                Collections.singleton(goalNode), lmGoalChecker, 0, false, false, LocalDateTime.now());

		long startTime = System.currentTimeMillis();

		multiDijktra.run();

		if (testResult != null) {
			testResult.setLmTimeMillis(System.currentTimeMillis()-startTime);
			testResult.setLmNumOfIters(multiDijktra.getNumOfIters());
		}

        Map<Integer, List<Path<EvLabel>>> paths = multiDijktra.getPaths();

        Map<Integer, EvEdge> lmEdges = new HashMap<>();
        if (paths.isEmpty()) return lmEdges;

        long start = System.currentTimeMillis();

        for (Integer chargerNodeId : paths.keySet()) {

            List<Path<EvLabel>> paths1 = paths.get(chargerNodeId);
			List<EvEdge> parallelEdges = transformPathsToEvEdges(paths1, RoadType.LAST_MILE, extendedGraph);

			EvEdgeBetweenChargers evEdgeBetweenChargers = new EvEdgeBetweenChargers(chargerNodeId,
                    GraphExtender.GOAL_ID, RoadType.LAST_MILE, 0, null, parallelEdges);
            lmEdges.put(chargerNodeId, evEdgeBetweenChargers);
        }

//        for (Path<EvLabel> sol: paths) {
//
//            PrecomputedEvEdge edge = new PrecomputedEvEdge(sol.startNodeId, sol.goalNodeId, sol.length, RoadType.LAST_MILE,
//					sol.travelTimeMillis, sol.consumptionProfile, sol.getRouteNodeIds());
//            int stateToChargeId = ChargingModel.getStCIdFromSoC(sol.statesOfCharge.get(0));
//            multiEdgeBuilder.addEvEdge(edge, stateToChargeId);
//        }
        long elapsedTime = System.currentTimeMillis() - start;
//        log.debug("LM parsing from solution " + elapsedTime + "ms");
//        return multiEdgeBuilder.createLMMultiEdges();
        return lmEdges;
    }


    public static Set<EvEdge> computeMultiEdgesBetweenSChargers(SearchGraph<EvLabel> evGraph,
			GraphStructure<EvNode, EvEdge> graph, int numOfChargers, Set<Integer> chargerNodeIds) {

//        MultiEdgeBuilder<EvEdge> multiEdgeBuilder = new MultiEdgeBuilder<>();
        Set<EvEdge> multiEdges = new HashSet<>();

        for (int chargerStartNodeId: chargerNodeIds) {
            log.debug("Precomputing edges from charger (" + chargerStartNodeId + ")");
            //            for (int i = 0; i < ChargingModel.STATES_TO_CHARGE.length; i++) { //for every state to charge

            GoalChecker<EvLabel> gc = new FMGoalChecker<>(chargerNodeIds);
            EvLabel chargerStartNode = new EvLabel(chargerStartNodeId, ChargingModel.MAX_STATE_OF_CHARGE);

            MulticriteriaDijkstra<EvLabel> multiDijkstra = new MulticriteriaDijkstra<>(evGraph,
                        Collections.singleton(chargerStartNode), gc);

            multiDijkstra.run();

            Map<Integer, List<Path<EvLabel>>> paths = multiDijkstra.getPaths();

            List<Integer> edgeSizes = new ArrayList<>();
            for (Integer chargerGoalNodeId : paths.keySet()) {
                if(chargerGoalNodeId.equals(chargerStartNodeId)) continue; //remove cycle

                List<Path<EvLabel>> paths1 = paths.get(chargerGoalNodeId);

//				ConsumptionProfile optimisticCP = ConsumptionProfileUtils
//						.computeConsumptionProfileBetweenTwoLocs(graph.getNode(chargerStartNodeId),
//								graph.getNode(chargerGoalNodeId));
//
//				for (Path<EvLabel> evLabelPath : paths1) {
//					ConsumptionProfile pathCP = evLabelPath.getNodes()
//							.get(evLabelPath.getNodes().size() - 1).consProfile;
//					if (!pathCP.isDominatedBy(optimisticCP)){
//						log.info("Ou nouuu." + optimisticCP + " not dominates " + pathCP);
//					}
//				}

				edgeSizes.add(paths1.size());
				List<EvEdge> parallelEdges = transformPathsToEvEdges(paths1, RoadType.PRECOMPUTED, graph);
//				edgeSizes.add(parallelEdges.size());

				EvEdgeBetweenChargers evEdgeBetweenChargers = new EvEdgeBetweenChargers(chargerStartNodeId,
                        chargerGoalNodeId, RoadType.PRECOMPUTED, 0, null, parallelEdges);
                multiEdges.add(evEdgeBetweenChargers);
            }
            log.debug("Charger (" + chargerStartNodeId + ") has " + edgeSizes + "paths");

            //                GoalChecker<EvLabel> goalChecker = new AllChargersGC<>(chargerNodeIds);
                //                    System.out.println("State to charge: " + ChargingModel.STATES_TO_CHARGE[i]);
//                List<Solution> solutions = ModifiedDijkstra.modifiedDijkstra(chargerStartNodeId, goalChecker, evGraph,
//                        ChargingModel.STATES_TO_CHARGE[i], ModifiedDijkstra.OPTIMAL_REDUCE_CONST);
//
//                for (Solution sol: solutions) {
//
//                    PrecomputedEvEdge edge = new PrecomputedEvEdge(chargerStartNodeId, sol.goalNodeId, sol.length, RoadType.PRECOMPUTED,
//                            sol.travelTimeMillis, sol.consumptionProfile, sol.getRouteNodeIds());
//                    multiEdgeBuilder.addEvEdge(edge, i);
//                }
//            }
        }

        return multiEdges;

//        return multiEdgeBuilder.createMultiEdges();
    }

	private static List<EvEdge> transformPathsToEvEdges(List<Path<EvLabel>> paths, RoadType roadType,
			GraphStructure<EvNode, EvEdge> extendedGraph) {

//		List<Path<EvLabel>> reducedPaths = paths;
		List<Path<EvLabel>> reducedPaths = reducePaths(paths);

		List<EvEdge> parallelEdges = new ArrayList<>();
		for (Path<EvLabel> path : reducedPaths) {
			parallelEdges.add(transformPathToEvEdge(path, roadType, extendedGraph));
		}

		return parallelEdges;
	}

	private static List<Path<EvLabel>> reducePaths(List<Path<EvLabel>> paths) {

		int numOfPaths = paths.size();

		if (numOfPaths < 6) return paths;

		List<Path<EvLabel>> reducedPaths = new ArrayList<>(5);

		// add paths on indexes 0, n/4, n/2,(3/4)n, n-1
		reducedPaths.add(paths.get(0));
		reducedPaths.add(paths.get(numOfPaths/4));
		reducedPaths.add(paths.get(numOfPaths/2));
		reducedPaths.add(paths.get(3*numOfPaths/4));
		reducedPaths.add(paths.get(numOfPaths-1));

		int minInSoC = Integer.MAX_VALUE;
		Path<EvLabel> minPath = null;
		for (Path<EvLabel> path : paths) {
			int pathInSoc = path.getNodes().get(path.getNodes().size() - 1).consProfile.inSoc;
			if (pathInSoc < minInSoC) {
				minInSoC = pathInSoc;
				minPath = path;
			}
		}

		//check path with minimal in SoC to ensure feasibility
		Path<EvLabel> lastPath = paths.get(numOfPaths-1);
		int lastInSoc = lastPath.getNodes().get(lastPath.getNodes().size() - 1).consProfile.inSoc;
		if (lastInSoc != minInSoC) {
			log.info(String.format("Last path does not have minimal InSOC in ParetoSet. "
					+ "Adding path with min inSoC %d. Last path have inSoC %d", minInSoC, lastInSoc));
			reducedPaths.add(minPath);
		}

		return reducedPaths;

	}

	private static EvEdge transformPathToEvEdge(Path<EvLabel> path, RoadType roadType,
			GraphStructure<EvNode, EvEdge> extendedGraph) {

		List<EvLabel> nodes = path.getNodes();

		EvLabel startNode = nodes.get(0);

		List<GPSLocation> viaNodes = new ArrayList<>();
		for (int i = 1; i < nodes.size()-1; i++) {
			EvLabel evLabel = nodes.get(i);
			viaNodes.add(extendedGraph.getNode(evLabel.nodeId));
		}

		EvLabel goalNode = nodes.get(nodes.size() - 1);

		return new EvEdge(startNode.nodeId, goalNode.nodeId, 0, roadType, goalNode.travelTimeMillis, goalNode.consProfile, viaNodes);
	}

}
