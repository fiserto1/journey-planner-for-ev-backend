package cz.cvut.felk.its.zonebuilder.tns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class TnsEdgeFactory {

	private TnsEdgeFactory() {
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param fromNode
	 * @param toNode
	 * @param tags
	 * @return
	 */
	public static TnsEdge getTnsEdge(long fromNodeId, long toNodeId, Map<String, String> tags) {
		List<TnsNode> via = Collections.emptyList();
		Map<String, String> newTags = new HashMap<String, String>(tags);
		Collection<Long> wayIds = new HashSet<Long>();

		return new TnsEdge(fromNodeId, via, toNodeId, wayIds, newTags);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param fromNode
	 * @param toNode
	 * @param tags
	 * @return
	 */
	public static TnsEdge getTnsEdge(long fromNodeId, long toNodeId, long wayId,
			Map<String, String> tags) {
		List<TnsNode> via = Collections.emptyList();
		Map<String, String> newTags = new HashMap<String, String>(tags);
		Collection<Long> wayIds = new HashSet<Long>();

		wayIds.add(wayId);

		return new TnsEdge(fromNodeId, via, toNodeId, wayIds, newTags);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param generalEdge
	 * @return
	 */
	public static TnsEdge getOppositeTnsEdge(TnsEdge generalEdge) {
		List<TnsNode> via = new ArrayList<TnsNode>(generalEdge.getVia());

		Collections.reverse(via);

		Map<String, String> tags = new HashMap<String, String>(generalEdge.getTags());
		Collection<Long> wayIds = new HashSet<Long>(generalEdge.getWayIds());

		return new TnsEdge(generalEdge.toId, via, generalEdge.fromId, wayIds, tags);
	}
}
