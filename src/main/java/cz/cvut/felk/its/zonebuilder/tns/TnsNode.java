package cz.cvut.felk.its.zonebuilder.tns;

import cz.agents.basestructures.GPSLocation;

import java.util.Collections;
import java.util.Map;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
@SuppressWarnings("serial")
public class TnsNode extends GPSLocation implements TnsObjectWithTags {

	public final long id;

	/**
	 * id of the node derived from source data set, e.g., osm map
	 */
	public final long sourceId;

	/**
	 * TODO javadoc
	 */
	private final Map<String, String> tags;

	// public TnsNode(long osmId, int latE6, int lonE6, int projectedLat, int
	// projectedLon,
	// int elevation, Collection<String> tags) {
	// super(latE6, lonE6, projectedLat, projectedLon, elevation);
	// this.id = osmId;
	// this.tags = tags;
	// }

	public TnsNode(long id, long sourceId, GPSLocation location, Map<String, String> tags) {
		this(id, sourceId, location.latE6, location.lonE6, location.latProjected,
				location.lonProjected, location.elevation, tags);
	}

	public TnsNode(long id, long sourceId, int latE6, int lonE6, int projectedLat,
			int projectedLon,
			int elevation, Map<String, String> tags) {
		super(latE6, lonE6, projectedLat, projectedLon, elevation);

		this.id = id;
		this.sourceId = sourceId;
		this.tags = tags;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addTag(String tagKey, String tagValue) {
		tags.put(tagKey, tagValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addTags(Map<String, String> additionalTags) {
		tags.putAll(additionalTags);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeTag(String tagKey) {
		tags.remove(tagKey);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> getTags() {
		return Collections.unmodifiableMap(tags);
	}

	@Override
	public String toString() {
		return "TnsNode{" +
				"id=" + id +
				", sourceId=" + sourceId +
				", tags=" + tags +
				"} " + super.toString();
	}
}
