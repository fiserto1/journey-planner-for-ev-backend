package cz.cvut.felk.its.zonebuilder.tns;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class TnsRelationMember {

	private final long id;
	private final String type;
	private final String role;

	public TnsRelationMember(long id, String type, String role) {
		super();
		this.id = id;
		this.type = type;
		this.role = role;
	}

	public long getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public String getRole() {
		return role;
	}

	@Override
	public String toString() {
		return "TnsRelationMember [id=" + id + ", type=" + type + ", role=" + role + "]";
	}
}
