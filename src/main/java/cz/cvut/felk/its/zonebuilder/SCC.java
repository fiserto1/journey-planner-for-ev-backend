package cz.cvut.felk.its.zonebuilder;

import cz.agents.basestructures.Edge;
import cz.agents.basestructures.Graph;
import cz.agents.basestructures.Node;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Created by Tomas on 13-Jan-16.
 */
public class SCC {

    public static <TNode extends Node, TEdge extends Edge> Set<Integer> findBiggestSCC(Graph<TNode, TEdge> graph) {

        Set<Set<Integer>> sCComponents = kosarajuAlgorithm(graph);
        Set<Integer> biggestSCC = null;
        int biggestSize = Integer.MIN_VALUE;

        for(Set<Integer> oneSCC: sCComponents) {
            if (oneSCC.size() > biggestSize) {
                biggestSize = oneSCC.size();
                biggestSCC = oneSCC;
            }
        }
        return biggestSCC;
    }

    public static<TNode extends Node, TEdge extends Edge> Set<Set<Integer>> kosarajuAlgorithm(
            Graph<TNode, TEdge> graph) {

        int numOfNodes = graph.getAllNodes().size();
        Set<Set<Integer>> sCComponents = new HashSet<>();

        Stack<Integer> stack = new Stack<>();
        /*
        Forward part
         */
        boolean[] visited = new boolean[numOfNodes];

        //graph must have nodeIds from 0 to n
        for (int nodeId = 0; nodeId < numOfNodes; nodeId++) {
            if (!visited[nodeId]) {
                findReachableNodes(nodeId, visited, stack, graph, false, null);
            }
        }

        /*
        Backward part
         */
        Arrays.fill(visited, false);

        while (!stack.isEmpty()) {
            int nodeId = stack.pop();
            if (!visited[nodeId]) {
                Set<Integer> oneSCComponent = new HashSet<>();
                findReachableNodes(nodeId, visited, stack, graph, true, oneSCComponent);
                sCComponents.add(oneSCComponent);
            }
        }

        return sCComponents;
    }

    private static <TNode extends Node, TEdge extends Edge> void findReachableNodes(
            int nodeId, boolean[] visited, Stack<Integer> stack, Graph<TNode, TEdge> graph, boolean reverse,
            Set<Integer> oneSCComponent) {

        visited[nodeId] = true;

        if (!reverse) {
            for (TEdge edge: graph.getOutEdges(nodeId)) {
                if (!visited[edge.toId]) {
                    findReachableNodes(edge.toId, visited, stack, graph, false, null);
                }
            }
            stack.push(nodeId);
        } else {
            oneSCComponent.add(nodeId);
            for (TEdge edge: graph.getInEdges(nodeId)) {
                if (!visited[edge.fromId]) {
                    findReachableNodes(edge.fromId, visited, stack, graph, true, oneSCComponent);
                }
            }
        }
    }
}
