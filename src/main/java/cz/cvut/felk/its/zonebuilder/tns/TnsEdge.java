package cz.cvut.felk.its.zonebuilder.tns;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class TnsEdge implements TnsObjectWithTags {

	/**
	 * id of the tail node
	 */
	public final long fromId;

	/**
	 * id of the goal node
	 */
	public final long toId;

	private final List<TnsNode> via;

	/**
	 * TODO javadoc
	 */
	private Collection<Long> wayIds;

	/**
	 * TODO javadoc
	 */
	private Map<String, String> tags;

	public TnsEdge(long fromNodeId, List<TnsNode> via, long toNodeId, Collection<Long> wayIds,
			Map<String, String> tags) {

		this.fromId = fromNodeId;
		this.toId = toNodeId;
		this.via = via;
		this.wayIds = wayIds;
		this.tags = tags;
	}

	public TnsEdge(long fromNodeId, List<TnsNode> via, long toNodeId, long wayId,
			Map<String, String> tags) {

		this.fromId = fromNodeId;
		this.toId = toNodeId;
		this.via = via;
		this.tags = tags;
		this.wayIds = new HashSet<Long>();

		this.wayIds.add(wayId);
	}

	public List<TnsNode> getVia() {
		return via;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public Collection<Long> getWayIds() {
		return wayIds;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * One edge can be part of more ways, mostly because there is a lot of
	 * contributors
	 * 
	 * @param wayId
	 * @return
	 */
	public boolean addWayId(long wayId) {
		return wayIds.add(wayId);
	}

	public boolean addWayIds(Collection<Long> wayIds) {
		return this.wayIds.addAll(wayIds);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addTag(String tagKey, String tagValue) {
		tags.put(tagKey, tagValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addTags(Map<String, String> additionalTags) {
		tags.putAll(additionalTags);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeTag(String tagKey) {
		tags.remove(tagKey);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> getTags() {
		return Collections.unmodifiableMap(tags);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (fromId ^ (fromId >>> 32));
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + (int) (toId ^ (toId >>> 32));
		result = prime * result + ((via == null) ? 0 : via.hashCode());
		result = prime * result + ((wayIds == null) ? 0 : wayIds.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TnsEdge other = (TnsEdge) obj;
		if (fromId != other.fromId)
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (toId != other.toId)
			return false;
		if (via == null) {
			if (other.via != null)
				return false;
		} else if (!via.equals(other.via))
			return false;
		if (wayIds == null) {
			if (other.wayIds != null)
				return false;
		} else if (!wayIds.equals(other.wayIds))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TnsEdge [fromId=" + fromId + ", toId=" + toId + ", via=" + via + ", wayIds="
				+ wayIds + ", tags=" + tags + "]";
	}

}
