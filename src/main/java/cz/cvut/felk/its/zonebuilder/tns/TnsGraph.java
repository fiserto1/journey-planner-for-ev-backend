package cz.cvut.felk.its.zonebuilder.tns;

import java.util.*;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class TnsGraph {

	// private static final Logger log = Logger.getLogger(GeneralGraph.class);

	private Map<Long, TnsNode> nodesByNodeId;
	private Map<Long, Long> sourceNodeIdToNodeId;

	private Map<EdgeId, TnsEdge> edgeByFromToNodeIds;
	private Map<Long, Collection<TnsEdge>> nodeOutgoingEdges;
	private Map<Long, Collection<TnsEdge>> nodeIncomingEdges;

	private Map<Long, Collection<TnsEdge>> waysByWayId;

	private Map<Long, TnsRelation> relations;

	public TnsGraph() {
		nodesByNodeId = new LinkedHashMap<Long, TnsNode>();
		sourceNodeIdToNodeId = new LinkedHashMap<Long, Long>();

		edgeByFromToNodeIds = new LinkedHashMap<EdgeId, TnsEdge>();
		nodeOutgoingEdges = new HashMap<Long, Collection<TnsEdge>>();
		nodeIncomingEdges = new HashMap<Long, Collection<TnsEdge>>();

		waysByWayId = new HashMap<Long, Collection<TnsEdge>>();

		relations = new HashMap<Long, TnsRelation>();
	}

	// *************************************************************************
	// Nodes
	// *************************************************************************

	/**
	 * Add a collection of nodes to the graph
	 * 
	 * @param nodes
	 *            Nodes
	 */
	public void addNodes(Collection<TnsNode> nodes) {
		for (TnsNode node : nodes) {
			addNode(node);
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param node
	 */
	public void addNode(TnsNode node) {
		nodesByNodeId.put(node.id, node);
		sourceNodeIdToNodeId.put(node.sourceId, node.id);
		nodeOutgoingEdges.put(node.id, new ArrayList<TnsEdge>());
		nodeIncomingEdges.put(node.id, new ArrayList<TnsEdge>());
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nodeId
	 * @return
	 */
	public boolean containsNode(long nodeId) {
		return nodesByNodeId.containsKey(nodeId);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public Collection<TnsNode> getAllNodes() {
		return nodesByNodeId.values();
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nodeId
	 * @return
	 */
	public TnsNode getNode(long nodeId) {
		return nodesByNodeId.get(nodeId);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nodeSourceId
	 * @return otherwise null
	 */
	public Long getNodeIdByNodeSourceId(long nodeSourceId) {
		return sourceNodeIdToNodeId.get(nodeSourceId);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nodeId
	 */
	public void removeNode(long nodeId) {
		removeEdges(new HashSet<>(nodeOutgoingEdges.get(nodeId)));
		removeEdges(new HashSet<>(nodeIncomingEdges.get(nodeId)));
		
		TnsNode removedNode = nodesByNodeId.remove(nodeId);
		
		if (removedNode != null) {
			sourceNodeIdToNodeId.remove(removedNode.sourceId);
		}		
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param oldNode
	 * @param newNode
	 */
	public void replaceNode(TnsNode oldNode, TnsNode newNode) {
		assert oldNode.id == newNode.id : "Id of oldNode must be equal to id of newNode!";

		nodesByNodeId.put(newNode.id, newNode);
		sourceNodeIdToNodeId.put(newNode.sourceId, newNode.id);
	}

	// *************************************************************************
	// Edges
	// *************************************************************************

	/**
	 * Add a collection of edges to the graph
	 * 
	 * @param edges
	 *            Edges
	 */
	public void addEdges(Collection<TnsEdge> edges) {
		for (TnsEdge edge : edges) {
			addEdge(edge);
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edge
	 */
	public void addEdge(TnsEdge edge) {

		assert nodesByNodeId.get(edge.fromId) != null && nodesByNodeId.get(edge.toId) != null : "Node has to be in graph builder before inserting edge";

		if (!containsEdge(edge.fromId, edge.toId)) {

			addNewEdge(edge);

		} else {

			mergeToExistingEdge(edge);
		}

		addEdgeToWays(edge);
	}

	private void addNewEdge(TnsEdge edge) {
		Collection<TnsEdge> outgoingEdgesFromNode = nodeOutgoingEdges.get(edge.fromId);
		Collection<TnsEdge> incomingEdgesToNode = nodeIncomingEdges.get(edge.toId);

		outgoingEdgesFromNode.add(edge);
		incomingEdgesToNode.add(edge);

		EdgeId edgeId = new EdgeId(edge.fromId, edge.toId);

		edgeByFromToNodeIds.put(edgeId, edge);
	}

	private void mergeToExistingEdge(TnsEdge edge) {
		EdgeId edgeId = new EdgeId(edge.fromId, edge.toId);
		TnsEdge existingEdge = edgeByFromToNodeIds.get(edgeId);

		existingEdge.addTags(edge.getTags());
		existingEdge.addWayIds(edge.getWayIds());
	}

	private void addEdgeToWays(TnsEdge edge) {
		for (long wayId : edge.getWayIds()) {
			Collection<TnsEdge> way = waysByWayId.get(wayId);

			if (way == null) {
				way = new HashSet<TnsEdge>();

				waysByWayId.put(wayId, way);
			}

			way.add(edge);
		}
	}

	/**
	 * 
	 * Returns true when the graph contains edge with given edgeId
	 * 
	 * @param
	 * @return
	 */
	public boolean containsEdge(long edgeFromId, long edgeToId) {
		EdgeId edgeId = new EdgeId(edgeFromId, edgeToId);
		return edgeByFromToNodeIds.containsKey(edgeId);
	}

	/**
	 * 
	 * Provide current Collection of edges
	 * 
	 * @return
	 */
	public Collection<TnsEdge> getAllEdges() {
		return Collections.unmodifiableCollection(edgeByFromToNodeIds.values());
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param
	 * @return
	 */
	public TnsEdge getEdge(long edgeFromId, long edgeToId) {
		EdgeId edgeId = new EdgeId(edgeFromId, edgeToId);
		return edgeByFromToNodeIds.get(edgeId);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nodeId
	 * @return
	 */
	public Collection<TnsEdge> getAllNodeOutgoingEdges(long nodeId) {
		return Collections.unmodifiableCollection(nodeOutgoingEdges.get(nodeId));
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nodeId
	 * @return
	 */
	public Collection<TnsEdge> getAllNodeIncomingEdges(long nodeId) {
		return Collections.unmodifiableCollection(nodeIncomingEdges.get(nodeId));
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param
	 * @return
	 */
	public void removeEdges(Collection<TnsEdge> edges) {
		for (TnsEdge generalEdge : edges) {
			removeEdge(generalEdge);
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param
	 */
	public void removeEdge(TnsEdge edge) {
		Collection<TnsEdge> outgoingEdgesFromNode = nodeOutgoingEdges.get(edge.fromId);
		Collection<TnsEdge> incomingEdgesToNode = nodeIncomingEdges.get(edge.toId);
		EdgeId edgeId = new EdgeId(edge.fromId, edge.toId);

		removeEdgeFromCollectionOfEdges(outgoingEdgesFromNode, edge.fromId, edge.toId);
		removeEdgeFromCollectionOfEdges(incomingEdgesToNode, edge.fromId, edge.toId);

		edgeByFromToNodeIds.remove(edgeId);

		removeEdgeFromWays(edge);
	}

	private void removeEdgeFromWays(TnsEdge edge) {
		for (long wayId : edge.getWayIds()) {
			removeEdgeFromCollectionOfEdges(waysByWayId.get(wayId), edge.fromId, edge.toId);
		}
	}

	private void removeEdgeFromCollectionOfEdges(Collection<TnsEdge> edges,
			long edgeToRemoveFromId, long edgeToRemoveToId) {
		for (Iterator<TnsEdge> iterator = edges.iterator(); iterator.hasNext();) {
			TnsEdge generalEdge = iterator.next();

			if (generalEdge.fromId == edgeToRemoveFromId && generalEdge.toId == edgeToRemoveToId) {
				iterator.remove();
			}
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edge
	 */
	public Collection<TnsEdge> shrinkEdgeToNode(TnsEdge edge) {
		removeEdge(edge);

		TnsNode from = getNode(edge.fromId);
		TnsNode to = getNode(edge.toId);

		Collection<TnsEdge> outgoingEdgesToNode = nodeOutgoingEdges.get(to.id);
		Collection<TnsEdge> newEdges = new ArrayList<>();

		for (TnsEdge outgoingEdge : outgoingEdgesToNode) {
			TnsEdge newOutgoingEdge = TnsEdgeFactory.getTnsEdge(from.id, outgoingEdge.toId,
					outgoingEdge.getTags());

			newOutgoingEdge.addWayIds(outgoingEdge.getWayIds());
			newOutgoingEdge.addWayIds(edge.getWayIds());

			addEdge(newOutgoingEdge);
			newEdges.add(newOutgoingEdge);
		}

		Collection<TnsEdge> incomingEdgesToNode = nodeIncomingEdges.get(to.id);

		for (TnsEdge incomingEdge : incomingEdgesToNode) {
			TnsEdge newIncomingEdge = TnsEdgeFactory.getTnsEdge(incomingEdge.fromId, from.id,
					incomingEdge.getTags());

			newIncomingEdge.addWayIds(incomingEdge.getWayIds());
			newIncomingEdge.addWayIds(edge.getWayIds());

			addEdge(newIncomingEdge);
			newEdges.add(newIncomingEdge);
		}

		removeNode(to.id);

		return newEdges;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param wayId
	 * @return
	 */
	public boolean containsWay(long wayId) {
		return waysByWayId.containsKey(wayId);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param wayId
	 * @return collection of general edges that together creates specified way
	 */
	public Collection<TnsEdge> getAllEdgesOfWay(long wayId) {
		return Collections.unmodifiableCollection(waysByWayId.get(wayId));
	}

	// *************************************************************************
	// Relations
	// *************************************************************************

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param sourceId
	 * @param relation
	 */
	public void addRelation(TnsRelation relation) {
		relations.put(relation.id, relation);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param relationId
	 * @return
	 */
	public boolean containsRelation(long relationId) {
		return relations.containsKey(relationId);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public Collection<TnsRelation> getAllRelations() {
		return Collections.unmodifiableCollection(relations.values());
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param relationId
	 * @return
	 */
	public TnsRelation getRelation(long relationId) {
		return relations.get(relationId);
	}

	private class EdgeId {
		public final long fromNodeId;
		public final long toNodeId;

		public EdgeId(long fromNodeId, long toNodeId) {
			this.fromNodeId = fromNodeId;
			this.toNodeId = toNodeId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + (int) (fromNodeId ^ (fromNodeId >>> 32));
			result = prime * result + (int) (toNodeId ^ (toNodeId >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			EdgeId other = (EdgeId) obj;
			if (fromNodeId != other.fromNodeId)
				return false;
			return toNodeId == other.toNodeId;
		}

		private TnsGraph getOuterType() {
			return TnsGraph.this;
		}
	}
}
