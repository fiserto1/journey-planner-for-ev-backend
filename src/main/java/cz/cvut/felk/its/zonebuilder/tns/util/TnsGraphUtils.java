package cz.cvut.felk.its.zonebuilder.tns.util;

import com.vividsolutions.jts.geom.Coordinate;
import cz.agents.basestructures.GPSLocation;
import cz.agents.geotools.*;
import cz.cvut.felk.its.evstructures.chargers.Supercharger;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.evstructures.RoadType;
import cz.cvut.felk.its.zonebuilder.tns.*;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Pattern;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class TnsGraphUtils {


	private static final int MIN_ELEVATION = -10; //elevation from tags
	public static final double MOTORWAY_RECOMMENDED_SPEED = 130 / 3.6;
	public static final double MOTORWAY_LINK_SPEED_LIMIT = 50 / 3.6;
	public static final double OUTSIDE_URBAN_SPEED_LIMIT = 100 / 3.6;
	public static final double PRIMARY_SPEED_LIMIT = 80 /3.6;
	public static final double SECONDARY_SPEED_LIMIT = 70 /3.6;
	public static final double TERTIARY_SPEED_LIMIT = 50 /3.6;
	public static final double LINK_SPEED_LIMIT = 50 /3.6;
	public static final double URBAN_SPEED_LIMIT = 50 /3.6;
	public static final double PARKING_SPEED_LIMIT = 200 /3.6; //10 // TODO: 20-Sep-16 this is for testing on long parking edges in my-graph.ser - assign small edge travel time

	private static final Pattern IS_NUMBER_REGEX = Pattern.compile("^-?\\d*\\.{0,1}\\d+$");
	private static final Pattern IS_NUMBER_WITH_COMMA_REGEX = Pattern
			.compile("^-?\\d*\\,{0,1}\\d+$");

	private static final Logger log = Logger.getLogger(TnsGraphUtils.class);

	private static Transformer projection;

	static {
		projection = new Transformer(GraphTools.GERMANY_SRID);
		ConsoleAppender console = new ConsoleAppender(); //create appender
		//configure the appender
		String PATTERN = "%d [%p|%c|%C{1}] %m%n";
		console.setLayout(new PatternLayout(PATTERN));
		console.setThreshold(Level.INFO);
		console.activateOptions();
		//add appender to any Logger (here is root)
//		log.addAppender(console); // UNCOMMENT IF INFO IS NOT PRINTED TO OUTPUT


//		FileAppender fa = new FileAppender();
//		fa.setName("FileLogger");
//		fa.setFile("mylog.log");
//		fa.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
//		fa.setThreshold(Level.DEBUG);
//		fa.setAppend(true);
//		fa.activateOptions();
//
//		//add appender to any Logger (here is root)
//		Logger.getRootLogger().addAppender(fa)
	}

	private TnsGraphUtils() {

	}


	//tomasFISER
	@Deprecated //moved to RoadType
	public static double computeSpeedLimit(Map<String, String> elementTags) {
		//TODO get another tags to set appropriate speedlimit
		if (elementTags.containsKey("supercharger")) {
			String highwayVal = elementTags.get("supercharger");
			switch (highwayVal) {
			case "link_in":
				return PARKING_SPEED_LIMIT;
			case "link_out":
				return PARKING_SPEED_LIMIT;
			}
		} else if (elementTags.containsKey("highway")) {
			String highwayVal = elementTags.get("highway");
			switch (highwayVal) {
				case "motorway":
					return MOTORWAY_RECOMMENDED_SPEED;
				case "motorway_link":
					return MOTORWAY_LINK_SPEED_LIMIT;
				case "trunk":
					return OUTSIDE_URBAN_SPEED_LIMIT;
				case "primary":
					return PRIMARY_SPEED_LIMIT;
				case "secondary":
					return SECONDARY_SPEED_LIMIT;
				case "tertiary":
					return TERTIARY_SPEED_LIMIT;
				default:
					//trunk, primary, secondary, tertiary links
					return LINK_SPEED_LIMIT;
			}
		}
		return SECONDARY_SPEED_LIMIT;
	}

	//tomasFISER
	public static RoadType assingEdgeRoadType(Map<String, String> elementTags) {

		if (elementTags.containsKey("highway")) {
			String highwayVal = elementTags.get("highway");
			switch (highwayVal) {
				case "motorway":
					return RoadType.MOTORWAY;
				case "motorway_link":
					return RoadType.MOTORWAY_LINK;
				case "trunk":
					return RoadType.TRUNK;
				case "trunk_link":
					return RoadType.TRUNK_LINK;
				case "primary":
					return RoadType.PRIMARY;
				case "primary_link":
					return RoadType.PRIMARY_LINK;
				case "secondary":
					return RoadType.SECONDARY;
				case "secondary_link":
					return RoadType.SECONDARY_LINK;
				case "tertiary":
					return RoadType.TERTIARY;
				case "tertiary_link":
					return RoadType.TERTIARY_LINK;
				default:
					return RoadType.UNKNOWN;
			}
		}

		if (elementTags.containsKey("supercharger")) {
			String type = elementTags.get("supercharger");
			switch (type) {
			case "link_in":
				return RoadType.SUPERCHARGER_LINK_IN;
			case "link_out":
				return RoadType.SUPERCHARGER_LINK_OUT;
			}
		}
		return RoadType.UNKNOWN;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * Detect malformed edges with nodes with same id or same coordinates
	 * 
	 * @param tnsGraph
	 */
	public static void deleteEdgesWithZeroLength(TnsGraph tnsGraph) {

		int countMalformedEdges = 0;
		Collection<TnsEdge> edges = tnsGraph.getAllEdges();

		while (!edges.isEmpty()) {
			Collection<TnsEdge> malformedEdges = new ArrayList<TnsEdge>();

			for (TnsEdge edge : edges) {
				TnsNode fromNode = tnsGraph.getNode(edge.fromId);
				TnsNode toNode = tnsGraph.getNode(edge.toId);

				if (fromNode == null || toNode == null) {
					continue;
				}

				double length = Math.ceil(GPSLocationTools.computeDistanceAsDouble(fromNode, toNode));

				if (length < 0.1) {
					countMalformedEdges++;
					malformedEdges.add(edge);
				}
			}

			edges = new ArrayList<>();

			for (TnsEdge malformedEdge : malformedEdges) {

				if (tnsGraph.containsNode(malformedEdge.fromId)
						&& tnsGraph.containsNode(malformedEdge.toId)) {

					Collection<TnsEdge> newEdges = tnsGraph.shrinkEdgeToNode(malformedEdge);

					edges.addAll(newEdges);
				}
			}

			log.info("Created " + edges.size() + " new edges by shrinkedEdgeToNode method");
		}

		log.info("Number of malformed edges: " + countMalformedEdges);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tnsGraph
	 */
	public static void repairNodesWithNegativeElevation(TnsGraph tnsGraph, KDTree<TnsNode> kdTree) {

		int nNearest = 100;
		int countNodes = 0;
		List<Integer> nearestElevations;

		for (TnsNode node : tnsGraph.getAllNodes()) {
			nearestElevations = new ArrayList<>();

			if (node.elevation < MIN_ELEVATION) {
				countNodes++;
				List<TnsNode> nearestNodes = kdTree.getNNearestNodes(GPSLocationKDTreeResolver.getCoords(node), nNearest);

				for (TnsNode nearestNode: nearestNodes) {
					if (nearestNode.elevation > MIN_ELEVATION) {
						nearestElevations.add(nearestNode.elevation);
					}
				}
				nearestElevations.sort(Comparator.naturalOrder());

				double medianElevation;
				int arraySize = nearestElevations.size();
				if (arraySize % 2 == 0)
					medianElevation = ((double)nearestElevations.get(arraySize/2) + (double)nearestElevations.get(arraySize/2 - 1))/2;
				else
					medianElevation = (double) nearestElevations.get(arraySize/2);
//				node.getTags().put("elevation", String.valueOf(medianElevation)); //unmodifiable map
				TnsNode newNode = new TnsNode(node.id, node.sourceId, node.latE6, node.lonE6,
						node.latProjected, node.lonProjected, (int) medianElevation, node.getTags());

//				log.info("New node: " + newNode + ", negative elevation was: " + node.elevation);

				tnsGraph.replaceNode(node, newNode);
			}
		}

		log.info("Number of detected nodes with negative elevation: " + countNodes);
	}

	public static void checkTagsWithExpectedIntegerValue(TnsGraph tnsGraph) {
		// Take care of nodes
		log.info("Checking for inconsistent tags among nodes...");
		for (TnsNode tnsNode : tnsGraph.getAllNodes()) {
			Map<String, String> tags = tnsNode.getTags();

			checkTagsWithExpectedIntegerValue(tags, tnsNode);
		}

		// Take care of edges
		log.info("Checking for inconsistent tags among edges...");
		for (TnsEdge tnsEdge : tnsGraph.getAllEdges()) {
			Map<String, String> tags = tnsEdge.getTags();

			checkTagsWithExpectedIntegerValue(tags, tnsEdge);
		}

		// Take care of relations
		log.info("Checking for inconsistent tags among relations...");
		for (TnsRelation tnsRelation : tnsGraph.getAllRelations()) {
			Map<String, String> tags = tnsRelation.getTags();

			checkTagsWithExpectedIntegerValue(tags, tnsRelation);
		}

	}

	private static void checkTagsWithExpectedIntegerValue(Map<String, String> tags,
			TnsObjectWithTags tnsObject) {

		String[] tagKeys = { "maxspeed", "lanes", "width" };

		for (int i = 0; i < tagKeys.length; i++) {
			if (tags.containsKey(tagKeys[i])) {
				String tagValue = tags.get(tagKeys[i]);

				checkTagValueToContainsOnlyNumber(tagKeys[i], tagValue, tnsObject);
			}
		}

	}

	private static boolean checkTagValueToContainsOnlyNumber(String tagKey, String tagValue,
			TnsObjectWithTags tnsObject) {

		if (IS_NUMBER_WITH_COMMA_REGEX.matcher(tagValue).matches()) {
			tagValue = tagValue.replaceAll(",", ".");
		}

		if (!IS_NUMBER_REGEX.matcher(tagValue).matches()) {
			log.info("Inconistent tag: " + tagKey + "=>" + tagValue);

			// tagValue = tagValue.replaceAll("[^0-9]", "");
			//
			// log.info("Inconistent tag after deleting non-number characters: "
			// + tagKey + "=>"
			// + tagValue);
			//
			// if (tagValue.equals("")) {
			tnsObject.removeTag(tagKey); // we remove inconsistent tag
			// } else {
			// tnsObject.addTag(tagKey, tagValue);
			// }

			return true;
		}
		return false;
	}

	/**
	 * Move highway traffic signals tag from node before junction to the node
	 * closer to junction and with tags crossing traffic signals
	 * 
	 * TODO javadoc
	 * 
	 * In a lot of cases there exists node in OSM representing traffic signals,
	 * it has one in edge and one out edge. We want to eliminate such a nodes
	 * and create more compact junction
	 * 
	 * @param generalGraphBuilder
	 */
	public static void moveHighwayTrafficSignalsTagFromNodeBeforeJunction(
			TnsGraph generalGraphBuilder) {
		int count = 0;

		for (TnsNode node : generalGraphBuilder.getAllNodes()) {
			if (node.getTags().toString().matches(".*node::highway::traffic_signals.*")) {

				Collection<TnsEdge> incomingEdges = generalGraphBuilder
						.getAllNodeIncomingEdges(node.id);
				Collection<TnsEdge> outgoingEdges = generalGraphBuilder
						.getAllNodeOutgoingEdges(node.id);

				if (incomingEdges.size() == 1 && outgoingEdges.size() == 1) {

					boolean tagOnIncoming = false;
					boolean tagOnOutgoing = false;
					TnsEdge incomingEdge = incomingEdges.iterator().next();
					TnsEdge outgoingEdge = outgoingEdges.iterator().next();
					TnsNode incomingEdgeFromNode = generalGraphBuilder.getNode(incomingEdge.fromId);
					TnsNode outgoingEdgeToNode = generalGraphBuilder.getNode(outgoingEdge.toId);

					if (incomingEdgeFromNode.getTags().toString()
							.matches(".*node::crossing::traffic_signals.*")) {
						count++;
						tagOnIncoming = true;
					}

					if (outgoingEdgeToNode.getTags().toString()
							.matches(".*node::crossing::traffic_signals.*")) {
						count++;
						tagOnOutgoing = true;
					}

					if (tagOnIncoming && tagOnOutgoing) {
						double incomingEdgeDistance = GPSLocationTools.computeDistanceAsDouble(
								incomingEdgeFromNode, node);
						double outgoingEdgeDistance = GPSLocationTools.computeDistanceAsDouble(node,
								outgoingEdgeToNode);

						if (incomingEdgeDistance <= outgoingEdgeDistance) {
							moveHighwayTrafficSignalTag(node, incomingEdgeFromNode);
						} else {
							moveHighwayTrafficSignalTag(node, outgoingEdgeToNode);
						}

					} else if (tagOnIncoming) {
						moveHighwayTrafficSignalTag(node, incomingEdgeFromNode);
					} else if (tagOnOutgoing) {
						moveHighwayTrafficSignalTag(node, outgoingEdgeToNode);
					}
				}
			}
		}

		log.info("Number of nodes with tag highway::traffic_signals with in/out degree 1 is "
				+ count);
	}

	private static void moveHighwayTrafficSignalTag(TnsNode fromNode, TnsNode toNode) {
		toNode.addTag("highway", "traffic_signals");

		if (fromNode.getTags().toString().matches(".*node::traffic_signals:direction::backward.*")) {

			toNode.addTag("traffic_signals:direction", "backward");
			fromNode.removeTag("traffic_signals:direction");

		} else if (fromNode.getTags().toString()
				.matches(".*node::traffic_signals:direction::forward.*")) {

			toNode.addTag("traffic_signals:direction", "forward");
			fromNode.removeTag("traffic_signals:direction");
		}

		// Remove tag: highway => traffic_signals
		fromNode.removeTag("highway");
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param generalGraphBuilder
	 */
	// TODO implement!!!
	public static void moveImportantNodeTagsToEdges(TnsGraph generalGraphBuilder) {
		// TODO implement according to new representation of TNS evstructures
		throw new UnsupportedOperationException("Not implemented, yet");

		// Pattern crossing = Pattern
		// .compile("node::crossing::(island|uncontrolled|unmarked|yes|zebra)|"
		// + "node::highway::crossing|node::railway::crossing");
		// Pattern pedestrianTrafficSignals =
		// Pattern.compile("node::crossing::traffic_signals");
		// Pattern roadTrafficSignalsAndStopSign = Pattern
		// .compile("node::highway::traffic_signals|node::highway::stop");
		// Pattern roads =
		// Pattern.compile(".*highway::(primary|secondary|tertiary|"
		// + "unclassified|residential|service|" +
		// "motorway_link|trunk_link|primary_link|"
		// + "secondary_link|tertiary_link|living_street|" + "track|road).*");
		// Pattern obstacles = Pattern
		// .compile("node::highway::elevator|node::highway::steps|"
		// +
		// "node::barrier::(yes|block|chain|rope|cycle_barrier|motorcycle_barrier|gate|lift_gate|swing_gate)|"
		// + "node::traffic_calming::(yes|bump)");
		// Pattern trafficCalmings =
		// Pattern.compile("node::traffic_calming::(yes|bump)");
		//
		// for (TnsNode generalNode : generalGraphBuilder.getNumOfNodes()) {
		// Collection<TnsEdge> edgesNotDesignedForVehicles = new
		// ArrayList<TnsEdge>();
		// Collection<TnsEdge> edgesDesignedForVehicles = new
		// ArrayList<TnsEdge>();
		// Collection<TnsEdge> edges = new ArrayList<TnsEdge>(
		// generalGraphBuilder.getAllNodeOutgoingEdges(generalNode.id));
		//
		// edges.addAll(generalGraphBuilder.getAllNodeIncomingEdges(generalNode.id));
		//
		// for (TnsEdge edge : edges) {
		// Map<String, String> tagsCollection = edge.getTags();
		// String tags = tagsCollection.toString();
		//
		// if (roads.matcher(tags).matches()) {
		// edgesDesignedForVehicles.add(edge);
		// } else {
		// edgesNotDesignedForVehicles.add(edge);
		// }
		// }
		//
		// Collection<String> tagsToMoveToAllEdges = new ArrayList<String>();
		// Collection<String> tagsToMoveToEdgesDesignedForVehicles = new
		// ArrayList<String>();
		// Collection<String> tagsToMoveToEdgesNotDesignedForVehicles = new
		// ArrayList<String>();
		//
		// for (String tag : generalNode.getTags()) {
		// if (crossing.matcher(tag).matches()) {
		// tagsToMoveToEdgesNotDesignedForVehicles.add(tag);
		// }
		//
		// if (pedestrianTrafficSignals.matcher(tag).matches()) {
		// tagsToMoveToEdgesNotDesignedForVehicles.add(tag);
		// }
		//
		// if (roadTrafficSignalsAndStopSign.matcher(tag).matches()) {
		// tagsToMoveToEdgesDesignedForVehicles.add(tag);
		// }
		//
		// if (obstacles.matcher(tag).matches()) {
		// tagsToMoveToAllEdges.add(tag);
		// }
		//
		// if (trafficCalmings.matcher(tag).matches()) {
		// tagsToMoveToEdgesDesignedForVehicles.add(tag);
		// }
		// }
		//
		// moveTagsFromNodeToEdges(tagsToMoveToAllEdges, generalNode, edges);
		// moveTagsFromNodeToEdges(tagsToMoveToEdgesNotDesignedForVehicles,
		// generalNode,
		// edgesNotDesignedForVehicles);
		// moveTagsFromNodeToEdges(tagsToMoveToEdgesDesignedForVehicles,
		// generalNode,
		// edgesDesignedForVehicles);
		//
		// }
	}

	private static void moveTagsFromNodeToEdges(Map<String, String> tags, TnsNode from,
			Collection<TnsEdge> to) {
		for (Entry<String, String> tag : tags.entrySet()) {
			moveTagFromNodeToEdges(tag.getKey(), tag.getValue(), from, to);
		}
	}

	private static void moveTagFromNodeToEdges(String tagKey, String tagValue, TnsNode from,
			Collection<TnsEdge> to) {
		from.removeTag(tagKey);

		for (TnsEdge generalEdge : to) {
			generalEdge.addTag(tagKey, tagValue);
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param generalGraphBuilder
	 */
	public static void interpolate(TnsGraph generalGraphBuilder) {

		final int minDistanceIncrement = 25;
		Collection<TnsEdge> edges = new HashSet<TnsEdge>(generalGraphBuilder.getAllEdges());

		for (TnsEdge edge : edges) {
			TnsNode fromNode = generalGraphBuilder.getNode(edge.fromId);
			TnsNode toNode = generalGraphBuilder.getNode(edge.toId);
			double distance = GPSLocationTools.computeDistanceAsDouble(fromNode, toNode);

			int numberOfInterpolatedNodes = (int) Math.floor(distance / minDistanceIncrement) - 1;

			if (numberOfInterpolatedNodes > 0) {
				TnsNode[] interpolatedNodes = new TnsNode[numberOfInterpolatedNodes + 1];

				// x=longitude=[0], y=latitude=[1], z=elevation[2]
				double[] fromPoint = { fromNode.lonProjected, fromNode.latProjected,
						fromNode.elevation };
				double[] fromToVector = { toNode.lonProjected - fromPoint[0],
						toNode.latProjected - fromPoint[1], toNode.elevation - fromPoint[2] };
				double fromToVectorSize = Math.sqrt(fromToVector[0] * fromToVector[0]
						+ fromToVector[1] * fromToVector[1] + fromToVector[2] * fromToVector[2]);
				double[] normalizedFromToVector = { fromToVector[0] / fromToVectorSize,
						fromToVector[1] / fromToVectorSize, fromToVector[2] / fromToVectorSize };
				double distanceIncrement = distance / (numberOfInterpolatedNodes + 1);
				// log.info("Distance: " + distance+", distance increment: " +
				// distanceIncrement +", # of interpolated nodes: " +
				// numberOfInterpolatedNodes);

				for (int i = 0; i < numberOfInterpolatedNodes; i++) {
					double distanceFromFromPoint = distanceIncrement * (i + 1);

					TnsNode interpolatedNode = getInterpolatedTnsNode(fromPoint[1]
							+ normalizedFromToVector[1] * distanceFromFromPoint, fromPoint[0]
							+ normalizedFromToVector[0] * distanceFromFromPoint, fromPoint[2]
							+ normalizedFromToVector[2] * distanceFromFromPoint);

					interpolatedNodes[i] = interpolatedNode;
					generalGraphBuilder.addNode(interpolatedNode);
				}

				interpolatedNodes[numberOfInterpolatedNodes] = toNode;

				for (TnsNode node : interpolatedNodes) {
					List<TnsNode> via = Collections.emptyList();
					Map<String, String> tags = new HashMap<String, String>(edge.getTags());
					Collection<Long> wayIds = new HashSet<Long>(edge.getWayIds());
					TnsEdge newEdge = new TnsEdge(fromNode.id, via, node.id, wayIds,
							tags);

					generalGraphBuilder.addEdge(newEdge);

					fromNode = node;
				}

				generalGraphBuilder.removeEdge(edge);
			}

		}
	}

	private static TnsNode getInterpolatedTnsNode(double projectedLatitude,
			double projectedLongitude, double elevation) {
		Coordinate projectedCoord = new Coordinate(projectedLatitude, projectedLongitude, elevation);
		Coordinate realCoord = projection.toReal(projectedCoord);

		//fixed code - hope its correct
		TnsNode interpolatedNode = new TnsNode(NewNodeIdUtil.getNewNodeId(), -1,(int)(realCoord.x*1E6), (int)(realCoord.y*1E6),
				(int)projectedCoord.x, (int)projectedCoord.y, (int)elevation, new HashMap<String, String>());
		return interpolatedNode;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * If node in and out degree is equal to 1, and in and out edge have same
	 * tags, then this node is discarded and new edge is created containing
	 * discarded node in order to preserve the correct geographical shape of
	 * newly created edge
	 * 
	 * This method should be used before introducing edges in opposite
	 * direction.
	 * 
	 * Simplify while there are only edges in one direction
	 * 
	 * Disable nodes that are not intersections
	 * 
	 * @param tnsGraph
	 */
	// TODO rename
	public static void simplify(TnsGraph tnsGraph) {
		// TODO: 19-Sep-16 do not simplify edges with different speedLimit
		// TODO: 19-Sep-16 rewrite simplification to run it after adding edges in opposite direction

		// Simplify while there are only edges in one direction
		int count = 0;
		int count2 = 0;
		Set<TnsNode> mergedNodes = new HashSet<TnsNode>();

		for (TnsNode node : tnsGraph.getAllNodes()) {
			Collection<TnsEdge> inEdges = tnsGraph.getAllNodeIncomingEdges(node.id);
			Collection<TnsEdge> outEdges = tnsGraph.getAllNodeOutgoingEdges(node.id);

			if (inEdges.size() == 1 && outEdges.size() == 1) {
				TnsEdge inEdge = inEdges.iterator().next();
				TnsEdge outEdge = outEdges.iterator().next();

				// We do not want to create self loop
				if (outEdge.toId == inEdge.fromId) {
					continue;
				}

				// According to implementation of equals in AbstractMap we are
				// able to do this
				if (inEdge.getTags().equals(outEdge.getTags())
						&& !tnsGraph.containsEdge(inEdge.fromId, outEdge.toId)) {
					count2++;

					List<TnsNode> via = new ArrayList<>(inEdge.getVia());
					via.add(node);
					via.addAll(outEdge.getVia());

					Map<String, String> tags = new HashMap<String, String>(inEdge.getTags());

					Collection<Long> wayIds = new HashSet<Long>(inEdge.getWayIds());
					wayIds.addAll(outEdge.getWayIds());

					TnsEdge newEdge = new TnsEdge(inEdge.fromId, via, outEdge.toId, wayIds, tags);

					// log.debug(count2 + " - merged two edges! new Edge " +
					// newEdge.toString() + ", original ones: "
					// + inEdge.toString() + ", " + outEdge.toString());

					mergedNodes.add(node);
//					log.debug(node.id);
					tnsGraph.removeEdge(inEdge);
					tnsGraph.removeEdge(outEdge);
					tnsGraph.addEdge(newEdge);

				}
				count++;
			}
		}

		log.info("# nodes with inDegree and outDegree equals to 1: " + count);
		log.info("# nodes with inDegree and outDegree equals to 1 and its tags are the same: "
				+ count2);
	}

	// private static boolean areSetOfTagsEqual(Map<String, String> tags1,
	// Map<String, String> tags2) {
	// if (tags1.size() != tags2.size()) {
	// return false;
	// }
	// HashMap<String, String> map = new HashMap<>();
	// map.equals(o)
	// for (String tag : tags1) {
	// if (!tags2.contains(tag)) {
	// return false;
	// }
	// }
	// return true;
	// }

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param generalGraphBuilder
	 */
	public static void createEdgesInOppositeDirection(TnsGraph generalGraphBuilder,
			OneWay oneWayChecker) {
		// Process one-way streets
		// TODO use information from route=bicycle role=backward/forward?
		// TODO spracuj takisto ways v TnsGraph

		Collection<TnsEdge> newTnsEdges = new ArrayList<TnsEdge>();
		Collection<TnsEdge> generalEdgesToRemove = new ArrayList<TnsEdge>();
//		Pattern onlyOneWayEdges = Pattern
//				.compile(".*highway::(motorway_link|trunk_link|primary|primary_link|secondary|secondary_link).*");

		for (TnsEdge generalEdge : generalGraphBuilder.getAllEdges()) {

			if (!oneWayChecker.isOneWay(generalEdge)) {
				TnsEdge oppositeTnsEdge = TnsEdgeFactory.getOppositeTnsEdge(generalEdge);

				newTnsEdges.add(oppositeTnsEdge);

				if (oneWayChecker.isOneWayInOppositeDirection(generalEdge)) {
					generalEdgesToRemove.add(generalEdge);

					// String edgeJointTags = generalEdge.getTags().toString();
					//
					// if (!onlyOneWayEdges.matcher(edgeJointTags).matches()) {
					// oppositeTnsEdge.addTag("way::oneway::opposite");
					// newTnsEdges.add(oppositeTnsEdge);
					// }
				}
			} else {
				// Is oneway, creates opposite edge if it does not contain tag
				// highway::motorway_link|trunk_link|primary|primary_link|secondary|secondary_link
				// String edgeJointTags = generalEdge.getTags().toString();
				//
				// if (!onlyOneWayEdges.matcher(edgeJointTags).matches()) {
				// TnsEdge oppositeTnsEdge = TnsEdgeFactory
				// .getOppositeTnsEdge(generalEdge);
				//
				// oppositeTnsEdge.addTag("way::oneway::opposite");
				// newTnsEdges.add(oppositeTnsEdge);
				// }
			}
		}

		log.info("How many edges before taking care of oneway? "
				+ generalGraphBuilder.getAllEdges().size());

		log.info("How many edges are we deleting? " + generalEdgesToRemove.size());
		generalGraphBuilder.removeEdges(generalEdgesToRemove);

		log.info("How many edges are we adding? " + newTnsEdges.size());
		generalGraphBuilder.addEdges(newTnsEdges);

		log.info("So far we have how many edges? " + generalGraphBuilder.getAllEdges().size());
		// Cisla nesedia -- v grafu uz existovali niektore hrany, ktore sme
		// vytvorily pocas spracovavanie smerov hran

		// Remove one-way tags
		// for (TnsEdge generalEdge : generalGraphBuilder.getAllEdges()) {
		//
		// Collection<String> tags = generalEdge.getTags();
		// log.debug("tags pred vymazanim: " + tags);
		// oneWayChecker.removeOneWayTags(tags);
		// log.debug("tags po vymazani tagov: " + tags);
		// }
	}

	public static void grabTheBiggestSCC(TnsGraph tnsGraph) {
		Set<Integer> nodeIds = new HashSet<>();
		Map<Integer, Set<Integer>> edges = new HashMap<>();
		for(TnsNode node: tnsGraph.getAllNodes()) {
			int nodeId = (int) node.id;
			nodeIds.add(nodeId);
			Collection<TnsEdge> outEdges = tnsGraph.getAllNodeOutgoingEdges(nodeId);

			Set<Integer> outNodeIds = new HashSet<>();
			for (TnsEdge edge: outEdges) {
				outNodeIds.add((int) edge.toId);
			}
			edges.put(nodeId, outNodeIds);
		}

		HashSet<Integer> biggestSCC = StronglyConnectedComponentsFinder
				.getStronglyConnectedComponentsSortedBySize(nodeIds, edges).get(0);


		Set<Long> nodeIdsToRemove = new HashSet<>();
		tnsGraph.getAllNodes().stream().filter(node -> !biggestSCC.contains((int) node.id)).forEach(node -> {
			nodeIdsToRemove.add(node.id);
		});
		nodeIdsToRemove.forEach(tnsGraph::removeNode);
	}

	public static void extendBySuperchargers(TnsGraph tnsGraph, ArrayList<Supercharger> superchargers) {

		Collection<TnsNode> allNodes = tnsGraph.getAllNodes();

		// viaNodes has automatically in and out edge fromNode->viaNode->toNode
		// TODO: 08-Dec-16 consider to not map all vias .. can be motorways can be filtered
		Map<TnsNode, Collection<TnsEdge>> viaNodesWithMappedEdges = mapEdgesOnViaNodes(tnsGraph);

		//fill KDTree
		KDTree<TnsNode> kdTreeMotorwaysFiltered = new KDTree<>(2, new GPSLocationKDTreeResolver<>(), -1);

		allNodes.stream()
				.filter(node -> !hasMotorwayEdges(node, tnsGraph, viaNodesWithMappedEdges))
				.forEach(kdTreeMotorwaysFiltered::insert);

		Set<Long> viaNodesIds = new HashSet<>();
		for (TnsNode tnsNode : viaNodesWithMappedEdges.keySet()) {

			viaNodesIds.add(tnsNode.id);

			if (!hasMotorwayEdges(tnsNode, tnsGraph, viaNodesWithMappedEdges)) {
				kdTreeMotorwaysFiltered.insert(tnsNode);
			}
		}

		long supNodeId;
		double maxDistance = 10*1E3;
		for(Supercharger supercharger: superchargers) {

			TnsNode nearestNode = findNearestNodeOutsideMotorway(supercharger.location, kdTreeMotorwaysFiltered, tnsGraph, viaNodesWithMappedEdges);

			if (nearestNode == null) {
				log.info("Nearest node not found. " + supercharger);
				continue;
			}

			int distance = GPSLocationTools.computeDistance(supercharger.location, nearestNode);
			if (distance > maxDistance) {
				log.info(String.format("Charger location is too far from road network. "
						+ "Distance: %d (max=%f); %s. NearestNode is %s.", distance, maxDistance, supercharger, nearestNode));
				continue;
			}

			while(true) {
				supNodeId = NewNodeIdUtil.getNewNodeId();
				if (!tnsGraph.containsNode(supNodeId) && !viaNodesIds.contains(supNodeId)) break;
			}

			if (!tnsGraph.containsNode(nearestNode.id)) {

				tnsGraph.addNode(nearestNode);

				Collection<TnsEdge> edgesToDivide = viaNodesWithMappedEdges.get(nearestNode);
				for (TnsEdge tnsEdge : edgesToDivide) {
					List<TnsNode> via = tnsEdge.getVia();
					int dividerIndex = via.indexOf(nearestNode);

					List<TnsNode> outEdgeViaNodes = Collections.emptyList();

					if (dividerIndex != via.size()-1) {
						outEdgeViaNodes = via.subList(dividerIndex+1, via.size());
					}


					TnsEdge outEdge = new TnsEdge(nearestNode.id, outEdgeViaNodes, tnsEdge.toId, tnsEdge.getWayIds(), tnsEdge.getTags());
					tnsGraph.addEdge(outEdge);

					List<TnsNode> inEdgeViaNodes = Collections.emptyList();
					if (dividerIndex != 0) {
						inEdgeViaNodes = via.subList(0, dividerIndex);
					}

					TnsEdge inEdge = new TnsEdge(tnsEdge.fromId, inEdgeViaNodes, nearestNode.id, tnsEdge.getWayIds(), tnsEdge.getTags());
					tnsGraph.addEdge(inEdge);

					if (computeDistanceAsDouble(outEdge, tnsGraph) <= 0 || computeDistanceAsDouble(inEdge, tnsGraph) <= 0) {
						log.error("new divided edge is too small!");
					}
				}
				tnsGraph.removeEdges(edgesToDivide);
			}

			Map<String, String> tags = new HashMap<>();
			tags.put("supercharger", "yes");
			tnsGraph.addNode(new TnsNode(supNodeId, supercharger.sourceId, supercharger.location, tags));

			// TODO: 05-Sep-16 Check if viaNodes to nearest GOOD node in simplified graph is needed && maybe use tag ROAD_TYPE
			tags = new HashMap<>();
			tags.put("supercharger", "link_in");
			TnsEdge inEdge = TnsEdgeFactory.getTnsEdge(nearestNode.id, supNodeId, tags);
			tnsGraph.addEdge(inEdge);
			tags = new HashMap<>();
			tags.put("supercharger", "link_out");
			TnsEdge outEdge = TnsEdgeFactory.getTnsEdge(supNodeId, nearestNode.id, tags);
			tnsGraph.addEdge(outEdge);

			if (computeDistanceAsDouble(outEdge, tnsGraph) <= 0 || computeDistanceAsDouble(inEdge, tnsGraph) <= 0) {
				log.error("new divided edge is too small!");
			}
		}

	}

	private static TnsNode findNearestNodeOutsideMotorway(GPSLocation location, KDTree<TnsNode> kdTree,
			TnsGraph tnsGraph, Map<TnsNode, Collection<TnsEdge>> viaNodesWithMappedEdges) {

		return kdTree.getNearestNode(GPSLocationKDTreeResolver.getCoords(location));

	}

	private static boolean hasMotorwayEdges(TnsNode nNearestNode, TnsGraph tnsGraph, Map<TnsNode, Collection<TnsEdge>> viaNodesWithMappedEdges) {

		if (tnsGraph.containsNode(nNearestNode.id)) {

			Collection<TnsEdge> inEdges = tnsGraph.getAllNodeIncomingEdges(nNearestNode.id);
			if (isMotorwayEdgeInList(inEdges)) {
				return true;
			}

			Collection<TnsEdge> outEdges = tnsGraph.getAllNodeOutgoingEdges(nNearestNode.id);
			if (isMotorwayEdgeInList(outEdges)) {
				return true;
			}
		} else {
			Collection<TnsEdge> tnsEdges = viaNodesWithMappedEdges.get(nNearestNode);
			if (isMotorwayEdgeInList(tnsEdges)) {
				return true;
			}

		}
		return false;

	}

	private static boolean isMotorwayEdgeInList(Collection<TnsEdge> edges) {

		String highwayTag = "highway";
		String motorwayValue = "motorway";
		String motorwayLinkValue = "motorway_link";

		for (TnsEdge inEdge : edges) {
			Map<String, String> tags = inEdge.getTags();
			if (tags.containsKey(highwayTag)) {
				if (tags.get(highwayTag).equals(motorwayValue) || tags.get(highwayTag).equals(motorwayLinkValue)) {
					return true;
				}
			}
		}
		return false;
	}

	private static Map<TnsNode, Collection<TnsEdge>> mapEdgesOnViaNodes(TnsGraph graph) {

		Map<TnsNode, Collection<TnsEdge>> contractedNodes = new HashMap<>();

		for (TnsEdge edge : graph.getAllEdges()) {

			for (TnsNode viaNode : edge.getVia()) {
				Collection<TnsEdge> contractedNode = contractedNodes.get(viaNode);

				if (contractedNode == null) {
					contractedNode = new HashSet<>();
					contractedNodes.put(viaNode, contractedNode);
				}

				contractedNode.add(edge);
			}
		}

		return contractedNodes;
	}

	public static double computeDistanceAsDouble(TnsEdge edge, TnsGraph tnsGraph) {
		TnsNode fromNode = tnsGraph.getNode(edge.fromId);
		double totalLength = 0, length;

		TnsNode toNode;
		for (TnsNode tnsNode : edge.getVia()) {
			length = GPSLocationTools.computeDistanceAsDouble(fromNode, tnsNode);
			totalLength += length;
			fromNode = tnsNode;

		}
		toNode = tnsGraph.getNode(edge.toId);
		if (fromNode == null) {
			log.info(fromNode);
		}
		length = GPSLocationTools.computeDistanceAsDouble(fromNode, toNode);
		totalLength += length;
		return totalLength;
	}
}
