package cz.cvut.felk.its.zonebuilder.tns.util;

import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.MultiEvEdge;
import javafx.util.Pair;

import java.util.*;

/**
 * Created by Tomas on 08-May-16.
 */
@Deprecated
public class MultiEdgeBuilder<TEdge extends EvEdge> {
    HashMap<Pair<Integer, Integer>, List<TEdge>> edgesByFromTos;
    HashMap<Pair<Integer, Integer>, int[]> minConsFunctions;

    Set<TEdge> allAddedEdges;

    public MultiEdgeBuilder() {
        edgesByFromTos = new HashMap<>();
        minConsFunctions = new HashMap<>();
        allAddedEdges = new HashSet<>();
    }

    //if edge duplicity occurs, start by adding edges with smallest stateToCharge
    public void addEvEdge(TEdge edge, int stateToChargeId) {
        if (allAddedEdges.contains(edge)) return;
        allAddedEdges.add(edge);

        if (edge.fromId == edge.toId) return;
//        int[] fromToIds = new int[]{edge.fromId, edge.toId};
        Pair<Integer, Integer> fromToIds = new Pair<>(edge.fromId, edge.toId);
        List<TEdge> edges = edgesByFromTos.get(fromToIds);
        int[] minConsFunction;
        if (edges == null) {
            edges = new ArrayList<>();
            minConsFunction = new int[ChargingModel.STATES_TO_CHARGE.length];
            edgesByFromTos.put(fromToIds, edges);
            minConsFunctions.put(fromToIds, minConsFunction);
            Arrays.fill(minConsFunction, -1);
        } else {
            minConsFunction = minConsFunctions.get(fromToIds);
        }

//        if (!allAddedEdges.contains(edge)){
//            edges.add(edge);
//            allAddedEdges.add(edge);
//        }

        edges.add(edge);
        minConsFunction[stateToChargeId] = edges.size()-1;
    }

    public void addBackwardEvEdge(TEdge edge, int stateOfCharge) {
        //TODO implement [MultiEdgeBuilder:addBackwardEvEdge]
        throw new UnsupportedOperationException ("[MultiEdgeBuilder:addBackwardEvEdge] has not been implemented yet");
    }

    private void addEdge(TEdge edge, int stateToChargeId, boolean isBackwardEdge) {
        //TODO implement [MultiEdgeBuilder:addEdge]
        throw new UnsupportedOperationException ("[MultiEdgeBuilder:addEdge] has not been implemented yet");
    }

    public Set<MultiEvEdge<TEdge>> createMultiEdges() {
        Set<MultiEvEdge<TEdge>> multiEvEdges = new HashSet<>(edgesByFromTos.size());
        for (Pair<Integer,Integer> fromToIds: edgesByFromTos.keySet()) {
            int[] minConsFunction = minConsFunctions.get(fromToIds);
            for (int i = 1; i < minConsFunction.length; i++) {
                if (minConsFunction[i] < 0 && minConsFunction[i-1] > -1){
                    minConsFunction[i] = minConsFunction[i-1];
                }
            }
//            System.out.println(fromToIds);
//            System.out.println(Arrays.toString(minConsFunction));
//            System.out.println(edgesByFromTos.get(fromToIds));
            multiEvEdges.add(new MultiEvEdge<>(fromToIds.getKey(), fromToIds.getValue(), 0, edgesByFromTos.get(fromToIds),
                    minConsFunction));
        }
        return multiEvEdges;
    }

    public HashMap<Integer, EvEdge> createFMMultiEdges() {
        HashMap<Integer, EvEdge> multiEdges = new HashMap<>(edgesByFromTos.size());
        for (Pair<Integer,Integer> fromToIds: edgesByFromTos.keySet()) {
//            System.out.println(fromToIds);
//            System.out.println(Arrays.toString(minConsFunctions.get(fromToIds)));
//            System.out.println(edgesByFromTos.get(fromToIds));
            multiEdges.put(fromToIds.getValue(), new MultiEvEdge<>(fromToIds.getKey(), fromToIds.getValue(), 0, edgesByFromTos.get(fromToIds),
                    minConsFunctions.get(fromToIds)));
        }
        return multiEdges;
    }

    public HashMap<Integer, EvEdge> createLMMultiEdges() {
        HashMap<Integer, EvEdge> multiEdges = new HashMap<>(edgesByFromTos.size());
        for (Pair<Integer,Integer> fromToIds: edgesByFromTos.keySet()) {
            int[] minConsFunction = minConsFunctions.get(fromToIds);
            for (int i = 1; i < minConsFunction.length; i++) {
                if (minConsFunction[i] < 0 && minConsFunction[i-1] > -1){
                    minConsFunction[i] = minConsFunction[i-1];
                }
            }
//            System.out.println(fromToIds);
//            System.out.println(Arrays.toString(minConsFunction));
//            System.out.println(edgesByFromTos.get(fromToIds));
            multiEdges.put(fromToIds.getKey(), new MultiEvEdge<>(fromToIds.getKey(), fromToIds.getValue(), 0, edgesByFromTos.get(fromToIds),
                    minConsFunction));
        }
        return multiEdges;
    }
}
