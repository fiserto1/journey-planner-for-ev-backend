package cz.cvut.felk.its.zonebuilder;

import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.Graph;
import cz.agents.basestructures.GraphBuilder;
import cz.agents.geotools.GPSLocationKDTreeResolver;
import cz.agents.geotools.GPSLocationTools;
import cz.agents.geotools.KDTree;
import cz.cvut.felk.its.evstructures.chargers.Supercharger;
import cz.cvut.felk.its.evstructures.chargers.SuperchargerFileLoader;
import cz.cvut.felk.its.evstructures.*;
import cz.cvut.felk.its.zonebuilder.tns.TnsEdge;
import cz.cvut.felk.its.zonebuilder.tns.TnsEdgeFactory;
import cz.cvut.felk.its.zonebuilder.tns.TnsGraph;
import cz.cvut.felk.its.zonebuilder.tns.TnsNode;
import cz.cvut.felk.its.zonebuilder.tns.util.NewNodeIdUtil;
import cz.cvut.felk.its.zonebuilder.tns.util.OneWay;
import cz.cvut.felk.its.zonebuilder.tns.util.TnsGraphUtils;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.*;

/**
 * Created by Tomas on 18.11.2015.
 */
public class OsmToGraphHandler extends DefaultHandler {
    
    private final Logger log = Logger.getLogger(OsmToGraphHandler.class);
    
    private KDTree<TnsNode> kdTree;
    private final TnsGraph tnsGraph;

//    private int nodeId = 0;
    private Map<String, String> elementTags;

    // Node - id, latE6, lonE6, elevation (in tags)
    private long nodeOsmId;
    private double lat;
    private double lon;

    // Edge - fromNodeId, toNodeId, wayId, length (need to be computed)
    private List<Long> wayMembers;
    private long wayId;
	private String superchargersFilepath;
	private int srid;

	public OsmToGraphHandler(String superchargersFilepath, int srid) {
		this.superchargersFilepath = superchargersFilepath;
		this.srid = srid;
		kdTree = new KDTree<>(2, new GPSLocationKDTreeResolver<>(), -1);
        tnsGraph = new TnsGraph();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {

        switch (qName) {
            case "node":
                nodeOsmId = Long.parseLong(attributes.getValue("id"));
                lat = Double.parseDouble(attributes.getValue("lat"));
                lon = Double.parseDouble(attributes.getValue("lon"));
                elementTags = new HashMap<>();
                break;

            case "way":
                wayMembers = new ArrayList<>();
                elementTags = new HashMap<>();
                wayId = Long.parseLong(attributes.getValue("id"));
                break;

            case "nd":
                wayMembers.add(tnsGraph.getNodeIdByNodeSourceId(Long.parseLong(attributes.getValue("ref"))));
                break;

            case "tag":
                String tagKey = attributes.getValue("k");
                String tagValue = attributes.getValue("v");
                elementTags.put(tagKey, tagValue);
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("node")) {
            int elevation = (int) Double.parseDouble(elementTags.get("elevation"));

            GPSLocation location = GPSLocationTools.createGPSLocation(lat, lon, elevation, srid);
            TnsNode node = new TnsNode(NewNodeIdUtil.getNewNodeId(), nodeOsmId, location, elementTags);
            kdTree.insert(node);
            tnsGraph.addNode(node);

        } else if (qName.equals("way")) {
            Iterator<Long> wayMemberIdIterator = wayMembers.iterator();
            long fromNodeId = wayMemberIdIterator.next();
            long toNodeId;

            while (wayMemberIdIterator.hasNext()) {
                toNodeId = wayMemberIdIterator.next();
                TnsEdge edge = TnsEdgeFactory.getTnsEdge(fromNodeId, toNodeId, wayId, elementTags);
                tnsGraph.addEdge(edge);
                fromNodeId = toNodeId;

            }
        }
    }

    private void printTns(String graphDesc) {
        log.info(String.format("%s [Nodes: %d, Edges: %d]",
				graphDesc, tnsGraph.getAllNodes().size(), tnsGraph.getAllEdges().size()));
    }

    public Graph<EvNode, EvEdge> getGraph() {

        optimizeGraph();

		//Parse supercharger data
		SuperchargerFileLoader superchargerFileLoader = new SuperchargerFileLoader();
		ArrayList<Supercharger> superchargers = superchargerFileLoader
                .parseSuperchargers(superchargersFilepath, "chargers", srid);

		TnsGraphUtils.extendBySuperchargers(tnsGraph, superchargers);

        return buildGraphFromTns(superchargers);
    }

    private Graph<EvNode, EvEdge> buildGraphFromTns(ArrayList<Supercharger> superchargers) {
		GraphBuilder<EvNode, EvEdge> graphBuilder = new GraphBuilder<>();
		Collection<TnsNode> allNodes = tnsGraph.getAllNodes();
        Collection<TnsEdge> allEdges = tnsGraph.getAllEdges();
        HashMap<Long, Integer> oldToNewNodeId = new HashMap<>();

		//Put superchargers to hashmap to get them simply by sourceId
		HashMap<Integer, Supercharger> supBySourceId = new HashMap<>();
		for (Supercharger supercharger : superchargers) {
			supBySourceId.put(supercharger.sourceId, supercharger);
		}

        //ADD NODES
        int counter=0;
        int newNodeId = 0;
        for (TnsNode node: allNodes) {
            if (tnsGraph.getAllNodeOutgoingEdges(node.id).size() != 0
                    || tnsGraph.getAllNodeIncomingEdges(node.id).size() != 0) {
                oldToNewNodeId.put(node.id, newNodeId);
				Supercharger supercharger = null;
				if (node.getTags().containsKey("supercharger") && node.getTags().get("supercharger").equals("yes")) {
					supercharger = supBySourceId.get((int) node.sourceId);
				}
                graphBuilder.addNode(new EvNode(newNodeId, node.sourceId, node, supercharger));
				newNodeId++;
            } else {
                counter++;
            }
        }
        log.info("Nodes without edges: " + counter);

		int unfeasiblePathCounter = 0;
		//ADD EDGES
		//        int newEdgeId = 0;
		for (TnsEdge edge: allEdges) {
			TnsNode fromNode = tnsGraph.getNode(edge.fromId);
			TnsNode toNode = tnsGraph.getNode(edge.toId);

			// TODO: 06-Sep-16 check if supercharger in-out-edges are calculated properly - maybe set a static values to length etc.
			RoadType roadType = TnsGraphUtils.assingEdgeRoadType(edge.getTags());
			double speedLimitMs = roadType.getSpeedLimitMs();
			//            double speedLimitMs = TnsGraphUtils.computeSpeedLimit(edge.getTags());
			double length = TnsGraphUtils.computeDistanceAsDouble(edge, tnsGraph);
			double travelTimeMillis = GraphTools.convertToE3Format(length/speedLimitMs);
			//            int consumption = ConsumptionProfileUtils.computeEdgeConsumption(edge, tnsGraph);
			ConsumptionProfile consumptionProfile = ConsumptionProfileUtils.computeConsumptionProfile(edge, tnsGraph);
			if (consumptionProfile == null) {
				unfeasiblePathCounter++;
				//unfeasible path - this could create nodes without edges
				continue;
			}

            List<TnsNode> viaTnsNodes = edge.getVia();
            List<GPSLocation> viaNodes = new ArrayList<>(viaTnsNodes.size());

			// TODO: 10-Apr-16 think about viaIds as nodes and insert them into graph to locate them while computing FM,LM - compare .ser file size

            for (TnsNode tnsNode: viaTnsNodes) {
//                viaNodes.add(new ViaNode(tnsNode, newEdgeId));
                viaNodes.add(GPSLocationTools.createGPSLocation(tnsNode.getLatitude(), tnsNode.getLongitude(),
						tnsNode.getElevation(), srid));
            }

			EvEdge pEdge = new EvEdge(oldToNewNodeId.get(fromNode.id), oldToNewNodeId.get(toNode.id), (int) length,
                    roadType, (int) travelTimeMillis, consumptionProfile, viaNodes);

            graphBuilder.addEdge(pEdge);
//            newEdgeId++;

        }

		log.info("# of unfeasible paths: " + unfeasiblePathCounter);

		return graphBuilder.createGraph();
    }


	private void optimizeGraph() {
//        printTns();
//        TnsGraphUtils.checkTagsWithExpectedIntegerValue(tnsGraph);
        printTns("Start optimizing: ");
        TnsGraphUtils.repairNodesWithNegativeElevation(tnsGraph, kdTree);
        printTns("After repair nodes with negative elevation: ");
        TnsGraphUtils.deleteEdgesWithZeroLength(tnsGraph);
        printTns("After delete zero-length edges: ");
        TnsGraphUtils.simplify(tnsGraph); // TODO: 19-Sep-16 run after adding supercharger nodes and in/out edges
        printTns("After simplification: ");
        TnsGraphUtils.createEdgesInOppositeDirection(tnsGraph, new OneWay());
        printTns("After addition ways in opposite direction: ");
		TnsGraphUtils.grabTheBiggestSCC(tnsGraph);
		printTns("The biggest SCC: ");
    }

    
    

}
