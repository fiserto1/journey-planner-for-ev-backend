package cz.cvut.felk.its.zonebuilder;

/**
 * Created by Tomas on 13-Jan-16.
 */
public class GraphTools {
    public static final int GERMANY_SRID = 25832;
    public static final int BERLIN_SRID = 3068;


    public static int convertToE3Format(double travelTime) {
        return (int) Math.round(travelTime * 1E3);
    }

    public static int convertToE6Format(double travelTime) {
        return (int) Math.round(travelTime * 1E6);
    }

    public static int convertFromE3Format(double travelTimeMillis) {

        return (int) ((travelTimeMillis)/1E3);
    }

    public static double convertFromE6Format(double coord) {
        return coord/1E6;
    }
    //
//    public static Graph<EvNode, PlanningEdge> simplifyGraph(Graph<EvNode, PlanningEdge> graph) {
//
//        GraphBuilder<EvNode, PlanningEdge> graphBuilder = new GraphBuilder<>();
//        HashMap<Integer, ArrayList<PlanningEdge>> newSimplifiedEdges = new HashMap<>();
//        int numOfNodes = graph.getAllNodes().size();
//        boolean[] visited = new boolean[numOfNodes];
//        int[] oldIdToNewId = new int[numOfNodes];
//
//        //remove nodes which has exactly 1out and 1in edge
//        int newId = 0;
//        for (EvNode node: graph.getAllNodes()) {
//            if (!is1Out1InNode(graph, node.id)) {
//                oldIdToNewId[node.id] = newId;
//                graphBuilder.addNode(new EvNode(newId, node.sourceId, node, node.getCharger()));
//                newId++;
//            }
//        }
//
//        //add simplified edges and return useless edges
//        Set<PlanningEdge> uselessEdges = addSimplifiedEdges(graph, graphBuilder, visited, oldIdToNewId, newSimplifiedEdges);
//
//        //add useful edges
//        for (PlanningEdge pEdge: graph.getAllEdges()) {
//            if (!uselessEdges.contains(pEdge)) {
//                EvEdge newEdge = new EvEdge(oldIdToNewId[pEdge.fromId], oldIdToNewId[pEdge.toId], (EvEdge) pEdge);
//                protectEdgeDuplicity(newSimplifiedEdges, newEdge);
//            }
//        }
//
//        for (ArrayList<PlanningEdge> edges: newSimplifiedEdges.values()) {
//            for (PlanningEdge e: edges) {
//                graphBuilder.addEdge(e); // dont use addEdges - it remove edges with same fromToIds
//            }
//        }
//
//        return graphBuilder.createGraph();
//    }
//
//    private static boolean is1Out1InNode(Graph<EvNode, PlanningEdge> graphLvl1, int nodeId) {
//        return graphLvl1.getInEdges(nodeId).size() == 1
//                && graphLvl1.getOutEdges(nodeId).size() == 1
//                && graphLvl1.getNode(nodeId).getCharger() == null;
//    }
//
//    private static Set<PlanningEdge> addSimplifiedEdges(Graph<EvNode, PlanningEdge> graph,
//                                                        GraphBuilder<EvNode, PlanningEdge> graphBuilder,
//                                                        boolean[] visited, int[] oldIdToNewId, HashMap<Integer,
//                                                    ArrayList<PlanningEdge>> newSimplifiedEdges) {
//        Set<PlanningEdge> uselessEdges = new HashSet<>();
//
//        for (EvNode node: graph.getAllNodes()) {
//            if (!visited[node.id]) {
//                if (is1Out1InNode(graph, node.id)) {
//
//                    visited[node.id] = true;
//                    LinkedList<GPSLocation> routeNodeCoords = new LinkedList<>();
//                    LinkedList<Integer> routePartConsumptions = new LinkedList<>();
//                    LinkedList<Integer> routePartLengths = new LinkedList<>();
//                    routeNodeCoords.add(node);
//
//                    //find start node- half edge
//                    int[] startHalfEdge = findStartOrEndOfSimplEdge(true, node.id, graph, uselessEdges, routeNodeCoords, routePartConsumptions, routePartLengths, visited);
//
//                    //find end node - half edge
//                    int[] endHalfEdge = findStartOrEndOfSimplEdge(false, node.id, graph, uselessEdges, routeNodeCoords, routePartConsumptions, routePartLengths, visited);
//
//                    int length = startHalfEdge[1] + endHalfEdge[1];
//                    int travelTimeMillis = startHalfEdge[2] + endHalfEdge[2];
//                    int consumption = startHalfEdge[3] + endHalfEdge[3];
//
//
//                    EvEdge newEdge = new EvEdge(oldIdToNewId[startHalfEdge[0]], oldIdToNewId[endHalfEdge[0]],
//                            length, RoadType.UNKNOWN, travelTimeMillis, consumption, null, routeNodeCoords);
//                    protectEdgeDuplicity(newSimplifiedEdges, newEdge);
//                }
//            }
//        }
//        return uselessEdges;
//    }
//
//    //return array with end or start node ID at index 0 and half edge parameters
//    //start-true, end-false
//    private static int[] findStartOrEndOfSimplEdge(boolean start, int nodeId, Graph<EvNode, PlanningEdge> graph,
//                                                   Set<PlanningEdge> uselessEdges, LinkedList<GPSLocation> routeNodeCoords,
//                                                   LinkedList<Integer> routePartConsumptions, LinkedList<Integer> routePartLengths, boolean[] visited) {
//        EvEdge inOrOutEdge;
//        if (start) {
//            inOrOutEdge = (EvEdge) graph.getInEdges(nodeId).get(0);
////            routePartConsumptions.addAll(0, inOrOutEdge.routePartConsumptions);
////            routePartLengths.addAll(0, inOrOutEdge.routePartConsumptions);
//        } else {
//            inOrOutEdge = (EvEdge) graph.getOutEdges(nodeId).get(0);
////            routePartConsumptions.addAll(inOrOutEdge.routePartConsumptions);
////            routePartLengths.addAll(inOrOutEdge.routePartLengths);
//        }
//
////        EvEdge incomingEdge = (EvEdge) inEdge;
//        uselessEdges.add(inOrOutEdge);
//
//        int length = inOrOutEdge.length;
//        int travelTimeMillis = inOrOutEdge.travelTimeMillis;
//        int consumption = inOrOutEdge.consumption;
//        int prevOrNextNodeId;
//        if (start) {
//            prevOrNextNodeId = inOrOutEdge.fromId;
//        } else {
//            prevOrNextNodeId = inOrOutEdge.toId;
//        }
//
//        while (is1Out1InNode(graph, prevOrNextNodeId) && !visited[prevOrNextNodeId]) {
//            visited[prevOrNextNodeId] = true;
//            EvNode prevOrNextNode = graph.getNode(prevOrNextNodeId);
//            if (start) {
//                routeNodeCoords.addFirst(prevOrNextNode);
//                inOrOutEdge = (EvEdge) graph.getInEdges(prevOrNextNodeId).get(0);
////                routePartConsumptions.addAll(0, inOrOutEdge.routePartConsumptions);
////                routePartLengths.addAll(0, inOrOutEdge.routePartLengths);
//            } else {
//                routeNodeCoords.addLast(prevOrNextNode);
//                inOrOutEdge = (EvEdge) graph.getInEdges(prevOrNextNodeId).get(0);
////                routePartConsumptions.addAll(inOrOutEdge.routePartConsumptions);
////                routePartLengths.addAll(inOrOutEdge.routePartLengths);
//            }
//            uselessEdges.add(inOrOutEdge);
//            length += inOrOutEdge.length;
//            travelTimeMillis += inOrOutEdge.travelTimeMillis;
//            consumption += inOrOutEdge.consumption;
//            if (start) {
//                prevOrNextNodeId = inOrOutEdge.fromId;
//            } else {
//                prevOrNextNodeId = inOrOutEdge.toId;
//            }
//        }
//        return new int[]{prevOrNextNodeId, length, travelTimeMillis, consumption};
//    }
//
//    private static void protectEdgeDuplicity(HashMap<Integer, ArrayList<PlanningEdge>> newSimplifiedEdges, EvEdge newEdge) {
//        // TODO: 27-Mar-16 this removes some edges with better consumption but worse traveTime because of same fromID and toID!!!
//        ArrayList<PlanningEdge> toEdges = newSimplifiedEdges.get(newEdge.fromId);
//        if (toEdges == null) {
//            toEdges = new ArrayList<>();
//            toEdges.add(newEdge);
//            newSimplifiedEdges.put(newEdge.fromId, toEdges);
//        } else {
//            boolean sameIds = false;
//            boolean remove= false;
//            PlanningEdge edgeToRemove = null;
//            for(PlanningEdge toEdge: toEdges) {
//                if (newEdge.toId == toEdge.toId) {
//                    sameIds = true;
//                    if (toEdge instanceof EvEdge) {
//                        EvEdge toEvEdge = (EvEdge) toEdge;
//                        if (newEdge.travelTimeMillis < toEvEdge.travelTimeMillis) {
//                            edgeToRemove = toEdge;
//                            remove = true;
//                        } else if (newEdge.consumption < toEvEdge.consumption ){
//                            System.out.println("Valid:" + toEdge);
//                            System.out.println("Removed:" + newEdge);
//                        }
//                    }
//                    break;
//                }
//            }
//            if (remove) {
//                toEdges.remove(edgeToRemove);
//                toEdges.add(newEdge);
//            }
//            if (!sameIds) {
//                toEdges.add(newEdge);
//            }
//        }
//    }
//
//    @Deprecated
//    public static int computeNextPartConsumption(int leavingStateOfCharge, int partConsumption) {
//        int condition = leavingStateOfCharge - partConsumption;
//        if (condition < ChargingModel.MIN_STATE_OF_CHARGE) {
//            return Integer.MAX_VALUE;
//        } else if (condition > ChargingModel.MAX_STATE_OF_CHARGE) {
//            return leavingStateOfCharge - ChargingModel.MAX_STATE_OF_CHARGE;
//        } else {
//            //in charging interval
//            return partConsumption;
//        }
//    }
//
//    public static void printNegativeEdges(Graph<EvNode, PlanningEdge> deserializedSuperGraph) {
//        int counterCons = 0;
//        int counterEdge = 0;
//        for(PlanningEdge planningEdge: deserializedSuperGraph.getAllEdges()) {
//            if (planningEdge instanceof EvEdge) {
//                EvEdge evEdge = (EvEdge) planningEdge;
//                //                for (int cons : evEdge.routePartConsumptions) {
//                //                    if (cons < 0) {
//                //                        counterCons++;
//                //                    }
//                //                }
//                if (evEdge.consumptionProfile.costs < 0) {
//                    counterEdge++;
//                }
//            }
//        }
//        System.out.println("Negative partCons: " + counterCons);
//        System.out.println("Negative edges: " + counterEdge);
//    }
}
