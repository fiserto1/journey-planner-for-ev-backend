package cz.cvut.felk.its.zonebuilder.tns;

import cz.cvut.felk.its.evstructures.EvEdge;

import java.util.ArrayList;

/**
 * Created by Tomas on 08-May-16.
 */
public class TnsMultiEdge {
    private int fromId;
    private int toId;
    private ArrayList<EvEdge> edges;
    private int[] minConsFunc; //index matches to incomingSoC, value matches to index of charger edges
    private int[] statesToCharge;
}
