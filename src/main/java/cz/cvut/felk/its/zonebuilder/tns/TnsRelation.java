package cz.cvut.felk.its.zonebuilder.tns;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public abstract class TnsRelation implements TnsObjectWithTags {

	public final long id;
	public final String type;
	protected List<TnsRelationMember> members;
	protected Map<String, String> tags;

	public TnsRelation(long relationId, String relationType, List<TnsRelationMember> members,
			Map<String, String> tags) {
		this.id = relationId;
		this.type = relationType;
		this.members = members;
		this.tags = tags;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param generalGraphBuilder
	 */
	public abstract void apply(TnsGraph generalGraph);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addTag(String tagKey, String tagValue) {
		tags.put(tagKey, tagValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addTags(Map<String, String> additionalTags) {
		tags.putAll(additionalTags);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeTag(String tagKey) {
		tags.remove(tagKey);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> getTags() {
		return Collections.unmodifiableMap(tags);
	}

	public List<TnsRelationMember> getMembers() {
		return Collections.unmodifiableList(members);
	}
}
