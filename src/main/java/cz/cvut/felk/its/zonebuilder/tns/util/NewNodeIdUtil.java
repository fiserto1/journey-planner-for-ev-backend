package cz.cvut.felk.its.zonebuilder.tns.util;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class NewNodeIdUtil {

	private static long newNodeId = 0;

	private NewNodeIdUtil() {
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public static long getNewNodeId() {
		return newNodeId++;
	}
}
