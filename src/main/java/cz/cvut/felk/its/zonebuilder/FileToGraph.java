package cz.cvut.felk.its.zonebuilder;

import cz.agents.basestructures.Graph;
import cz.agents.basestructures.GraphBuilder;
import cz.agents.basestructures.GraphStructure;
import cz.cvut.felk.its.evstructures.chargers.Supercharger;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.graph.EvGraph;
import cz.cvut.felk.its.routing.structures.graph.FMSearchGraph;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;
import cz.cvut.felk.its.utils.EdgePrecomputer;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.PlanningEdge;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by Tomas on 08.11.2015.
 */
public class FileToGraph {

    private int srid;

    public FileToGraph(int srid) {
        this.srid = srid;
    }

    public Graph<EvNode, EvEdge> buildPrecomputedGraph(String osmGraphFilepath, String superchargersFilepath)
            throws ParserConfigurationException, SAXException, IOException {

        // TODO: 08-May-16 SCC on TnsGraph -> add superchargers -> build graph
        Graph<EvNode, EvEdge> parsedGraph = parseOsmToGraph(osmGraphFilepath, superchargersFilepath);
        System.out.println("Parsed graph: " + parsedGraph);
//        Graph<EvNode, PlanningEdge> biggestSCCGraph = buildSCCGraph(parsedGraph);
//        System.out.println("Scc graph: " + biggestSCCGraph);
//        SuperchargerFileLoader csvParser = new SuperchargerFileLoader();
//        ArrayList<Supercharger> superchargers = csvParser.parseSuperchargers(superchargersFilepath, ";");

//        return buildDoubleGraph(biggestSCCGraph, superchargers);
        return buildDoubleGraph(parsedGraph, null);
    }

    public Graph<EvNode, EvEdge> parseOsmToGraph(String filepath, String superchargersFilepath)
            throws IOException, SAXException, ParserConfigurationException {

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        XMLReader xmlreader = parser.getXMLReader();
        File file = new File(filepath);
        OsmToGraphHandler osmToGraphHandler = new OsmToGraphHandler(superchargersFilepath, srid);

        xmlreader.setContentHandler(osmToGraphHandler);
        xmlreader.parse(new InputSource(new FileInputStream(file)));

        return osmToGraphHandler.getGraph();
    }

    public Graph<EvNode, PlanningEdge> buildSCCGraph(Graph<EvNode, PlanningEdge> graph) {
        GraphBuilder<EvNode, PlanningEdge> graphBuilder = new GraphBuilder<>();
        Set<Integer> biggestSCC = SCC.findBiggestSCC(graph);
//        Set<Integer> biggestSCC = StronglyConnectedComponentsFinder.getStronglyConnectedComponentsSortedBySize(graph).get(0);
        Map<Integer, Integer> oldIdToNewId = new HashMap<>();
        int newNodeId = 0;

        for (int oldNodeId: biggestSCC) {
            EvNode oldNode = graph.getNode(oldNodeId);
            graphBuilder.addNode(new EvNode(newNodeId, oldNode.sourceId, oldNode, oldNode.getCharger()));
            oldIdToNewId.put(oldNodeId, newNodeId);
            newNodeId++;
        }
        for (PlanningEdge oldPEdge: graph.getAllEdges()) {
            if (biggestSCC.contains(oldPEdge.fromId) && biggestSCC.contains(oldPEdge.toId)) {
                EvEdge oldEdge = (EvEdge) oldPEdge;
                graphBuilder.addEdge(new EvEdge(oldIdToNewId.get(oldEdge.fromId), oldIdToNewId.get(oldEdge.toId), oldEdge));
            }
        }
        return graphBuilder.createGraph();
    }


    public Graph<EvNode, EvEdge> buildDoubleGraph(Graph<EvNode, EvEdge> graphLvl1,
                                                      ArrayList<Supercharger> superchargers) {
        //ASSIGN SUPERCHARGERS TO NODES
        GraphBuilder<EvNode, EvEdge> graphBuilder = new GraphBuilder<>();
//        KDTree<EvNode> kdTree = new KDTree<>(2, new GPSLocationKDTreeResolver<>(),-1);

//        graphLvl1.getNumOfNodes().forEach(kdTree::insert);

//        for(Supercharger supercharger: superchargers) {
//            EvNode nearestNode = kdTree.getNearestNode(GPSLocationKDTreeResolver.getCoords(supercharger.location));
//            nearestNode.setCharger(supercharger);
//        }

        //add to second level graph nodes with same ID!!!
        Set<Integer> chargerNodeIds = new HashSet<>();
        int numOfChargers = 0;
        for (EvNode node: graphLvl1.getAllNodes()) {
            if (node.isCharger()) {
                chargerNodeIds.add(node.id);
                numOfChargers++;
            }
            graphBuilder.addNode(node);
        }
        for (EvEdge edge: graphLvl1.getAllEdges()) {
            graphBuilder.addEdge(edge);
        }


        EvGraph<EvNode, EvEdge> evGraph = new EvGraph<>(graphLvl1);
        Set<EvEdge> multiEdges = EdgePrecomputer.computeMultiEdgesBetweenSChargers(evGraph, graphLvl1, numOfChargers,
                chargerNodeIds);
        System.out.println("Num of multiedges: " + multiEdges.size());
//        System.out.println(multiEdges);
        graphBuilder.addEdges(multiEdges);

        return graphBuilder.createGraph();
    }

    public GraphStructure<EvNode, EvEdge> buildChargerGraph(GraphStructure<EvNode, EvEdge> graphLvl1) {

        GraphBuilder<EvNode, EvEdge> graphBuilder = new GraphBuilder<>();

        //add to charger nodes withs same ID as in evGraph!!!
        Set<Integer> chargerNodeIds = new HashSet<>();
        int numOfChargers = 0;
        for (EvNode node: graphLvl1.getAllNodes()) {
            if (node.isCharger()) {
                chargerNodeIds.add(node.id);
                numOfChargers++;
            }
            graphBuilder.addNode(node);
        }
        for (EvEdge edge: graphLvl1.getAllEdges()) {
            graphBuilder.addEdge(edge);
        }


        SearchGraph<EvLabel> searchGraph = new FMSearchGraph<>(graphLvl1);
        Set<EvEdge> multiEdges = EdgePrecomputer.computeMultiEdgesBetweenSChargers(searchGraph, graphLvl1, numOfChargers,
                chargerNodeIds);
        System.out.println("Num of multiedges: " + multiEdges.size());
        //        System.out.println(multiEdges);
        graphBuilder.addEdges(multiEdges);

        return graphBuilder.createGraph();
    }
}
