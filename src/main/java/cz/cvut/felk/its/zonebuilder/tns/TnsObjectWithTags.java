package cz.cvut.felk.its.zonebuilder.tns;

import java.util.Map;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public interface TnsObjectWithTags {

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tagKey
	 * @param tagValue
	 */
	public void addTag(String tagKey, String tagValue);

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param additionalTags
	 */
	public void addTags(Map<String, String> additionalTags);

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tagKey
	 */
	public void removeTag(String tagKey);

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public Map<String, String> getTags();
}
