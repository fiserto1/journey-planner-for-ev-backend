package cz.cvut.felk.its.zonebuilder.tns.util;

import cz.cvut.felk.its.zonebuilder.tns.TnsEdge;

import java.util.Map;
import java.util.regex.Pattern;


/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class OneWay {

	// private static final Pattern oneWay = Pattern.compile("way::oneway::yes|"
	// + "way::oneway::true|" + "way::oneway::1|" + "way::junction::roundabout|"
	// + "way::highway::motorway|" + "way::highway::motorway_link");
	//
	// // driving on this edges is allowed in opposite direction
	// private static final Pattern oneWayInOppositeDirection =
	// Pattern.compile("way::oneway::-1|"
	// + "way::oneway::reverse");
	//
	// private static final Pattern noOneWay =
	// Pattern.compile("way::oneway::no|"
	// + "way::oneway::false|" + "way::oneway::0|" + "way::oneway::reversible");

	private static final Pattern oneWayTagValuePattern = Pattern.compile("yes|true|1");
//	private static final Pattern junctionOneWayTagValuePattern = Pattern.compile("roundabout");
	private static final Pattern highwayOneWayTagValuePattern = Pattern.compile("motorway.*");

	private static final Pattern oppositeDirectionOneWayTagValuePattern = Pattern
			.compile("-1|reverse");

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tnsEdge
	 * @return
	 */
	public boolean isOneWay(TnsEdge tnsEdge) {
		Map<String, String> tags = tnsEdge.getTags();

		String onewayTagValue = tags.get("oneway");
//		String junctionTagValue = tags.get("junction");
		String highwayTagValue = tags.get("highway");

		return checkTagValue(onewayTagValue, oneWayTagValuePattern)
//				|| checkTagValue(junctionTagValue, junctionOneWayTagValuePattern)
				|| checkTagValue(highwayTagValue, highwayOneWayTagValuePattern);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edge
	 * @return
	 */
	public boolean isOneWayInOppositeDirection(TnsEdge edge) {

		String onewayTagValue = edge.getTags().get("oneway");

		return checkTagValue(onewayTagValue, oppositeDirectionOneWayTagValuePattern);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tagValue
	 * @param oneWayTagValuePattern
	 * @return
	 */
	private boolean checkTagValue(String tagValue, Pattern oneWayTagValuePattern) {
		if (tagValue != null && oneWayTagValuePattern.matcher(tagValue).matches()) {
			return true;
		}

		return false;
	}

	// /**
	// *
	// * TODO javadoc
	// *
	// * @param tag
	// * @return
	// */
	// protected boolean checkOneWay(String tag) {
	// return oneWay.matcher(tag).matches();
	// }
	//
	// /**
	// *
	// * TODO javadoc
	// *
	// * @param tag
	// * @return
	// */
	// protected boolean checkReverseOneWay(String tag) {
	// return oneWayInOppositeDirection.matcher(tag).matches();
	// }
	//
	// /**
	// *
	// * TODO javadoc
	// *
	// * @param tag
	// * @return
	// */
	// protected boolean checkNoOneWay(String tag) {
	// return noOneWay.matcher(tag).matches();
	// }

	// /**
	// *
	// * TODO javadoc
	// *
	// * @param tags
	// * @return
	// */
	// public Collection<String> removeOneWayTags(Collection<String> tags) {
	// Pattern oneWayKey = Pattern.compile(".*oneway.*");
	//
	// for (Iterator<String> it = tags.iterator(); it.hasNext();) {
	// String tag = it.next();
	//
	// if (oneWayKey.matcher(tag).matches()) {
	// it.remove();
	// }
	// }
	//
	// return tags;
	// }

}
