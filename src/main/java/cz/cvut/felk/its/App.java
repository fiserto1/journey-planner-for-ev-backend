package cz.cvut.felk.its;

import cz.agents.basestructures.GraphStructure;
import cz.cvut.felk.its.zonebuilder.FileToGraph;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.utils.JSONConverter;
import cz.cvut.felk.its.utils.SerializeUtil;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App {

    public static void main( String[] args ) throws IOException, SAXException, ParserConfigurationException, FactoryException, TransformException, ClassNotFoundException {
//        String graphNameIn = "my-graph-v1";
//        String graphNameIn = "germany-filtered-trunk-elev";
//		EVZone<EvNode, EvEdge> zone = EVZoneProvider.INSTANCE.getZone();
//		System.out.println(zone);

		String graphNameIn = "data/germany-filtered-trunk-elev";
		String graphNameOut = "ge-charger-graph-trunk-v1.1-reduced";
		//        String graphNameOut = "my-graph-v1.2";
		//        String graphNameOut = "ge-graph-trunk-v2.0";

		if (args.length != 0) {
			graphNameIn = "germany-filtered-"+ args[0] +"-elev";
			graphNameOut = "ge-charger-graph-"+ args[0] +"-v1.1-reduced";
		}

		String chargersFilename = "ge-schargers.csv";
		String osmGraphFilepath = graphNameIn + ".osm";
//		String osmGraphFilepath = "data/" + graphNameIn + ".osm";
		String serGraphFilepath = graphNameOut + ".ser";
//		String serGraphFilepath = "serialized/" + graphNameOut + ".ser";
		String jsonEdgesOutFilepath = graphNameOut + "-edges.geojson";
		String jsonNodesOutFilepath = graphNameOut + "-nodes.geojson";

		FileToGraph fileToGraph = new FileToGraph(GraphTools.GERMANY_SRID);

		//		Graph<EvNode, EvEdge> precomputedGraph = fileToGraph.buildPrecomputedGraph(osmGraphFilepath, superchargersInFilepath);
		GraphStructure<EvNode, EvEdge> graph = fileToGraph.parseOsmToGraph(osmGraphFilepath, chargersFilename);
		System.out.println(graph);
//		System.out.println(precomputedGraph);
		//        boolean print = true;
		//        for (PlanningEdge e: precomputedGraph.getAllEdges()){
		//            if (print) {
		//                System.out.println(e);
		//                print = false;
		//            }
		//        }

		//        JSONConverter jsonConverter = new JSONConverter();
		//        String geojsonFilepath = "geojson/GE-bytns-to-trunk-ultra.geojson";
		//        jsonConverter.convertGraphEdgesToJSON(geojsonFilepath, precomputedGraph, true);
		//        SerializeUtil.serializeGraph(precomputedGraph, "serialized/GE-bytns-to-trunk-ultra.ser");
//		SerializeUtil.serializeObject(graph, serGraphFilepath);

		GraphStructure<EvNode, EvEdge> chargerGraph = fileToGraph.buildChargerGraph(graph);

		System.out.println("Charger graph completed: " + chargerGraph);

		SerializeUtil.serializeObject(chargerGraph, serGraphFilepath);

		//        String dualGraphFilePath = "C:/Users/Tomas/Desktop/ge-osm/germany-filtered-tertiary-elev.osm";
		//        FileToGraph fileToGraph = new FileToGraph();
		//        Graph<EvNode, PlanningEdge> osmGraph = fileToGraph.parseOsmToGraph(dualGraphFilePath);
		//        System.out.println(osmGraph);
		//        SerializeUtil.serializeGraph(osmGraph, "serialized/GE-tertiary-elev-parsed.ser");

		//        BufferedReader bufferedReader = new BufferedReader(new FileReader(dualGraphFilePath));
		//        System.out.println(bufferedReader.readLine());
		//        String line;
		//        String[] lines;
		//        double elev;
		//        double lastElevInRange = 0;
		//        int lineNumber = 0;
		//        int counter = 0;
		//        while (null != (line = bufferedReader.readLine())) {
		//            lines = line.trim().split("\"");
		////            System.out.println(Arrays.toString(lines));
		//            if (lines[0].equals("<tag k=") && lines[1].equals("elevation")) {
		////                System.out.println(".");
		//                elev = Double.parseDouble(lines[3]);
		//                if (elev < -10) {
		//                    counter++;
		//
		////                    System.out.println(lineNumber);
		//                    System.out.println(elev);
		////                    System.out.println("rep: " + lastElevInRange);
		//                } else {
		////                    lastElevInRange = elev;
		//                }
		//            }
		//            lineNumber++;
		//        }
		//        System.out.println(counter);
		//        bufferedReader.close();

		//        buildEvSimplifiedGraph();
		//        buildEvDualGraph();
		//        buildExtendedGraph();

		JSONConverter jsonConverter = new JSONConverter();
		//        FileToGraph fileToGraph = new FileToGraph();
		//
		////        PRECOMPUTED GRAPH
		////        BUILD, SERIALIZE AND PARSE(geojson)
		//        String osmGraphFilepath = "data/germany/filtered-to-trunk.osm";
		//        String superchargersInFilepath = "data/ge-superchargers-with-chtime.csv";
		//        int radiusInKm = 500;
		//        String serializeFilepath = "serialized/GE-to-trunk-dualGraph-simpl-rad" + radiusInKm + ".ser";
		//        String geojsonFilepath = "geojson/GE-to-trunk-dualGraph-simpl-rad" + radiusInKm + ".geojson";
		//
		//        Graph<EvNode, PlanningEdge> precomputedGraph = fileToGraph.buildPrecomputedGraph(
		//                osmGraphFilepath, superchargersInFilepath, radiusInKm*1000, true);
		//        SerializeUtil.serializeGraph(precomputedGraph, serializeFilepath);
		jsonConverter.convertGraphEdgesToJSON(jsonEdgesOutFilepath, chargerGraph, true);
		jsonConverter.convertNodesToJSON(jsonNodesOutFilepath, chargerGraph);





        //TOY GRAPH
//        Graph<EvNode, PlanningEdge> graph = fileParser.parseXmlToGraph("data/my-simple-ge-graph.xml");
//        System.out.println(graph);
//        RoadGraph<EvNode, PlanningEdge> roadGraph = new RoadGraph<>(graph);
//        Set<Solution> solution = Dijkstra.dijkstraAlgorithm(0,5, roadGraph);


        //CREATE GRAPH LVL 1
//        Graph<EvNode, PlanningEdge> lvl1Graph = fileParser.parseOsmToGraph("data/berlin-filtered.osm");
//        Graph<EvNode, PlanningEdge> lvl1Graph = fileParser.parseOsmToGraph("data/germany-filtered-to-trunk.osm");
//        System.out.println(lvl1Graph);
//        SerializeUtil.serializeGraph(lvl1Graph, "serialized/germany-to-trunk-lvl1-graph.ser");
        //PARSE SUPERCHARGERS
//        SuperchargerFileLoader csvParser = new SuperchargerFileLoader();
//        ArrayList<Supercharger> stopovers = csvParser.parseSuperchargers("data/ge-superchargers-with-chtime-faked-for-berlin.csv", ";");
//        ArrayList<Supercharger> superchargers = csvParser.parseSuperchargers("data/ge-superchargers-with-chtime.csv", ";");
//        System.out.println(superchargers.size());
        //DESERIALIZE LVL1 GRAPH
//        Graph<EvNode, PlanningEdge> lvl1Graph = SerializeUtil.deserializeGraph("serialized/germany-to-trunk-lvl1-graph.ser");


        //CREATE GRAPH LVL 2
//        Graph<EvNode, PlanningEdge> superchargerGraph = fileToGraph.buildDoubleGraph(lvl1Graph, superchargers, 300*1000, true);
//        System.out.println(superchargerGraph);

        //SERIALIZE GRAPH TO FILE
//        SerializeUtil.serializeGraph(superchargerGraph, "serialized/berlin-to-tertiary-double-graph.ser");
//        SerializeUtil.serializeGraph(superchargerGraph, "serialized/germany-to-trunk-double-graph-radius300-simplified.ser");

//        //DESERIALIZE GRAPH FROM FILE
//        Graph<EvNode, PlanningEdge> deserializedSuperGraph = SerializeUtil.deserializeGraph("serialized/berlin-to-tertiary-double-graph.ser");
//        Graph<EvNode, PlanningEdge> deserializedSuperGraph = SerializeUtil.deserializeGraph("serialized/germany-to-trunk-double-graph-radius200-simplified.ser");
//        System.out.println(deserializedSuperGraph);
//        System.out.println(deserializedSuperGraph.getNodeByNodeId(0));
////

        //CHECKING SCC ON SUPERCHARGER EDGES
//        int counter = 0;
//        List<Integer> biggestSCC = fileToGraph.findBiggestSCC(superchargerGraph, true);
//        for (int nodeId: biggestSCC) {
//            counter++;
////            System.out.println(superchargerGraph.getNodeByNodeId(nodeId).supercharger.name);
//        }
//        System.out.println(counter);
//
//
////        //FIND FASTEST ROAD BETWEEN TWO COORDINATES
//        Transformer transformer = new Transformer(3068);
//        GPSLocation startLoc = GPSLocationTools.createGPSLocation(52.532810, 13.446736, 0, transformer);
//        GPSLocation endLoc = GPSLocationTools.createGPSLocation(52.497450, 13.294413, 0, transformer);
//        Solution solut2 = Dijkstra.findFastestRoute(startLoc, endLoc, deserializedSuperGraph, 5*1000);
//        System.out.println(solut2);
//        System.out.println(solut2.getRouteNodeIds());


        //CONVERT GRAPH TO GEOJSON
//        EvLabel fNode = (EvLabel) solut2.get(16650);
//        JSONConverter parser = new JSONConverter();

        //parse lvl 1 graph
//        parser.convertGraphEdgesToJSON("geojson/allEdgesLvl1.geojson", lvl1Graph, false);
//        parser.convertNodesToJSON("geojson/allNodesLvl1.geojson", lvl1Graph);

        //parse lvl 2 graph
//        parser.convertGraphEdgesToJSON("geojson/simplifiedlvl2germany300radius.geojson", superchargerGraph, false);
//        parser.convertNodesToJSON("geojson/allNodesLvl2.geojson", superchargerGraph);


        //PRINT OUTS
//        Collection<EvNode> allNodes = deserializedSuperGraph.getNumOfNodes();
//        int counter=0;
//        for (int j=0;j < allNodes.size(); j++) {
//
//            if (deserializedSuperGraph.getNodeOutgoingEdges(j).size() == 1 && deserializedSuperGraph.getNodeIncomingEdges(j).size() == 1) {
//                counter++;
////                System.out.println(graph.getNodeByNodeId(j));
//            }
//        }
//        System.out.println(counter);
//        System.out.println(allNodes.size());
//        System.out.println(allEdges);

//        int counter = 0;
//        for (PlanningEdge edge: deserializedSuperGraph.getAllEdges()) {
////            System.out.println(edge);
////            if (edge.length < 5000) {
////                System.out.println(edge + " length: " + edge.length);
////            }
//        }
//        System.out.println(counter);

    }



//    private static void buildEvSearchGraph() throws IOException {
//        String dualGraphFilePath = "serialized/GE-to-trunk-multi22.ser";
//        Graph<EvNode, PlanningEdge> dualGraph = SerializeUtil.deserializeGraph(dualGraphFilePath);
//        System.out.println(dualGraph);
//        EvGraph<EvNode, PlanningEdge> evGraph = new EvGraph<>(dualGraph);
//
////        JSONConverter jsonConverter = new JSONConverter();
////        String jsonFilepath = "geojson/evgraph.geojson";
////        jsonConverter.convertGraphEdgesToJSON(jsonFilepath, dualGraph, true);
//
////        int[] statesOfCharge = {1000, 2000, 4000, 6000, 8000, 10000};//max 10000
//        HashMap<Integer, PlanningEdge> firstMileMultiEdges = new HashMap<>();
//
//        KDTree<EvNode> kdTree = new KDTree<>(2, new GPSLocationKDTreeResolver<>(), -1);
//        int numOfChargers = 0;
//        for (EvNode node: dualGraph.getAllNodes()) {
//            if (node.getCharger() != null) numOfChargers++;
//            kdTree.insert(node);
//        }
//
//        GPSLocation origin = GPSLocationTools.createGPSLocation(52.506191, 13.392334, 0, GraphTools.GERMANY_SRID);
//        GPSLocation destination = GPSLocationTools.createGPSLocation(48.125768, 11.557617, 0, GraphTools.GERMANY_SRID);
////        int startNodeId = EdgePrecomputer.findNearestNodeId(origin, dualGraph);
////        int goalNodeId = EdgePrecomputer.findNearestNodeId(destination, dualGraph);
//        EvNode startNearestNode = kdTree.getNearestNode(GPSLocationKDTreeResolver.getCoords(origin));
//        EvNode goalNearestNode = kdTree.getNearestNode(GPSLocationKDTreeResolver.getCoords(destination));
////        System.out.println(goalNodes);
////        System.out.println(dualGraph.getNodeByNodeId(2407));
////        EvNode startNode = dualGraph.getNodeByNodeId(startNodes);
////        EvNode goalNode = dualGraph.getNodeByNodeId(goalNodes);
//
//        firstMileMultiEdges = EdgePrecomputer
//                .computeFMEdges(evGraph, -1, -2, ChargingModel.MAX_STATE_OF_CHARGE, numOfChargers, 0.9);
//        System.out.println(firstMileMultiEdges.size());
////        for (PlanningEdge mEdge: firstMileMultiEdges) {
////            MultiEvEdge edge = (MultiEvEdge) mEdge;
////            System.out.println(edge);
////            System.out.println(dualGraph.getNodeByNodeId(edge.toId));
////        }
//        ////////////////////////////////////////////////////////////
//        ///////////////////////LAST MILE////////////////////////////
//        ////////////////////////////////////////////////////////////
//
//
//        long start = System.currentTimeMillis();
//        HashMap<Integer, PlanningEdge> lastMileMultiEdges = EdgePrecomputer.computeLMByBckwEdges(evGraph, -1,-2, 0.9);
//        System.out.println(System.currentTimeMillis() - start);
////
////        start = System.currentTimeMillis();
////        List<PlanningEdge> lastMileMultiEdges2 = EdgePrecomputer.computerLM(evGraph, goalNodes);
////        System.out.println(System.currentTimeMillis() - start);
////
////        System.out.println(lastMileMultiEdges.size());
////        for (PlanningEdge edge: lastMileMultiEdges) {
////            System.out.println(edge);
////        }
////        System.out.println("fff");
////        System.out.println(lastMileMultiEdges2.size());
////        for (PlanningEdge edge: lastMileMultiEdges2) {
////            System.out.println(edge);
////        }
////        System.out.println(lastMileMultiEdges.equals(lastMileMultiEdges2));
//
//
////        TimeGraph<EvNode, PlanningEdge> timeGraph = new TimeGraph<>(dualGraph, startNodeId, firstMileMultiEdges, goalNodeId, lastMileMultiEdges, ChargingModel.STATES_TO_CHARGE, new ChargingModel(0, 10000), null);
//////
////        ModifiedDijkstra modifiedDijkstra = new ModifiedDijkstra();
////        List<Solution> sols = modifiedDijkstra.modifiedDijkstra(startNodeId, goalNodeId, timeGraph, 10000, false);
//////
////        for (Solution sol: sols) {
////            System.out.println(sol);
////        }
//    }



//    private static void buildEvDualGraph() throws IOException {
//        String simpleGraphFilepath = "serialized/GE-to-trunk-simple-wo-duplicity.ser";
//        Graph<EvNode, PlanningEdge> simpleGraph = SerializeUtil.deserializeGraph(simpleGraphFilepath);
//        EvGraph<EvNode, PlanningEdge> evGraph = new EvGraph<>(simpleGraph);
//        System.out.println(simpleGraph);
////        ArrayList<PlanningEdge> mEdges = EdgePrecomputer.computeMultiEdgesBetweenSChargers(evGraph);
//
//        GraphBuilder<EvNode, PlanningEdge> graphBuilder = new GraphBuilder<>();
//        List<PlanningEdge> out = simpleGraph.getOutEdges(21909);
//        List<PlanningEdge> in = simpleGraph.getInEdges(21909);
//        for (PlanningEdge e: out) {
//            System.out.println(e);
//        }
//        for (PlanningEdge e: in) {
//            System.out.println(e);
//        }
//        graphBuilder.addNodes(simpleGraph.getAllNodes());
////        graphBuilder.addEdges(simpleGraph.getAllEdges()); //THIS REMOVES EDGES WITH SAME FROM AND TO ID
//        for (PlanningEdge edge: simpleGraph.getAllEdges()) { //THIS DOESNT
//            graphBuilder.addEdge(edge);
//        }
////        graphBuilder.addEdges(mEdges);
//        Graph<EvNode, PlanningEdge> dualGraph = graphBuilder.createGraph();
//        System.out.println(dualGraph);
//
//        String dualGraphFilePath = "serialized/GE-to-trunk-multi22.ser";
//        String geojsonFilepath = "geojson/GE-to-trunk-multi22.geojson";
//        String geojsonSimpleFilepath = "geojson/GE-to-trunk-simple22.geojson";
//        SerializeUtil.serializeGraph(dualGraph, dualGraphFilePath);
//        JSONConverter jsonConverter = new JSONConverter();
//        jsonConverter.convertGraphEdgesToJSON(geojsonFilepath, dualGraph, true);
////        jsonConverter.convertGraphEdgesToJSON(geojsonSimpleFilepath, simpleGraph, true);
//    }
//
//    private static void buildEvSimplifiedGraph() throws ParserConfigurationException, SAXException, IOException {
////        JSONConverter jsonConverter = new JSONConverter();
//        String superchargersFilepath = "data/ge-superchargers-with-chtime.csv";
//        FileToGraph fileToGraph = new FileToGraph(GraphTools.GERMANY_SRID);
//
//        //        PRECOMPUTED GRAPH
//        //        BUILD, SERIALIZE AND PARSE(geojson)
//        String osmFilepath = "data/germany-filtered-trunk-elev.osm";
//        //        String parsedFilepath = "serialized/GE-to-trunk-parsed.ser";
//        String simplFilepath = "serialized/GE-to-trunk-simple2.ser";
////        String serializeFilepath = "serialized/GE-to-trunk-scc2.ser";
//        String sccFilepath = "serialized/GE-to-trunk-scc2.ser";
////        String serializeFilepath = "serialized/GE-to-trunk-dualGraph-simpl-rad" + incomingSoC + ".ser";
////        String geojsonFilepath = "geojson/GE-to-trunk-scc2.geojson";
//
//        Graph<EvNode, PlanningEdge> parsedGraph = fileToGraph.parseOsmToGraph(osmFilepath, superchargersFilepath);
//        System.out.println(parsedGraph);
//        Graph<EvNode, PlanningEdge> sccGraph = fileToGraph.buildSCCGraph(parsedGraph);
//        System.out.println("scc: " + sccGraph);
//
//        KDTree<EvNode> kdTree = new KDTree<>(2, new GPSLocationKDTreeResolver<>(), -1);
//        for (EvNode node: sccGraph.getAllNodes()) {
//            kdTree.insert(node);
//        }
//
//        SuperchargerFileLoader superchargerFileLoader = new SuperchargerFileLoader();
//        ArrayList<Supercharger> superchargers = superchargerFileLoader
//                .parseSuperchargers(superchargersFilepath, ";", GraphTools.GERMANY_SRID);
//        for(Supercharger supercharger: superchargers) {
////            int nodeId = EdgePrecomputer.findNearestNodeId(supercharger.location, sccGraph);
//            EvNode nearestNode = kdTree.getNearestNode(GPSLocationKDTreeResolver.getCoords(supercharger.location));
//
//            //setCharger is needed for adding to traveltime... or i have to add chTime to edges after graph is created
////            sccGraph.getNode(nodeId).setCharger(supercharger);
//            nearestNode.setCharger(supercharger);
////            superchargerNodeIds.add(nodeId);
//        }
//        Graph<EvNode, PlanningEdge> simplifyGraph = GraphTools.simplifyGraph(sccGraph);
//        System.out.println("simple: " + simplifyGraph);
//        SerializeUtil.serializeGraph(sccGraph, sccFilepath);
//        SerializeUtil.serializeGraph(simplifyGraph, simplFilepath);
//
////        jsonConverter.convertGraphEdgesToJSON(geojsonFilepath, sccGraph, true);
//    }
}

