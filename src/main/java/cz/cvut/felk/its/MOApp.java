/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its;

import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.Graph;
import cz.agents.geotools.GPSLocationKDTreeResolver;
import cz.agents.geotools.GPSLocationTools;
import cz.agents.geotools.KDTree;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.utils.SerializeUtil;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.PlanningEdge;

public class MOApp {

//	private static final String graphFilepath = "serialized/GE-bytns-to-trunk-ultra.ser";
	private static final String graphFilepath = "serialized/my-graph-v1.ser";
	private static final String jsonFilename = "mo-fastest.geojson";

	public static void main(String[] args) {
//		simulateMOServlet(48.608201, 8.415863, 50.320310, 11.729338, (int) (ChargingModel.MAX_STATE_OF_CHARGE));
//		simulateMOServlet(51.153046, 11.986368, 50.320948, 11.787249, (int) (ChargingModel.MAX_STATE_OF_CHARGE));

		Graph<EvNode, PlanningEdge> graph = SerializeUtil.deserializeGraph(graphFilepath);

		for (EvNode node : graph.getAllNodes()) {
			System.out.println(node);
		}

	}

	private static void simulateMOServlet(double originLat, double originLon, double destLat, double destLon, int initSOC) {
		Graph<EvNode, PlanningEdge> deserializedSuperGraph = SerializeUtil.deserializeGraph(graphFilepath);
		System.out.println("Graph loaded: " + deserializedSuperGraph);

		KDTree<GPSLocation> kdTree = new KDTree<>(2, new GPSLocationKDTreeResolver<>(),-1);
		if (deserializedSuperGraph != null) {
			deserializedSuperGraph.getAllNodes().forEach(kdTree::insert);
		}
		ChargingModel chargingModel = new ChargingModel(ChargingModel.MIN_STATE_OF_CHARGE, ChargingModel.MAX_STATE_OF_CHARGE);

		GPSLocation startLocation = GPSLocationTools.createGPSLocation(originLat, originLon, 0, GraphTools.GERMANY_SRID);
		GPSLocation goalLocation = GPSLocationTools.createGPSLocation(destLat, destLon, 0, GraphTools.GERMANY_SRID);

		EvNode startNode = (EvNode) kdTree.getNearestNode(GPSLocationKDTreeResolver.getCoords(startLocation));
		EvNode goalNode = (EvNode) kdTree.getNearestNode(GPSLocationKDTreeResolver.getCoords(goalLocation));

//		Collection<PlanningEdge> fastestAndCheapestSolution = new MOFastestPathEvRouter()
//				.findMOFastestJourney(deserializedSuperGraph, startNode, goalNode, initSOC, chargingModel);

//		System.out.println(fastestAndCheapestSolution.size());
//		JSONConverter converter = new JSONConverter();
//		converter.convertEdgesToJSON(jsonFilename, deserializedSuperGraph, fastestAndCheapestSolution, false);
	}
}
