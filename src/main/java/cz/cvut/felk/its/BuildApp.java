/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its;

import cz.agents.basestructures.GraphStructure;
import cz.cvut.felk.its.zonebuilder.FileToGraph;
import cz.cvut.felk.its.utils.SerializeUtil;
import cz.cvut.felk.its.evstructures.EVZone;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.utils.EVZoneProvider;

public class BuildApp {

	private static String graphNameOut = "ge-charger-graph-primary-v1.0";
	private static String serGraphFilepath = graphNameOut + ".ser";


//	private static String serGraphFilepath = "GE-charger-graph-v1.ser";

	public static void main(String[] args) {

		EVZone<EvNode, EvEdge> zone = EVZoneProvider.INSTANCE.getZone();
		GraphStructure<EvNode, EvEdge> graph = zone.getGraph();

		GraphStructure<EvNode, EvEdge> chargerGraph = new FileToGraph(zone.srid).buildChargerGraph(graph);

		System.out.println("Charger graph completed: " + chargerGraph);

		SerializeUtil.serializeObject(chargerGraph, serGraphFilepath);

	}
}
