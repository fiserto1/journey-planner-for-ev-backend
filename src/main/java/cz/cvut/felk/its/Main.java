package cz.cvut.felk.its;

import cz.agents.basestructures.Graph;
import cz.agents.basestructures.GraphStructure;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.PlanningEdge;
import cz.cvut.felk.its.utils.EVZoneProvider;
import cz.cvut.felk.its.utils.JSONConverter;
import cz.cvut.felk.its.utils.SerializeUtil;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by Tomas on 08-Mar-16.
 */
public class Main {

//    private static final String graphFilepath = "serialized/GE-primary-bytns-prec-graph-ultra.ser";
    private static final String graphFilepath = "GE-tertiary-bytns-prec-graph-ultra.ser";
//    private static final String graphFilepath = "GE-tertiary-bytns-prec-graph-ultra.ser";
//    private static final String graphFilepath = "serialized/GE-bytns-to-trunk-ultra.ser";
//        String graphFilepath = "serialized/GE-tertiary-bytns-prec-graph.ser";
//        String graphFilepath = "serialized/GE-trunk-bytns-prec-graph.ser";
//        String graphFilepath = "serialized/berlin-to-tertiary-double-graph.ser";
//        String graphFilepath = "serialized/germany-to-trunk-double-graph-radius500-simplified.ser";

    private static final String jsonNodesFilepath = "geojson/GE-primary-bytns-prec-graph-ultra-nodes.geojson";
    private static final String jsonLocationsFilepath = "geojson/GE-primary-bytns-prec-graph-ultra-locs.geojson";
    private static final String jsonEdgesFilepath = "geojson/GE-primary-bytns-prec-graph-ultra-edges.geojson";
//    private static final String jsonNodesFilepath = "GE-tertiary-bytns-prec-graph-ultra-nodes.geojson";
//    private static final String jsonLocationsFilepath = "GE-tertiary-bytns-prec-graph-ultra-locs.geojson";
//    private static final String jsonEdgesFilepath = "GE-tertiary-bytns-prec-graph-ultra-edges.geojson";

    public static void main( String[] args ) throws ParserConfigurationException, SAXException, IOException {
//        Set<Integer> set = new HashSet<>();
//        Set<Integer> integers = SerializeUtil.deserializeObject("nodeids.ser", set);

		EVZoneProvider instance = EVZoneProvider.INSTANCE;
				GraphStructure<EvNode, EvEdge> graph = EVZoneProvider.INSTANCE.getZone().getGraph();
//		System.out.println(graph);
//		for (EvEdge evEdge : graph.getAllEdges()) {
//			evEdge.viaNodes
//			if (evEdge instanceof EvEdgeBetweenChargers) {
//				EvEdgeBetweenChargers e = (EvEdgeBetweenChargers) evEdge;
//				List<EvEdge> paths = e.getPaths();
//				for (EvEdge path : paths) {
//					e.
//				}
//			}
//		}
//		int k = 12;
//		float k2 = 12.3f;
//		double k3 = 12.3;
//		System.out.println(ObjectSizeFetcher.getObjectSize(graph));
//		System.out.println(ObjectSizeFetcher.getObjectSize(k));
//		System.out.println(ObjectSizeFetcher.getObjectSize(k2));
//		System.out.println(ObjectSizeFetcher.getObjectSize(k3));
//		System.out.println(ObjectSizeFetcher.getObjectSize("hello"));
//		System.out.println(ObjectSizeFetcher.getObjectSize(graph.getNode(0)));
//		System.out.println(ObjectSizeFetcher.getObjectSize(graph.getInEdges(0).get(0)));
//        List<EvNode> collect = integers.stream().map(graph::getNode).collect(Collectors.toList());
//        new JSONConverter().convertLocationsToJSON("geojson/fringed-nodes.geojson", collect);

        //        simulateServlet(53.173119, 10.129395, 48.327039, 11.052246, ChargingModel.MAX_STATE_OF_CHARGE);
//        simulateServlet(49.624946, 10.986328, 48.239309, 8.547363, (int) (0.8*ChargingModel.MAX_STATE_OF_CHARGE));
//        simulateServlet(52.506191, 13.392334, 48.125768, 11.557617, (int) (0.3*ChargingModel.MAX_STATE_OF_CHARGE));
//        simulateServlet(53.020711, 10.978359, 48.323061, 12.512438, (int) (0.3*ChargingModel.MAX_STATE_OF_CHARGE));
//        simulateServlet(48.608201, 8.415863, 50.320310, 11.729338, (int) (ChargingModel.MAX_STATE_OF_CHARGE));
//        simulateServlet(48.608201, 8.415863, 50.320310, 11.729338, (int) (ChargingModel.MAX_STATE_OF_CHARGE));
//        printGraphToJSON();

//        FileToGraph fileToGraph = new FileToGraph();
//        String osmFilepath = "data/germany-filtered-trunk-elev.osm";
//        String serFilepath = "serialized/GE-to-trunk-multi22.ser";
//        Graph<EvNode, PlanningEdge> parsedGraph = fileToGraph.parseOsmToGraph(osmFilepath);
//        Graph<EvNode, PlanningEdge> parsedGraph = SerializeUtil.deserializeGraph(serFilepath);
//        System.out.println(parsedGraph);
//        Graph<EvNode, PlanningEdge> biggestSCCGraph = fileToGraph.buildSCCGraph(parsedGraph);
//        System.out.println(biggestSCCGraph);
//        SerializeUtil.serializeGraph(biggestSCCGraph, serFilepath);
//
//        String geojsonFilepath = "geojson/GE-trunk-scc.geojson";
//        JSONConverter jsonConverter = new JSONConverter();
//        jsonConverter.convertGraphEdgesToJSON(geojsonFilepath, biggestSCCGraph, true);

//        testModifiedDijkstra();
//        testBellmanFord2();
//
//        double[][] latLons = {
//                {52.506191, 13.392334},
//                {48.125768, 11.557617},
//                {50.095917, 8.640747},
//                {50.986099, 6.987305},
//                {53.507651, 9.975586},
//                {48.835797, 9.173584}
//        };
//        int[] elevations = {
//                1200,
//                258,
//                52,
//                186,
//                234,
//                360
//        };
//
//        int[][] edgeFromTos = {
//                {0, 1},
//                {1, 2},
//                {2, 3},
//                {4, 5},
//                {5, 4},
//                {4, 2},
//                {3, 1},
//                {5, 2},
//                {2, 1},
//                {3, 4},
//                {1, 5}
//        };

//        GraphBuilder<EvNode, EvEdge> graphBuilder = new GraphBuilder<>();
//        ArrayList<EvNode> nodes = createNodesFromCoords(latLons, elevations);
//        graphBuilder.addNodes(nodes);
//        graphBuilder.addEdges(createEdges(edgeFromTos, nodes));
//        Graph<EvNode, EvEdge> graph = graphBuilder.createGraph();
//        System.out.println(graph.getNumOfNodes().toString());
//        System.out.println(graph.getAllEdges().toString());
//        BellmanFord bf = new BellmanFord();
//        System.out.println(Arrays.toString(bf.bellmanFord(graph, 0)));

    }

    private static void printGraphToJSON() throws IOException {
        Graph<EvNode, PlanningEdge> deserializedSuperGraph = SerializeUtil.deserializeGraph(graphFilepath);
        System.out.println("Graph loaded: " + deserializedSuperGraph);
        JSONConverter jsonConverter = new JSONConverter();
//        jsonConverter.convertNodesToJSON(jsonNodesFilepath, deserializedSuperGraph);
//        jsonConverter.convertLocationsToJSON(jsonLocationsFilepath, deserializedSuperGraph);
        jsonConverter.convertGraphEdgesToJSON(jsonEdgesFilepath, deserializedSuperGraph, false);
    }

//    private static void simulateServlet(double originLat, double originLon, double destLat, double destLon, int initSOC) throws IOException {
//
//        Graph<EvNode, PlanningEdge> deserializedSuperGraph = SerializeUtil.deserializeGraph(graphFilepath);
//        System.out.println("Graph loaded: " + deserializedSuperGraph);
////        JSONConverter jsonConverter = new JSONConverter();
////        jsonConverter.convertNodesToJSON("serialized/tertiary-nodes.geojson", deserializedSuperGraph);
//
////        Transformer transformer = new Transformer(GraphTools.GERMANY_SRID);
////        int[] pots = SerializeUtil.deserializePotentials("serialized/potentials.ser");
//
//
//        KDTree<GPSLocation> kdTree = new KDTree<>(2, new GPSLocationKDTreeResolver<>(),-1);
//        KDTree<GPSLocation> kdTree2 = new KDTree<>(2, new GPSLocationKDTreeResolver<>(),-1);
//        if (deserializedSuperGraph != null) {
//            deserializedSuperGraph.getAllNodes().forEach(kdTree::insert);
//        }
//        ChargingModel chargingModel = new ChargingModel(ChargingModel.MIN_STATE_OF_CHARGE, ChargingModel.MAX_STATE_OF_CHARGE);
////        ChargerTools.updateChargingPrices(deserializedSuperGraph);
//
//        GPSLocation startLocation = GPSLocationTools.createGPSLocation(originLat, originLon, 0, GraphTools.GERMANY_SRID);
//        GPSLocation goalLocation = GPSLocationTools.createGPSLocation(destLat, destLon, 0, GraphTools.GERMANY_SRID);
//
//
//        int numOfChargers = 0;
//        for (EvNode evNode : deserializedSuperGraph.getAllNodes()) {
//            if (evNode.getCharger() != null) {
//                numOfChargers++;
//            }
//
//            kdTree.insert(evNode);
//            kdTree2.insert(evNode);
//        }
//
//        for(PlanningEdge edge: deserializedSuperGraph.getAllEdges()) {
//            if (edge instanceof EvEdge) {
//                EvEdge evEdge = (EvEdge) edge;
//
//                for (GPSLocation viaNode: evEdge.viaNodes){
//                    kdTree.insert(viaNode);
//                }
//            }
//        }
//        ChargingPriceLoader chpl = new ChargingPriceFileLoader();
//        chpl.loadPrices(deserializedSuperGraph);
//
////        Set<EvNode> nearestS = EdgePrecomputer.findNearestNodesByKDTree(startLocation, kdTree);
////        Set<EvNode> nearestG = EdgePrecomputer.findNearestNodesByKDTree(goalLocation, kdTree);
//
////        nearestS.forEach(System.out::println);
////        nearestG.forEach(System.out::println);
//
////        GPSLocation newStartLocation = GPSLocationTools.createGPSLocation(originLat, originLon, 91, GraphTools.GERMANY_SRID);
////        GPSLocation newGoalLocation = GPSLocationTools.createGPSLocation(destLat, destLon, 309, GraphTools.GERMANY_SRID);
//
////        for (EvNode n: nearestS) {
////            System.out.println(GPSLocationTools.computeDistanceAsDouble(startLocation, n));
////            System.out.println(GPSLocationTools.computeDistanceAsDouble(newStartLocation, n));
////        }
////
////        for (EvNode n: nearestG) {
////            System.out.println(GPSLocationTools.computeDistanceAsDouble(goalLocation, n));
////            System.out.println(GPSLocationTools.computeDistanceAsDouble(newGoalLocation, n));
////        }
//
//        SearchGraph<EvLabel> evSearchGraph = EdgePrecomputer.buildEvSearchGraph(deserializedSuperGraph,
//                startLocation, goalLocation, kdTree);
//
////        for (int i = 0; i < 100; i++) {
//            Solution[] fastestAndCheapestSolution = EdgePrecomputer.findFastestAndCheapestSolution(deserializedSuperGraph,
//                    evSearchGraph, initSOC, numOfChargers, chargingModel, 0, 0.9);
//
//
//        EvRouter router = new MOFastestPathEvRouter();
//        Journey moJourney = router.findOptimalJourney(deserializedSuperGraph, startLocation, goalLocation, initSOC, chargingModel, new ChargingFunction(), kdTree2);
//        Plan moPlan = EvResponseBuilder.buildMOPlan(deserializedSuperGraph, moJourney, new ChargingFunction());
//            EvResponse resp = EvResponseBuilder
//                    .buildResponse(evSearchGraph, fastestAndCheapestSolution, moPlan, startLocation, goalLocation);
////        }
//        System.out.println(Arrays.toString(fastestAndCheapestSolution));
//        System.out.println(resp.plans.get(0).steps.get(0));
//        System.out.println(resp.plans.get(1).steps.get(0));
//
//    }
//
//    private static void testModifiedDijkstra() {
//        Supercharger s0 = new Supercharger(-1, "s0", new GPSLocation(0,0,0,0), "open");
//        Supercharger s1 = new Supercharger(-2, "s1", new GPSLocation(0,0,0,0), "open");
//        Supercharger s2 = new Supercharger(-3, "s2", new GPSLocation(0,0,0,0), "open");
//        GraphBuilder<EvNode, PlanningEdge> gb = new GraphBuilder<>();
//        gb.addNode(new EvNode(0, 0, 0, 0, 0, 0, 0, s0));
//        gb.addNode(new EvNode(1, 1, 0, 0, 0, 0, 0));
//        gb.addNode(new EvNode(2, 2, 0, 0, 0, 0, 0, s1));
//        gb.addNode(new EvNode(3, 3, 0, 0, 0, 0, 0));
//        gb.addNode(new EvNode(4, 4, 0, 0, 0, 0, 0, s2));
//        gb.addNode(new EvNode(5, 5, 0, 0, 0, 0, 0));
//
//
//        //JNYKL TEST
//        gb.addEdge(new EvEdge(0, 1, 0, RoadType.UNKNOWN, 3, 20));
//        gb.addEdge(new EvEdge(1, 3, 0, RoadType.UNKNOWN, 3, 20));
//        gb.addEdge(new EvEdge(0, 2, 0, RoadType.UNKNOWN, 2, 30));
//        gb.addEdge(new EvEdge(2, 3, 0, RoadType.UNKNOWN, 2, 30));
//        gb.addEdge(new EvEdge(0, 3, 0, RoadType.UNKNOWN, 5, 55));
//        gb.addEdge(new EvEdge(3, 4, 0, RoadType.UNKNOWN, 2, 50));
//
//
////        gb.addEdge(new EvEdge(0, 1, 0, 20, 3));
////        gb.addEdge(new EvEdge(1, 3, 0, 20, 3));
////        gb.addEdge(new EvEdge(0, 2, 0, 30, 2));
////        gb.addEdge(new EvEdge(2, 3, 0, 30, 2));
////        gb.addEdge(new EvEdge(0, 3, 0, 55, 5));
////        gb.addEdge(new EvEdge(3, 4, 0, 50, 2));
//
//
//
////        gb.addEdge(new EvEdge(0, 1, 0, 10, 10));
////        gb.addEdge(new EvEdge(0, 5, 0, 40, 3));
////        gb.addEdge(new EvEdge(1, 2, 0, 15, 2));
////        gb.addEdge(new EvEdge(1, 4, 0, 25, -3));
////        gb.addEdge(new EvEdge(2, 1, 0, 18, -1));
////        gb.addEdge(new EvEdge(3, 2, 0, 30, 4));
////        gb.addEdge(new EvEdge(4, 3, 0, 5, 1));
////        gb.addEdge(new EvEdge(4, 2, 0, 23, 4));
////        gb.addEdge(new EvEdge(5, 4, 0, 11, 3));
//////
////        gb.addEdge(new EvEdge(1, 5, 0, 15, -2));
////        gb.addEdge(new EvEdge(0, 2, 0, 30, 3));
//
//        Graph<EvNode, PlanningEdge> g = gb.createGraph();
//        BellmanFord bfbf = new BellmanFord();
////        int[] potentials = bfbf.bellmanFord(g, 0);
//
//        //reweight
////        System.out.println(Arrays.toString(potentials));
////        for (EvEdge edge: g.getAllEdges()) {
////            System.out.println(edge + " new weight: "+ (edge.consumption - potentials[edge.toId] + potentials[edge.fromId]));
////        }
//
//
//
//        EvGraph<EvNode, PlanningEdge> evGraph = new EvGraph<>(g);
//        GoalChecker<EvNode, PlanningEdge> gc = new ByNodeIdsGoalChecker<>(4);
//        GoalChecker<EvNode, PlanningEdge> gc2 = new AllChargersGC<>(3);
//        GoalChecker<EvNode, PlanningEdge> gc3 = new AllNodesAreGoalsChecker<>(6);
//        Set<Integer> goalNodes = new HashSet<>();
//        goalNodes.add(1);
//        goalNodes.add(2);
//        goalNodes.add(3);
//        GoalChecker<EvNode, PlanningEdge> gc4 = new ByNodeIdsGoalChecker<>(goalNodes, true);
//        List<Solution> sols = ModifiedDijkstra.modifiedDijkstra(0, gc4, evGraph, 605, ModifiedDijkstra.OPTIMAL_REDUCE_CONST);
//        System.out.println(sols);
////        HashMap<Integer, PlanningEdge> edges = EdgePrecomputer.computeLMByBckwEdges(evGraph, 3);
////        for (PlanningEdge edge : edges.values()) {
////            System.out.println(edge);
////        }
////        System.out.println("ff");
////        List<PlanningEdge> lastMileMultiEdges2 = EdgePrecomputer.computerLM(evGraph, 3);
////        for (PlanningEdge edge: lastMileMultiEdges2) {
////            System.out.println(edge);
////        }
//    }
//
//    //create edges from array
//    public static Collection<EvEdge> createEdges(int[][] edgeFromTos, ArrayList<EvNode> nodes) {
//        Collection<EvEdge> edges = new LinkedList<>();
//        for (int i = 0; i < edgeFromTos.length; i++) {
//            int from = edgeFromTos[i][0];
//            int to = edgeFromTos[i][1];
//            int length = GPSLocationTools.computeDistance(nodes.get(from), nodes.get(to));
//            int consumption = ConsumptionProfileUtils.computeConsumptionBetweenTwoPoints(nodes.get(from), nodes.get(to), length);
//            edges.add(new EvEdge(from, to, length, RoadType.UNKNOWN, length/130, consumption));
//        }
//
//        return edges;
//    }
//
//    private static ArrayList<EvNode> createNodesFromCoords(double[][] latLons, int[] elevations, int srid) {
//        ArrayList<EvNode> nodes = new ArrayList<>();
//        for (int i = 0; i < latLons.length; i++) {
//            GPSLocation location = GPSLocationTools.createGPSLocation(latLons[i][0], latLons[i][1], elevations[i], srid);
//            nodes.add(new EvNode(i, i, location));
//        }
//        return nodes;
//    }
//
//    public static void testBellmanFord() {
//        GraphBuilder<EvNode, EvEdge> gb = new GraphBuilder<>();
//        gb.addNode(new EvNode(0, 0, 0, 0, 0, 0, 0));
//        gb.addNode(new EvNode(1, 1, 0, 0, 0, 0, 0));
//        gb.addNode(new EvNode(2, 2, 0, 0, 0, 0, 0));
//        gb.addNode(new EvNode(3, 3, 0, 0, 0, 0, 0));
//        gb.addNode(new EvNode(4, 4, 0, 0, 0, 0, 0));
//        gb.addNode(new EvNode(5, 5, 0, 0, 0, 0, 0));
//
//        gb.addEdge(new EvEdge(0, 1, 0, RoadType.UNKNOWN, 0, 10));
//        gb.addEdge(new EvEdge(0, 5, 0, RoadType.UNKNOWN, 0, 3));
//        gb.addEdge(new EvEdge(1, 2, 0, RoadType.UNKNOWN, 0, 2));
//        gb.addEdge(new EvEdge(1, 4, 0, RoadType.UNKNOWN, 0, -3));
//        gb.addEdge(new EvEdge(2, 1, 0, RoadType.UNKNOWN, 0, -1));
//        gb.addEdge(new EvEdge(3, 2, 0, RoadType.UNKNOWN, 0, 4));
//        gb.addEdge(new EvEdge(4, 3, 0, RoadType.UNKNOWN, 0, 1));
//        gb.addEdge(new EvEdge(4, 2, 0, RoadType.UNKNOWN, 0, 4));
//        gb.addEdge(new EvEdge(5, 4, 0, RoadType.UNKNOWN, 0, 3));
//
//        Graph<EvNode, EvEdge> g = gb.createGraph();
//        BellmanFord bfbf = new BellmanFord();
//        int[] potentials = bfbf.bellmanFord(g, 0);
//        System.out.println(Arrays.toString(potentials));
//        //reweight
//
//        for (EvEdge edge: g.getAllEdges()) {
//            System.out.println(edge + " new weight: "+ (edge.consumption - potentials[edge.toId] + potentials[edge.fromId]));
//        }
//        EvGraph<EvNode, EvEdge> evGraph = new EvGraph<>(g);
//
//
//        int[] ranges = new int[21];
//        for (int i=0; i < ranges.length; i++) {
//            ranges[i] = i*10;
//        }
//
//    }
//
//    public static void testBellmanFord2() {
//        String filepath = "serialized/GE-to-trunk-simple.ser";
//        Graph<EvNode, EvEdge> g = SerializeUtil.deserializeEvGraph(filepath);
//        BellmanFord bfbf = new BellmanFord();
//        int[] potentials = bfbf.bellmanFord(g, 0);
////        System.out.println(Arrays.toString(potentials));
////        //reweight
////
////        for (EvEdge edge: g.getAllEdges()) {
////            System.out.println(edge + " new weight: "+ (edge.consumption - potentials[edge.toId] + potentials[edge.fromId]));
////        }
//
//        SerializeUtil.serializeObject(potentials, "serialized/potentials.ser");
//        int[] pots = SerializeUtil.deserializePotentials("serialized/potentials.ser");
//        System.out.println(Arrays.toString(pots));
//
//    }
}
