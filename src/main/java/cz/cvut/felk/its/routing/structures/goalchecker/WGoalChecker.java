package cz.cvut.felk.its.routing.structures.goalchecker;

import cz.cvut.felk.its.routing.structures.EwLabel;

/**
 * Created by Tomas on 26-Apr-16.
 */
@Deprecated
public interface WGoalChecker<FNode extends EwLabel> {

    boolean isGoal(FNode node);
    boolean isSearchFinished();
    void setSearchFinished(boolean isSearchFinished);
}
