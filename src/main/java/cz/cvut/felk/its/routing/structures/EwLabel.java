package cz.cvut.felk.its.routing.structures;

import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.routing.SpeedUpSettings;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.evstructures.ConsumptionProfile;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.utils.EVZoneProvider;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * equals on nodeId
 * compare on costs
 *
 * Created by Tomas on 17.11.2015.
 */
@Deprecated
public class EwLabel implements Comparable<EwLabel>, Serializable {

    private static final long serialVersionUID = -8986970321749002244L;

    public final int nodeId;
    public final int[] costs;
    public final int travelTimeMillis; //traveltime including charging at the last charger
	public final ConsumptionProfile consProfile;
	public final double inSoC;
	public final double lastSeenChargerOutSoC;
    public final double cpkWh; //cents per Wh
    public final EwLabel prevLabel;
	public final int inEdgeId;
    private boolean settled;

//    public EwLabel(int nodeId, int[] costs, EwLabel prevLabel, ConsumptionProfile consProfile) {
//        this(nodeId, costs, prevLabel, consProfile, 0, null, -1, 0);
//    }
//
//	public EwLabel(int nodeId, int[] costs, EwLabel prevLabel, ConsumptionProfile consProfile, int travelTimeMillis) {
//		this(nodeId, costs, prevLabel, consProfile, travelTimeMillis, null, -1, 0);
//	}
//
	public EwLabel(int nodeId, int[] costs, int travelTimeMillis, ConsumptionProfile consProfile,
			double lastSeenChargerSoC, double lastSeenChargerCpkWh,EwLabel prevLabel) {
		this(nodeId, costs, travelTimeMillis, consProfile, -1, lastSeenChargerSoC,
				lastSeenChargerCpkWh, prevLabel, -1);
	}

	public EwLabel(int nodeId, int[] costs, int travelTimeMillis, ConsumptionProfile consProfile, double inSoC, double lastSeenChargerOutSoC, double cpkWh,
			EwLabel prevLabel, int inEdgeId) {
		this.nodeId = nodeId;
		this.costs = costs;
		this.travelTimeMillis = travelTimeMillis;
		this.consProfile = consProfile;
		this.inSoC = inSoC;
		this.lastSeenChargerOutSoC = lastSeenChargerOutSoC;
		this.cpkWh = cpkWh;
		this.prevLabel = prevLabel;
		this.inEdgeId = inEdgeId;
		this.settled = false;
	}


	// testing startNode
//    public EwLabel(int nodeId) {
//        this(nodeId, new int[]{0}, null, null, 0, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);
//    }

//    public EvLabel(int nodeId, int costs, EvLabel prevLabel, LocalDateTime localDateTime, ConsumptionProfile consProfile,
//            int travelTimeMillis, int lastSeenCharger, double lastSeenChargerSoC) {
//        this(nodeId, costs, prevLabel, localDateTime, consProfile, travelTimeMillis, lastSeenCharger,
//                lastSeenChargerSoC);
//    }

    public EwLabel(int startNodeId, int initSOC) {
        this(startNodeId, new int[]{0, 0}, 0, null, initSOC, initSOC, -1, null, Integer.MAX_VALUE);
    }

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof EwLabel))
			return false;

		EwLabel ewLabel = (EwLabel) o;

		if (nodeId != ewLabel.nodeId)
			return false;
		if (travelTimeMillis != ewLabel.travelTimeMillis)
			return false;
		if (Double.compare(ewLabel.inSoC, inSoC) != 0)
			return false;
		if (Double.compare(ewLabel.lastSeenChargerOutSoC, lastSeenChargerOutSoC) != 0)
			return false;
		if (Double.compare(ewLabel.cpkWh, cpkWh) != 0)
			return false;
		return Arrays.equals(costs, ewLabel.costs);

	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = nodeId;
		result = 31 * result + Arrays.hashCode(costs);
		result = 31 * result + travelTimeMillis;
		temp = Double.doubleToLongBits(inSoC);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(lastSeenChargerOutSoC);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(cpkWh);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public String toString() {
		int pLabelId = Integer.MAX_VALUE;
		if (prevLabel != null) {
			pLabelId  = prevLabel.nodeId;
		}
		return "EwLabel [" +
				"nodeId=" + nodeId +
				", costs=" + Arrays.toString(costs) +
				", travelTimeMillis=" + travelTimeMillis +
				", inSoC=" + inSoC +
				", lastSeenChargerOutSoC=" + lastSeenChargerOutSoC +
				", cpkWh=" + cpkWh +
				", inEdgeId=" + inEdgeId +
				", settled=" + settled +
				", prevLabelId=" + pLabelId +
				']';
	}

	@Override
    public int compareTo(@Nonnull EwLabel o) {
		for (int i = 0; i < costs.length; i++) {
			if (this.costs[i] != o.costs[i]) {
				return Integer.compare(this.costs[i], o.costs[i]);
			}
		}

		//more lastSeenChargerOutSoC is better!
		return Double.compare(o.inSoC, this.inSoC);
    }

    public boolean isSettled() {
        return settled;
    }

    public void markSettled() {
        this.settled = true;
    }

//    public boolean isDominatedBy(int[] nextCosts, ConsumptionProfile nextConsProfile, int nextLastSeenChargerId) { //int nodeId
//
//        if (consProfile.equals(nextConsProfile) && costs[0] > nextCosts[0] && costs[1] >= nextCosts[1] && this.lastSeenCharger
//                == nextLastSeenChargerId) return true;
//        return (costs[0] >= nextCosts[0] && consProfile.isDominatedBy(nextConsProfile) && this.lastSeenCharger
//                == nextLastSeenChargerId);
//    }


//	public boolean isDominatedBy(EvLabel label, double centsPerSecond) {
//		return isDominatedBy(label, 1, centsPerSecond);
//	}
    public boolean isDominatedBy(EwLabel label, double centsPerSecond) {
		if (this.equals(label)) return true; // TODO: 05-Dec-16 check this!

		double domRelaxPerc = SpeedUpSettings.getDomRelaxPerc();

		if (nodeId == -2) {
			//REMOVE THIS FOR TESTS - to get full pareto set
			for (int i = 0; i < costs.length; i++) {
//				if (this.costs[i] < label.costs[i]* domRelaxPerc) {
				if (this.costs[i] < label.costs[i]) {
					return false;
				}
			}
			return true; //last correcting node has real costs, so it is dominated if costs are dominated
		}

		if (this.cpkWh != label.cpkWh) return false;
		if (SpeedUpSettings.DOM_BASIC_APPROACH && isDominatedByBasicApproach(label, domRelaxPerc)) {
			return true;
		}

		if (SpeedUpSettings.DOM_BY_CHARGING) {

			int[] labCosts;
			int[] thisCosts;
			if (label.inSoC <= this.inSoC) {
				labCosts = label.chargeTo(this.inSoC, centsPerSecond);
				thisCosts = this.costs;
			} else {
				labCosts = label.costs;
				thisCosts = this.chargeTo(label.inSoC, centsPerSecond);
			}

			//sameSoC
			if (labCosts[0] <= thisCosts[0]) {
				double differencePrice = GraphTools.convertFromE3Format(thisCosts[0] - labCosts[0]) * centsPerSecond ;
				if (labCosts[1] <= differencePrice + labCosts[1]) {
					return true;
				}
			}
		}


		if (SpeedUpSettings.DOM_DELTA_REDUCTION && (this.costs[0] - label.costs[0]) > SpeedUpSettings.earlySearchFinishTTMillis) {//2hours later
			//						System.out.println("Big difference: " + (this.costs[0]- label.costs[0]));
			return true;
		}


		return false;
	}

	private int[] chargeTo(double outSoC, double centsPerSecond) {

		EvNode node = null;
		if (nodeId > -1) {
			node = EVZoneProvider.INSTANCE.getZone().getGraph().getNode(this.nodeId);
		}

		// TODO: 26-Dec-16 this returns wierd solutions(worse best price), inspect behaviour for LM
		//		double pricePerkWh = 50; //WORST price for charging everywhere;
		double pricePerkWh = this.cpkWh; //WORST price for charging everywhere;

		if (node != null && node.isCharger()) {
			LocalDateTime localDateTime = LocalDateTime.of(2016, 12, 24, 10, 0, 0)
					.plusSeconds(GraphTools.convertFromE3Format(this.travelTimeMillis));
			pricePerkWh = node.getCharger().getPricePerkWh(localDateTime);
		}

		////		//recharge label to same SoC as this
		double chargingTimeMillis = new ChargingFunction().getChargingTimeMillis(this.inSoC, outSoC);

		int timeCostMoney = (int) (centsPerSecond* GraphTools.convertFromE3Format(chargingTimeMillis));
		int priceForCharging = (int) (pricePerkWh *((outSoC-this.inSoC)/1000)) + timeCostMoney;

		return new int[]{this.costs[0]+(int)chargingTimeMillis, this.costs[1]+priceForCharging};
	}

	public int[] chargeTo(double chargeableSoC, double centsPerSecond, ChargingFunction chFunction) {

		////		//recharge label to same SoC as this
		double chargingTimeMillis = chFunction.getChargingTimeMillis(this.inSoC, chargeableSoC);

		int timeCostMoney = (int) (centsPerSecond* GraphTools.convertFromE3Format(chargingTimeMillis));
		int priceForCharging = (int) (cpkWh *((chargeableSoC-this.inSoC)/1000)) + timeCostMoney;

		return new int[]{this.costs[0]+(int)chargingTimeMillis, this.costs[1]+priceForCharging};
	}



	private boolean isDominatedByBasicApproach(EwLabel label, double domRelaxPerc) {
		//OLD APPROACH

		for (int i = 0; i < costs.length; i++) {
			if (this.costs[i] < label.costs[i]*domRelaxPerc) {
				return false;
			}
		}

		if (this.consProfile == null && label.consProfile == null) {
			if (this.inSoC *domRelaxPerc > label.inSoC) {

				return false;
			} else {
				return true;
			}
		} else {
			if (this.consProfile != null) {
				return this.consProfile.isDominatedBy(label.consProfile);
			}
		}
		return false;
	}


	private int priceForCharging(double centsPerSecond, double pricePerKwh, double stateBeforeCharge, double stateAfterCharge) {
		// TODO: 22-Dec-16 move to another class
		//TIME COST MONEY
		int chargingTimeMillis = (int) new ChargingFunction().getChargingTimeMillis(stateBeforeCharge, stateAfterCharge);
		int timeCostMoney = (int) (centsPerSecond*GraphTools.convertFromE3Format(chargingTimeMillis));
		//TIME COST MONEY

		return (int) (pricePerKwh*((stateAfterCharge-stateBeforeCharge)/1000)) + timeCostMoney;
	}

}
