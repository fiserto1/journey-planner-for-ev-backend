package cz.cvut.felk.its.routing.structures;

import cz.cvut.felk.its.evstructures.ConsumptionProfile;

import java.util.List;
import java.util.Objects;

/**
 * Created by Tomas on 05.01.2016.
 */
@Deprecated
public class OptimalPathSolution extends Solution {
    public final int numOfSegments;
    public final List<Integer> superchargerNodeIds;
    public final List<Integer> superchargerStepIndexes; //indexes of step, where supercharger is used!!
    //todo dont forget, travelTimeMillis is with charging time
    public final List<Integer> segmentTravelTimesMillis;
    public final List<Integer> segmentLengths;
    public final List<Integer> supChargingTimes;
    public final List<Integer> chargingPrices;
    public final int cost;


    /**
     * @param routeNodeIds
     * @param length       in meters
     * @param travelTime   in seconds
     * @param numOfSegments
     * @param superchargerNodeIds
     * @param segmentTravelTimesMillis
     * @param segmentLengths
     * @param supChargingTimes
     */
    public OptimalPathSolution(List<Integer> routeNodeIds, int cost, int length, int travelTime, int price,
                               List<Integer> statesOfCharge, List<Integer> statesAfterCharge, int numOfSegments,
                               List<Integer> superchargerNodeIds,  List<Integer> superchargerStepIndexes,
                               List<Integer> segmentTravelTimesMillis, List<Integer> segmentLengths,
                               List<Integer> supChargingTimes, List<Integer> chargingPrices, ConsumptionProfile consumptionProfile) {

        super(routeNodeIds, length, travelTime, price, statesOfCharge, statesAfterCharge, consumptionProfile);
        this.numOfSegments = numOfSegments;
        this.superchargerNodeIds = superchargerNodeIds;
        this.superchargerStepIndexes = superchargerStepIndexes;
        this.segmentTravelTimesMillis = segmentTravelTimesMillis;
        this.segmentLengths = segmentLengths;
        this.supChargingTimes = supChargingTimes;
        this.chargingPrices = chargingPrices;
        this.cost = cost;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        OptimalPathSolution that = (OptimalPathSolution) o;
        return numOfSegments == that.numOfSegments &&
                Objects.equals(superchargerNodeIds, that.superchargerNodeIds) &&
                Objects.equals(segmentTravelTimesMillis, that.segmentTravelTimesMillis) &&
                Objects.equals(segmentLengths, that.segmentLengths) &&
                Objects.equals(supChargingTimes, that.supChargingTimes) &&
                Objects.equals(chargingPrices, that.chargingPrices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), numOfSegments, superchargerNodeIds, segmentTravelTimesMillis, segmentLengths, supChargingTimes, chargingPrices);
    }

    @Override
    public String toString() {
        return "OptimalPathSolution{" +
                "numOfSegments=" + numOfSegments +
                ", superchargerNodeIds=" + superchargerNodeIds +
                ", segmentTravelTimesMillis=" + segmentTravelTimesMillis +
                ", segmentLengths=" + segmentLengths +
                ", supChargingTimes=" + supChargingTimes +
                "} " + super.toString();
    }
}
