package cz.cvut.felk.its.routing.structures;

import cz.cvut.felk.its.evstructures.ConsumptionProfile;

import java.util.List;
import java.util.Objects;

/**
 * Created by Tomas on 12.11.2015.
 */
@Deprecated
public class Solution {
    public final int startNodeId;
    public final int goalNodeId;
    private final List<Integer> routeNodeIds;
    public final int length;
    public final int travelTimeMillis;
//    public final ConsumptionProfile consProfile;
    public final int price;
    public final List<Integer> statesOfCharge;
    public final List<Integer> statesAfterCharge;
    public ConsumptionProfile consumptionProfile;

    /**
     * @param routeNodeIds
     * @param length in meters
     * @param travelTimeMillis in seconds
     * @param price
     */
    public Solution(List<Integer> routeNodeIds, int length, int travelTimeMillis, int price,
            List<Integer> statesOfCharge, List<Integer> statesAfterCharge, ConsumptionProfile consProfile) {
        this.routeNodeIds = routeNodeIds;
        this.length = length;
        this.travelTimeMillis = travelTimeMillis;
        this.price = price;
        this.statesOfCharge = statesOfCharge;
        this.statesAfterCharge = statesAfterCharge;
        this.consumptionProfile = consProfile;

        if (!routeNodeIds.isEmpty()) {
            this.startNodeId = routeNodeIds.get(0);
            this.goalNodeId = routeNodeIds.get(routeNodeIds.size() - 1);
        } else {
            this.startNodeId = 0;
            this.goalNodeId = 0;
        }
    }

    public Solution(List<Integer> routeNodeIds, int length, int travelTimeMillis) {
        this(routeNodeIds, length, travelTimeMillis, 0, null, null, null);
    }

    public List<Integer> getRouteNodeIds() {
        return routeNodeIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Solution))
            return false;
        Solution solution = (Solution) o;
        return startNodeId == solution.startNodeId &&
                goalNodeId == solution.goalNodeId &&
                length == solution.length &&
                travelTimeMillis == solution.travelTimeMillis &&
                price == solution.price &&
                Objects.equals(routeNodeIds, solution.routeNodeIds) &&
                Objects.equals(statesOfCharge, solution.statesOfCharge) &&
                Objects.equals(statesAfterCharge, solution.statesAfterCharge) &&
                Objects.equals(consumptionProfile, solution.consumptionProfile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startNodeId, goalNodeId, routeNodeIds, length, travelTimeMillis, price, statesOfCharge,
                statesAfterCharge, consumptionProfile);
    }

    @Override
    public String toString() {
        return "Solution{" +
                "startNodes=" + startNodeId +
                ", goalNodes=" + goalNodeId +
                ", length=" + length + " meters" +
                ", travelTimeMillis=" + (double) travelTimeMillis /60 + " mins" +
//                ", routeIds=" + routeNodeIds +
                ", statesAfterCharge=" + statesAfterCharge +
                ", statesOfCharge=" + statesOfCharge +
                ", consumptionProfile=" + consumptionProfile +
                '}';
    }
}
