package cz.cvut.felk.its.routing.structures.graph;

import cz.agents.basestructures.GraphStructure;
import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.evstructures.chargers.Charger;
import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.routing.SpeedUpSettings;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.evstructures.*;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Tomas on 21-Jul-16.
 */
public class MOSearchGraph<TNode extends EvNode, TEdge extends EvEdge> implements SearchGraph<EvLabel> {

    private final GraphStructure<TNode, TEdge> graph;
    private final int startNodeId;
    private final int goalNodeId;
    private final LocalDateTime initTime;
    private final int lastCorrectingNodeId;
	private final BatteryType battery;
	private final ChargingFunction chFunction;
	private final double centsPerSecond;
	private final int ellipseThreshold;

	//    private final TNode startNode;
    //    private final TNode goalNode;
    //    private final HashMap<Integer, TEdge> startEdges; //key - edge.toId
    //    private final HashMap<Integer, TEdge> goalEdges; //key - edge.fromId
    Set<Integer> chargerNodeIds;

	public MOSearchGraph(GraphStructure<TNode, TEdge> graph) {
        this(graph, -2, LocalDateTime.now());
    }

	public MOSearchGraph(GraphStructure<TNode, TEdge> graph, int goalNodeId, LocalDateTime initTime) {
		this(graph, goalNodeId, initTime, -3, new ChargingFunction(), BatteryType.TESLA, Double.MAX_VALUE);
	}
    public MOSearchGraph(GraphStructure<TNode, TEdge> graph, int goalNodeId, LocalDateTime initTime, int lastCorrectingNodeId, ChargingFunction chFunction, BatteryType battery, double centsPerSecond) {
        this.graph = graph;
        this.initTime = initTime;
        this.chargerNodeIds = graph.getAllNodes().stream()
                .filter(EvNode::isCharger)
                .map(node -> node.id)
                .collect(Collectors.toSet());
        this.startNodeId = -1;
        this.goalNodeId = goalNodeId;
		this.lastCorrectingNodeId = lastCorrectingNodeId;
		this.chFunction = chFunction;
		this.battery = battery;
		this.centsPerSecond = centsPerSecond;
		double distBetweenOAndD = GPSLocationTools.computeDistanceAsDouble(
				graph.getNode(startNodeId),
				graph.getNode(goalNodeId));
		this.ellipseThreshold = (int) (distBetweenOAndD*1.5);
    }

    @Override
    public List<EvLabel> getSuccessors(EvLabel currFNode) {
        List<EvLabel> successors = new LinkedList<>();

        if (currFNode.nodeId != lastCorrectingNodeId) {

            if (currFNode.nodeId == goalNodeId) {
                inspectLastEdge(currFNode, successors);
            } else {

//				if (currFNode.nodeId == 26992) {
//					System.out.println("here");
//				}

                Collection<TEdge> outEdges = graph.getOutEdges(currFNode.nodeId);

                for (TEdge pEdge: outEdges) {

					if (!isInSearchEllipse(pEdge.toId)) continue;

                    if (pEdge instanceof EvEdgeBetweenChargers) {
                        EvEdgeBetweenChargers edge = (EvEdgeBetweenChargers) pEdge;
                        inspectOutEdgeBetweenCharger(edge, currFNode, successors);
                    }
                    //					for (EvEdge edge: pEdge.getEvEdges) {
                    //						inspectOutEdge(pEdge, currFNode, successors);
                    //					}
                }
            }

        }


        return successors;
    }

	private boolean isInSearchEllipse(int nodeId) {
		//destination is in Ellipse
		if (!SpeedUpSettings.REDUCE_BY_ELLIPSE || !chargerNodeIds.contains(nodeId)) return true;

		TNode node = graph.getNode(nodeId);
		int lengthFromOrigin = GPSLocationTools.computeDistance(
				graph.getNode(startNodeId), node);

		int lengthToDestination = GPSLocationTools.computeDistance(
				node, graph.getNode(goalNodeId));

		return lengthFromOrigin + lengthToDestination <= ellipseThreshold;
	}

	private void inspectOutEdgeBetweenCharger(EvEdgeBetweenChargers edge, EvLabel currFNode, List<EvLabel> successors) {

		int inEdgeId = -1;
        for (EvEdge parallelEdge : edge.getPaths()) {

			inEdgeId++;

//            List<EvLabel> nodes = evLabelPath.getNodes();
//            EvLabel toChargerLabel = nodes.get(nodes.size() - 1);

			// TODO: 22-Dec-16 investigate if
            if (SpeedUpSettings.ABORT_CHARGER_CYCLE && visitedChargerTwice(edge.toId, currFNode)) continue;

            //            if (!isFeasiblePath(currFNode, evLabelPath)) continue;

//            List<GPSLocation> viaNodes = new ArrayList<>();

//            List<EvLabel> viaLabels = nodes.subList(1, nodes.size()-1);
//            for (EvLabel node : viaLabels) {
//                TNode node1 = graph.getNode(node.nodeId);
//                viaNodes.add(node1);
//            }
//            EvEdge evEdge = new EvEdge(edge.fromId, edge.toId, 0, RoadType.PRECOMPUTED, toChargerLabel.travelTimeMillis,
//                    toChargerLabel.consProfile, viaNodes);
            inspectOutEdge(parallelEdge, currFNode, inEdgeId, successors);


        }
    }

    private boolean isFeasiblePath(EvLabel currFNode, Path<EvLabel> evLabelPath) {

        //TODO implement [MOSearchGraph:isFeasiblePath]
        throw new UnsupportedOperationException("[MOSearchGraph:isFeasiblePath] has not been implemented yet");
    }

    private void inspectLastEdge(EvLabel currFNode, List<EvLabel> successors) {

        if(currFNode.lastSeenCharger != null) {
            //compute min needed charging pair
            List<double[]> chPairs = ConsumptionProfileUtils.computePossibleChargingPairs(currFNode, chFunction, battery);
            if (chPairs == null) return;

            double[] bestChPair = chPairs.get(0);

            int nextTravelTimeMillis = (int) (bestChPair[0]); //edge.travelTimeMillis=0
            //			ConsumptionProfile nextConsProfile = goalNode.consProfile; // test this!!

            // TODO: 13-Nov-16 this holds only if optimizing on traveltime
            int nextCost = currFNode.costs[0] + (int) bestChPair[0] - currFNode.travelTimeMillis;


            int nextPrice = currFNode.costs[1] + computePriceForCharging(currFNode, bestChPair[1]);

            // TODO: 22-Nov-16 consider to replace last node not add!!!

            currFNode = new EvLabel(lastCorrectingNodeId, new int[]{nextCost, nextPrice}, currFNode, new ConsumptionProfile(), nextTravelTimeMillis,
                    currFNode, currFNode.inEdgeId ,bestChPair[1], 0);
        } else {
            // add last correcting label - even if no charger was visited
            double realStateOfChargeInLastNode = currFNode.consProfile.computeSocInB(currFNode.lastSeenChargerSoC);
            currFNode = new EvLabel(lastCorrectingNodeId, currFNode.costs, currFNode, new ConsumptionProfile(), currFNode.travelTimeMillis,
                    null, currFNode.inEdgeId, realStateOfChargeInLastNode, 0);
        }
        successors.add(currFNode);
    }

    private int computePriceForCharging(EvLabel currFNode, double stateInNextChargerAfterCharge) {

        double stateAfterCharge = ConsumptionProfileUtils.retrieveSoCInA(currFNode, stateInNextChargerAfterCharge, battery);
        Charger charger = graph.getNode(currFNode.lastSeenCharger.nodeId).getCharger();
        int pricePerKwh = charger.getPricePerkWh(
                initTime.plusSeconds(GraphTools.convertFromE3Format(currFNode.lastSeenCharger.travelTimeMillis)));


		double stateBeforeCharge = currFNode.lastSeenChargerSoC;

		//TIME COST MONEY
		int chargingTimeMillis = (int) new ChargingFunction().getChargingTimeMillis(stateBeforeCharge, stateAfterCharge);
		int timeCostMoney = (int) (centsPerSecond*GraphTools.convertFromE3Format(chargingTimeMillis));
		//TIME COST MONEY

		return (int) (pricePerKwh*((stateAfterCharge-stateBeforeCharge)/1000)) + timeCostMoney;
    }

    private void inspectOutEdge(EvEdge edge, EvLabel currFNode, int inEdgeId, List<EvLabel> successors) {

        EvLabel successor;

        // int nextCost = currFNode.costs + edge.consFunction() - potentials[edge.toId] + potentials[edge.fromId];
        int nextCost = currFNode.costs[0] + edge.getWeight();
        int nextTravelTimeMillis = currFNode.travelTimeMillis + edge.travelTimeMillis;
		int nextMoneyCost = currFNode.costs[1] + (int) (centsPerSecond*GraphTools.convertFromE3Format(edge.travelTimeMillis));

		ConsumptionProfile nextConsProfile =
                ConsumptionProfileUtils.linkConsumptionProfiles(currFNode.consProfile, edge.consumptionProfile);

		if (chargerNodeIds.contains(currFNode.nodeId)) {
			//is charger!!
			Charger charger = graph.getNode(currFNode.nodeId).getCharger();
			int pricePerKwh = charger.getPricePerkWh(
					initTime.plusSeconds(GraphTools.convertFromE3Format(currFNode.travelTimeMillis)));

			if (currFNode.lastSeenCharger == null) {
				//is first charger in the journey

				double initSoC = currFNode.lastSeenChargerSoC;
				double realSoCAtCharger = currFNode.consProfile.computeSocInB(initSoC);
				nextConsProfile = edge.consumptionProfile;


                successor = new EvLabel(edge.toId, new int[]{nextCost, nextMoneyCost}, currFNode, nextConsProfile, nextTravelTimeMillis, currFNode,
                        inEdgeId , realSoCAtCharger, pricePerKwh);
                if (isInBatteryRange(successor)) {
                    successors.add(successor);
                }

            } else {
                //is second or following charger
                //chargingPair := 2d int array - [0]chTime; [1]state of charge after charging (stateAfterCharge)

                List<double[]> chargingPairs = ConsumptionProfileUtils.computePossibleChargingPairs(currFNode, chFunction, battery);
                if (chargingPairs == null) return;

                for (double[] chPair: chargingPairs) {
                    nextTravelTimeMillis = (int) chPair[0] + edge.travelTimeMillis;
                    nextConsProfile = edge.consumptionProfile;

                    // TODO: 13-Nov-16 this holds only if optimizing on traveltime
                    nextCost = currFNode.costs[0] + (int) chPair[0] - currFNode.travelTimeMillis + edge.travelTimeMillis;

                    int nextPrice = currFNode.costs[1] + computePriceForCharging(currFNode, chPair[1])
							+ (int) (centsPerSecond*GraphTools.convertFromE3Format(edge.travelTimeMillis));


					successor = new EvLabel(edge.toId, new int[]{nextCost, nextPrice}, currFNode, nextConsProfile,
                            nextTravelTimeMillis, currFNode, inEdgeId, chPair[1], pricePerKwh);

//                    if (visitedChargerTwice(successor)) {
//                        log.debug("id(" + successor.nodeId + ") charger visited twice");
//                    }

                    if (isInBatteryRange(successor)) {
                        successors.add(successor);
                    }

                }
            }
        } else {
            //not charger

            successor = new EvLabel(edge.toId, new int[]{nextCost, nextMoneyCost}, currFNode, nextConsProfile, nextTravelTimeMillis,
                    currFNode.lastSeenCharger, inEdgeId, currFNode.lastSeenChargerSoC, currFNode.lastSeenChargerCpkWh);

            if (isInBatteryRange(successor)) {
                successors.add(successor);
            }
        }
    }

    private boolean isInBatteryRange(EvLabel successor) {
        if (successor.consProfile == null) return true;

        //		if (visitedChargerTwice(successor)) return false;

        return !(successor.consProfile.inSoc > battery.getMaxSoC()
                || (successor.consProfile.outSoc < battery.getMinSoC() && successor.lastSeenCharger == null)
                || (successor.lastSeenChargerSoC < successor.consProfile.inSoc && successor.lastSeenCharger == null)
                || (successor.lastSeenChargerSoC > successor.consProfile.inSoc && successor.lastSeenCharger != null));

    }

    private boolean visitedChargerTwice(EvLabel successor) {

        int nextNodeId = successor.nodeId;

        EvLabel prevLabel = successor.prevLabel;
        while (prevLabel.lastSeenCharger != null) {
            if (prevLabel.lastSeenCharger.nodeId == nextNodeId) {
                return true;
            }
            prevLabel = prevLabel.prevLabel;
        }
        return false;
    }

    private boolean visitedChargerTwice(int nextChargerId, EvLabel currNode) {


        EvLabel prevLabel = currNode;
        while (prevLabel.lastSeenCharger != null) {
            if (prevLabel.lastSeenCharger.nodeId == nextChargerId) {
//                System.out.println("Charger visited twice.");
				return true;
            }
            prevLabel = prevLabel.prevLabel;
        }
        return false;
    }


    @Override
    public Path<EvLabel> buildPath(EvLabel goalNode) {

        //		if(goalNode.lastSeenCharger >= 0) {
        //			double[] bestChPair = ConsumptionProfileUtils.computePossibleChargingPairs(goalNode).get(0);
        //			int nextTravelTimeMillis = (int) (bestChPair[0]);
        ////			ConsumptionProfile nextConsProfile = goalNode.consProfile; // test this!!
        //
        //			// TODO: 13-Nov-16 this holds only if optimizing on traveltime
        //			int nextCost = goalNode.costs + (int) bestChPair[0] - goalNode.travelTimeMillis;
        //
        //			// TODO: 22-Nov-16 consider to replace last node not add!!!
        //
        //			goalNode = new EvLabel(goalNode.nodeId, nextCost, goalNode, null, nextTravelTimeMillis,
        //					goalNode.nodeId, bestChPair[1]);
        //		}

        List<EvLabel> nodes = new ArrayList<>();

        EvLabel node = goalNode;
        while (node != null) {
            nodes.add(node);
            node = node.prevLabel;
        }

        Collections.reverse(nodes);

        return new Path<>(goalNode.costs, nodes);
    }

    @Override
    public Set<Integer> getAllNodeIds() {
        Set<Integer> allNodeIds = graph.getAllNodes().stream().mapToInt(n -> n.id).boxed().collect(Collectors.toSet());
        allNodeIds.add(lastCorrectingNodeId);
        return allNodeIds;
    }

}
