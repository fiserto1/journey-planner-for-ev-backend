package cz.cvut.felk.its.routing.alg;

import cz.cvut.felk.its.routing.structures.goalchecker.GoalChecker;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.MOFringeNode;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by Tomas on 21-Jul-16.
 */
@Deprecated
public class MODijkstra<FLabel extends EvLabel> {

	private final Logger log = Logger.getLogger(MODijkstra.class);

	private Set<FLabel> startNodes;
	private SearchGraph<FLabel> graph;
	private List<Path<FLabel>> paths = new ArrayList<>();
	private GoalChecker<FLabel> goalChecker;

	private PriorityQueue<MOFringeNode> fringe = new PriorityQueue<>();
	private Map<Integer, PriorityQueue<FLabel>> labelSet;
	private Map<Integer, List<FLabel>> settledLabels; //maybe do for every vertex


	public MODijkstra(SearchGraph<FLabel> graph, Set<FLabel> startNodes,
			GoalChecker<FLabel> goalChecker) {
		this.graph = graph;
		this.goalChecker = goalChecker;
		this.startNodes = startNodes;

		Set<Integer> nodeIds = graph.getAllNodeIds();
		int numOfNodes = nodeIds.size();

		settledLabels = new HashMap<>(numOfNodes);
		labelSet = new HashMap<>(numOfNodes);

		for (Integer nodeId : nodeIds) {
			labelSet.put(nodeId, new PriorityQueue<>());
			settledLabels.put(nodeId, new ArrayList<>());
		}
	}

    public void run() {

		for (FLabel startNode: startNodes) {
            labelSet.get(startNode.nodeId).add(startNode); //consumptionProfile null or some values??
            fringe.add(new MOFringeNode(startNode.nodeId, startNode.costs[0]));
        }

        MOFringeNode currentFNode;

//		int counter = 0;
        while(!fringe.isEmpty()) {
//			counter++;
//			if (fringe.size() > 103000) {
//				log.debug(fringe.size());
//			}
			currentFNode = fringe.poll();
//			if (counter > 10000) {
//				counter = 0;
//				log.debug("currentTravelTime: " + GraphTools.convertFromE3Format(currentFNode.travelTimeMillis) + " s");
//			}
			int currentNodeId = currentFNode.nodeId;

			if (labelSet.get(currentNodeId).isEmpty()) {
				continue; //duplicates in fringe.... count how often this happens
			}


			//get min label and mark as settled
			FLabel minLabel = settle(labelSet.get(currentNodeId), settledLabels.get(currentNodeId));
			if (!labelSet.get(currentNodeId).isEmpty()) {
				fringe.add(new MOFringeNode(currentNodeId, labelSet.get(currentNodeId).peek().travelTimeMillis));
			}

			//CUT SIMILAR LABELS
//			if (minLabel.consProfile != null) {
//				boolean skip = false;
//				for (FLabel label : settledLabels.get(currentNodeId)) {
//					if (
//						//					(successor.travelTimeMillis > label.travelTimeMillis && successor.travelTimeMillis*0.95 <= label.travelTimeMillis) ||
//							(minLabel.travelTimeMillis > label.travelTimeMillis && (minLabel.travelTimeMillis-label.travelTimeMillis)<60000) ||
//									(minLabel.travelTimeMillis < label.travelTimeMillis && (label.travelTimeMillis-minLabel.travelTimeMillis)<60000) ||
//									minLabel.isDominatedBy(label)) {
//						skip = true;
//					}
//				}
//
//				if (skip) continue;
//			}
//
//						if (minLabel == null) {
//				continue; // duplicates in the fringe!!
//			}

			if (goalChecker.isGoal(minLabel)){

				log.debug(currentFNode.travelTimeMillis);
				log.debug(labelSet.get(currentNodeId));
				log.debug(settledLabels.get(currentNodeId));

				Path<FLabel> fLabelPath = graph.buildPath(minLabel);
				paths.add(fLabelPath);
				if (goalChecker.isSearchFinished()) {
					break;
				}
			}


			List<FLabel> successors = graph.getSuccessors(minLabel);

			for (FLabel successor : successors) {
				if (!isDominatedByEarlierSuccessor(successor)) {
					PriorityQueue<FLabel> earlierSuccessorQueue = labelSet.get(successor.nodeId);
					if (earlierSuccessorQueue.isEmpty() || successor.travelTimeMillis < earlierSuccessorQueue.peek().travelTimeMillis) {
						fringe.add(new MOFringeNode(successor.nodeId, successor.travelTimeMillis));
					}

					//remove worse labels from labelSet - test with and without this
//							earlierSuccessorQueue.removeIf(l -> l.isDominatedBy(successor));
					//OR
					//		Iterator<FLabel> iterator = sameIdSuccessors.iterator();
					//		while (iterator.hasNext()) {
					//			if (iterator.next().isDominatedBy(successor)) {
					//				iterator.remove();
					//			}
					//		}

					earlierSuccessorQueue.add(successor);
				}
			}
        }
    }

	public List<Path<FLabel>> getPaths() {
		return paths;
	}

	/**
	 * @param successor the new successor
	 * @return true if some label visited earlier is better than new successor
	 */
    public boolean isDominatedByEarlierSuccessor(FLabel successor) {

		int maxTravelTimeDiff = 600000;

		PriorityQueue<FLabel> sameIdSuccessors = labelSet.get(successor.nodeId);
		List<FLabel> settledAlready = settledLabels.get(successor.nodeId);

		// TODO: 24-Nov-16 try switch these for-cycles
		for (FLabel label : settledAlready) {
			if (
//					(successor.travelTimeMillis > label.travelTimeMillis && successor.travelTimeMillis*0.95 <= label.travelTimeMillis) ||
					(successor.travelTimeMillis > label.travelTimeMillis && (successor.travelTimeMillis-label.travelTimeMillis)< maxTravelTimeDiff) ||
//					(successor.travelTimeMillis < label.travelTimeMillis && (label.travelTimeMillis-successor.travelTimeMillis)< maxTravelTimeDiff) ||
					successor.isDominatedBy(label, 0, LocalDateTime.now())) {
				return true;
			}
		}
		for (FLabel label : sameIdSuccessors) {
			if (
				//					(successor.travelTimeMillis > label.travelTimeMillis && successor.travelTimeMillis*0.95 <= label.travelTimeMillis) ||
					(successor.travelTimeMillis > label.travelTimeMillis && (successor.travelTimeMillis-label.travelTimeMillis)< maxTravelTimeDiff) ||
//							(successor.travelTimeMillis < label.travelTimeMillis && (label.travelTimeMillis-successor.travelTimeMillis)< maxTravelTimeDiff) ||
							successor.isDominatedBy(label, 0, LocalDateTime.now())) {
				return true;
			}
		}


		return false;
    }

    private FLabel settle(PriorityQueue<FLabel> labels, List<FLabel> settledLabels) {
        FLabel minLabel = labels.poll();
        settledLabels.add(minLabel);
        return minLabel;
    }
}
