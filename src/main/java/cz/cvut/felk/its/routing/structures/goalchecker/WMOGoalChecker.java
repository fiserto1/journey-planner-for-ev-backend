/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures.goalchecker;

import cz.cvut.felk.its.routing.SpeedUpSettings;
import cz.cvut.felk.its.routing.structures.EwLabel;

@Deprecated
public class WMOGoalChecker<FLabel extends EwLabel> implements WGoalChecker<FLabel> {
	private int maxTravelTimeMillis;
	private final int goalNodeId;
	private FLabel firstHit;
	private boolean isSearchFinished;

	public WMOGoalChecker() {
		this(-2);
	}

	public WMOGoalChecker(int goalNodeId) {
		this.goalNodeId = goalNodeId;
		this.isSearchFinished = false;
	}

	@Override
	public boolean isGoal(FLabel fLabel) {
		if (firstHit != null && SpeedUpSettings.EARLY_SEARCH_FINISH &&
		(fLabel.travelTimeMillis > maxTravelTimeMillis)) {

			isSearchFinished = true;
			// TODO: 25-Nov-16 change this... now the first goal is optimal because of -3 Id
		}

		if (fLabel.nodeId == goalNodeId) {
//			isSearchFinished = true;
			if (firstHit == null) {
				firstHit = fLabel;
				maxTravelTimeMillis = fLabel.travelTimeMillis + SpeedUpSettings.earlySearchFinishTTMillis;
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isSearchFinished() {

		return isSearchFinished;
	}

	@Override
	public void setSearchFinished(boolean isSearchFinished) {

		//TODO implement [MOGoalChecker:setSearchFinished]
		throw new UnsupportedOperationException("[MOGoalChecker:setSearchFinished] has not been implemented yet");
	}
}
