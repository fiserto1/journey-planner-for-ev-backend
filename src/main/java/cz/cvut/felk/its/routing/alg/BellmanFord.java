package cz.cvut.felk.its.routing.alg;

import cz.agents.basestructures.Graph;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;

import java.util.Arrays;

/**
 * Created by Tomas on 09-Mar-16.
 */
public class BellmanFord {

    public int[] bellmanFord(Graph<EvNode, EvEdge> graph, int sourceId) {
        int numberOfNodes = graph.getAllNodes().size();
        int[] distances = new int[numberOfNodes];

        Arrays.fill(distances, Integer.MAX_VALUE);

        distances[sourceId] = 0;
        int newDistance;
        for (int i = 0; i < numberOfNodes-1; i++) {
            for (EvEdge edge: graph.getAllEdges()) {
                newDistance = distances[edge.fromId] + edge.consumptionProfile.cost;
                if (distances[edge.fromId] != Integer.MAX_VALUE && newDistance < distances[edge.toId]) {
                    distances[edge.toId] = newDistance;
                }
            }
        }

        for (EvEdge edge: graph.getAllEdges()) {
            newDistance = distances[edge.fromId] + edge.consumptionProfile.cost;
            if (distances[edge.fromId] != Integer.MAX_VALUE && newDistance < distances[edge.toId]) {
                System.out.println("Graph has a negative cycle.");
                return null;
            }
        }

        return distances;
    }
}
