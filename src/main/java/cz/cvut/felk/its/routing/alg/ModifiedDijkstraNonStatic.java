/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.alg;

import cz.cvut.felk.its.routing.structures.goalchecker.GoalChecker;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;
import cz.cvut.felk.its.evstructures.ConsumptionProfile;

import java.util.*;

public class ModifiedDijkstraNonStatic<FNode extends EvLabel> {
	private PriorityQueue<FNode> fringe = new PriorityQueue<>();
//	private HashMap<Integer, FNode> inspected = new HashMap<>();
	private Map<Integer, ConsumptionProfile> bestConsProfilesOfClosedNodes = new HashMap<>();
	private Map<Integer, Integer> bestTravelTimeOfClosedNodes = new HashMap<>();

	private SearchGraph<FNode> graph;

	private Set<FNode> startNodes;


	private List<Path<FNode>> paths = new ArrayList<>();
	private GoalChecker<FNode> goalChecker;

	private int maxRadius;

	int[] visitedSoCLimitById;

	public ModifiedDijkstraNonStatic(SearchGraph<FNode> graph) {
	}

	public ModifiedDijkstraNonStatic(SearchGraph<FNode> graph, FNode startNode,	GoalChecker<FNode> goalChecker) {
		this(graph, Collections.singleton(startNode), goalChecker, Integer.MAX_VALUE);
	}

	public ModifiedDijkstraNonStatic(SearchGraph<FNode> graph, FNode startNode,	GoalChecker<FNode> goalChecker,
			int maxRadius) {
		this(graph, Collections.singleton(startNode), goalChecker, maxRadius);
	}

	public ModifiedDijkstraNonStatic(SearchGraph<FNode> graph, Set<FNode> startNodes,
			GoalChecker<FNode> goalChecker) {
		this(graph, startNodes, goalChecker, Integer.MAX_VALUE);
	}

	public ModifiedDijkstraNonStatic(SearchGraph<FNode> graph, Set<FNode> startNodes,
			GoalChecker<FNode> goalChecker, int maxRadius) {
		this.graph = graph;
		this.startNodes = startNodes;
		this.goalChecker = goalChecker;
		this.maxRadius = maxRadius;
	}


	public void run() {

		for (FNode startNode : startNodes) {
			fringe.add(startNode);
//			inspected.put(startNode.nodeId, startNode);
		}

		FNode currentFNode;

		while(!fringe.isEmpty()) {

//			if (fringe.size() > 2000) {
//				Set<Integer> collect1 = fringe.stream().mapToInt(f -> f.nodeId).boxed().collect(Collectors.toSet());
//				collect1.addAll(bestConsProfilesOfClosedNodes.keySet());
//
//				SerializeUtil.serializeObject(collect1, "nodeids.ser");
//				return;
//
//			}
			currentFNode = fringe.poll();

//			if (currentFNode.nodeId == 19227 && currentFNode.costs == 3460920) {
//				System.out.println(currentFNode);
//			}
//			inspected.remove(currentFNode.nodeId);

//			if (bestConsProfilesOfClosedNodes.contains(currentFNode)) continue;

			if (goalChecker.isGoal(currentFNode)) { //!bestConsProfilesOfClosedNodes.containsKey(currentFNode.nodeId) &&

				paths.add(graph.buildPath(currentFNode));
				//				paths.add(buildPath(currentFNode));

				if (goalChecker.isSearchFinished()) {
					break;

				}
			}
			if (!bestConsProfilesOfClosedNodes.containsKey(currentFNode.nodeId)) {
				bestConsProfilesOfClosedNodes.put(currentFNode.nodeId, currentFNode.consProfile); //bestConsProfilesOfClosedNodes list can be removed by modifying goalChecker
				bestTravelTimeOfClosedNodes.put(currentFNode.nodeId, currentFNode.travelTimeMillis);
			} else {
				if (!hasBetterConsProfileThanNodeInClosed(currentFNode)) {
					continue;
				} {
					bestConsProfilesOfClosedNodes.put(currentFNode.nodeId, currentFNode.consProfile);
				}
			}

			List<FNode> successors = graph.getSuccessors(currentFNode);

			for (FNode successor : successors) {
//				if (bestConsProfilesOfClosedNodes.contains(successor)) continue;

				if (bestConsProfilesOfClosedNodes.containsKey(successor.nodeId)) {
					inspectNewClosedSuccessor(successor);
				} else {

					inspectSuccessor(successor);
				}
				//				if (inspected.containsKey(successor.nodeId)) {
				//					inspectVisitedSuccessor(successor);
				//				} else {
				//				}
			}
		}

		fringe.clear();
//		inspected.clear();
		bestConsProfilesOfClosedNodes.clear();

	}

	private void inspectNewClosedSuccessor(FNode successor) {
		int bestTravelTimeOfNode = bestTravelTimeOfClosedNodes.get(successor.nodeId);

		// TODO: 24-Nov-16 this is wrong.... need to check all settled and unsettled labels
		if (successor.travelTimeMillis < bestTravelTimeOfNode
//				|| !successor.isDominatedBy(bestLabel)
				|| (successor.travelTimeMillis >= bestTravelTimeOfNode && hasBetterConsProfileThanNodeInClosed(successor))
				){
			inspectSuccessor(successor);
		}

	}

	private boolean hasBetterConsProfileThanNodeInClosed(FNode successor) {
		ConsumptionProfile consumptionProfile = bestConsProfilesOfClosedNodes.get(successor.nodeId);
		if (consumptionProfile.equals(successor.consProfile)) return false;
		//			bestConsProfilesOfClosedNodes.put(successor.nodeId, successor.consProfile);
		return consumptionProfile.isDominatedBy(successor.consProfile);
//		return !successor.consProfile.isDominatedBy(fNode.consProfile);
	}

//	private void inspectVisitedSuccessor(FNode successor) {
//
//		FNode oldFNode = inspected.get(successor.nodeId);
//		if (successor.isBetterThan(oldFNode)) {
//			fringe.remove(oldFNode);
//			inspectSuccessor(successor);
//		}
//
//	}

	private void inspectSuccessor(FNode successor) {

		if (successor.costs[0] > maxRadius) return;

//		inspected.put(successor.nodeId, successor);
		fringe.add(successor);

	}

	public List<Path<FNode>> getPaths() {
		return paths;
	}
}
