package cz.cvut.felk.its.routing.structures;

/**
 * Created by Tomas on 21-Jul-16.
 */
@Deprecated
public class MOFringeNode implements Comparable<MOFringeNode> {
    public final int nodeId;
    public final int travelTimeMillis;

    public MOFringeNode(int nodeId, int travelTimeMillis) {
        this.nodeId = nodeId;
        this.travelTimeMillis = travelTimeMillis;
    }

    @Override
    public int compareTo(MOFringeNode o) {
        return travelTimeMillis - o.travelTimeMillis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof MOFringeNode))
            return false;

        MOFringeNode that = (MOFringeNode) o;

        if (nodeId != that.nodeId)
            return false;
        return travelTimeMillis == that.travelTimeMillis;

    }

    @Override
    public int hashCode() {
        int result = nodeId;
        result = 31 * result + travelTimeMillis;
        return result;
    }

    @Override
    public String toString() {
        return "MOFringeNode [" +
                "nodeId=" + nodeId +
                ", travelTimeMillis=" + travelTimeMillis +
                ']';
    }
}
