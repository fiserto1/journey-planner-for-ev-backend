/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures.routers;

import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.GraphStructure;
import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.evstructures.chargers.Charger;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.EwLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.routing.structures.WPath;
import cz.cvut.felk.its.evstructures.*;
import cz.cvut.felk.its.evstructures.journey.Journey;
import cz.cvut.felk.its.evstructures.journey.JourneyNode;
import cz.cvut.felk.its.evstructures.journey.JourneySegment;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class JourneyBuilder {

	private static final Logger log = Logger.getLogger(JourneyBuilder.class);

	public static Journey buildJourney(Path<EvLabel> bestPath, GraphStructure<EvNode, EvEdge> extendedEvGraph,
			LocalDateTime initTime, BatteryType battery, RouterType routerType) {

//		GraphStructure<EvNode, EvEdge> extendedEvGraph = plInstance.getGraphFMLM();

		List<JourneySegment> journeySegments = new ArrayList<>();
		int length = 0;
		int consumption = 0;
		int price = 0;


		List<EvLabel> nodes = bestPath.getNodes();
		EvLabel startNode = nodes.get(0);
		EvLabel lastCorrectingNode = nodes.get(nodes.size()-1);
		EvLabel goalNode = nodes.get(nodes.size()-2);
		List<JourneyNode> viaNodes = new ArrayList<>();

		int travelTimeMillis = lastCorrectingNode.travelTimeMillis;

		int stateOfCharge = -1;
		int stateAfterChargeOnLastCharger = (int) startNode.lastSeenChargerSoC;

		int i=0;
		for (EvLabel node : nodes) {



			int stateBeforeCharge;
			if (node.consProfile == null) {
				stateBeforeCharge = (int) node.lastSeenChargerSoC;
			} else if (extendedEvGraph.getNode(node.nodeId).isCharger()) {
				stateBeforeCharge = (int) nodes.get(i+1).lastSeenChargerSoC;
			} else {
				// TODO: 24-Nov-16 is this correct????
				//				stateBeforeCharge = (int) node.consProfile.computeSocInB(stateOfCharge);
				stateBeforeCharge = (int) node.consProfile.computeSocInB(stateAfterChargeOnLastCharger);
			}
			int stateAfterCharge = stateBeforeCharge;
			int pricePerKwh = -1;


			if (extendedEvGraph.getNode(node.nodeId).isCharger()) {
				int j = i+1;
				while (j < nodes.size()-2 && !extendedEvGraph.getNode(nodes.get(j).nodeId).isCharger()) {
					j++;
				}
				EvLabel nextCharger = nodes.get(j);
				//				double stateInNextChargerBeforeCharge = nextCharger.consProfile.computeSocInB(stateBeforeCharge);
				double stateInNextChargerAfterCharge = nodes.get(j+1).lastSeenChargerSoC;
				stateAfterCharge = ConsumptionProfileUtils.retrieveSoCInA(nextCharger, stateInNextChargerAfterCharge,
						battery);
				double energyCharged = stateAfterCharge - stateBeforeCharge;
				//				stateAfterCharge = (int) (stateBeforeCharge + energyCharged);
				Charger charger = extendedEvGraph.getNode(node.nodeId).getCharger();
				pricePerKwh = charger.getPricePerkWh(
						initTime.plusSeconds(GraphTools.convertFromE3Format(node.travelTimeMillis)));

				//				double chargingTimeMillis = new ChargingFunction()
				//						.getChargingTimeMillis(stateBeforeCharge, stateAfterCharge);
				//				travelTimeMillis += chargingTimeMillis;

				price += pricePerKwh*(((double) stateAfterCharge-stateBeforeCharge)/1000);


			}

			if (node.inEdgeId >= 0) {
				EvEdge edge = extendedEvGraph.getEdge(node.prevLabel.nodeId, node.nodeId);
				if (edge instanceof EvEdgeBetweenChargers) {
					EvEdgeBetweenChargers betweenEdge = (EvEdgeBetweenChargers) edge;

					//PATHS WERE REMOVED
//					List<EvLabel> viaLabels = betweenEdge.getPaths().get(node.inEdgeId).getNodes();
//					viaLabels = viaLabels.subList(1, viaLabels.size()-1);
//
//					for (EvLabel viaLabel : viaLabels) {
//						viaNodes.add(new JourneyNode(viaLabel.nodeId, 0, 0, -1));
//					}
				}
			}

			viaNodes.add(new JourneyNode(node.nodeId, stateBeforeCharge, stateAfterCharge, pricePerKwh));

			if (extendedEvGraph.getNode(node.nodeId).isCharger()) {

				consumption += node.consProfile.getConsumption(stateAfterChargeOnLastCharger);
				log.debug(node.consProfile.getConsumption(stateAfterChargeOnLastCharger));
				stateAfterChargeOnLastCharger = stateAfterCharge;

				JourneyNode from = viaNodes.remove(0);
				JourneyNode to = viaNodes.remove(viaNodes.size()-1);

				journeySegments.add(new JourneySegment(from, to, viaNodes));

				viaNodes = new ArrayList<>();
				viaNodes.add(to);
			}

			//			travelTimeMillis += node.travelTimeMillis;

			if (i < nodes.size()-2) {
				length += GPSLocationTools.computeDistance(extendedEvGraph.getNode(node.nodeId),
						extendedEvGraph.getNode(nodes.get(i+1).nodeId));

			}
			if (node.nodeId == goalNode.nodeId) {
				//last segment
				JourneyNode from = viaNodes.remove(0);
				JourneyNode to = viaNodes.remove(viaNodes.size()-1);

				consumption += node.consProfile.getConsumption(stateAfterChargeOnLastCharger);
//				if (node.lastSeenCharger != -1) {
//					//					consumption += node.prevLabel.consProfile.getConsumption(node.lastSeenChargerSoC);
//					log.debug(node.consProfile.getConsumption(stateAfterChargeOnLastCharger));
//
////					JourneyNode preLast = viaNodes.remove(viaNodes.size()-1);
//				} else {
//					//					consumption += node.consProfile.getConsumption(node.lastSeenChargerSoC);
//					consumption += node.consProfile.getConsumption(stateAfterChargeOnLastCharger);
//
//				}

				journeySegments.add(new JourneySegment(from, to, viaNodes));

				break; //skip last correcting node

			}

			stateOfCharge = stateAfterCharge;
			i++;
		}

		return new Journey(startNode.nodeId, goalNode.nodeId, journeySegments,
				length, travelTimeMillis, consumption, price, routerType);
	}

	public static Journey buildJourneyOnFMLMGraph(Path<EvLabel> bestPath, GraphStructure<EvNode, EvEdge> fmLMGraph,
			LocalDateTime initTime, BatteryType battery, RouterType routerType) {

		List<JourneySegment> journeySegments = new ArrayList<>();
		int length = 0;
		int consumption = 0;
		int price = 0;


		List<EvLabel> nodes = bestPath.getNodes();
		EvLabel startNode = nodes.get(0);
		EvLabel lastCorrectingNode = nodes.get(nodes.size()-1);
		EvLabel goalNode = nodes.get(nodes.size()-2);
		List<JourneyNode> viaNodes = new ArrayList<>();

		int travelTimeMillis = lastCorrectingNode.travelTimeMillis;
		int totalPrice = bestPath.getCostVector()[1];

//		int stateOfCharge = -1;
		int stateAfterChargeOnLastCharger = (int) startNode.lastSeenChargerSoC;

//		int i=0;
		EvLabel segmentStart = nodes.get(0);
		EvLabel segmentEnd;

		int initSoC = (int) segmentStart.lastSeenChargerSoC;
		JourneyNode fromNode = new JourneyNode(segmentStart.nodeId, initSoC,
				initSoC, -1);
		JourneyNode toNode;

		for (int i = 0; i < nodes.size()-2; i++) {
			segmentStart = nodes.get(i);
			segmentEnd = nodes.get(i+1);

//			if (segmentEnd.nodeId == 26992) {
//				System.out.println("here");
//			}

			int stateBeforeCharge = (int) segmentEnd.consProfile.computeSocInB(stateAfterChargeOnLastCharger);
//			if (fmLMGraph.getNode(segmentStart.nodeId).isCharger()) {
//				stateBeforeCharge = (int) nodes.get(i+1).lastSeenChargerSoC; // equals segmentStart.consProfile.computeSocInB(stateAfterChargeOnLastCharger);
//			}

			//for destination
			int stateAfterCharge = stateBeforeCharge;
			int pricePerKwh = -1;
			toNode = new JourneyNode(segmentEnd.nodeId, stateBeforeCharge, stateAfterCharge, pricePerKwh);


			if (fmLMGraph.getNode(segmentEnd.nodeId).isCharger()) {

				EvLabel nextCharger = nodes.get(i+1);
				EvLabel nextNextCharger = nodes.get(i+2);

				//				double stateInNextChargerBeforeCharge = nextCharger.consProfile.computeSocInB(stateBeforeCharge);
				double stateInNextChargerAfterCurrCharge = nodes.get(i+3).lastSeenChargerSoC;

				stateAfterCharge = ConsumptionProfileUtils.retrieveSoCInA(nextNextCharger, stateInNextChargerAfterCurrCharge,
						battery);
				double energyCharged = stateAfterCharge - stateBeforeCharge;
				if (energyCharged < 0) {
					log.debug("Ouuuuch.");
				}
				Charger charger = fmLMGraph.getNode(segmentEnd.nodeId).getCharger();
				pricePerKwh = charger.getPricePerkWh(
						initTime.plusSeconds(GraphTools.convertFromE3Format(segmentEnd.travelTimeMillis)));

				//				double chargingTimeMillis = new ChargingFunction()
				//						.getChargingTimeMillis(stateBeforeCharge, stateAfterCharge);
				//				travelTimeMillis += chargingTimeMillis;

				price += pricePerKwh*(((double) stateAfterCharge-stateBeforeCharge)/1000);

				toNode = new JourneyNode(nextCharger.nodeId, stateBeforeCharge, stateAfterCharge, pricePerKwh);
			}

			// get Via nodes
			EvEdge edge = fmLMGraph.getEdge(segmentStart.nodeId, segmentEnd.nodeId);
			if (edge instanceof EvEdgeBetweenChargers) {
				EvEdgeBetweenChargers betweenEdge = (EvEdgeBetweenChargers) edge;
//				List<EvLabel> viaAndToLabels = betweenEdge.getPaths().get(segmentEnd.inEdgeId).getNodes();
				//EvNodes -> ViaNodes
				List<GPSLocation> evViaNodes = betweenEdge.getPaths().get(segmentEnd.inEdgeId).viaNodes;
//				viaAndToLabels = viaAndToLabels.subList(1, viaAndToLabels.size());
				EvNode node = fmLMGraph.getNode(betweenEdge.fromId);
				ConsumptionProfile consumptionProfile = null;

				for (GPSLocation viaLoc : evViaNodes) {
					EvNode viaEvNode = (EvNode) viaLoc;
					//build and link cons profiles
					ConsumptionProfile newConsProfile = ConsumptionProfileUtils
							.computeConsumptionProfileBetweenTwoLocs(node, viaEvNode);

					consumptionProfile = ConsumptionProfileUtils
							.linkConsumptionProfiles(consumptionProfile, newConsProfile);

					//never charger
					int soc = (int) consumptionProfile.computeSocInB(stateAfterChargeOnLastCharger);
					// TODO: 06-Dec-16 compare sBc after for-loop if match stateAfterCharge

					length += GPSLocationTools.computeDistance(node, viaEvNode);

					viaNodes.add(new JourneyNode(viaEvNode.id, soc, soc, -1));

					node = viaEvNode;
				}

				EvNode to = fmLMGraph.getNode(betweenEdge.toId);
				length += GPSLocationTools.computeDistance(node, to);

				/////////////////////////////////////////////////////////////////////////////////////
				// TODO: 06-Dec-16 compare scc after for-loop if match stateAfterCharge
				ConsumptionProfile newConsProfile = ConsumptionProfileUtils
						.computeConsumptionProfileBetweenTwoLocs(node, to);
				consumptionProfile = ConsumptionProfileUtils
						.linkConsumptionProfiles(consumptionProfile, newConsProfile);
				//never charger
				int soc = (int) consumptionProfile.computeSocInB(stateAfterChargeOnLastCharger);
				//////////////////////////////////////////////////////////////////////////////////////

				//OLD VERSION WITH PATHS
//				for (EvLabel viaLabel : viaAndToLabels) {
//					//never charger
//					int soc = (int) viaLabel.consProfile.computeSocInB(stateAfterChargeOnLastCharger);
//					// TODO: 06-Dec-16 compare sBc after for-loop if match stateAfterCharge
//
//					length += GPSLocationTools.computeDistance(fmLMGraph.getNode(viaLabel.prevLabel.nodeId),
//							fmLMGraph.getNode(viaLabel.nodeId));
//
//					if (viaLabel.nodeId != viaAndToLabels.get(viaAndToLabels.size()-1).nodeId) {
//						//do not add last node
//						viaNodes.add(new JourneyNode(viaLabel.nodeId, soc, soc, -1));
//					}
//				}

			}

			consumption += segmentEnd.consProfile.getConsumption(stateAfterChargeOnLastCharger);
			if (consumption == 3000000 ) {
				log.debug("Bug... not again.");
			}
			stateAfterChargeOnLastCharger = stateAfterCharge;

			//createSegment
			journeySegments.add(new JourneySegment(fromNode, toNode, viaNodes));

			fromNode = toNode;
			viaNodes = new ArrayList<>();

		}

		return new Journey(startNode.nodeId, goalNode.nodeId, journeySegments,
				length, travelTimeMillis, consumption, totalPrice, routerType);
	}

	public static Journey buildJourneyOnFMLMGraph(WPath<EwLabel> bestPath, GraphStructure<EvNode, EvEdge> fmLMGraph,
			LocalDateTime initTime, BatteryType battery, RouterType routerType) {

		List<JourneySegment> journeySegments = new ArrayList<>();
		int length = 0;
		int consumption = 0;
		int price = 0;


		List<EwLabel> nodes = bestPath.getNodes();
		EwLabel startNode = nodes.get(0);
		EwLabel goalNode = nodes.get(nodes.size()-1);
		List<JourneyNode> viaNodes = new ArrayList<>();

		int travelTimeMillis = bestPath.getCostVector()[0];
		int totalPrice = bestPath.getCostVector()[1];

		//		int stateOfCharge = -1;
//		int stateAfterChargeOnLastCharger = (int) startNode.lastSeenChargerSoC;

		//		int i=0;
		EwLabel segmentStart = nodes.get(0);
		EwLabel segmentEnd;
		EwLabel nextSegmentEnd;

		int initSoC = (int) segmentStart.inSoC;
		JourneyNode fromNode = new JourneyNode(segmentStart.nodeId, initSoC,
				initSoC, (int) segmentStart.cpkWh);
		JourneyNode toNode;

		for (int i = 0; i < nodes.size()-2; i++) {
			segmentStart = nodes.get(i);
			segmentEnd = nodes.get(i+1);

			nextSegmentEnd = nodes.get(i+2);
			//			if (segmentEnd.nodeId == 26992) {
			//				System.out.println("here");
			//			}

//			int stateBeforeCharge = (int) segmentEnd.consProfile.computeSocInB(stateAfterChargeOnLastCharger);
			//			if (fmLMGraph.getNode(segmentStart.nodeId).isCharger()) {
			//				stateBeforeCharge = (int) nodes.get(i+1).lastSeenChargerSoC; // equals segmentStart.consProfile.computeSocInB(stateAfterChargeOnLastCharger);
			//			}

			//for destination
//			int stateAfterCharge = stateBeforeCharge;
//			int pricePerKwh = -1;
			toNode = new JourneyNode(segmentEnd.nodeId, (int) segmentEnd.inSoC,(int) nextSegmentEnd.lastSeenChargerOutSoC,
					(int) segmentEnd.cpkWh);


//			if (fmLMGraph.getNode(segmentEnd.nodeId).isCharger()) {
//
//				EwLabel nextCharger = nodes.get(i+1);
//				EwLabel nextNextCharger = nodes.get(i+2);
//
//				//				double stateInNextChargerBeforeCharge = nextCharger.consProfile.computeSocInB(stateBeforeCharge);
//				double stateInNextChargerAfterCurrCharge = nodes.get(i+3).lastSeenChargerSoC;
//
//				stateAfterCharge = ConsumptionProfileUtils.retrieveSoCInA(nextNextCharger, stateInNextChargerAfterCurrCharge,
//						battery);
//				double energyCharged = stateAfterCharge - stateBeforeCharge;
//				if (energyCharged < 0) {
//					log.debug("Ouuuuch.");
//				}
//				Charger charger = fmLMGraph.getNode(segmentEnd.nodeId).getCharger();
//				pricePerKwh = charger.getPricePerkWh(
//						initTime.plusSeconds(GraphTools.convertFromE3Format(segmentEnd.travelTimeMillis)));
//
//				//				double chargingTimeMillis = new ChargingFunction()
//				//						.getChargingTimeMillis(stateBeforeCharge, stateAfterCharge);
//				//				travelTimeMillis += chargingTimeMillis;
//
//				price += pricePerKwh*(((double) stateAfterCharge-stateBeforeCharge)/1000);
//
//				toNode = new JourneyNode(nextCharger.nodeId, stateBeforeCharge, stateAfterCharge, pricePerKwh);
//			}

			// get Via nodes
			EvEdge edge = fmLMGraph.getEdge(segmentStart.nodeId, segmentEnd.nodeId);
			if (edge instanceof EvEdgeBetweenChargers) {
				EvEdgeBetweenChargers betweenEdge = (EvEdgeBetweenChargers) edge;
				//				List<EvLabel> viaAndToLabels = betweenEdge.getPaths().get(segmentEnd.inEdgeId).getNodes();
				//EvNodes -> ViaNodes
				List<GPSLocation> evViaNodes = betweenEdge.getPaths().get(segmentEnd.inEdgeId).viaNodes;
				//				viaAndToLabels = viaAndToLabels.subList(1, viaAndToLabels.size());
				EvNode node = fmLMGraph.getNode(betweenEdge.fromId);
				ConsumptionProfile consumptionProfile = null;

				for (GPSLocation viaLoc : evViaNodes) {
					EvNode viaEvNode = (EvNode) viaLoc;
					//build and link cons profiles
					ConsumptionProfile newConsProfile = ConsumptionProfileUtils
							.computeConsumptionProfileBetweenTwoLocs(node, viaEvNode);

					consumptionProfile = ConsumptionProfileUtils
							.linkConsumptionProfiles(consumptionProfile, newConsProfile);

					//never charger
					int soc = (int) consumptionProfile.computeSocInB(nextSegmentEnd.lastSeenChargerOutSoC);
					// TODO: 06-Dec-16 compare sBc after for-loop if match stateAfterCharge

					length += GPSLocationTools.computeDistance(node, viaEvNode);

					viaNodes.add(new JourneyNode(viaEvNode.id, soc, soc, -1));

					node = viaEvNode;
				}

				EvNode to = fmLMGraph.getNode(betweenEdge.toId);
				length += GPSLocationTools.computeDistance(node, to);

				/////////////////////////////////////////////////////////////////////////////////////
				// TODO: 06-Dec-16 compare scc after for-loop if match stateAfterCharge
				ConsumptionProfile newConsProfile = ConsumptionProfileUtils
						.computeConsumptionProfileBetweenTwoLocs(node, to);
				consumptionProfile = ConsumptionProfileUtils
						.linkConsumptionProfiles(consumptionProfile, newConsProfile);
				//never charger
				int soc = (int) consumptionProfile.computeSocInB(nextSegmentEnd.lastSeenChargerOutSoC);
				//////////////////////////////////////////////////////////////////////////////////////

				//OLD VERSION WITH PATHS
				//				for (EvLabel viaLabel : viaAndToLabels) {
				//					//never charger
				//					int soc = (int) viaLabel.consProfile.computeSocInB(stateAfterChargeOnLastCharger);
				//					// TODO: 06-Dec-16 compare sBc after for-loop if match stateAfterCharge
				//
				//					length += GPSLocationTools.computeDistance(fmLMGraph.getNode(viaLabel.prevLabel.nodeId),
				//							fmLMGraph.getNode(viaLabel.nodeId));
				//
				//					if (viaLabel.nodeId != viaAndToLabels.get(viaAndToLabels.size()-1).nodeId) {
				//						//do not add last node
				//						viaNodes.add(new JourneyNode(viaLabel.nodeId, soc, soc, -1));
				//					}
				//				}

				consumption += consumptionProfile.getConsumption(nextSegmentEnd.lastSeenChargerOutSoC);
			}

			if (consumption == 3000000 ) {
				log.debug("Bug... not again.");
			}
//			stateAfterChargeOnLastCharger = stateAfterCharge;

			//createSegment
			journeySegments.add(new JourneySegment(fromNode, toNode, viaNodes));

			fromNode = toNode;
			viaNodes = new ArrayList<>();

		}

		return new Journey(startNode.nodeId, goalNode.nodeId, journeySegments,
				length, travelTimeMillis, consumption, totalPrice, routerType);
	}

}
