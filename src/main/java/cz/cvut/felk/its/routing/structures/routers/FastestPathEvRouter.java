/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures.routers;

import cz.cvut.felk.its.experiments.TestResult;
import cz.cvut.felk.its.routing.alg.ModifiedDijkstraNonStatic;
import cz.cvut.felk.its.routing.structures.goalchecker.ByNodeIdsGoalChecker;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.routing.structures.PlanningInstance;
import cz.cvut.felk.its.routing.structures.graph.EvGraph;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;
import cz.cvut.felk.its.evstructures.journey.Journey;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;

@Deprecated
public class FastestPathEvRouter implements EvRouter {

	private static Logger log = Logger.getLogger(FastestPathEvRouter.class);

	@Override
	public List<Journey> findOptimalJourney(PlanningInstance plInstance) {

		int startNodeId = plInstance.getExtendedEvGraph().getStartNode().id;
		int goalNodeId = plInstance.getExtendedEvGraph().getGoalNode().id;
		SearchGraph<EvLabel> searchGraph = new EvGraph<>(plInstance.getExtendedEvGraph());

//		try {
//			new JSONConverter().convertGraphEdgesToJSON("geojson/ev-graph-edges.geojson", plInstance.getExtendedEvGraph(), false);
//			new JSONConverter().convertNodesToJSON("geojson/ev-graph-nodes.geojson", plInstance.getExtendedEvGraph());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

		ByNodeIdsGoalChecker<EvLabel> gc = new ByNodeIdsGoalChecker<>(goalNodeId);

		EvLabel startFNode = new EvLabel(startNodeId, plInstance.getInitSOC());

		ModifiedDijkstraNonStatic<EvLabel> dijkstra = new ModifiedDijkstraNonStatic<>(searchGraph, startFNode, gc);

		log.debug("Search started.");
		dijkstra.run();

		List<Path<EvLabel>> paths = dijkstra.getPaths();

		if (paths.isEmpty()) return Collections.singletonList(new Journey(startNodeId, goalNodeId, RouterType.FAST));

		Path<EvLabel> bestPath = paths.get(0);

		log.debug("Building journey started.");
		return Collections.singletonList(JourneyBuilder.buildJourney(bestPath, plInstance.getExtendedEvGraph(),
				plInstance.getInitTime(), plInstance.getBatteryType(), RouterType.FAST));


//		GoalChecker<EvNode, PlanningEdge> goalChecker = new ByNodeIdsGoalChecker<>(GraphExtender.GOAL_ID);
//
//		SearchGraph<EvLabel> timeGraph = new TimeGraph<>(plInstance.getFringeGraph(),
//				plInstance.getFmMultiEdges(), plInstance.getLmMultiEdges(),
//				ChargingModel.STATES_TO_CHARGE, plInstance.getChargingModel());
//
//		List<Solution> fastestSols = ModifiedDijkstra.modifiedDijkstra(START_ID, goalChecker, timeGraph,
//				initSOC, initTimeMins, 1);
	}

	@Override
	public List<Journey> findOptimalJourneyTest(PlanningInstance planningInstance, TestResult testResult) {

		//TODO implement [FastestPathEvRouter:findOptimalJourneyTest]
		throw new UnsupportedOperationException(
				"[FastestPathEvRouter:findOptimalJourneyTest] has not been implemented yet");
	}

}

