package cz.cvut.felk.its.routing.structures.goalchecker;

import cz.cvut.felk.its.routing.structures.EvLabel;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Tomas on 26-Apr-16.
 */
@Deprecated
public class ByNodeIdsGoalChecker<FNode extends EvLabel>
        extends AllNodesAreGoalsChecker<FNode> {
    private Set<Integer> goalNodeIds;
    private boolean stopOnFirst;

    public ByNodeIdsGoalChecker(int goalNodeId) {
        super(1);
        this.goalNodeIds = new HashSet<>();
        this.goalNodeIds.add(goalNodeId);
        this.stopOnFirst = true;
    }

    public ByNodeIdsGoalChecker(Set<Integer> goalNodeIds, boolean stopOnFirst) {
        super(goalNodeIds.size());
        this.goalNodeIds = new HashSet<>();
        this.goalNodeIds.addAll(goalNodeIds);
        this.stopOnFirst = stopOnFirst;
    }

    @Override
    public boolean isGoal(FNode node) {
        if (goalCounter.contains(node.nodeId)) return false;
        if (goalNodeIds.contains(node.nodeId)) {
            goalCounter.add(node.nodeId);
            if (stopOnFirst || goalNodeIds.size() == goalCounter.size()) {
                isSearchFinished = true;
                goalCounter = new HashSet<>();
            }
            return true;
        }
        return false;
    }

}
