package cz.cvut.felk.its.routing.alg;

import cz.cvut.felk.its.routing.structures.goalchecker.GoalChecker;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Solution;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.PlanningEdge;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by Tomas on 09-Mar-16.
 */
@Deprecated
public class ModifiedDijkstra {
    //only FM and LM need to be reduced
    public static double FM_LM_REDUCE_CONST = 1;
    public static final double OPTIMAL_REDUCE_CONST = 1;

////    private static int allAddcounter;
////    private static int prunnableAddcounter;
////    private static int[] inFringeSoCLimitById;
////    private static int[] inFringeCostLimitById;
//
//    public static void setFMLMReduceConst(double reduceConst) {
//        FM_LM_REDUCE_CONST = reduceConst;
//    }
//
    // PRECOMPUTATION
    public static <TNode extends EvNode, TEdge extends PlanningEdge> List<Solution> modifiedDijkstra(
            int startId, GoalChecker<EvLabel> goalChecker, SearchGraph<EvLabel> graph, int initSoc,
            double succReducingConst) {
        Set<Integer> startNodeIds = new HashSet<>();
        startNodeIds.add(startId);
        return modifiedDijkstra(startNodeIds, new int[]{0}, goalChecker, graph, initSoc, null, false, succReducingConst);
    }

    //FIRST MILE
    public static <TNode extends EvNode, TEdge extends PlanningEdge> List<Solution> modifiedDijkstra(
            int startId, GoalChecker<EvLabel> goalChecker, SearchGraph<EvLabel> graph, int initSoc) {
        Set<Integer> startNodeIds = new HashSet<>();
        startNodeIds.add(startId);
        return modifiedDijkstra(startNodeIds, new int[]{0}, goalChecker, graph, initSoc, null, false, FM_LM_REDUCE_CONST);
    }

    //LAST MILE
    public static <TNode extends EvNode, TEdge extends PlanningEdge> List<Solution> modifiedBackwardDijkstra(
            int startId, GoalChecker<EvLabel> goalChecker,
            SearchGraph<EvLabel> graph, int initSoc, double fmlmReduceConst) {
        Set<Integer> startNodeIds = new HashSet<>();
        startNodeIds.add(startId);
        return modifiedDijkstra(startNodeIds, new int[]{0}, goalChecker, graph, initSoc, null,
                true, fmlmReduceConst);
    }

    //used for price and time graph
    public static <TNode extends EvNode, TEdge extends PlanningEdge> List<Solution> modifiedDijkstra(
            int startId, GoalChecker<EvLabel> goalChecker, SearchGraph<EvLabel> graph, int initSoc,
            LocalDateTime startDateTime, int succReducingConst) {
        Set<Integer> startNodeIds = new HashSet<>();
        startNodeIds.add(startId);
        return modifiedDijkstra(startNodeIds, new int[]{0}, goalChecker, graph, initSoc, startDateTime, false, succReducingConst);
    }
//
//    /**
//     *
//     * @param startNodeIds
//     * @param startCosts
//     * @param graph
//     * @param initSoc
//     * @param startDateTime
//     * @param isBackwardSearch false-normal search, true-backward search
//     * @param succReducingConst interval <0,1> 1-optimal solution; reduce expanded successors
//     * @param <TNode>
//     * @param <TEdge>
//     * @return
//     */
    private static <TNode extends EvNode, TEdge extends PlanningEdge> List<Solution> modifiedDijkstra(
            Set<Integer> startNodeIds, int[] startCosts, GoalChecker<EvLabel> goalChecker,
            SearchGraph<EvLabel> graph, int initSoc, LocalDateTime startDateTime, boolean isBackwardSearch,
            double succReducingConst) {
        //TODO implement [ModifiedDijkstra:modifiedDijkstra]
        throw new UnsupportedOperationException ("[ModifiedDijkstra:modifiedDijkstra] has not been implemented yet");
//
////        allAddcounter = 0;
////        prunnableAddcounter = 0;
//        // TODO: 24-Apr-16 try to isolate mod. dijkstra from charging model if possible
//        int numOfNodes = graph.getNumOfNodes();
////        boolean isGoal;
////        int numOfStatesToCharge = ChargingModel.STATES_TO_CHARGE.length;
//        PriorityQueue<EvLabel> fringe = new PriorityQueue<>();
//
//        HashMap<Integer, EvLabel> closedList = new HashMap<>();
//        int[] visitedSoCLimitById = new int[numOfNodes + 10]; // store best SoC of visited nodes - pruning
////        inFringeSoCLimitById = new int[numOfNodes + 10];
////        inFringeCostLimitById = new int[numOfNodes + 10];
////        boolean[][] visitedStatesOfCharge = new boolean[numOfStatesToCharge][numOfNodes];
//
//        if (!isBackwardSearch) {
//            Arrays.fill(visitedSoCLimitById, Integer.MIN_VALUE);
////            Arrays.fill(inFringeSoCLimitById, Integer.MIN_VALUE);
////            Arrays.fill(inFringeCostLimitById, Integer.MAX_VALUE);
//        } else {
//            Arrays.fill(visitedSoCLimitById, Integer.MAX_VALUE);
//        }
//
//        int index = 0;
//        for (int startNodeId: startNodeIds) {
//            fringe.add(new EvLabel(startNodeId, startCosts[index], initSoc, null, initSoc, startDateTime,
//                    consProfile));
//            index++;
//        }
//
//        EvLabel currentFNode;
//        List<EvLabel> newFringeNodes;
//        while(!fringe.isEmpty()) {
//            currentFNode = fringe.poll();
//            int currVisitIndex = currentFNode.nodeId;
//            if (currVisitIndex < 0) {
//                currVisitIndex = numOfNodes - currVisitIndex;
//            }
//
//            if (isPrunable(currentFNode.stateOfCharge, visitedSoCLimitById[currVisitIndex], isBackwardSearch,
//            succReducingConst)) continue;
//            visitedSoCLimitById[currVisitIndex] = currentFNode.stateOfCharge;
//
//            if (goalChecker.isGoal(graph, currentFNode)) {
//                if (isBackwardSearch) {
//                    // TODO: 26-Apr-16 change hashmap to hashset or list!!!
//                    closedList.put(closedList.size(), currentFNode);
//                } else {
//                    closedList.put(currentFNode.nodeId, currentFNode);
//                }
//            }
//            if (goalChecker.isSearchFinished()){
//                goalChecker.setSearchFinished(false);
//                break;
//            }
//
////            isGoal = updateClosedList(graph, currentFNode, closedList, goalNodeIds, visitedStatesOfCharge,
////                    areChargersGoals, isBackwardSearch);
////            if (stopOnFirstGoal && isGoal) {
////                //WARNING: does not work in backward search, but our backward search looking for chargers not a goal
////                closedList = new HashMap<>();
////                closedList.put(currentFNode.nodeId, currentFNode);
////                return getSolutionsFromClosedList(graph, closedList, isBackwardSearch);
////            }
//
//            if (!isBackwardSearch) {
//                newFringeNodes = graph.getSuccessors(currentFNode);
//            } else {
//                newFringeNodes = graph.getPredecessors(currentFNode);
//            }
//
//            updateFringe(newFringeNodes, fringe, visitedSoCLimitById, isBackwardSearch, succReducingConst);
//
//        }
//
////        if (!areChargersGoals && goalNodeIds != null) return null; //no solution to goal
////        System.out.println("added: "+allAddcounter + " z toho prunnable: " + prunnableAddcounter);
//        return getSolutionsFromClosedList(graph, closedList, isBackwardSearch);
    }

    private static boolean isGoal(int currentNodeId, Set<Integer> goalNodeIds, boolean reverse) {
        return !reverse && goalNodeIds != null && goalNodeIds.contains(currentNodeId);
    }



    private static boolean isPrunable(int currStateOfCharge, int visitedSoCLimit, boolean isBackwardSearch,
                                      double succReducingConst) {

//        System.out.println(visitedSoCLimit + " and " + currStateOfCharge * succReducingConst);
//        System.out.println(visitedSoCLimit >= currStateOfCharge * succReducingConst);
//        System.out.println(visitedSoCLimit + " and " + currStateOfCharge * (2-succReducingConst));
//        System.out.println(visitedSoCLimit <= currStateOfCharge * succReducingConst);

        return !isBackwardSearch && visitedSoCLimit >= currStateOfCharge * succReducingConst
                || isBackwardSearch && visitedSoCLimit <= currStateOfCharge * (2 - succReducingConst);
    }

    //Return true if it is goal
//    private static <TNode extends EvNode, TEdge extends PlanningEdge> boolean updateClosedList(
//            SearchGraph<TNode, TEdge> graph, EvLabel currentFNode, HashMap<Integer, EvLabel> closedList,
//            Set<Integer> goalNodeIds, boolean[][] visitedStatesOfCharge, boolean areChargersGoals,
//            boolean isBackwardSearch) {
//
//        if (isBackwardSearch) {
//            //BACKWARD SEARCH - add to closed list (sorted from best(0) to worst(n))
//            //nodes with same Ids are inserted to closed list - different paths and better goal SoC
//            //but maximum number of nodes with same Ids are limited by size of charging states
//            if (areChargersGoals && graph.isCharger(currentFNode.nodeId)) {
//                int index = 0;
//                //Find corresponding SoC interval
//                while (index < ChargingModel.STATES_TO_CHARGE.length &&
//                        ChargingModel.STATES_TO_CHARGE[index] <= currentFNode.incomingSoC) {
//                    index++;
//                }
//                index--;
//
//                if (!visitedStatesOfCharge[index][currentFNode.nodeId]) {
//                    closedList.put(closedList.size(), currentFNode);
//                    visitedStatesOfCharge[index][currentFNode.nodeId] = true;
//                }
//            }
//        } else if (!closedList.containsKey(currentFNode.nodeId)) {
//            //Normal Search
//            if (goalNodeIds == null && !areChargersGoals) {
//                //all nodes are goals
//                closedList.put(currentFNode.nodeId, currentFNode);
//            } else if (areChargersGoals && graph.isCharger(currentFNode.nodeId)) {
//                // TODO: 25-Apr-16 FM need to add more more chargers with same ID to closed list !!!!!
//                // chargers are goals
//                closedList.put(currentFNode.nodeId, currentFNode);
//            } else if (goalNodeIds != null && goalNodeIds.contains(currentFNode.nodeId)) {
//                // goals are goals, and StopOnFirstGoal is false
//                closedList.put(currentFNode.nodeId, currentFNode);
//                return true;
//            }
//        }
//        return false;
//
//    }

    private static void updateFringe(List<EvLabel> newEvLabels, PriorityQueue<EvLabel> fringe,
                                     int[] visitedSoCLimitById, boolean isBackwardSearch, double succReducingConst) {
//
        //TODO implement [ModifiedDijkstra:updateFringe]
        throw new UnsupportedOperationException ("[ModifiedDijkstra:updateFringe] has not been implemented yet");
        // for (EvLabel newFringeNode: newEvLabels) {
//            int newVisitIndex = newFringeNode.nodeId;
//            if (newVisitIndex < 0) {
//                newVisitIndex = (visitedSoCLimitById.length-10) - newVisitIndex;
//            }
//            if (isPrunable(newFringeNode.stateOfCharge, visitedSoCLimitById[newVisitIndex], isBackwardSearch,
//                    succReducingConst)) continue;
////        if (!isBackwardSearch && visitedSoCLimitById[newFringeNode.nodeId] >= newFringeNode.incomingSoC) return;
////        else if (isBackwardSearch && visitedSoCLimitById[newFringeNode.nodeId] <= newFringeNode.incomingSoC) return;
//            fringe.add(newFringeNode);
////            allAddcounter++;
////
////            if (!isBackwardSearch && inFringeCostLimitById[newVisitIndex] <= newFringeNode.costs
////                    && inFringeSoCLimitById[newVisitIndex] >= newFringeNode.incomingSoC) {
////                prunnableAddcounter++;
////            } else if (!isBackwardSearch && inFringeCostLimitById[newVisitIndex] > newFringeNode.costs) {
////                inFringeCostLimitById[newVisitIndex] = newFringeNode.costs;
////                inFringeSoCLimitById[newVisitIndex] = newFringeNode.stateAfterCharge;
////            }
//
//        }
    }

//    private static List<Solution> getSolutionsFromClosedList(
//            SearchGraph<EvLabel> graph, HashMap<Integer, EvLabel> closedList, boolean reverse) {
//
//        if (closedList.isEmpty()) return null;
//        List<Solution> solutions = new ArrayList<>(closedList.size());
//        for (EvLabel goalNode: closedList.values()) {
//            Solution solution;
//            if (!reverse) {
//                solution = graph.createSolution(goalNode);
//            } else {
//                solution = graph.createSolutionFromBackward(goalNode);
//            }
//            if (solution != null) solutions.add(solution);
//        }
//        return solutions;
//    }

}
