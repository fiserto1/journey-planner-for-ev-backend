/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures;

import cz.agents.basestructures.GPSLocation;
import cz.cvut.felk.its.evstructures.chargers.ChargerType;
import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.evstructures.*;
import cz.cvut.felk.its.evstructures.graphs.ExtendedGraph;

import java.time.LocalDateTime;
import java.util.List;

public class PlanningInstance {
	private final List<GPSLocation> origins;
	private final List<GPSLocation> destinations;
	// TODO: 27-Oct-16 check if zone is needed 
	private final EVZone<EvNode, EvEdge> zone;
	private final ExtendedGraph<EvNode, EvEdge> extendedEvGraph;
	private final ExtendedGraph<EvNode, EvEdge> graphFMLM;

	private final int initSOC;
	private final BatteryType batteryType;
	private final ChargerType chargerType;
	private final ChargingFunction chargingFunction;
	private final LocalDateTime initTime;
	private final double centsPerSecond;

	public PlanningInstance(List<GPSLocation> origins, List<GPSLocation> destinations, EVZone<EvNode, EvEdge> zone,
			ExtendedGraph<EvNode, EvEdge> extendedEvGraph, ExtendedGraph<EvNode, EvEdge> graphFMLM, int initSOC,
			BatteryType batteryType, ChargerType chargerType, ChargingFunction chargingFunction,
			LocalDateTime initTime, double centsPerSecond) {
		this.origins = origins;
		this.destinations = destinations;
		this.zone = zone;
		this.extendedEvGraph = extendedEvGraph;
		this.graphFMLM = graphFMLM;
		this.initSOC = initSOC;
		this.batteryType = batteryType;
		this.chargerType = chargerType;
		this.chargingFunction = chargingFunction;
		this.initTime = initTime;
		this.centsPerSecond = centsPerSecond;
	}

	public PlanningInstance(ExtendedGraph<EvNode, EvEdge> graph, int initSoC, LocalDateTime initTime, double centsPerSecond) {

		this(null, null, null, graph, null, initSoC, null, null, null, initTime, centsPerSecond);
	}

	public PlanningInstance(ExtendedGraph<EvNode, EvEdge> graph, int maxStateOfCharge, double centsPerSecond) {

		this(null, null, null, graph, null, maxStateOfCharge, null, null, null, null, centsPerSecond);
	}

	public int getInitSOC() {
		return initSOC;
	}


	public EVZone<EvNode, EvEdge> getZone() {
		return zone;
	}

	public ChargingFunction getChargingFunction() {
		return chargingFunction;
	}

	public ExtendedGraph<EvNode, EvEdge> getExtendedEvGraph() {
		return extendedEvGraph;
	}

	public ExtendedGraph<EvNode, EvEdge> getGraphFMLM() {
		return graphFMLM;
	}

	public LocalDateTime getInitTime() {
		return initTime;
	}

	public BatteryType getBatteryType() {
		return batteryType;
	}

	public double getCentsPerSecond() {
		return centsPerSecond;
	}
}
