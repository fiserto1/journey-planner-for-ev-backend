package cz.cvut.felk.its.routing.structures.graph;

import cz.cvut.felk.its.routing.structures.EwLabel;
import cz.cvut.felk.its.routing.structures.WPath;

import java.util.List;
import java.util.Set;

/**
 * Created by Tomas on 29.11.2015.
 */
@Deprecated
public interface WSearchGraph<FNode extends EwLabel> {

    List<FNode> getSuccessors(FNode currentFNode);

    WPath<FNode> buildPath(FNode currentFNode);

    Set<Integer> getAllNodeIds();

}
