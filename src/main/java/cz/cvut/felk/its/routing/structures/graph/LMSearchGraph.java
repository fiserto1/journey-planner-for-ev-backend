/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures.graph;

import cz.agents.basestructures.GraphStructure;
import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.routing.SpeedUpSettings;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.evstructures.*;

import java.util.*;
import java.util.stream.Collectors;

public class LMSearchGraph<TNode extends EvNode, TEdge extends EvEdge> implements SearchGraph<EvLabel> {

	private final GraphStructure<TNode, TEdge> graph;
	private final Set<Integer> chargerNodeIds;
	private final int startNodeId;
	private final int goalNodeId;
	private final int ellipseThreshold;

	public LMSearchGraph(GraphStructure<TNode, TEdge> graph) {
		this.graph = graph;
		this.chargerNodeIds = graph.getAllNodes().stream()
				.filter(EvNode::isCharger)
				.map(node -> node.id)
				.collect(Collectors.toSet());
		this.startNodeId = -2;
		this.goalNodeId = -1;
		double distBetweenOAndD = GPSLocationTools.computeDistanceAsDouble(
				graph.getNode(startNodeId),
				graph.getNode(goalNodeId));
		this.ellipseThreshold = (int) (distBetweenOAndD*1.5);
	}

	@Override
	public List<EvLabel> getSuccessors(EvLabel currFNode) {
		List<EvLabel> successors = new LinkedList<>();

		if (!chargerNodeIds.contains(currFNode.nodeId) || currFNode.prevLabel == null) {

			Collection<TEdge> outEdges = graph.getInEdges(currFNode.nodeId);

			for (TEdge pEdge: outEdges) {
				if (!isInSearchEllipse(pEdge.toId)) continue;
				inspectOutEdge(pEdge, currFNode, successors);
			}
		}

		return successors;
	}

	private boolean isInSearchEllipse(int nodeId) {
		if (!SpeedUpSettings.REDUCE_BY_ELLIPSE) return true;

		//destination is in Ellipse
//		if (!chargerNodeIds.contains(nodeId)) return true;

		TNode node = graph.getNode(nodeId);
		int lengthFromOrigin = GPSLocationTools.computeDistance(
				graph.getNode(startNodeId), node);

		int lengthToDestination = GPSLocationTools.computeDistance(
				node, graph.getNode(goalNodeId));

		return lengthFromOrigin + lengthToDestination <= ellipseThreshold;
	}

	private void inspectOutEdge(TEdge edge, EvLabel currFNode, List<EvLabel> successors) {

		EvLabel successor;

		// int nextCost = currFNode.costs + edge.consFunction() - potentials[edge.toId] + potentials[edge.fromId];
		int nextCost = currFNode.costs[0] + edge.getWeight();
		int nextTravelTimeMillis = currFNode.travelTimeMillis + edge.travelTimeMillis;

		//swapped linking
		ConsumptionProfile nextConsProfile =
				ConsumptionProfileUtils.linkConsumptionProfiles(edge.consumptionProfile, currFNode.consProfile);

		successor = new EvLabel(edge.fromId, new int[]{nextCost}, currFNode, nextConsProfile, nextTravelTimeMillis,
				currFNode.lastSeenCharger, currFNode.lastSeenChargerSoC, currFNode.lastSeenChargerCpkWh);

		if (isInBatteryRange(successor)) {
			successors.add(successor);
		}
	}

	private boolean isInBatteryRange(EvLabel successor) {

		if (successor.consProfile == null) return true;

		return !(successor.consProfile.inSoc > ChargingModel.MAX_STATE_OF_CHARGE
				// TODO: 29-Nov-16 think about following row if its correct
				|| (successor.consProfile.outSoc < ChargingModel.MIN_STATE_OF_CHARGE));// && successor.lastSeenCharger == null)
//				|| (successor.lastSeenChargerSoC < successor.consProfile.inSoc)); //&& successor.lastSeenCharger == null));
	}

	@Override
	public Path<EvLabel> buildPath(EvLabel goalNode) {

		// TODO: 29-Nov-16 check if it is same schema like forward Path
		List<EvLabel> nodes = new ArrayList<>();

		EvLabel node = goalNode;
		int totalTTMillis = goalNode.travelTimeMillis;
		ConsumptionProfile totalConsProfile = goalNode.consProfile;
		while (node != null) {
//			if (node.prevLabel == null) {
//				// TODO: 05-Dec-16 replace this by inserting totalValues to Path or completely reconstruct path
//				EvLabel evLabel = new EvLabel(node.nodeId, goalNode.costs, null, goalNode.consProfile,
//						goalNode.travelTimeMillis, node.lastSeenCharger, node.lastSeenChargerSoC);
//				nodes.add(evLabel);
//			} else {
//				nodes.add(node);
//			}
			nodes.add(node);
			node = node.prevLabel;
		}


		nodes = rebuildPathFromForward(nodes);

		return new Path<>(goalNode.costs, nodes);
	}

	private List<EvLabel> rebuildPathFromForward(List<EvLabel> backwardNodes) {

		List<EvLabel> newNodes = new ArrayList<>();

		int travelTimeMillis = 0;

		EvLabel firstLabel = new EvLabel(backwardNodes.get(0).nodeId, ChargingModel.MAX_STATE_OF_CHARGE);
		newNodes.add(firstLabel);

		EvLabel newPrevLabel = firstLabel;

		for (int i = 0; i < backwardNodes.size()-1; i++) {

			EvLabel label = backwardNodes.get(i);
			EvLabel nextLabel = backwardNodes.get(i+1);

			EvEdge edge = graph.getEdge(label.nodeId, nextLabel.nodeId);

			travelTimeMillis += edge.travelTimeMillis;
			ConsumptionProfile newConsProfile = ConsumptionProfileUtils
					.linkConsumptionProfiles(newPrevLabel.consProfile, edge.consumptionProfile);

			EvLabel newLabel = new EvLabel(nextLabel.nodeId, new int[] { travelTimeMillis }, newPrevLabel,
					newConsProfile, travelTimeMillis);

			newNodes.add(newLabel);

			newPrevLabel = newLabel;

		}

		return newNodes;
	}

	@Override
	public Set<Integer> getAllNodeIds() {
		return graph.getAllNodes().stream().mapToInt(n -> n.id).boxed().collect(Collectors.toSet());
	}
}
