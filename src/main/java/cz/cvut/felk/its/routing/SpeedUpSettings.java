/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing;

public class SpeedUpSettings {
	public static boolean BAG_LIMIT = false;
	public static int MAX_BAG_SIZE = 50;

	public static boolean DOM_BASIC_APPROACH = true;
	public static double DOM_RELAX_PERC_ENABLED = 0.90;
	public static boolean REDUCE_BY_ELLIPSE = true; //true-servlet //false-build

	public static boolean DOM_BY_CHARGING = true; //true-servlet //false-build
	public static boolean DOM_RELAX = true; //true-servlet //false-build
	public static boolean DOM_DELTA_REDUCTION = false;

	public static boolean ABORT_CHARGER_CYCLE = false; //false-servlet //true-build

	public static boolean EARLY_SEARCH_FINISH = false;
	public static int earlySearchFinishTTMillis = 7200000;

	public static double getDomRelaxPerc() {
		if (DOM_RELAX) {
			return DOM_RELAX_PERC_ENABLED;
		} else {
			return 1;
		}
	}


	public static final int maxKLabelsFromParetoSet = 5;
	public static final int EVERY_KTH_ITERATION = 1000000;

	public static final int minRadarToRefreshFringe = 50000;
	public static final int radarStepToRefreshFringe = 50000;


	public static boolean minChargeableSoc = true;
	public static boolean maxChargeableSoc = true;
	public static boolean alterChargeableSoc = true;
	public static int chargingLimitStepInPerc = 20;
	public static int minChargingLimitInPerc = 20;
	public static int minChargingAmountInWh = 500;

	public static String allValuesSettingsToString() {
		return String.format("DOM_RELAX: %b,%.2f", DOM_RELAX, getDomRelaxPerc()) + "\n"
				+ String.format("DOM_DELTA: %b, %d", DOM_DELTA_REDUCTION, earlySearchFinishTTMillis)+ "\n"
				+ String.format("DOM_CHARGE: %b", DOM_BY_CHARGING)+ "\n"
				+ String.format("DOM_BASIC: %b", DOM_BASIC_APPROACH)+ "\n"
				+ String.format("ELLIPSE: %b", REDUCE_BY_ELLIPSE)+ "\n"
				+ String.format("ParetoSizeBCh: %d", maxKLabelsFromParetoSet)+ "\n"
				+ String.format("EveryKthIter: %d", EVERY_KTH_ITERATION)+ "\n"
				+ String.format("FringeRefreshRadar (min,step): %d,%d", minRadarToRefreshFringe,
					radarStepToRefreshFringe)+ "\n"
				+ String.format("BagLimit : %b,%d", BAG_LIMIT, MAX_BAG_SIZE)+ "\n"
				+ String.format("min/max/alter chargeableSOc: %b,%b,%b", minChargeableSoc,
					maxChargeableSoc, alterChargeableSoc)+ "\n"
				+ String.format("minChLimitPerc/stepPerc/minChAmountWh: %d,%d,%d", minChargingLimitInPerc,
						chargingLimitStepInPerc, minChargingAmountInWh);
	}

}
