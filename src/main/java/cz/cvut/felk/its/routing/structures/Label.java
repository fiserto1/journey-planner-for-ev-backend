package cz.cvut.felk.its.routing.structures;

import cz.cvut.felk.its.evstructures.ConsumptionProfile;

/**
 * Created by Tomas on 21-Jul-16.
 */
@Deprecated
public class Label implements Comparable<Label> {
    public final int nodeId;
    // TODO: 06-Nov-16 merge costs to one object
    public final int travelTimeMillis; //traveltime excluding charging at the last charger
    public final ConsumptionProfile consProfile;
    public final int lastSeenChargerId;
    public final double lastSeenChargerSoC;
    public final Label prevLabel; //prev label?
    private boolean settled;

    public Label(int nodeId, int travelTimeMillis, ConsumptionProfile consProfile, int lastSeenChargerId, double lastSeenChargerSoC, Label prevLabel) {
        this.nodeId = nodeId;
        this.travelTimeMillis = travelTimeMillis;
        this.consProfile = consProfile;
        this.lastSeenChargerId = lastSeenChargerId;
        this.lastSeenChargerSoC = lastSeenChargerSoC;
        this.prevLabel = prevLabel;
        this.settled = false;
    }

    public boolean isSettled() {
        return settled;
    }

    public void setSettled() {
        this.settled = true;
    }

    public boolean isDominatedBy(int nextTravelTime, ConsumptionProfile nextConsProfile, int lastSeenSCHId) { //int nodeId
        return (nextTravelTime > travelTimeMillis && nextConsProfile.isDominatedBy(consProfile) && this.lastSeenChargerId
                == lastSeenSCHId);
    }
    public boolean isDominatedBy(Label label) {
        return isDominatedBy(label.travelTimeMillis, label.consProfile, label.lastSeenChargerId);
    }

    @Override
    public int compareTo(Label o) {
        return travelTimeMillis- o.travelTimeMillis;
//        // TODO: 01-Aug-16 rewrite
//        if (travelTimeMillis < o.travelTimeMillis) {
//            return 1;
//        } else if (travelTimeMillis == o.travelTimeMillis) {
//            return 0;
//        } else {
//            return -1;
//        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Label))
            return false;

        Label label = (Label) o;

        if (nodeId != label.nodeId)
            return false;
        if (travelTimeMillis != label.travelTimeMillis)
            return false;
        if (lastSeenChargerId != label.lastSeenChargerId)
            return false;
        if (Double.compare(label.lastSeenChargerSoC, lastSeenChargerSoC) != 0)
            return false;
        if (settled != label.settled)
            return false;
        return consProfile != null ? consProfile.equals(label.consProfile) : label.consProfile == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = nodeId;
        result = 31 * result + travelTimeMillis;
        result = 31 * result + (consProfile != null ? consProfile.hashCode() : 0);
        result = 31 * result + lastSeenChargerId;
        temp = Double.doubleToLongBits(lastSeenChargerSoC);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (settled ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Label [" +
                "settled=" + settled +
                ", nodeId=" + nodeId +
                ", travelTimeMillis=" + travelTimeMillis +
                ", consProfile=" + consProfile +
                ", lastSeenCharger=" + lastSeenChargerId +
                ", lastSeenChargerSoC=" + lastSeenChargerSoC +
                ']';
    }
}
