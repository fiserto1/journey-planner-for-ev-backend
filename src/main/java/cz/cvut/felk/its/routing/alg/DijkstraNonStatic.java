/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.alg;

import cz.cvut.felk.its.routing.structures.goalchecker.GoalChecker;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;

import java.time.LocalDateTime;
import java.util.*;

public class DijkstraNonStatic<FNode extends EvLabel> {

	private PriorityQueue<FNode> fringe = new PriorityQueue<>();
	private HashMap<Integer, FNode> inspected = new HashMap<>();
	private Map<Integer, FNode> closed = new HashMap<>();

	private SearchGraph<FNode> graph;

	private Set<FNode> startNodes;


	private List<Path<FNode>> paths = new ArrayList<>();
	private GoalChecker<FNode> goalChecker;

	private int maxRadius;

	public DijkstraNonStatic(SearchGraph<FNode> graph) {
	}

	public DijkstraNonStatic(SearchGraph<FNode> graph, FNode startNode,	GoalChecker<FNode> goalChecker) {
		this(graph, Collections.singleton(startNode), goalChecker, Integer.MAX_VALUE);
	}

	public DijkstraNonStatic(SearchGraph<FNode> graph, FNode startNode,	GoalChecker<FNode> goalChecker,
			int maxRadius) {
		this(graph, Collections.singleton(startNode), goalChecker, maxRadius);
	}

	public DijkstraNonStatic(SearchGraph<FNode> graph, Set<FNode> startNodes,
			GoalChecker<FNode> goalChecker) {
		this(graph, startNodes, goalChecker, Integer.MAX_VALUE);
	}

	public DijkstraNonStatic(SearchGraph<FNode> graph, Set<FNode> startNodes,
			GoalChecker<FNode> goalChecker, int maxRadius) {
		this.graph = graph;
		this.startNodes = startNodes;
		this.goalChecker = goalChecker;
		this.maxRadius = maxRadius;
	}


	public void run() {

		for (FNode startNode : startNodes) {
			fringe.add(startNode);
			inspected.put(startNode.nodeId, startNode);
		}

		FNode currentFNode;

		while(!fringe.isEmpty()) {
			currentFNode = fringe.poll();
			inspected.remove(currentFNode.nodeId);

			if (closed.containsKey(currentFNode.nodeId)) continue;
			closed.put(currentFNode.nodeId, currentFNode);

			if (goalChecker.isGoal(currentFNode)) {

				paths.add(graph.buildPath(currentFNode));
//				paths.add(buildPath(currentFNode));

				if (goalChecker.isSearchFinished()) {
					return;
				}
			}

			List<FNode> successors = graph.getSuccessors(currentFNode);

			for (FNode successor : successors) {
				if (closed.containsKey(successor.nodeId)) continue;

				if (inspected.containsKey(successor.nodeId)) {
					inspectVisitedSuccessor(successor);
				} else {
					inspectSuccessor(successor);
				}
			}
		}

	}

	private void inspectVisitedSuccessor(FNode successor) {

		FNode oldFNode = inspected.get(successor.nodeId);
		if (oldFNode.isDominatedBy(successor, 0, LocalDateTime.now())) {
			fringe.remove(oldFNode);
			inspectSuccessor(successor);
		}

	}

//	private Path<FNode> buildPath(FNode goalNode) {
//
//		List<FNode> nodes = new ArrayList<>();
//
//		FNode node = goalNode;
//		while (node != null) {
//			nodes.add(node);
//			node = node.prevLabel;
//		}
//
//		Collections.reverse(nodes);
//
//		return new Path<>(new int[]{goalNode.costs}, nodes);
//	}

	private void inspectSuccessor(FNode successor) {

		if (successor.costs[0] > maxRadius) return;

		inspected.put(successor.nodeId, successor);
		fringe.add(successor);

	}

	public List<Path<FNode>> getPaths() {
		return paths;
	}
}
