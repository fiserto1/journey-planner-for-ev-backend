/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures;

import cz.cvut.felk.its.evstructures.ConsumptionProfile;

import java.time.LocalDateTime;

@Deprecated
public abstract class AbstractFringeNode<FNode extends AbstractFringeNode<FNode>> {
	public final int nodeId;
	public final int cost;
	public final int stateOfCharge;
	public final int stateAfterCharge; // needed for getting edge from MULTI EDGE
	public final int pathMinSOC;
	public final LocalDateTime currTimeMins;
	public final ConsumptionProfile consProfile;
	public final FNode prevFNode;

	public AbstractFringeNode(int nodeId, int cost, int stateOfCharge, FNode prevFNode, ConsumptionProfile consProfile) {
		this(nodeId, cost, stateOfCharge, prevFNode, 0, null, consProfile);
	}

	public AbstractFringeNode(int nodeId, int cost, int stateOfCharge, FNode prevFNode, int stateAfterCharge,
			LocalDateTime currTimeMins, ConsumptionProfile consProfile) {
		this(nodeId, cost, stateOfCharge, prevFNode, stateAfterCharge, 0, currTimeMins, consProfile);
	}

	public AbstractFringeNode(int nodeId, int cost, int stateOfCharge, FNode prevFNode, int stateAfterCharge,
			int pathMinSOC, LocalDateTime currTimeMins, ConsumptionProfile consProfile) {
		this.nodeId = nodeId;
		this.cost = cost;
		this.stateOfCharge = stateOfCharge;
		this.prevFNode = prevFNode;
		this.stateAfterCharge = stateAfterCharge;
		this.pathMinSOC = pathMinSOC;
		this.currTimeMins = currTimeMins;
		this.consProfile = consProfile;
	}

	public FNode getPrevFNode() {
		return prevFNode;
	}
}
