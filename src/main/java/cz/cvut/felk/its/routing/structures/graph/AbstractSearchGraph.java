//package cz.cvut.felk.its.routing.structures.graph;
//
//import cz.agents.basestructures.Graph;
//import cz.agents.basestructures.GraphStructure;
//import cz.cvut.felk.its.evstructures.chargers.Charger;
//import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
//import cz.cvut.felk.its.routing.evstructures.EvLabel;
//import cz.cvut.felk.its.routing.evstructures.OptimalPathSolution;
//import cz.cvut.felk.its.routing.evstructures.Solution;
//import cz.cvut.felk.its.evstructures.EvEdge;
//import cz.cvut.felk.its.evstructures.EvNode;
//import cz.cvut.felk.its.evstructures.MultiEvEdge;
//
//import java.time.LocalDateTime;
//import java.util.*;
//
///**
// * Created by Tomas on 31-Mar-16.
// */
//public abstract class AbstractSearchGraph<TNode extends EvNode, TEdge extends EvEdge>
//        implements SearchGraph<EvLabel>, GraphStructure<TNode, TEdge> {
//
//    private final Graph<TNode, TEdge> dualGraph;
//    private final HashMap<Integer, TEdge> firstMileEdges; //key - edge.toId
//    private final HashMap<Integer, TEdge> lastMileEdges; //key - edge.fromId
//    private final int[] soCToCharge;
//    private final ChargingModel chModel;
////    public final Set<TNode> startNodes;
////    public final Set<TNode> goalNodes;
//    public final int startId = -1;
//    public final int goalId = -2;
//    Set<Integer> chargerNodeIds;
//
//    public AbstractSearchGraph(Graph<TNode, TEdge> dualGraph, HashMap<Integer, TEdge> firstMileEdges,
//                               HashMap<Integer, TEdge> lastMileEdges, int[] soCToCharge,
//                               ChargingModel chModel) {
//        this.dualGraph = dualGraph;
//        this.firstMileEdges = firstMileEdges;
//        this.lastMileEdges = lastMileEdges;
//        this.soCToCharge = soCToCharge;
//        this.chModel = chModel;
//        chargerNodeIds = new HashSet<>(55);
//        for (EvNode node: dualGraph.getAllNodes()) {
//            if (node.getCharger() != null) chargerNodeIds.add(node.id);
//        }
//    }
//
//    public int convertFromE3(int travelTimeMillis) {
//        return (int) (travelTimeMillis/1E3);
//    }
//
//    protected int convertToE3(int travelTime) {
//        return (int) (travelTime*1E3);
//    }
//
//    @Override
//    public List<EvLabel> getSuccessors(EvLabel currentFNode) {
//        int currentFNodeId = currentFNode.nodeId;
//
//        List<EvLabel> outgoingFNodes = new LinkedList<>();
//        if (startId == currentFNodeId) {
//            for(TEdge pEdge: firstMileEdges.values()) {
////                if (multiEdge.getBestEdge(currentFNode.incomingSoC).fromId == currentFNodeId) // firstmile has more startNodes
//                inspectOutgoingEdge(currentFNode.stateOfCharge, currentFNode, pEdge, outgoingFNodes);
//            }
//            return outgoingFNodes;
//        }
//
//        for (TEdge pEdge: dualGraph.getOutEdges(currentFNodeId)) {
//            tryAllRechargeStates(currentFNode, pEdge, outgoingFNodes);
//        }
//
//        if (lastMileEdges != null && lastMileEdges.containsKey(currentFNodeId)) {
//            tryAllRechargeStates(currentFNode, lastMileEdges.get(currentFNodeId), outgoingFNodes);
//        }
//
//        return outgoingFNodes;
//    }
//
//    abstract int computeTraverseCostWithoutCharging(TEdge bestEdge);
//
//    abstract int computeTraverseCostWithCharging(TEdge bestEdge, int chargingTime, Charger supercharger,
//                                                 int energyCharged, LocalDateTime currentTimeMins);
//
//    private void tryAllRechargeStates(EvLabel currentFNode, TEdge edge, List<EvLabel> outgoingFNodes) {
//        for (int stateAfterCharge: soCToCharge) {
//            if (stateAfterCharge > currentFNode.stateOfCharge) {
//                inspectOutgoingEdge(stateAfterCharge, currentFNode, edge, outgoingFNodes);
//            }
//        }
//    }
//
//    private void inspectOutgoingEdge(int stateAfterCharge, EvLabel currentFNode, TEdge pEdge,
//                                     List<EvLabel> outgoingFNodes) {
//        TEdge bestEdge = null;
//        MultiEvEdge<TEdge> mEdge = null;
//        if (pEdge instanceof MultiEvEdge) {
//            // TODO: 29-Oct-16 use class.isAssignableFrom() to check
//            mEdge = (MultiEvEdge<TEdge>) pEdge;
//            bestEdge = mEdge.getBestEdge(stateAfterCharge);
//
//        }
//        if (bestEdge == null) return;
//
//        // TODO: 02-Apr-16 this is useful only for first mile, try to move there
//        //        if (bestEdge.fromId != currentFNode.nodeId) return;
//
//        if (bestEdge instanceof EvEdge) {
//            EvEdge bestEvEdge = (EvEdge) bestEdge;
//            int evWeight = bestEvEdge.consFunction(stateAfterCharge);
//            if (evWeight != Integer.MAX_VALUE) {
//                int newCost = currentFNode.costs;
//                //// TODO: 02-Apr-16 think how check if startNodes.contains(currentFnode)
//                // if (startNodes.contains(dualGraph.getNode(currentFNode.nodeId))
//                if (mEdge.fromId == -1) {
//                    newCost += computeTraverseCostWithoutCharging(bestEdge);
//                } else {
//                    int chargingTime = chModel.computeChargingTime(currentFNode.stateOfCharge, stateAfterCharge);
//                    Charger charger = dualGraph.getNode(currentFNode.nodeId).getCharger();
//                    int energyCharged = (stateAfterCharge-currentFNode.stateOfCharge);
//
//                    newCost += computeTraverseCostWithCharging(bestEdge, chargingTime, charger, energyCharged, currentFNode.currTimeMins);
//                }
//                int edgeTravelTimeInMins = convertMillisToMins(bestEvEdge.travelTimeMillis);
//                LocalDateTime nextDateTime = currentFNode.currTimeMins.plusMinutes(edgeTravelTimeInMins);
//
//                outgoingFNodes.add(new EvLabel(bestEdge.toId, newCost, stateAfterCharge - evWeight, currentFNode,
//                        stateAfterCharge, nextDateTime, consProfile));
//            }
//        }
//
//
//
//    }
//
//    private int convertMillisToMins(int travelTimeMillis) {
//        return ((int) ((double)convertFromE3(travelTimeMillis))/60);
//    }
//
//    @Override
//    public List<EvLabel> getPredecessors(EvLabel currentFNode) {
//        throw new UnsupportedOperationException("Not supported");
//    }
//
//    @Override
//    public Set<Integer> getChargerNodeIds() {
//
//        return chargerNodeIds;
//    }
//
//    @Override
//    public boolean isCharger(int nodeId) {
//        return chargerNodeIds.contains(nodeId);
////        return nodeId >= 0 && dualGraph.getNode(nodeId).isCharger();
//    }
//
////    @Override
////    public TNode getNode(int nodeId) {
////        return dualGraph.getNode(nodeId);
////    }
//
////    @Override
//    private TEdge getEdge(int fromId, int toId) {
//        if (fromId == startId) {
//            return firstMileEdges.get(toId);
//        } else if (toId == goalId) {
//            return lastMileEdges.get(fromId);
//        } else {
//            return dualGraph.getEdge(fromId, toId);
//        }
//    }
//
//    @Override
//    public int getNumOfNodes() {
//        return dualGraph.getNumOfNodes();
//    }
//
//    @Override
//    public Solution createSolution(EvLabel goalNode) {
//        int price = 0;
//        int travelTimeMillis = 0;
//        int consumption = 0;
//        int length = 0;
//        LinkedList<Integer> routeNodeIds = new LinkedList<>();
//        LinkedList<Integer> statesOfCharge = new LinkedList<>();
//        LinkedList<Integer> statesAfterCharge = new LinkedList<>();
//        ArrayList<Integer> segmentTravelTimesMillis = new ArrayList<>();
//        ArrayList<Integer> supChargingTimes = new ArrayList<>();
//        ArrayList<Integer> chargingPrices = new ArrayList<>();
//        ArrayList<Integer> segmentLengths = new ArrayList<>();
//        ArrayList<Integer> solutionSuperchargerNodeIds = new ArrayList<>();
//        ArrayList<Integer> superchargerStepBckwIndexes = new ArrayList<>();
//        ArrayList<Integer> superchargerStepIndexes = new ArrayList<>();
//        EvLabel currentNode = goalNode;
//        int numOfsegments = 0;
//        int superchargerStepIndexFromBckwrd = 0;
//        while (currentNode.prevLabel != null) {
//			routeNodeIds.add(currentNode.nodeId);
//            numOfsegments++;
//            EvLabel prevLabel = currentNode.prevLabel;
////            TNode prevEvNode = dualGraph.getNode(prevLabel.nodeId);
////            TNode currentEvNode = dualGraph.getNode(currentNode.nodeId);
//            MultiEvEdge<TEdge> edge = null;
//            TEdge bestEdge = null;
//            if (startId == prevLabel.nodeId) {
//                edge = (MultiEvEdge<TEdge>) firstMileEdges.get(currentNode.nodeId);
//                bestEdge = edge.getBestEdge(currentNode.stateAfterCharge);
////                statesOfCharge.addFirst(prevLabel.incomingSoC);
//                supChargingTimes.add(0);
//                chargingPrices.add(0);
////                statesAfterCharge.addFirst(prevLabel.stateAfterCharge);
//            } else if (goalId == currentNode.nodeId) {
//                edge = (MultiEvEdge) lastMileEdges.get(prevLabel.nodeId);
//                bestEdge = edge.getBestEdge(currentNode.stateAfterCharge);
//                statesOfCharge.addFirst(currentNode.stateOfCharge);
//                superchargerStepIndexFromBckwrd = routeNodeIds.size();
//                superchargerStepBckwIndexes.add(superchargerStepIndexFromBckwrd);
//                solutionSuperchargerNodeIds.add(prevLabel.nodeId);
//                statesAfterCharge.addFirst(currentNode.stateOfCharge);
//            } else {
//                edge = (MultiEvEdge) dualGraph.getEdge(prevLabel.nodeId, currentNode.nodeId);
//                bestEdge = edge.getBestEdge(currentNode.stateAfterCharge);
////                statesOfCharge.addFirst(prevLabel.incomingSoC);
//                superchargerStepIndexFromBckwrd = routeNodeIds.size();
//                superchargerStepBckwIndexes.add(superchargerStepIndexFromBckwrd);
//                solutionSuperchargerNodeIds.add(prevLabel.nodeId);
////                supChargingTimes.add(chModel.computeChargingTime(prevLabel.incomingSoC, currentNode.stateAfterCharge));
//            }
//            if (edge == null) return null;
//            segmentLengths.add(bestEdge.length);
//            statesOfCharge.addFirst(prevLabel.stateOfCharge);
//            statesAfterCharge.addFirst(currentNode.stateAfterCharge);
//
//			if (bestEdge instanceof EvEdge) {
//				EvEdge bestEvEdge = (EvEdge) bestEdge;
//				//todo dont forget, travelTimeMillis is without charging time
//				segmentTravelTimesMillis.add(bestEvEdge.travelTimeMillis);
//				length += bestEdge.length;
//				consumption += bestEvEdge.consFunction(currentNode.stateAfterCharge);
//				travelTimeMillis += bestEvEdge.travelTimeMillis;
//				//            price += bestEdge.getEvWeight(currentNode.stateAfterCharge) - potentials[bestEdge.toId] + potentials[bestEdge.fromId];
//				//            price += ChargingModel.TIME_COST_CONSTANT*convertFromE3(bestEdge.getWeight());
//			}
//            if (edge.fromId != -1) {
//                int chargingTime = chModel.computeChargingTime(prevLabel.stateOfCharge, currentNode.stateAfterCharge);
////                price += chargingTime + dualGraph.getNodeByNodeId(bestEdge.fromId).supercharger.getPricePerkWh()*(currentNode.stateAfterCharge-prevLabel.incomingSoC);
////                price += ChargingModel.TIME_COST_CONSTANT*chargingTime + dualGraph.getNode(bestEdge.fromId).supercharger.getPricePerkWh()*(currentNode.stateAfterCharge-prevLabel.incomingSoC);
//                int chargingPricePerKwh = dualGraph.getNode(bestEdge.fromId).getCharger().getPricePerkWh(prevLabel.currTimeMins);
//                price += chargingPricePerKwh*(((double) currentNode.stateAfterCharge-prevLabel.stateOfCharge)/1000);
//                travelTimeMillis += convertToE3(chargingTime);
//                supChargingTimes.add(chargingTime);
//                chargingPrices.add(chargingPricePerKwh);
//            }
//            currentNode = prevLabel;
//        }
////        System.out.println("price:" + price + " vs. " + " PriceCost:" + goalNode.costs);
////        System.out.println("TT:" + travelTimeMillis + " vs. " + " TTCost:" + goalNode.costs);
////        System.out.println("num of segments: " + numOfsegments);
////        System.out.println("num of superch: " + solutionSuperchargerNodeIds.size());
////        System.out.println("num of segment lengths: " + segmentLengths.size());
//        Collections.reverse(routeNodeIds);
//        Collections.reverse(segmentLengths);
//        Collections.reverse(segmentTravelTimesMillis);
//        Collections.reverse(supChargingTimes);
//        Collections.reverse(chargingPrices);
//        Collections.reverse(solutionSuperchargerNodeIds);
//        Collections.reverse(superchargerStepBckwIndexes);
//
//        for (int bckwrdIndex : superchargerStepBckwIndexes) {
//            superchargerStepIndexes.add(routeNodeIds.size() - bckwrdIndex);
//        }
//
//        return new OptimalPathSolution(routeNodeIds, goalNode.costs, length, travelTimeMillis, consumption, price, statesOfCharge,
//                statesAfterCharge, numOfsegments, solutionSuperchargerNodeIds, superchargerStepIndexes,
//                segmentTravelTimesMillis, segmentLengths, supChargingTimes, chargingPrices);
//    }
//
//    @Override
//    public Solution createSolutionFromBackward(EvLabel startNode) {
//        throw new UnsupportedOperationException("Not supported");
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        AbstractSearchGraph<?, ?> that = (AbstractSearchGraph<?, ?>) o;
//        return
//                Objects.equals(dualGraph, that.dualGraph) &&
//                Objects.equals(firstMileEdges, that.firstMileEdges) &&
//                Objects.equals(lastMileEdges, that.lastMileEdges) &&
//                Arrays.equals(soCToCharge, that.soCToCharge) &&
//                Objects.equals(chModel, that.chModel) &&
//                Objects.equals(chargerNodeIds, that.chargerNodeIds);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(dualGraph, firstMileEdges, lastMileEdges, soCToCharge, chModel, startId, goalId, chargerNodeIds);
//    }
//}
