package cz.cvut.felk.its.routing.structures.graph;

import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;

import java.util.List;
import java.util.Set;

/**
 * Created by Tomas on 29.11.2015.
 */
public interface SearchGraph<FNode extends EvLabel> {

    List<FNode> getSuccessors(FNode currentFNode);

    Path<FNode> buildPath(FNode currentFNode);

    Set<Integer> getAllNodeIds();

}
