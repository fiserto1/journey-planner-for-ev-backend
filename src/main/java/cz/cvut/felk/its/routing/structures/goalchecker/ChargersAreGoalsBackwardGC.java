package cz.cvut.felk.its.routing.structures.goalchecker;

import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.routing.structures.EvLabel;

import java.util.Set;

/**
 * Created by Tomas on 26-Apr-16.
 */
@Deprecated
public class ChargersAreGoalsBackwardGC<FNode extends EvLabel>
        extends AllNodesAreGoalsChecker<FNode> {
    private final Set<Integer> chargerNodeIds;
    protected boolean[][] visitedStatesToCharge;
    protected int numOfStatesToCharge;
    protected int counter;
    protected int startNodeId;

    public ChargersAreGoalsBackwardGC(int numOfAllNodes, int startNodeId, Set<Integer> chargerNodeIds) {
        super(numOfAllNodes*ChargingModel.STATES_TO_CHARGE.length);
        this.chargerNodeIds = chargerNodeIds;
        this.numOfStatesToCharge = ChargingModel.STATES_TO_CHARGE.length;
        this.visitedStatesToCharge = new boolean[numOfStatesToCharge][numOfAllNodes];
        this.counter = 0;
        this.startNodeId = startNodeId;
    }

    @Override
    public boolean isGoal(FNode node) {

        //TODO implement [ChargersAreGoalsBackwardGC:isGoal]
        throw new UnsupportedOperationException ("[ChargersAreGoalsBackwardGC:isGoal] has not been implemented yet");
        //BACKWARD SEARCH - add to closed list (sorted from best(0) to worst(n))
        //nodes with same Ids are inserted to closed list - different paths and better goal SoC
        //but maximum number of nodes with same Ids are limited by size of charging states

//        if (startNodeId == node.nodeId) {
//            isSearchFinished = true;
//            goalCounter = new HashSet<>();
//            counter = 0;
//            int numOfAllNodes = visitedStatesToCharge[0].length;
//            visitedStatesToCharge = new boolean[numOfStatesToCharge][numOfAllNodes];
//            return true;
//        }
//        if (chargerNodeIds.contains(node.nodeId)) {
//            int underSoCMin = ChargingModel.MIN_STATE_OF_CHARGE - node.pathMinSOC;
//            int newShiftedSoC = node.stateOfCharge + underSoCMin;
////            if (newShiftedSoC < ChargingModel.MIN_STATE_OF_CHARGE) System.out.println("This node should not be expanded");
////            System.out.println(newShiftedSoC);
//            int stateToChargeId = ChargingModel.getStCIdFromSoC(newShiftedSoC);
//            if (stateToChargeId < 0) return false;
//            if (!visitedStatesToCharge[stateToChargeId][node.nodeId]) {
//                visitedStatesToCharge[stateToChargeId][node.nodeId] = true;
//                counter++;
//                if (counter == numOfGoals) {
//                    isSearchFinished = true;
//                    int numOfAllNodes = visitedStatesToCharge[0].length;
//                    visitedStatesToCharge = new boolean[numOfStatesToCharge][numOfAllNodes];
//                    //or arrays fill
//                    counter = 0;
//                }
//                return true;
//            }
//
//        }
//        return false;
    }


}
