/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures.routers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;

@ApiModel(description = "Router type")
@JsonIgnoreProperties(ignoreUnknown = true)
public enum RouterType {
	CHEAP("CHEAPEST_JOURNEY", "CHJ", null),
	FAST("FASTEST_JOURNEY", "FJ", new FastestPathEvRouter()),
	MO_FAST("MO_FASTEST_JOURNEY", "MOFJ", new MOFastestPathEvRouter()),
	WMO_FAST("WMO_FASTEST_JOURNEY", "WMOFJ", new WMOFastestPathEvRouter());

	private final String name;
	private final String abbreviation;
	private EvRouter evRouter;

	RouterType(String name, String abbreviation, EvRouter evRouter) {
		this.name = name;
		this.abbreviation = abbreviation;
		this.evRouter = evRouter;
	}


	@JsonValue
	public String getName() {
		return name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public EvRouter getEvRouter() {
		return evRouter;
	}
}
