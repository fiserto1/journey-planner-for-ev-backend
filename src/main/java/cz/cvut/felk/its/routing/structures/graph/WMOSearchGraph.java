package cz.cvut.felk.its.routing.structures.graph;

import cz.agents.basestructures.GraphStructure;
import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.evstructures.chargers.Charger;
import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.routing.SpeedUpSettings;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.routing.structures.EwLabel;
import cz.cvut.felk.its.routing.structures.WPath;
import cz.cvut.felk.its.evstructures.*;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Tomas on 21-Jul-16.
 */
@Deprecated
public class WMOSearchGraph<TNode extends EvNode, TEdge extends EvEdge> implements WSearchGraph<EwLabel> {

    private final GraphStructure<TNode, TEdge> graph;
    private final int startNodeId;
    private final int goalNodeId;
    private final LocalDateTime initTime;
	private final BatteryType battery;
	private final ChargingFunction chFunction;
	private final double centsPerSecond;
	private final int ellipseThreshold;

	//    private final TNode startNode;
    //    private final TNode goalNode;
    //    private final HashMap<Integer, TEdge> startEdges; //key - edge.toId
    //    private final HashMap<Integer, TEdge> goalEdges; //key - edge.fromId
    Set<Integer> chargerNodeIds;

	public WMOSearchGraph(GraphStructure<TNode, TEdge> graph) {
        this(graph, -2, LocalDateTime.now());
    }

	public WMOSearchGraph(GraphStructure<TNode, TEdge> graph, int goalNodeId, LocalDateTime initTime) {
		this(graph, goalNodeId, initTime, new ChargingFunction(), BatteryType.TESLA, Double.MAX_VALUE);
	}
    public WMOSearchGraph(GraphStructure<TNode, TEdge> graph, int goalNodeId, LocalDateTime initTime, ChargingFunction chFunction, BatteryType battery, double centsPerSecond) {
        this.graph = graph;
        this.initTime = initTime;
        this.chargerNodeIds = graph.getAllNodes().stream()
                .filter(EvNode::isCharger)
                .map(node -> node.id)
                .collect(Collectors.toSet());
        this.startNodeId = -1;
        this.goalNodeId = goalNodeId;
		this.chFunction = chFunction;
		this.battery = battery;
		this.centsPerSecond = centsPerSecond;
		double distBetweenOAndD = GPSLocationTools.computeDistanceAsDouble(
				graph.getNode(startNodeId),
				graph.getNode(goalNodeId));
		this.ellipseThreshold = (int) (distBetweenOAndD*1.5);
    }

    @Override
    public List<EwLabel> getSuccessors(EwLabel currFNode) {
        List<EwLabel> successors = new LinkedList<>();

        if (currFNode.nodeId != goalNodeId) {


                Collection<TEdge> outEdges = graph.getOutEdges(currFNode.nodeId);

                for (TEdge pEdge: outEdges) {

					if (!isInSearchEllipse(pEdge.toId)) continue;

                    if (pEdge instanceof EvEdgeBetweenChargers) {
                        EvEdgeBetweenChargers edge = (EvEdgeBetweenChargers) pEdge;
                        inspectOutEdgeBetweenCharger(edge, currFNode, successors);
                    }
                    //					for (EvEdge edge: pEdge.getEvEdges) {
                    //						inspectOutEdge(pEdge, currFNode, successors);
                    //					}
                }
            }


        return successors;
    }

	private boolean isInSearchEllipse(int nodeId) {
		//destination is in Ellipse
		if (!SpeedUpSettings.REDUCE_BY_ELLIPSE || !chargerNodeIds.contains(nodeId)) return true;

		TNode node = graph.getNode(nodeId);
		int lengthFromOrigin = GPSLocationTools.computeDistance(
				graph.getNode(startNodeId), node);

		int lengthToDestination = GPSLocationTools.computeDistance(
				node, graph.getNode(goalNodeId));

		return lengthFromOrigin + lengthToDestination <= ellipseThreshold;
	}

	private void inspectOutEdgeBetweenCharger(EvEdgeBetweenChargers edge, EwLabel currFNode, List<EwLabel> successors) {

		int inEdgeId = -1;
        for (EvEdge parallelEdge : edge.getPaths()) {

			inEdgeId++;

//            List<EwLabel> nodes = evLabelWPath.getNodes();
//            EwLabel toChargerLabel = nodes.get(nodes.size() - 1);

			// TODO: 22-Dec-16 investigate if
            if (SpeedUpSettings.ABORT_CHARGER_CYCLE && visitedChargerTwice(edge.toId, currFNode)) continue;

            //            if (!isFeasibleWPath(currFNode, evLabelWPath)) continue;

//            List<GPSLocation> viaNodes = new ArrayList<>();

//            List<EwLabel> viaLabels = nodes.subList(1, nodes.size()-1);
//            for (EwLabel node : viaLabels) {
//                TNode node1 = graph.getNode(node.nodeId);
//                viaNodes.add(node1);
//            }
//            EvEdge evEdge = new EvEdge(edge.fromId, edge.toId, 0, RoadType.PRECOMPUTED, toChargerLabel.travelTimeMillis,
//                    toChargerLabel.consProfile, viaNodes);
            inspectOutEdge(parallelEdge, currFNode, inEdgeId, successors);


        }
    }

    private boolean isFeasibleWPath(EwLabel currFNode, WPath<EwLabel> evLabelWPath) {

        //TODO implement [MOSearchGraph:isFeasibleWPath]
        throw new UnsupportedOperationException("[MOSearchGraph:isFeasibleWPath] has not been implemented yet");
    }

//    private void inspectLastEdge(EwLabel currFNode, List<EwLabel> successors) {
//
//        if(currFNode.lastSeenCharger != null) {
//            //compute min needed charging pair
//            List<double[]> chPairs = ConsumptionProfileUtils.computePossibleChargingPairs(currFNode, chFunction, battery);
//            if (chPairs == null) return;
//
//            double[] bestChPair = chPairs.get(0);
//
//            int nextTravelTimeMillis = (int) (bestChPair[0]); //edge.travelTimeMillis=0
//            //			ConsumptionProfile nextConsProfile = goalNode.consProfile; // test this!!
//
//            // TODO: 13-Nov-16 this holds only if optimizing on traveltime
//            int nextCost = currFNode.costs[0] + (int) bestChPair[0] - currFNode.travelTimeMillis;
//
//
//            int nextPrice = currFNode.costs[1] + computePriceForCharging(currFNode, bestChPair[1]);
//
//            // TODO: 22-Nov-16 consider to replace last node not add!!!
//
//            currFNode = new EwLabel(lastCorrectingNodeId, new int[]{nextCost, nextPrice}, currFNode, new ConsumptionProfile(), nextTravelTimeMillis,
//                    currFNode, currFNode.inEdgeId ,bestChPair[1], 0);
//        } else {
//            // add last correcting label - even if no charger was visited
//            double realStateOfChargeInLastNode = currFNode.consProfile.computeSocInB(currFNode.lastSeenChargerSoC);
//            currFNode = new EwLabel(lastCorrectingNodeId, currFNode.costs, currFNode, new ConsumptionProfile(), currFNode.travelTimeMillis,
//                    null, currFNode.inEdgeId, realStateOfChargeInLastNode, 0);
//        }
//        successors.add(currFNode);
//    }

//    private int computePriceForCharging(EwLabel currFNode, double stateInNextChargerAfterCharge) {
//
//        double stateAfterCharge = ConsumptionProfileUtils.retrieveSoCInA(currFNode, stateInNextChargerAfterCharge, battery);
//        Charger charger = graph.getNode(currFNode.lastSeenCharger.nodeId).getCharger();
//        int pricePerKwh = charger.getPricePerkWh(
//                initTime.plusSeconds(GraphTools.convertFromE3Format(currFNode.lastSeenCharger.travelTimeMillis)));
//
//
//		double stateBeforeCharge = currFNode.lastSeenChargerSoC;
//
//		//TIME COST MONEY
//		int chargingTimeMillis = (int) new ChargingFunction().getChargingTimeMillis(stateBeforeCharge, stateAfterCharge);
//		int timeCostMoney = (int) (centsPerSecond*GraphTools.convertFromE3Format(chargingTimeMillis));
//		//TIME COST MONEY
//
//		return (int) (pricePerKwh*((stateAfterCharge-stateBeforeCharge)/1000)) + timeCostMoney;
//    }

    private void inspectOutEdge(EvEdge edge, EwLabel currFNode, int inEdgeId, List<EwLabel> successors) {

        EwLabel successor;

        // int nextCost = currFNode.costs + edge.consFunction() - potentials[edge.toId] + potentials[edge.fromId];
        int nextCost = currFNode.costs[0] + edge.getWeight();
        int nextTravelTimeMillis = currFNode.travelTimeMillis + edge.travelTimeMillis;
		int nextMoneyCost = currFNode.costs[1] + (int) (centsPerSecond*GraphTools.convertFromE3Format(edge.travelTimeMillis));

//		ConsumptionProfile nextConsProfile =
//                ConsumptionProfileUtils.linkConsumptionProfiles(currFNode.consProfile, edge.consumptionProfile);

		Charger succCharger = null;
		if (edge.toId != goalNodeId) {
			succCharger = graph.getNode(edge.toId).getCharger();
		}

		if (chargerNodeIds.contains(currFNode.nodeId)) {
			//is charger!!
//			Charger currCharger = graph.getNode(currFNode.nodeId).getCharger();
//			int pricePerKwh = currCharger.getPricePerkWh(
//					initTime.plusSeconds(GraphTools.convertFromE3Format(currFNode.travelTimeMillis)));

			//chargingPair := 2d int array - [0]chTime; [1]state of charge after charging (stateAfterCharge)

			List<Double> chargeableSoCs = ConsumptionProfileUtils.computePossibleChargeableSoCs(currFNode, edge.consumptionProfile, battery);

			for (double chargeableSoC: chargeableSoCs) {

				int[] chargedCostsAtCurrLabel = currFNode.chargeTo(chargeableSoC, centsPerSecond, chFunction);

				if (chargedCostsAtCurrLabel[1] < 0) {
					System.out.println(">?");
				}

				nextTravelTimeMillis = chargedCostsAtCurrLabel[0] + edge.travelTimeMillis;
//				nextConsProfile = edge.consumptionProfile;

				// TODO: 13-Nov-16 this holds only if optimizing on traveltime
				nextCost = chargedCostsAtCurrLabel[0] + edge.travelTimeMillis;

				int nextPrice = chargedCostsAtCurrLabel[1]
						+ (int) (centsPerSecond*GraphTools.convertFromE3Format(edge.travelTimeMillis));

				int nextPricePerKwh = succCharger == null ? -1 : succCharger.getPricePerkWh(
						initTime.plusSeconds(GraphTools.convertFromE3Format(nextTravelTimeMillis)));

				double succChargerInSoC = edge.consumptionProfile.computeSocInB(chargeableSoC);
				successor = new EwLabel(edge.toId, new int[]{nextCost, nextPrice}, nextTravelTimeMillis, null,
						succChargerInSoC, chargeableSoC, nextPricePerKwh, currFNode, inEdgeId);

				//                    if (visitedChargerTwice(successor)) {
				//                        log.debug("id(" + successor.nodeId + ") charger visited twice");
				//                    }

				if (isInBatteryRange(successor)) {
					successors.add(successor);
				}

			}
        } else {
            //not charger, its origin

			double initSoC = currFNode.inSoC;
			double succChargerInSoC = edge.consumptionProfile.computeSocInB(initSoC);

			int nextPricePerKwh = succCharger == null ? -1 : succCharger.getPricePerkWh(
					initTime.plusSeconds(GraphTools.convertFromE3Format(nextTravelTimeMillis)));

			successor = new EwLabel(edge.toId, new int[]{nextCost, nextMoneyCost}, nextTravelTimeMillis, null, succChargerInSoC, initSoC, nextPricePerKwh, currFNode,
					inEdgeId);

            if (isInBatteryRange(successor)) {
                successors.add(successor);
            }
        }
    }

    private boolean isInBatteryRange(EwLabel successor) {
//        if (successor.consProfile == null) return true;

        //		if (visitedChargerTwice(successor)) return false;

        return !(successor.inSoC > battery.getMaxSoC()
                || (successor.lastSeenChargerOutSoC < battery.getMinSoC())// && successor.prevLabel == null)
//                || (successor.lastSeenChargerSoC < successor.consProfile.inSoc && successor.lastSeenCharger == null)
//                || (successor.lastSeenChargerSoC > successor.consProfile.inSoc && successor.lastSeenCharger != null)
		 );

    }

    private boolean visitedChargerTwice(EwLabel successor) {

        int nextNodeId = successor.nodeId;

        EwLabel prevLabel = successor.prevLabel;
        while (prevLabel.prevLabel != null) {
            if (prevLabel.prevLabel.nodeId == nextNodeId) {
                return true;
            }
            prevLabel = prevLabel.prevLabel;
        }
        return false;
    }

    private boolean visitedChargerTwice(int nextChargerId, EwLabel currNode) {


        EwLabel prevLabel = currNode;
        while (prevLabel.prevLabel != null) {
            if (prevLabel.prevLabel.nodeId == nextChargerId) {
//                System.out.println("Charger visited twice.");
				return true;
            }
            prevLabel = prevLabel.prevLabel;
        }
        return false;
    }


    @Override
    public WPath<EwLabel> buildPath(EwLabel goalNode) {

        //		if(goalNode.lastSeenCharger >= 0) {
        //			double[] bestChPair = ConsumptionProfileUtils.computePossibleChargingPairs(goalNode).get(0);
        //			int nextTravelTimeMillis = (int) (bestChPair[0]);
        ////			ConsumptionProfile nextConsProfile = goalNode.consProfile; // test this!!
        //
        //			// TODO: 13-Nov-16 this holds only if optimizing on traveltime
        //			int nextCost = goalNode.costs + (int) bestChPair[0] - goalNode.travelTimeMillis;
        //
        //			// TODO: 22-Nov-16 consider to replace last node not add!!!
        //
        //			goalNode = new EwLabel(goalNode.nodeId, nextCost, goalNode, null, nextTravelTimeMillis,
        //					goalNode.nodeId, bestChPair[1]);
        //		}

        List<EwLabel> nodes = new ArrayList<>();

        EwLabel node = goalNode;
        while (node != null) {
            nodes.add(node);
            node = node.prevLabel;
        }

        Collections.reverse(nodes);

        return new WPath<>(goalNode.costs, nodes);
    }

    @Override
    public Set<Integer> getAllNodeIds() {
        return graph.getAllNodes().stream().mapToInt(n -> n.id).boxed().collect(Collectors.toSet());
    }

}
