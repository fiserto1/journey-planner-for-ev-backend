/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures.graph;

import cz.agents.basestructures.GraphStructure;
import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.routing.SpeedUpSettings;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.evstructures.ConsumptionProfile;
import cz.cvut.felk.its.evstructures.ConsumptionProfileUtils;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;

import java.util.*;
import java.util.stream.Collectors;

public class FMSearchGraph<TNode extends EvNode, TEdge extends EvEdge> implements SearchGraph<EvLabel> {

	private static final int MIN_ELLIPSE_THRESHOLD = 30000;
	private final GraphStructure<TNode, TEdge> graph;
	private final Set<Integer> chargerNodeIds;
	private final int startNodeId;
	private final int goalNodeId;
	private final int ellipseThreshold;

	public FMSearchGraph(GraphStructure<TNode, TEdge> graph) {
		this.graph = graph;
		this.chargerNodeIds = graph.getAllNodes().stream()
				.filter(EvNode::isCharger)
				.map(node -> node.id)
				.collect(Collectors.toSet());
		this.startNodeId = -1;
		this.goalNodeId = -2;
		double distBetweenOAndD = 0;

		if (SpeedUpSettings.REDUCE_BY_ELLIPSE) {
			 distBetweenOAndD = GPSLocationTools.computeDistanceAsDouble(
					graph.getNode(startNodeId),
					graph.getNode(goalNodeId));
		}
		this.ellipseThreshold = Math.max(((int) (distBetweenOAndD*1.5)), MIN_ELLIPSE_THRESHOLD);
//		this.ellipseThreshold = (int) (distBetweenOAndD*1.5);
	}

	@Override
	public List<EvLabel> getSuccessors(EvLabel currFNode) {
		List<EvLabel> successors = new LinkedList<>();

		if (!chargerNodeIds.contains(currFNode.nodeId) || currFNode.prevLabel == null) {

			Collection<TEdge> outEdges = graph.getOutEdges(currFNode.nodeId);

			for (TEdge pEdge: outEdges) {
				if (!isInSearchEllipse(pEdge.toId)) continue;

				inspectOutEdge(pEdge, currFNode, successors);
			}
		}

		return successors;
	}

	private boolean isInSearchEllipse(int nodeId) {
		if (!SpeedUpSettings.REDUCE_BY_ELLIPSE) return true;
		//destination is in Ellipse
//		if (!chargerNodeIds.contains(nodeId)) return true;

		TNode node = graph.getNode(nodeId);
		int lengthFromOrigin = GPSLocationTools.computeDistance(
				graph.getNode(startNodeId), node);

		int lengthToDestination = GPSLocationTools.computeDistance(
				node, graph.getNode(goalNodeId));

		return lengthFromOrigin + lengthToDestination <= ellipseThreshold;
	}

	private void inspectOutEdge(TEdge edge, EvLabel currFNode, List<EvLabel> successors) {

		EvLabel successor;

		// int nextCost = currFNode.costs + edge.consFunction() - potentials[edge.toId] + potentials[edge.fromId];
		int nextCost = currFNode.costs[0] + edge.getWeight();
		int nextTravelTimeMillis = currFNode.travelTimeMillis + edge.travelTimeMillis;

		ConsumptionProfile nextConsProfile =
				ConsumptionProfileUtils.linkConsumptionProfiles(currFNode.consProfile, edge.consumptionProfile);

		successor = new EvLabel(edge.toId, new int[]{nextCost}, currFNode, nextConsProfile, nextTravelTimeMillis,
				currFNode.lastSeenCharger, currFNode.lastSeenChargerSoC, currFNode.lastSeenChargerCpkWh);

		if (isInBatteryRange(successor)) {
			successors.add(successor);
		}
	}

	private boolean isInBatteryRange(EvLabel successor) {

		if (successor.consProfile == null) return true;

		return !(successor.consProfile.inSoc > ChargingModel.MAX_STATE_OF_CHARGE
				|| (successor.consProfile.outSoc < ChargingModel.MIN_STATE_OF_CHARGE)// && successor.lastSeenCharger == null)
				|| (successor.lastSeenChargerSoC < successor.consProfile.inSoc)); //&& successor.lastSeenCharger == null));
	}

	@Override
	public Path<EvLabel> buildPath(EvLabel goalNode) {

		List<EvLabel> nodes = new ArrayList<>();

		EvLabel node = goalNode;
		while (node != null) {
			nodes.add(node);
			node = node.prevLabel;
		}

		Collections.reverse(nodes);

		return new Path<>(goalNode.costs, nodes);
	}

	@Override
	public Set<Integer> getAllNodeIds() {
		return graph.getAllNodes().stream().mapToInt(n -> n.id).boxed().collect(Collectors.toSet());
	}
}
