/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures.routers;

import cz.cvut.felk.its.experiments.TestResult;
import cz.cvut.felk.its.routing.structures.PlanningInstance;
import cz.cvut.felk.its.evstructures.journey.Journey;

import java.util.List;

public interface EvRouter {
	List<Journey> findOptimalJourney(PlanningInstance planningInstance);
	List<Journey> findOptimalJourneyTest(PlanningInstance planningInstance, TestResult testResult);
}
