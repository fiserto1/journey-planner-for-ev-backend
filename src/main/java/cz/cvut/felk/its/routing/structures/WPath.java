/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Deprecated
public class WPath<FNode extends EwLabel> implements Serializable{
	private static final long serialVersionUID = 6216108253772212857L;

	private final int[] costVector;

	private final List<FNode> nodes;

	public WPath(int[] costVector, List<FNode> nodes) {
		this.costVector = costVector;
		this.nodes = nodes;
	}

	public int[] getCostVector() {
		return costVector;
	}

	public List<FNode> getNodes() {
		return nodes;
	}

	@Override
	public String toString() {
		return "Path [" +
				"costVector=" + Arrays.toString(costVector) +
				", nodes=" + nodes +
				']';
	}

	public String prettyPring() {
		List<Integer> nodeIds = nodes.stream().map(node -> node.nodeId).collect(Collectors.toList());
		return "--------PATH----------\n"
				+ "[" +
				"costVector=" + Arrays.toString(costVector) +
				", nodes=" + nodeIds +
				"] \n----------------------";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof WPath))
			return false;

		WPath<?> path = (WPath<?>) o;

		if (!Arrays.equals(costVector, path.costVector))
			return false;
		return nodes != null ? nodes.equals(path.nodes) : path.nodes == null;

	}

	@Override
	public int hashCode() {
		int result = Arrays.hashCode(costVector);
		result = 31 * result + (nodes != null ? nodes.hashCode() : 0);
		return result;
	}
}
