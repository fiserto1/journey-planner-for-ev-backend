package cz.cvut.felk.its.routing.structures.goalchecker;

import cz.cvut.felk.its.routing.structures.EvLabel;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Tomas on 26-Apr-16.
 */
@Deprecated
public class ChargerWithGoalsChecker<FNode extends EvLabel> extends AllNodesAreGoalsChecker<FNode> {
    private Set<Integer> goalNodeIds;
    private Set<Integer> chargerNodeIds;

    public ChargerWithGoalsChecker(int goalNodeId, int numOfChargers, Set<Integer> chargerNodeIds) {
        super(numOfChargers + 1);
        this.chargerNodeIds = chargerNodeIds;
        this.goalNodeIds = new HashSet<>();
        this.goalNodeIds.add(goalNodeId);
    }

    public ChargerWithGoalsChecker(Set<Integer> goalNodeIds, int numOfChargers) {
        super(numOfChargers + goalNodeIds.size());
        this.goalNodeIds = new HashSet<>(goalNodeIds);
    }

    @Override
    public boolean isGoal(FNode node) {
        if (goalCounter.contains(node.nodeId)) return false;
        if (chargerNodeIds.contains(node.nodeId)) {
            goalCounter.add(node.nodeId);
            return true;
        } else if (goalNodeIds.contains(node.nodeId)) {
            //STOPS ON FIRST GOAL HIT
            isSearchFinished = true;
            goalCounter = new HashSet<>();
            return true;
        }
        return false;
    }


}
