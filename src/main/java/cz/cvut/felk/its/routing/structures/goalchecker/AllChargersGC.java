package cz.cvut.felk.its.routing.structures.goalchecker;

import cz.cvut.felk.its.routing.structures.EvLabel;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Tomas on 26-Apr-16.
 */
@Deprecated
public class AllChargersGC<FNode extends EvLabel> extends AllNodesAreGoalsChecker<FNode> {

    private final Set<Integer> chargerNodeIds;

    public AllChargersGC(Set<Integer> chargerNodeIds) {
        super(chargerNodeIds.size());
        this.chargerNodeIds = chargerNodeIds;
    }

    @Override
    public boolean isGoal(FNode node) {
        if (goalCounter.contains(node.nodeId)) return false;
        if (chargerNodeIds.contains(node.nodeId)) {
            goalCounter.add(node.nodeId);
            if (goalCounter.size() == numOfGoals) {
                isSearchFinished = true;
                goalCounter = new HashSet<>();
            }
            return true;
        }
        return false;
    }

}
