package cz.cvut.felk.its.routing.structures.goalchecker;

import cz.cvut.felk.its.routing.structures.EvLabel;

/**
 * Created by Tomas on 26-Apr-16.
 */
public interface GoalChecker<FNode extends EvLabel> {

    boolean isGoal(FNode node);
    boolean isSearchFinished();
    void setSearchFinished(boolean isSearchFinished);
}
