/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures.routers;

import cz.cvut.felk.its.experiments.TestResult;
import cz.cvut.felk.its.routing.alg.WMulticriteriaDijkstra;
import cz.cvut.felk.its.routing.structures.goalchecker.WMOGoalChecker;
import cz.cvut.felk.its.routing.structures.EwLabel;
import cz.cvut.felk.its.routing.structures.PlanningInstance;
import cz.cvut.felk.its.routing.structures.WPath;
import cz.cvut.felk.its.routing.structures.graph.WMOSearchGraph;
import cz.cvut.felk.its.routing.structures.graph.WSearchGraph;
import cz.cvut.felk.its.evstructures.journey.Journey;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Deprecated
public class WMOFastestPathEvRouter implements EvRouter {

	private static Logger log = Logger.getLogger(WMOFastestPathEvRouter.class);


	@Override
	public List<Journey> findOptimalJourney(PlanningInstance plInstance) {
		return findOptimalJourneyTest(plInstance, null);
	}

	@Override
	public List<Journey> findOptimalJourneyTest(PlanningInstance plInstance, TestResult testResult) {

		int startNodeId = plInstance.getGraphFMLM().getStartNode().id;
		int goalNodeId = plInstance.getGraphFMLM().getGoalNode().id;
		//
		WSearchGraph<EwLabel> graph = new WMOSearchGraph<>(plInstance.getGraphFMLM(), goalNodeId,
				plInstance.getInitTime(), plInstance.getChargingFunction(),
				plInstance.getBatteryType(), plInstance.getCentsPerSecond());


		//			new JSONConverter().convertGraphEdgesToJSON("geojson/my-search-graph-edges.geojson", plInstance.getExtendedEvGraph(), false);
		//		new JSONConverter().convertFMLMGraphEdgesToJSON("geojson/my-search-fmlm-graph-edges.geojson", plInstance.getGraphFMLM());
		//			new JSONConverter().convertNodesToJSON("geojson/my-search-graph-nodes.geojson", plInstance.getExtendedEvGraph());

		WMOGoalChecker<EwLabel> gc = new WMOGoalChecker<>();

		EwLabel startLabel = new EwLabel(startNodeId, plInstance.getInitSOC());

		WMulticriteriaDijkstra<EwLabel> dijkstra = new WMulticriteriaDijkstra<>(graph, Collections.singleton(startLabel), gc,
				plInstance.getCentsPerSecond(), false, true);


		long startTime = System.currentTimeMillis();

		dijkstra.run();

		if (testResult != null) {
			testResult.setSearchTimeMillis(System.currentTimeMillis()-startTime);
			testResult.setSearchNumOfIters(dijkstra.getNumOfIters());
		}

		List<WPath<EwLabel>> bestPaths = dijkstra.getPaths().get(goalNodeId);
		//		bestPaths.forEach(bestP -> log.debug("Paths: " + bestP));
		if (bestPaths == null) return new ArrayList<>();

		//		log.debug("Building journey started.");
		List<Journey> journeys = new ArrayList<>();

		for (WPath<EwLabel> path: bestPaths) {
			journeys.add(JourneyBuilder.buildJourneyOnFMLMGraph(path, plInstance.getGraphFMLM(),
					plInstance.getInitTime(), plInstance.getBatteryType(), RouterType.MO_FAST));
		}

		// TODO: 11-Dec-16 this is provisional solution -> filters good priced journeys too
		//		filterSimilarJourneys(journeys);

		return journeys;

	}

//	private void filterSimilarJourneys(List<Journey> journeys) {
//
//		int bestJourneyPrice = getBestPriceJourey(journeys);
//		int sizeBefore = journeys.size();
//		Set<Journey> journeysToRemove = new HashSet<>();
//		for (int i = 0; i < sizeBefore - 1; i++) {
//
//			Journey journey = journeys.get(i);
//
//			if (journeysToRemove.contains(journey))
//				continue;
//
//			for (int j = i + 1; j < sizeBefore; j++) {
//				Journey journeyToCompare = journeys.get(j);
//				if (journeysToRemove.contains(journeyToCompare))
//					continue;
//
//				if (JourneyComparator.areSimilar(journey, journeyToCompare)
//						&& bestJourneyPrice < journeyToCompare.price) {
//					journeysToRemove.add(journeyToCompare);
//				}
//			}
//		}
//
//		journeys.removeIf(journeysToRemove::contains);
//
//		int sizeAfter = journeys.size();
//		log.info(String.format("Filtered %d journeys from total %d", sizeBefore-sizeAfter, sizeBefore));
//
//	}
//
//	public int getBestPriceJourey(List<Journey> journeys) {
//		int bestPrice = Integer.MAX_VALUE;
//
//		for (Journey journey : journeys) {
//			if (journey.price < bestPrice) {
//				bestPrice = journey.price;
//			}
//		}
//
//		return bestPrice;
//	}

	//	public Collection<PlanningEdge> findMOFastestJourney(Graph<EvNode, PlanningEdge> graph, EvNode startNode,
//			EvNode goalNode, int initSOC, ChargingModel chargingModel) {
//
//		Set<Integer> startNodeIds = new HashSet<>();
//		startNodeIds.add(startNode.id);
//
//		ArrayList<ArrayList<Label>> settledLabels = MODijkstra
//				.mODijkstra(startNodeIds, goalNode.id, graph, initSOC);
//
//		return retrievePathAsEdges(graph, settledLabels.get(goalNode.id));
//	}

//	private Journey buildMOJourney(GraphStructure<EvNode, EvEdge> graph,
//			List<Label> settledLabels, int initSOC) {
//
//		//labels are the last point in solution - so we are iterating from destination to origin.
//
//		List<JourneyNode> routeJNodes = new ArrayList<>();
//		List<JourneySegment> journeySegments = new ArrayList<>();
//
//		Label minLabel = settledLabels.get(0);
//		int goalNodeId = minLabel.nodeId;
//		int costs = minLabel.travelTimeMillis;
//		int travelTimeMillis = minLabel.travelTimeMillis;
//		int segmentTravelTimeMillis = 0; //without charging times!!!!!
//		int segmentLength = 0;
//		int length = 0, price = 0;
//		int consumption = (int) minLabel.consProfile.getConsumption(initSOC);
//
//		int stateAfterCharge = (int) minLabel.lastSeenChargerSoC;
//		JourneyNode segmentFromJNode = new JourneyNode(minLabel.nodeId, (int )minLabel.lastSeenChargerSoC,
//				(int) minLabel.lastSeenChargerSoC, -1);
//
//
//		while(minLabel.prevLabel != null) {
//			EvNode node = graph.getNode(minLabel.nodeId);
//
//			int stateBeforeCharge = (int) minLabel.consProfile.computeSocInB(minLabel.lastSeenChargerSoC);
//			boolean isUsedSupercharger = false;
//
//			int pricePerKWh = -1;
//			if (node.isCharger() && (stateAfterCharge-stateBeforeCharge != 0)) {
//				isUsedSupercharger = true;
//				int timeFromOriginInMins = 0, chargingTime = 0; // TODO: 26-Aug-16 add timeFromOriginInMins
//				pricePerKWh = node.getCharger().getPricePerkWh(LocalDateTime.now());
//				price += pricePerKWh*(((double) stateAfterCharge-stateBeforeCharge)/1000);
//			}
//
//			JourneyNode jNode = new JourneyNode(node.id, stateBeforeCharge, stateAfterCharge, pricePerKWh);
//			routeJNodes.add(jNode);
//
//			if ((stateAfterCharge-stateBeforeCharge != 0)) { //end of segment
//				journeySegments.add(new JourneySegment(segmentFromJNode, jNode, segmentLength, segmentTravelTimeMillis));
//				segmentFromJNode = jNode;
//				segmentTravelTimeMillis = 0;
//				segmentLength = 0;
//			}
//
//			EvEdge edge = graph.getEdge(minLabel.prevLabel.nodeId, minLabel.nodeId);
//			length += edge.length;
//			segmentLength += edge.length;
//			segmentTravelTimeMillis += edge.getWeight(); //travelTimeMillis
//			//			travelTimeMillis += ((EvEdge)edge).travelTimeMillis;
//
//			//stateAfterCharge is on the point after this (nextLabel - we iterating from destination to origin)
//			//do not put this line before (stateAfterCharge-stateBeforeCharger)
//			stateAfterCharge = (int) minLabel.lastSeenChargerSoC;
//
//			minLabel = minLabel.prevLabel;
//		}
//		boolean isUsedSupercharger = false; int pricePerKWh = 0;
//		if (graph.getNode(minLabel.nodeId).isCharger() && initSOC != stateAfterCharge) {
//			isUsedSupercharger = true;
//			pricePerKWh = graph.getNode(minLabel.nodeId).getCharger().getPricePerkWh(LocalDateTime.now());
//		}
//		JourneyNode jNode = new JourneyNode(minLabel.nodeId, initSOC, stateAfterCharge, pricePerKWh);
//		routeJNodes.add(jNode);
//		journeySegments.add(new JourneySegment(segmentFromJNode, jNode, segmentLength, segmentTravelTimeMillis));
//		int startNodeId = minLabel.nodeId;
//
//		Collections.reverse(routeJNodes);
//		Collections.reverse(journeySegments);
//		return new Journey(startNodeId, goalNodeId, routeJNodes, journeySegments, length, travelTimeMillis,
//				consumption, price, costs, RouterType.MO_FAST);
//	}


//
//	private Collection<PlanningEdge> retrievePathAsEdges(Graph<EvNode, PlanningEdge> graph, ArrayList<Label> settledLabels) {
//		System.out.println(settledLabels.size());
//		Label minLabel = settledLabels.get(0);
//
//		Collection<PlanningEdge> edges = new HashSet<>();
//		while(minLabel.prevLabel != null) {
//			edges.add(graph.getEdge(minLabel.prevLabel.nodeId, minLabel.nodeId));
//			minLabel = minLabel.prevLabel;
//		}
//
//		return edges;
//	}
}
