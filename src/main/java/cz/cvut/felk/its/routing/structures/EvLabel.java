package cz.cvut.felk.its.routing.structures;

import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.routing.SpeedUpSettings;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.evstructures.ConsumptionProfile;
import cz.cvut.felk.its.evstructures.ConsumptionProfileUtils;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.utils.EVZoneProvider;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * equals on nodeId
 * compare on costs
 *
 * Created by Tomas on 17.11.2015.
 */
public class EvLabel implements Comparable<EvLabel>, Serializable {

    private static final long serialVersionUID = -8986970321749002244L;

    public final int nodeId;
    public final int[] costs;
    public final int travelTimeMillis; //traveltime excluding charging at the last charger
    public final EvLabel lastSeenCharger;
    public final double lastSeenChargerSoC;
    public final double lastSeenChargerCpkWh; //cents per Wh
    public final ConsumptionProfile consProfile;
    public final EvLabel prevLabel;
	public final int inEdgeId;
    private boolean settled;

    public EvLabel(int nodeId, int[] costs, EvLabel prevLabel, ConsumptionProfile consProfile) {
        this(nodeId, costs, prevLabel, consProfile, 0, null, -1, 0);
    }

	public EvLabel(int nodeId, int[] costs, EvLabel prevLabel, ConsumptionProfile consProfile, int travelTimeMillis) {
		this(nodeId, costs, prevLabel, consProfile, travelTimeMillis, null, -1, 0);
	}

	public EvLabel(int nodeId, int[] costs, EvLabel prevLabel,
			ConsumptionProfile consProfile, int travelTimeMillis, EvLabel lastSeenCharger,
			double lastSeenChargerSoC, double lastSeenChargerCpkWh) {
		this(nodeId, costs, prevLabel, consProfile, travelTimeMillis, lastSeenCharger, -1, lastSeenChargerSoC,
				lastSeenChargerCpkWh);
	}

    public EvLabel(int nodeId, int[] costs, EvLabel prevLabel,
            ConsumptionProfile consProfile, int travelTimeMillis, EvLabel lastSeenCharger, int inEdgeId,
			double lastSeenChargerSoC, double lastSeenChargerCpkWh) {
        this.nodeId = nodeId;
        this.costs = costs;
        this.prevLabel = prevLabel;
        this.consProfile = consProfile;
        this.travelTimeMillis = travelTimeMillis;
        this.lastSeenCharger = lastSeenCharger;
		this.inEdgeId = inEdgeId;
		this.lastSeenChargerSoC = lastSeenChargerSoC;
		this.lastSeenChargerCpkWh = lastSeenChargerCpkWh;
		this.settled = false;
    }

    // testing startNode
    public EvLabel(int nodeId) {
        this(nodeId, new int[]{0}, null, null, 0, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);
    }

//    public EvLabel(int nodeId, int costs, EvLabel prevLabel, LocalDateTime localDateTime, ConsumptionProfile consProfile,
//            int travelTimeMillis, int lastSeenCharger, double lastSeenChargerSoC) {
//        this(nodeId, costs, prevLabel, localDateTime, consProfile, travelTimeMillis, lastSeenCharger,
//                lastSeenChargerSoC);
//    }

    public EvLabel(int startNodeId, int initSOC) {
        this(startNodeId, new int[]{0, 0}, null, null, 0, null, initSOC, 0);
    }


	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof EvLabel))
			return false;

		EvLabel evLabel = (EvLabel) o;

		if (nodeId != evLabel.nodeId)
			return false;
		if (!equalsChargerIds(evLabel))
			return false;
		if (!Arrays.equals(costs, evLabel.costs))
			return false;
		return consProfile != null ? consProfile.equals(evLabel.consProfile) : evLabel.consProfile == null;
		//add lastSeenCpWh and lastSeenSOC?
	}

	private boolean equalsChargerIds(EvLabel o) {
		return this.lastSeenCharger == null && o.lastSeenCharger == null
				|| this.lastSeenCharger != null && o.lastSeenCharger != null
				&& this.lastSeenCharger.nodeId == o.lastSeenCharger.nodeId;
	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		result = nodeId;
		result = 31 * result + Arrays.hashCode(costs);
		result = 31 * result + travelTimeMillis;
		if (lastSeenCharger != null) {
			result = 31 * result + lastSeenCharger.nodeId; //THIS IS REALLY IMPORTANT - never hashcode whole previous EvLabel
		}
		temp = Double.doubleToLongBits(lastSeenChargerSoC);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + (consProfile != null ? consProfile.hashCode() : 0);
		return result;
	}

	@Override
    public String toString() {
		Integer lastSeenChargerId = null;
		if (lastSeenCharger != null) {
			lastSeenChargerId = lastSeenCharger.nodeId;
		}
        return "EvLabel [" +
                "nodeId=" + nodeId +
                ", costs=" + Arrays.toString(costs) +
                ", travelTimeMillis=" + travelTimeMillis +
                ", lastSeenChargerId=" + lastSeenChargerId +
                ", lastSeenChargerSoC=" + lastSeenChargerSoC +
                ", lastSeenChargerCpkWh=" + lastSeenChargerCpkWh +
                ", consProfile=" + consProfile +
                ", settled=" + settled +
                ']';
    }

    @Override
    public int compareTo(@Nonnull EvLabel o) {
		for (int i = 0; i < costs.length; i++) {
			if (this.costs[i] != o.costs[i]) {
				return Integer.compare(this.costs[i], o.costs[i]);
			}
		}

		return this.consProfile.compareTo(o.consProfile);
    }

    public boolean isSettled() {
        return settled;
    }

    public void markSettled() {
        this.settled = true;
    }

//    public boolean isDominatedBy(int[] nextCosts, ConsumptionProfile nextConsProfile, int nextLastSeenChargerId) { //int nodeId
//
//        if (consProfile.equals(nextConsProfile) && costs[0] > nextCosts[0] && costs[1] >= nextCosts[1] && this.lastSeenCharger
//                == nextLastSeenChargerId) return true;
//        return (costs[0] >= nextCosts[0] && consProfile.isDominatedBy(nextConsProfile) && this.lastSeenCharger
//                == nextLastSeenChargerId);
//    }


	public boolean isDominatedBy(EvLabel label, double centsPerSecond) {
		return isDominatedBy(label, centsPerSecond, LocalDateTime.now());
	}
    public boolean isDominatedBy(EvLabel label, double centsPerSecond, LocalDateTime initDateTime) {
		if (this.equals(label)) return true; // TODO: 05-Dec-16 check this!

		double domRelaxPerc = SpeedUpSettings.getDomRelaxPerc();

		if (nodeId == -3) {
			//REMOVE THIS FOR TESTS - to get full pareto set
			for (int i = 0; i < costs.length; i++) {
//				if (this.costs[i] < label.costs[i]* domRelaxPerc) {
				if (this.costs[i] < label.costs[i]) {
					return false;
				}
			}
			return true; //last correcting node has real costs, so it is dominated if costs are dominated
		}

		if (SpeedUpSettings.DOM_BASIC_APPROACH && isDominatedByBasicApproach(label, domRelaxPerc)) {
			return true;
		}

		/////////////////////////////////////////////////////////////
		///////////NEW DESIGN - DOMINANCE BY CHARGING LABELS/////////
		/////////////////////////////////////////////////////////////

		double[] worstCostsAfterChargingLabel = label.getWorstCostsAfterChargingInLastSeenCharger(centsPerSecond);

		double[] bestCostsAfterChargingThis = this.getBestCostsAfterChargingInLastSeenCharger(centsPerSecond);

		double[] worstCostsAfterChargingThis = this.getWorstCostsAfterChargingInLastSeenCharger(centsPerSecond);

//		if (this.consProfile.outSoc > worstCostsAfterChargingLabel[2] && this.nodeId != -2) {
//			if (!hasWorseCriteriaEvenIfLabelIsChargedToSameSoc(worstCostsAfterChargingLabel, worstCostsAfterChargingThis,
//					centsPerSecond, initDateTime)) {
//				return false;
//			}
//		}

		if (
//				SpeedUpSettings.DOM_BY_CHARGING &&
				worstCostsAfterChargingLabel[0]*domRelaxPerc <= bestCostsAfterChargingThis[0]
				&& worstCostsAfterChargingLabel[1]*domRelaxPerc <= bestCostsAfterChargingThis[1]
				&& worstCostsAfterChargingLabel[2] >= bestCostsAfterChargingThis[2]*domRelaxPerc) {

			return true;
		} else if (
//				worstCostsAfterChargingLabel[0]*DOM_RELAX_PERC <= bestCostsAfterChargingThis[0]
//				&& worstCostsAfterChargingLabel[1]*DOM_RELAX_PERC <= bestCostsAfterChargingThis[1]
//				&&
				this.nodeId !=-2 &&
						worstCostsAfterChargingLabel[2] < bestCostsAfterChargingThis[2]*domRelaxPerc) {

			if (SpeedUpSettings.DOM_BY_CHARGING && hasWorseCriteriaEvenIfLabelIsChargedToSameSoc(worstCostsAfterChargingLabel, bestCostsAfterChargingThis,
					centsPerSecond, initDateTime)) {
				return true;
			}
		} else if (
				(worstCostsAfterChargingLabel[0]*domRelaxPerc <= bestCostsAfterChargingThis[0]
//						|| worstCostsAfterChargingLabel[1]*DOM_RELAX_PERC <= bestCostsAfterChargingThis[1]
				)
				&& worstCostsAfterChargingLabel[2] > bestCostsAfterChargingThis[2]*domRelaxPerc) {
			//this has worse SoC but better traveltime or price
			//find chPair that come to B with same SoC as labelSoCInB and if it is not possible than recharge in B

			//			if (worstCostsAfterChargingLabel[0] <= bestCostsAfterChargingThis[0]) {
			double differencePrice = GraphTools.convertFromE3Format(bestCostsAfterChargingThis[0] - worstCostsAfterChargingLabel[0]) * centsPerSecond ;
			if (worstCostsAfterChargingLabel[1] <= differencePrice + bestCostsAfterChargingThis[1]) {
				return true;
				}
//			}

		}


		if (SpeedUpSettings.DOM_DELTA_REDUCTION && (bestCostsAfterChargingThis[0] - worstCostsAfterChargingLabel[0]) > SpeedUpSettings.earlySearchFinishTTMillis) {//2hours later
//						System.out.println("Big difference: " + (this.costs[0]- label.costs[0]));
			return true;
		}

//		if (SpeedUpSettings.DOM_DELTA_REDUCTION && (this.costs[0] - label.costs[0]) > SpeedUpSettings.earlySearchFinishTTMillis) {//2hours later
////								System.out.println("Big difference: " + (this.costs[0]- label.costs[0]));
//			return true;
//		}

		return false;


		//pokud NejhorsiDotankovaniLabelu je lepsi v kriteriich nez nejlepsiDotankovaniVThis pak return true


		/////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////


	}

	private boolean isDominatedByBasicApproach(EvLabel label, double domRelaxPerc) {
		//OLD APPROACH
		if ( !equalsChargerIds(label) || this.nodeId != label.nodeId) return false;
		if ( this.lastSeenChargerCpkWh != label.lastSeenChargerCpkWh) return false;

		for (int i = 0; i < costs.length; i++) {
			if (this.costs[i] < label.costs[i]*domRelaxPerc) {
				return false;
			}
		}

		if (consProfile.equals(label.consProfile)) {
			if (this.lastSeenChargerSoC*domRelaxPerc> label.lastSeenChargerSoC) {
				return false;
			} else {
				return true;
			}
		}

		return (this.lastSeenChargerSoC*domRelaxPerc <= label.lastSeenChargerSoC
				&& this.consProfile.isDominatedBy(label.consProfile, domRelaxPerc));



		// TODO: 11-Dec-16  this.lastSeenChargerSoC > label.lastSeenChargerSoC maybe also in case if CPs are not equal!
//		return this.consProfile.equals(label.consProfile) || this.consProfile.isDominatedBy(label.consProfile, domRelaxPerc);
	}

	private double[] getBestCostsAfterChargingInLastSeenCharger(double centsPerSecond) {

		if (this.lastSeenCharger == null) {
			double stateOfCharge;
			if (consProfile == null) {
				stateOfCharge = this.lastSeenChargerSoC;
			} else {
				stateOfCharge = this.consProfile.computeSocInB(this.lastSeenChargerSoC);
			}
			if (costs.length < 2) {
				// FM and LM search
				return new double[]{this.costs[0], 0,stateOfCharge};
			} else {
				return new double[]{this.costs[0], this.costs[1],stateOfCharge};
			}
		} else {
			double minChargableThisSoC =  this.getMinChargableSoCAtLastSeenCharger();
			double[] chPairThis = ConsumptionProfileUtils
					.computeChargingPair(this, new ChargingFunction(), this.consProfile, minChargableThisSoC);
			int bestPriceThis = this.costs[1] + priceForCharging(centsPerSecond, this.lastSeenChargerCpkWh, this.lastSeenChargerSoC, minChargableThisSoC);
			return new double[]{chPairThis[0], bestPriceThis, chPairThis[1]};
		}
	}

	private double[] getWorstCostsAfterChargingInLastSeenCharger(double centsPerSecond) {

		if (this.lastSeenCharger == null) {
			double stateOfCharge;
			if (consProfile == null) {
				stateOfCharge = this.lastSeenChargerSoC;
			} else {
				stateOfCharge = this.consProfile.computeSocInB(this.lastSeenChargerSoC);
			}

			if (costs.length < 2) {
				// FM and LM search
				return new double[]{this.costs[0], 0, stateOfCharge};
			} else {

				return new double[]{this.costs[0], this.costs[1], stateOfCharge};
			}
		} else {

			double maxChargableLabelSoC = this.getMaxChargableSoCAtLastSeenCharger();
			double[] chPairLabel = ConsumptionProfileUtils
					.computeChargingPair(this, new ChargingFunction(), this.consProfile, maxChargableLabelSoC);
			int worstPriceLabel = this.costs[1] + priceForCharging(centsPerSecond, this.lastSeenChargerCpkWh, this.lastSeenChargerSoC, maxChargableLabelSoC);
			return new double[]{chPairLabel[0], worstPriceLabel, chPairLabel[1]};
		}
	}

	private int priceForCharging(double centsPerSecond, double pricePerKwh, double stateBeforeCharge, double stateAfterCharge) {
		// TODO: 22-Dec-16 move to another class
		//TIME COST MONEY
		int chargingTimeMillis = (int) new ChargingFunction().getChargingTimeMillis(stateBeforeCharge, stateAfterCharge);
		int timeCostMoney = (int) (centsPerSecond*GraphTools.convertFromE3Format(chargingTimeMillis));
		//TIME COST MONEY

		return (int) (pricePerKwh*((stateAfterCharge-stateBeforeCharge)/1000)) + timeCostMoney;
	}

	private double getMinChargableSoCAtLastSeenCharger() {
		if (this.lastSeenCharger == null) {
			return this.lastSeenChargerSoC;// initSoC
		}
		return Math.max(this.consProfile.inSoc,
				Math.min(this.lastSeenChargerSoC+500, this.getMaxChargableSoCAtLastSeenCharger()));
	}

	private double getMaxChargableSoCAtLastSeenCharger() {
		if (this.lastSeenCharger == null) {
			return this.lastSeenChargerSoC;// initSoC
		}
		return this.consProfile.cost + this.consProfile.outSoc; // the max SoC that pays off to charge

	}

//	private boolean hasWorseCriteriaEvenIfLabelIsChargedToSameSoc(EvLabel label) {
//
////		//recharge label to same SoC as this
//		double[] chPair = ConsumptionProfileUtils
//				.computeChargingPair(label, new ChargingFunction(), label.consProfile, this.lastSeenChargerSoC);
//
//		int timeCostMoney = (int) (0.01* GraphTools.convertFromE3Format(chPair[0] - label.travelTimeMillis));
//		int priceForCharging = (int) (lastSeenChargerCpkWh *((this.lastSeenChargerSoC-label.lastSeenChargerSoC)/1000)) + timeCostMoney;
//
//		if (this.costs[0] > chPair[0] && this.costs[1] > label.costs[1]+priceForCharging) {
//			return true;
//		}
//		return false;
//	}

	private boolean hasWorseCriteriaEvenIfLabelIsChargedToSameSoc(double[] labelTripletInB, double[] thisTripletInB,
			double centsPerSecond, LocalDateTime initDateTime) {
		EvNode node = null;
		if (nodeId > -1) {
			node = EVZoneProvider.INSTANCE.getZone().getGraph().getNode(this.nodeId);
		}

		// TODO: 26-Dec-16 this returns wierd solutions(worse best price), inspect behaviour for LM
		double pricePerkWh = 50; //WORST price for charging everywhere;
//		double pricePerkWh = lastSeenChargerCpkWh; //WORST price for charging everywhere;

		if (node != null && node.isCharger()) {
			LocalDateTime localDateTime = initDateTime
					.plusSeconds(GraphTools.convertFromE3Format(labelTripletInB[0]));
			pricePerkWh = node.getCharger().getPricePerkWh(localDateTime);
		}

		////		//recharge label to same SoC as this
		double chargingTimeMillis = new ChargingFunction().getChargingTimeMillis(labelTripletInB[2], thisTripletInB[2]);

		int timeCostMoney = (int) (centsPerSecond* GraphTools.convertFromE3Format(chargingTimeMillis));
		int priceForCharging = (int) (pricePerkWh *((thisTripletInB[2]-labelTripletInB[2])/1000)) + timeCostMoney;


		if (labelTripletInB[0] + chargingTimeMillis <= thisTripletInB[0]) {
			double differencePrice = GraphTools.convertFromE3Format(thisTripletInB[0] - labelTripletInB[0] + chargingTimeMillis) * centsPerSecond ;
			if (labelTripletInB[1] + priceForCharging <= differencePrice + thisTripletInB[1]) {
				return true;
			}
		}

//		if (thisTripletInB[0] > labelTripletInB[0] + chargingTimeMillis && thisTripletInB[1] > labelTripletInB[1]+priceForCharging) {
//			return true;
//		}
		return false;
	}

}
