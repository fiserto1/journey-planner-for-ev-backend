/* This code is owned by Umotional s.r.o. (IN: 03974618). All Rights Reserved. */
package cz.cvut.felk.its.routing.structures.goalchecker;

import cz.cvut.felk.its.routing.structures.EvLabel;

import java.util.Set;

public class FMGoalChecker<TLabel extends EvLabel> implements GoalChecker<TLabel>{

	private final Set<Integer> chargerNodeIds;
	private boolean isSearchFinished;

	public FMGoalChecker(Set<Integer> chargerNodeIds) {
		this.chargerNodeIds = chargerNodeIds;
		this.isSearchFinished = false;
	}

	@Override
	public boolean isGoal(TLabel tLabel) {

		return chargerNodeIds.contains(tLabel.nodeId);
	}

	@Override
	public boolean isSearchFinished() {

		return isSearchFinished;
	}

	@Override
	public void setSearchFinished(boolean isSearchFinished) {

		//TODO implement [FMGoalChecker:setSearchFinished]
		throw new UnsupportedOperationException("[FMGoalChecker:setSearchFinished] has not been implemented yet");
	}
}
