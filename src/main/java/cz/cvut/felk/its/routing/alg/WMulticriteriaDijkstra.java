package cz.cvut.felk.its.routing.alg;

import cz.cvut.felk.its.routing.SpeedUpSettings;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.routing.structures.goalchecker.WGoalChecker;
import cz.cvut.felk.its.routing.structures.EwLabel;
import cz.cvut.felk.its.routing.structures.WPath;
import cz.cvut.felk.its.routing.structures.graph.WSearchGraph;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by Tomas on 21-Jul-16.
 *
 * tried approach with charging at current charger (not last seen)
 */
@Deprecated
public class WMulticriteriaDijkstra<FLabel extends EwLabel> {

	private final Logger log = Logger.getLogger(WMulticriteriaDijkstra.class);

	private Set<FLabel> startNodes;
	//create tunning constants object
	private boolean forgetSimlilarLabels;
	private WSearchGraph<FLabel> graph;
	private WGoalChecker<FLabel> goalChecker;
	private PriorityQueue<FLabel> fringe; //priority queue

	private Map<Integer, Set<FLabel>> bags; //settled and unsettled labels
	private Map<Integer, List<WPath<FLabel>>> paths = new HashMap<>(); //solutions
	private boolean singleGoalSearch;
	private double centsPerSecond;
	private int numOfIters;

//	public Map<Integer, List<Path<FLabel>>> getSearchSpace() {
//		Map<Integer, List<Path<FLabel>>> paths = new HashMap<>();
//		for (Set<FLabel> fLabels : bags.values()) {
//			for (FLabel fLabel : fLabels) {
//				if (fLabel.isSettled()) {
//					Path<FLabel> fLabelPath = graph.buildPath(fLabel);
//
//					if (!paths.containsKey(fLabel.nodeId)) {
//						List<Path<FLabel>> pareto = new ArrayList<>();
//						pareto.add(fLabelPath);
//						paths.put(fLabel.nodeId, pareto);
//					} else {
//						paths.get(fLabel.nodeId).add(fLabelPath);
//					}
//				}
//			}
//		}
//		return paths;
//	}

	public WMulticriteriaDijkstra(WSearchGraph<FLabel> graph, Set<FLabel> startNodes,
			WGoalChecker<FLabel> goalChecker) {
		this(graph, startNodes, goalChecker, 0, false, false);
	}

	public WMulticriteriaDijkstra(WSearchGraph<FLabel> graph, Set<FLabel> startNodes,
			WGoalChecker<FLabel> goalChecker, double centsPerSecond, boolean forgetSimilarLabels, boolean singleGoalSearch) {
		this.graph = graph;
		this.goalChecker = goalChecker;
		this.startNodes = startNodes;
		this.centsPerSecond = centsPerSecond;
		this.forgetSimlilarLabels = forgetSimilarLabels;
		this.singleGoalSearch = singleGoalSearch;

		Set<Integer> nodeIds = graph.getAllNodeIds();
		int numOfNodes = nodeIds.size();

		fringe = new PriorityQueue<>();
		bags = new HashMap<>(numOfNodes);

		for (Integer nodeId : nodeIds) {
			bags.put(nodeId, new HashSet<>());
		}
	}

    public void run() {

		for (FLabel startNode: startNodes) {
			bags.get(startNode.nodeId).add(startNode);
            fringe.add(startNode);
        }

        FLabel currLabel;

		numOfIters = 0;
		int radar = SpeedUpSettings.minRadarToRefreshFringe;
		int radarStep = SpeedUpSettings.radarStepToRefreshFringe;

        while(!fringe.isEmpty()) {

			currLabel = fringe.poll();
			if (!bags.get(currLabel.nodeId).contains(currLabel)) {
				continue; // already removed (dominated) label
			}
			// TODO: 24-Nov-16 sometimes refreshPriority queue by remove dominated labels

			//CUT SIMILAR LABELS
			//something like that...
			//			if (areSimilar(currLabel, eachNodeFromBag)) {
			//				continue;
			//			}

			//////DEBUG/////
			numOfIters++;
			if (fringe.size() > radar) {
				refreshFringe();

				if (fringe.size() > radar) {
					radar += radarStep;
				}
				log.debug("nextRadar: " + radar);
			}
			if (numOfIters > SpeedUpSettings.EVERY_KTH_ITERATION) {
				numOfIters = 0;
				log.debug("currentTravelTime: " + GraphTools.convertFromE3Format(currLabel.travelTimeMillis) + " s");
			}
			//////DEBUG/////

			//get min label and mark as settled
			currLabel.markSettled();

			if (goalChecker.isGoal(currLabel)){

//				log.debug(currLabel.travelTimeMillis);
//				log.debug(bags.get(currLabel.nodeId));

				WPath<FLabel> fLabelPath = graph.buildPath(currLabel);

				if (paths.containsKey(currLabel.nodeId)) {
					paths.get(currLabel.nodeId).add(fLabelPath);
				} else {
					List<WPath<FLabel>> pareto = new ArrayList<>();
					pareto.add(fLabelPath);
					paths.put(currLabel.nodeId, pareto);
				}

			}
			if (goalChecker.isSearchFinished()) {
				break;
			}


			List<FLabel> successors = graph.getSuccessors(currLabel);

			for (FLabel successor : successors) {

				if (!isDominatedByAnyLabelInBag(successor)) {

					Set<FLabel> succBag = bags.get(successor.nodeId);


					if (SpeedUpSettings.BAG_LIMIT && succBag.size() >= SpeedUpSettings.MAX_BAG_SIZE	) continue;
//
					 if (singleGoalSearch && succBag.size() > 50) {
//						log.debug(succBag.size());
					}

					//remove worse labels from labelSet - test with and without this
					//it has to be before bags.add() because equals node are removed
					succBag.removeIf(l -> !l.isSettled() && l.isDominatedBy(successor, centsPerSecond));

					succBag.add(successor);
					fringe.add(successor);
				}
			}
        }


		log.debug(String.format("Iters: %d", numOfIters));
	}

	private void refreshFringe() {
		List<FLabel> labels = new ArrayList<>(fringe);

		int counter = 0;
		log.debug("Refreshing fringe.");
		for (FLabel label : labels) {
			if (!bags.get(label.nodeId).contains(label)) {
				counter++;
				fringe.remove(label);
			}
		}

		log.debug(String.format("Fringe refreshed (size=%d). %d labels removed from total %d", fringe.size(), counter, labels.size()));
	}

	public int getNumOfIters() {
		return numOfIters;
	}

	public Map<Integer, List<WPath<FLabel>>> getPaths() {
		return paths;
	}

	/**
	 * @param successor the new successor
	 * @return true if some label visited earlier is better than new successor
	 */
    private boolean isDominatedByAnyLabelInBag(FLabel successor) {
		for (FLabel label : bags.get(successor.nodeId)) {
			if (
//					(forgetSimlilarLabels && areSimilar(successor, label)) ||
					successor.isDominatedBy(label, centsPerSecond)) {
				return true;
			}
		}
		return false;
    }

	private boolean isDominatedByAnyLabelInGoalBag(FLabel successor) {
		if (!singleGoalSearch) return false;

		for (FLabel label : bags.get(-2)) {
			if (
				//					(forgetSimlilarLabels && areSimilar(successor, label)) ||
					successor.isDominatedBy(label, centsPerSecond)) {
				return true;
			}
		}
		return false;
	}

//	private boolean areSimilar(FLabel successor, FLabel label) {
//		int maxTravelTimeDiff = 60000;
//		int maxTravelTimeDiff2 = 7200000;
//
//
//		return ((successor.costs[0] > label.costs[0]
//				|| (successor.costs[0] > label.costs[0] && label.costs.length > 1 && successor.costs[1] > label.costs[1]))
//				&& (((successor.costs[0]-label.costs[0])< maxTravelTimeDiff)
//				|| (successor.costs[0]-label.costs[0]) > maxTravelTimeDiff2));
//	}
}
