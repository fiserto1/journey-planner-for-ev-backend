package cz.cvut.felk.its.routing.structures.goalchecker;

import cz.cvut.felk.its.routing.structures.EvLabel;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Tomas on 26-Apr-16.
 */
@Deprecated
public class AllNodesAreGoalsChecker<FNode extends EvLabel>
        implements GoalChecker<FNode> {
    protected boolean isSearchFinished;
    protected int numOfGoals;
    protected Set<Integer> goalCounter;

    public AllNodesAreGoalsChecker() {
        this.numOfGoals = Integer.MAX_VALUE;
        this.isSearchFinished = false;
        this.goalCounter = new HashSet<>();
    }


    public AllNodesAreGoalsChecker(int numOfGoals) {
        this.numOfGoals = numOfGoals;
        this.isSearchFinished = false;
        this.goalCounter = new HashSet<>();
    }

    @Override
    public boolean isGoal(FNode node) {
        if (goalCounter.contains(node.nodeId)) return false;
        goalCounter.add(node.nodeId);
        if (goalCounter.size() == numOfGoals) {
            isSearchFinished = true;
            goalCounter = new HashSet<>();
        }
        return true;
    }


    @Override
    public boolean isSearchFinished() {
        return isSearchFinished;
    }

    @Override
    public void setSearchFinished(boolean isSearchFinished) {
        this.isSearchFinished = isSearchFinished;
    }


}
