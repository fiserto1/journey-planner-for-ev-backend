package cz.cvut.felk.its.routing.structures.graph;

import cz.agents.basestructures.GraphStructure;
import cz.cvut.felk.its.evstructures.chargers.Charger;
import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.evstructures.*;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Tomas on 10-Mar-16.
 */
@Deprecated
public class EvGraph<TNode extends EvNode, TEdge extends EvEdge>
        implements SearchGraph<EvLabel> {

    private final GraphStructure<TNode, TEdge> graph;
    private final int startNodeId;
    private final int goalNodeId;
	private final LocalDateTime initTime;
    private final int lastCorrectingNodeId = -3;

//    private final TNode startNode;
//    private final TNode goalNode;
//    private final HashMap<Integer, TEdge> startEdges; //key - edge.toId
//    private final HashMap<Integer, TEdge> goalEdges; //key - edge.fromId
    Set<Integer> chargerNodeIds;
	private ChargingFunction chFunction;
	private BatteryType battery;

	public EvGraph(GraphStructure<TNode, TEdge> graph) {
        this(graph, -2, LocalDateTime.now());
    }

	public EvGraph(GraphStructure<TNode, TEdge> graph, int goalNodeId, LocalDateTime initTime) {
		this.graph = graph;
		this.initTime = initTime;
		this.chargerNodeIds = graph.getAllNodes().stream()
				.filter(EvNode::isCharger)
				.map(node -> node.id)
				.collect(Collectors.toSet());
		this.startNodeId = -1;
		this.goalNodeId = goalNodeId;
	}

    @Override
    public List<EvLabel> getSuccessors(EvLabel currFNode) {
        List<EvLabel> successors = new LinkedList<>();

		if (currFNode.nodeId != lastCorrectingNodeId) {

			if (currFNode.nodeId == goalNodeId) {
				inspectLastEdge(currFNode, successors);
			} else {

				Collection<TEdge> outEdges = graph.getOutEdges(currFNode.nodeId);

				for (TEdge pEdge: outEdges) {
					inspectOutEdge(pEdge, currFNode, successors);
				}
			}

		}


        return successors;
    }

	private void inspectLastEdge(EvLabel currFNode, List<EvLabel> successors) {

		if(currFNode.lastSeenCharger != null) {
			//compute min needed charging pair

			double[] bestChPair = ConsumptionProfileUtils.computePossibleChargingPairs(currFNode, chFunction, battery).get(0);
			int nextTravelTimeMillis = (int) (bestChPair[0]);
			//			ConsumptionProfile nextConsProfile = goalNode.consProfile; // test this!!

			// TODO: 13-Nov-16 this holds only if optimizing on traveltime
			int nextCost = currFNode.costs[0] + (int) bestChPair[0] - currFNode.travelTimeMillis;


			int nextPrice = currFNode.costs[1] + computePriceForCharging(currFNode, bestChPair[1]);

			// TODO: 22-Nov-16 consider to replace last node not add!!!

			currFNode = new EvLabel(lastCorrectingNodeId, new int[]{nextCost, nextPrice}, currFNode, new ConsumptionProfile(), nextTravelTimeMillis,
					currFNode, bestChPair[1], 0);
		} else {
			// add last correcting label - even if no charger was visited
			double realStateOfChargeInLastNode = currFNode.consProfile.computeSocInB(currFNode.lastSeenChargerSoC);
			currFNode = new EvLabel(lastCorrectingNodeId, currFNode.costs, currFNode, new ConsumptionProfile(), currFNode.travelTimeMillis,
					null, realStateOfChargeInLastNode, 0);
		}
		successors.add(currFNode);
	}

	private int computePriceForCharging(EvLabel currFNode, double stateInNextChargerAfterCharge) {

		double stateAfterCharge = ConsumptionProfileUtils.retrieveSoCInA(currFNode, stateInNextChargerAfterCharge,
				battery);
		Charger charger = graph.getNode(currFNode.lastSeenCharger.nodeId).getCharger();
		int pricePerKwh = charger.getPricePerkWh(
				initTime.plusSeconds(GraphTools.convertFromE3Format(currFNode.lastSeenCharger.travelTimeMillis)));

		double stateBeforeCharge = currFNode.lastSeenChargerSoC;
		return (int) (pricePerKwh*((stateAfterCharge-stateBeforeCharge)/1000));
	}

	private void inspectOutEdge(TEdge edge, EvLabel currFNode, List<EvLabel> successors) {

		EvLabel successor;

		// int nextCost = currFNode.costs + edge.consFunction() - potentials[edge.toId] + potentials[edge.fromId];
		int nextCost = currFNode.costs[0] + edge.getWeight();
		int nextTravelTimeMillis = currFNode.travelTimeMillis + edge.travelTimeMillis;

		ConsumptionProfile nextConsProfile =
				ConsumptionProfileUtils.linkConsumptionProfiles(currFNode.consProfile, edge.consumptionProfile);

		if (chargerNodeIds.contains(currFNode.nodeId)) {
			//is charger!!
			if (currFNode.lastSeenCharger == null) {
				//is first charger in the journey
				Charger charger = graph.getNode(currFNode.nodeId).getCharger();
				int pricePerKwh = charger.getPricePerkWh(
						initTime.plusSeconds(GraphTools.convertFromE3Format(currFNode.travelTimeMillis)));

				double initSoC = currFNode.lastSeenChargerSoC;
				double realSoCAtCharger = currFNode.consProfile.computeSocInB(initSoC);
				nextConsProfile = edge.consumptionProfile;

				successor = new EvLabel(edge.toId, new int[]{nextCost, currFNode.costs[1]}, currFNode, nextConsProfile, nextTravelTimeMillis, currFNode,
						realSoCAtCharger, pricePerKwh);
				if (isInBatteryRange(successor)) {
					successors.add(successor);
				}

			} else {
				//is second or following charger
				//chargingPair := 2d int array - [0]chTime; [1]state of charge after charging (stateAfterCharge)

				List<double[]> chargingPairs = ConsumptionProfileUtils.computePossibleChargingPairs(currFNode, chFunction, battery);
				for (double[] chPair: chargingPairs) {
					nextTravelTimeMillis = (int) chPair[0];
					nextConsProfile = edge.consumptionProfile;

					Charger charger = graph.getNode(currFNode.nodeId).getCharger();
					int pricePerKwh = charger.getPricePerkWh(
							initTime.plusSeconds(GraphTools.convertFromE3Format(currFNode.travelTimeMillis)));

					// TODO: 13-Nov-16 this holds only if optimizing on traveltime
					nextCost = currFNode.costs[0] + (int) chPair[0] - currFNode.travelTimeMillis;
					int nextPrice = currFNode.costs[1] + computePriceForCharging(currFNode, chPair[1]);

					successor = new EvLabel(edge.toId, new int[]{nextCost, nextPrice}, currFNode, nextConsProfile,
							nextTravelTimeMillis, currFNode, chPair[1], pricePerKwh);

					if (visitedChargerTwice(successor)) {
						System.out.println("id(" + successor.nodeId + ") charger visited twice");
					}

					if (isInBatteryRange(successor)) {
						successors.add(successor);
					}

				}
			}
		} else {
			//not charger

			successor = new EvLabel(edge.toId, new int[]{nextCost, currFNode.costs[1]}, currFNode, nextConsProfile, nextTravelTimeMillis,
					currFNode.lastSeenCharger, currFNode.lastSeenChargerSoC, currFNode.lastSeenChargerCpkWh);

			if (isInBatteryRange(successor)) {
				successors.add(successor);
			}
		}
    }

	private boolean isInBatteryRange(EvLabel successor) {
		if (successor.consProfile == null) return true;

//		if (visitedChargerTwice(successor)) return false;

		return !(successor.consProfile.inSoc > ChargingModel.MAX_STATE_OF_CHARGE
				|| (successor.consProfile.outSoc < ChargingModel.MIN_STATE_OF_CHARGE && successor.lastSeenCharger == null)
				|| (successor.lastSeenChargerSoC < successor.consProfile.inSoc && successor.lastSeenCharger == null));

	}

	private boolean visitedChargerTwice(EvLabel successor) {

		int nextNodeId = successor.nodeId;

		EvLabel prevLabel = successor.prevLabel;
		while (prevLabel.lastSeenCharger != null) {
			if (prevLabel.lastSeenCharger.nodeId == nextNodeId) {
				return true;
			}
			prevLabel = prevLabel.prevLabel;
		}
		return false;
	}

	@Override
	public Path<EvLabel> buildPath(EvLabel goalNode) {

//		if(goalNode.lastSeenCharger >= 0) {
//			double[] bestChPair = ConsumptionProfileUtils.computePossibleChargingPairs(goalNode).get(0);
//			int nextTravelTimeMillis = (int) (bestChPair[0]);
////			ConsumptionProfile nextConsProfile = goalNode.consProfile; // test this!!
//
//			// TODO: 13-Nov-16 this holds only if optimizing on traveltime
//			int nextCost = goalNode.costs + (int) bestChPair[0] - goalNode.travelTimeMillis;
//
//			// TODO: 22-Nov-16 consider to replace last node not add!!!
//
//			goalNode = new EvLabel(goalNode.nodeId, nextCost, goalNode, null, nextTravelTimeMillis,
//					goalNode.nodeId, bestChPair[1]);
//		}

		List<EvLabel> nodes = new ArrayList<>();

		EvLabel node = goalNode;
		while (node != null) {
			nodes.add(node);
			node = node.prevLabel;
		}

		Collections.reverse(nodes);

		return new Path<>(goalNode.costs, nodes);
	}

	@Override
	public Set<Integer> getAllNodeIds() {
		Set<Integer> allNodeIds = graph.getAllNodes().stream().mapToInt(n -> n.id).boxed().collect(Collectors.toSet());
		allNodeIds.add(lastCorrectingNodeId);
		return allNodeIds;
	}

	//    @Deprecated
//    public List<EvLabel> getPredecessors(EvLabel currFNode) {
//        List<EvLabel> predecessors = new LinkedList<>();
//
//		Collection<TEdge> inEdges = graph.getInEdges(currFNode.nodeId);
//
//		for (TEdge pEdge: inEdges) {
//            inspectInEdge(pEdge, currFNode, predecessors);
//        }
//
//        return predecessors;
//    }
//
//	@Deprecated
//    private void inspectInEdge(TEdge edge, EvLabel currFNode, List<EvLabel> incomingFNodes) {
//		int newSOC = edge.getPrevSOC(currFNode.stateOfCharge, currFNode.pathMinSOC);
//		if (newSOC == Integer.MAX_VALUE) return;
//
//		int newCost = currFNode.costs + edge.getWeight();
//		int newPathMinSOC = currFNode.pathMinSOC;
//		if (newSOC < newPathMinSOC) {
//			newPathMinSOC = newSOC;
//		}
//		incomingFNodes.add(
//				new EvLabel(edge.fromId, newCost, newSOC, currFNode, newSOC,  newPathMinSOC, null, null, -1, -1, -1));
//    }
//
//    @Deprecated
//    public Solution createSolution(EvLabel goalNode) {
//        int travelTimeMillis = 0;
//        ConsumptionProfile consProfile = goalNode.consProfile;
//
//        int length = 0;
//
//        LinkedList<Integer> routeNodeIds = new LinkedList<>();
//        LinkedList<Integer> statesOfCharge = new LinkedList<>();
//		EvLabel currentNode = goalNode;
//        routeNodeIds.add(goalNode.nodeId);
//        statesOfCharge.add(goalNode.stateOfCharge);
//        while (currentNode.prevLabel != null) {
//			EvLabel prevLabel = currentNode.prevLabel;
//            TEdge edge = graph.getEdge(prevLabel.nodeId, currentNode.nodeId);
//			travelTimeMillis += edge.travelTimeMillis;
////			consumption += edge.consumptionProfileconsFunction(prevLabel.stateOfCharge);
//			length += edge.length;
//			routeNodeIds.addFirst(prevLabel.nodeId);
//			statesOfCharge.addFirst(prevLabel.stateOfCharge);
//            currentNode = prevLabel;
//        }
//        return new Solution(routeNodeIds, length, travelTimeMillis, 0, statesOfCharge, statesOfCharge, consProfile);
//    }
//
//
//    @Deprecated
//    public Solution createSolutionFromBackward(EvLabel startNode) {
//        int travelTimeMillis = 0;
//        ConsumptionProfile consProfile = null;
//        int length = 0;
//
//        LinkedList<Integer> routeNodeIds = new LinkedList<>();
//        LinkedList<Integer> statesOfCharge = new LinkedList<>();
//		EvLabel currentNode = startNode;
//
//        int underSoCMin = ChargingModel.MIN_STATE_OF_CHARGE - startNode.pathMinSOC;
//        int newShiftedSoC = startNode.stateOfCharge + underSoCMin;
//        if (newShiftedSoC < ChargingModel.MIN_STATE_OF_CHARGE) return null;
//        if (newShiftedSoC > ChargingModel.MAX_STATE_OF_CHARGE) System.err.println("Attention: initSoC is greater than SOCMAX");
//        routeNodeIds.add(startNode.nodeId);
//        statesOfCharge.add(newShiftedSoC);
//        while (currentNode.prevLabel != null) {
//            EvLabel nextNode = currentNode.prevLabel;
//            TEdge edge = graph.getEdge(currentNode.nodeId, nextNode.nodeId);
////
//			travelTimeMillis += edge.travelTimeMillis;
//			//                int currShiftedSoC = currentNode.incomingSoC + underSoCMin;
//			//                int nextNewSoC = nextNode.incomingSoC + underSoCMin;
//			int edgeConsumption = edge.consFunction(newShiftedSoC);
//			newShiftedSoC -= edgeConsumption;
////			consumption += edgeConsumption;
//			length += edge.length;
//			routeNodeIds.addLast(nextNode.nodeId);
//			statesOfCharge.addLast(newShiftedSoC);
//
//            currentNode = nextNode;
//        }
//
//        return new Solution(routeNodeIds, length, travelTimeMillis, 0, statesOfCharge, statesOfCharge, consProfile);
//    }
}
