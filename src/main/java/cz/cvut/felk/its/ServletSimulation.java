package cz.cvut.felk.its;

import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.Graph;
import cz.agents.geotools.GPSLocationKDTreeResolver;
import cz.agents.geotools.KDTree;
import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.evstructures.chargers.ChargingPriceFileLoader;
import cz.cvut.felk.its.evstructures.chargers.ChargingPriceLoader;
import cz.cvut.felk.its.utils.SerializeUtil;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.PlanningEdge;

/**
 * Created by Tomas on 14-May-16.
 */
@Deprecated
public class ServletSimulation {

    Graph<EvNode, PlanningEdge> deserializedSuperGraph;
    KDTree<GPSLocation> kdTree;
    KDTree<GPSLocation> kdTree2;
    ChargingModel chargingModel;
    ChargingFunction chargingFunction;
    int numOfChargers;
    ChargingPriceLoader chpl;
    long lastPriceLoadedTime;
    private static final long PRICE_LOAD_INTERVAL = 60000;

//    public static void main(String[] args) throws ServletException {
//        ServletSimulation servletSim = new ServletSimulation();
//        servletSim.init();
//
//    }


    public ServletSimulation() {
//        this("serialized/GE-bytns-to-trunk-ultra.ser");
//        this("GE-tertiary-bytns-prec-graph-ultra.ser");
//        String graphFilepath = "serialized/GE-bytns-to-trunk-ultra.ser";
    }

    public void init(String graphFilepath) {
        deserializedSuperGraph = SerializeUtil.deserializeGraph(graphFilepath);
        System.out.println("Graph loaded: " + deserializedSuperGraph);

        kdTree = new KDTree<>(2, new GPSLocationKDTreeResolver<>(),-1);
        kdTree2 = new KDTree<>(2, new GPSLocationKDTreeResolver<>(),-1);

        numOfChargers = 0;
        for (EvNode evNode : deserializedSuperGraph.getAllNodes()) {
            if (evNode.getCharger() != null) {
                numOfChargers++;
            }
            kdTree.insert(evNode);
            kdTree2.insert(evNode);
        }
        System.out.println(deserializedSuperGraph.getOutEdges(0).get(0));

        for(PlanningEdge edge: deserializedSuperGraph.getAllEdges()) {
            if (edge instanceof EvEdge) {
                EvEdge evEdge = (EvEdge) edge;
				if (evEdge.viaNodes == null) continue;
                for (GPSLocation viaNode: evEdge.viaNodes){
                    kdTree.insert(viaNode);
                }
            }
        }

        chargingModel = new ChargingModel(ChargingModel.MIN_STATE_OF_CHARGE, ChargingModel.MAX_STATE_OF_CHARGE);
        chargingFunction = new ChargingFunction();

        // TODO: 14-Apr-16 create thread which will load prices every 5-10 minutes
        chpl = new ChargingPriceFileLoader();
        lastPriceLoadedTime = System.currentTimeMillis();
        chpl.loadPrices(deserializedSuperGraph);
    }


//    public EvResponse doPost(int originLatE6, int originLonE6, int destLatE6, int destLonE6, int initSoCInPerc, double reduceConst) {
//
////        long timeDiff = System.currentTimeMillis() - lastPriceLoadedTime;
////        if (timeDiff > PRICE_LOAD_INTERVAL) {
////            chpl.loadPrices(deserializedSuperGraph);
////            lastPriceLoadedTime = System.currentTimeMillis();
////        }
//
////        System.out.println("Starting new plan");
//        Calendar calendar = Calendar.getInstance();
//        int initTimeMins = calendar.get(Calendar.HOUR_OF_DAY)*60 + calendar.get(Calendar.MINUTE);
//
//        long start = System.currentTimeMillis();
//
//        double originLat = ((double) originLatE6)/1E6;
//        double originLon = ((double) originLonE6)/1E6;
//        double destLat = ((double) destLatE6)/1E6;
//        double destLon = ((double) destLonE6)/1E6;
//        int initSOC = (int) ((((double) initSoCInPerc)/100) * ChargingModel.MAX_STATE_OF_CHARGE);
//        if (initSOC < ChargingModel.MIN_STATE_OF_CHARGE || initSOC > ChargingModel.MAX_STATE_OF_CHARGE) {
////            System.err.println("State Of Charge is out of range.");
//            calendar = Calendar.getInstance();
//            Timestamp currentTimestamp = new java.sql.Timestamp(calendar.getTime().getTime());
//            return new EvResponse(currentTimestamp.toString(), null, EvResponse.STATUS_FAIL);
//        }
//
//        GPSLocation startLocation = GPSLocationTools.createGPSLocation(originLat, originLon, 0, GraphTools.GERMANY_SRID);
//        GPSLocation goalLocation = GPSLocationTools.createGPSLocation(destLat, destLon, 0, GraphTools.GERMANY_SRID);
//
//        SearchGraph<EvLabel> evSearchGraph = EdgePrecomputer.buildEvSearchGraph(deserializedSuperGraph,
//                startLocation, goalLocation, kdTree2);
//        Solution[] fastestAndCheapestSolution = EdgePrecomputer.findFastestAndCheapestSolution(deserializedSuperGraph,
//                evSearchGraph, initSOC, numOfChargers, chargingModel, initTimeMins, reduceConst);
//
//        Journey moJourney = MOEvRouting.findMOFastestJourney(deserializedSuperGraph, startLocation, goalLocation,
//                initSOC, chargingModel, chargingFunction, kdTree);
//        Plan moPlan = EvResponseBuilder.buildMOPlan(deserializedSuperGraph, moJourney, chargingFunction);
//
//        EvResponse evResponse = EvResponseBuilder.buildResponse(evSearchGraph, fastestAndCheapestSolution, moPlan,
//                startLocation, goalLocation);
//        if (evResponse == null) {
////            System.err.println("Initial SoC is too small.");
//            calendar = Calendar.getInstance();
//            Timestamp currentTimestamp = new java.sql.Timestamp(calendar.getTime().getTime());
//            return new EvResponse(currentTimestamp.toString(), null, EvResponse.STATUS_FAIL);
//        }
//
//        long elapsedTime = System.currentTimeMillis() - start;
//        ServletTest.addRtR(elapsedTime);
////        System.out.println("Sending response, total time: " + elapsedTime + "ms");
////        System.out.println("--------------------------------------------------------");
//        return evResponse;
//    }
}
