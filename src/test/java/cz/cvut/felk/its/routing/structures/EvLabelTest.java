package cz.cvut.felk.its.routing.structures;

import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.evstructures.ConsumptionProfile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.PriorityQueue;

public class EvLabelTest {

	PriorityQueue<EvLabel> priorityQueue;

	ConsumptionProfile cp1;
	ConsumptionProfile cp2;

	EvLabel n1;
	EvLabel n2;
	EvLabel n3;
	EvLabel n4;

	@Before
	public void setUp() throws Exception {
		priorityQueue = new PriorityQueue<>();

		cp1 = new ConsumptionProfile(10, 5, 5);
		cp2 = new ConsumptionProfile(10, 7, 3); //better than cp1

		n1 = new EvLabel(0, new int[]{10}, null, cp1, 0, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);
		n2 = new EvLabel(0, new int[]{9}, null, cp1, 0, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);
		n3 = new EvLabel(0, new int[]{9}, null, cp2, 0, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);
		n4 = new EvLabel(0, new int[]{9}, null, cp2, 0, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);
	}

	@Test
	public void testCompareToInQueue() throws Exception {

		int compare = Integer.compare(10, 9);
		PriorityQueue<Integer> pq = new PriorityQueue<>();
		pq.add(10);
		pq.add(9);
		Integer poll = pq.poll();
		System.out.println(poll);


		int i = cp1.compareTo(cp2);
		int i2 = cp2.compareTo(cp1);
		System.out.println(i);
		System.out.println(i2);

		priorityQueue.add(n1);
		priorityQueue.add(n2);

		EvLabel first = priorityQueue.poll();
		Assert.assertEquals(n2, first);

		priorityQueue.add(n2);
		priorityQueue.add(n3);

		first = priorityQueue.poll();
		Assert.assertEquals(n3, first);


	}

	@Test
	public void testCompareTo() throws Exception {


		Assert.assertTrue(n1.compareTo(n2) > 0);

		Assert.assertTrue(n2.compareTo(n1) < 0);

		Assert.assertTrue(n2.compareTo(n3) > 0);

		Assert.assertTrue(n3.compareTo(n2) < 0);

		Assert.assertTrue(n3.compareTo(n4) == 0);

	}

	@Test
	public void testIsDominatedBy() throws Exception {

		Assert.assertTrue(n1.isDominatedBy(n2, 0));
		Assert.assertFalse(n2.isDominatedBy(n1, 0));

		Assert.assertTrue(n1.isDominatedBy(n3, 0));

		Assert.assertTrue(n2.isDominatedBy(n3, 0));


		//equal nodes are dominated by each other
		Assert.assertTrue(n3.isDominatedBy(n4, 0));
		Assert.assertTrue(n4.isDominatedBy(n3, 0));

	}
}