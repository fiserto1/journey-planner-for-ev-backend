package cz.cvut.felk.its.routing.alg;

import cz.agents.basestructures.Graph;
import cz.agents.basestructures.GraphBuilder;
import cz.cvut.felk.its.api.structures.EvResponseBuilder;
import cz.cvut.felk.its.api.structures.Plan;
import cz.cvut.felk.its.evstructures.chargers.Charger;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.evstructures.chargers.ChargingPriceRefresher;
import cz.cvut.felk.its.evstructures.chargers.Supercharger;
import cz.cvut.felk.its.routing.structures.goalchecker.MOGoalChecker;
import cz.cvut.felk.its.routing.structures.routers.JourneyBuilder;
import cz.cvut.felk.its.routing.structures.routers.RouterType;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.routing.structures.PlanningInstance;
import cz.cvut.felk.its.routing.structures.graph.EvGraph;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;
import cz.cvut.felk.its.evstructures.ConsumptionProfile;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.graphs.ExtendedGraph;
import cz.cvut.felk.its.evstructures.journey.Journey;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class MulticriteriaDijkstraTest {

	private static Graph<EvNode, EvEdge> graphWithChargers;

	@Before
	public void setUp() throws Exception {


		buildGraphWithChargers();
	}

	private static void buildGraphWithChargers() {


		GraphBuilder<EvNode, EvEdge> gb = new GraphBuilder<>();
		EvNode n0 = new EvNode(0);
		EvNode n1 = new EvNode(1);
		EvNode n2 = new EvNode(2);
		EvNode n3 = new EvNode(3);
		EvNode n4 = new EvNode(4);

		Charger s0 = new Supercharger(-1, "s0", null, "open");
		Charger s1 = new Supercharger(-2, "s1", null, "open");

		n1.setCharger(s0);
		n3.setCharger(s1);

		gb.addNode(n0);
		gb.addNode(n1);
		gb.addNode(n2);
		gb.addNode(n3);
		gb.addNode(n4);

		gb.addEdge(new EvEdge(0, 1, 7, 7, new ConsumptionProfile(84500, 500, 84000)));
		gb.addEdge(new EvEdge(1, 2, 9, 9, new ConsumptionProfile(5500, 80000, 5000)));
		gb.addEdge(new EvEdge(2, 3, 10, 10, new ConsumptionProfile(76500, 9000, 76000)));
		gb.addEdge(new EvEdge(3, 4, 11, 11, new ConsumptionProfile(5500, 80000, 5000)));

		graphWithChargers = gb.createGraph();
		ChargingPriceRefresher.INSTANCE.refreshPrices(graphWithChargers);
	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testRun() throws Exception {

		EvNode startNode = graphWithChargers.getNode(0);
		EvNode goalNode = graphWithChargers.getNode(4);
		Map<Integer, EvEdge> startEdges = graphWithChargers.getOutEdges(startNode).stream()
				.collect(Collectors.toMap(EvEdge::getToId, e -> e));
		Map<Integer, EvEdge> goalEdges = graphWithChargers.getInEdges(goalNode).stream()
				.collect(Collectors.toMap(EvEdge::getFromId, e -> e));

		ExtendedGraph<EvNode, EvEdge> extendedGraph = new ExtendedGraph<>(graphWithChargers, startNode, goalNode,
				startEdges, goalEdges);

		PlanningInstance plInstance = new PlanningInstance(extendedGraph, ChargingModel.MAX_STATE_OF_CHARGE, LocalDateTime.now(), 0.01);
		SearchGraph<EvLabel> searchGraph = new EvGraph<>(graphWithChargers, 4, plInstance.getInitTime());

		MOGoalChecker<EvLabel> gc = new MOGoalChecker<>();

		EvLabel startFNode = new EvLabel(0, ChargingModel.MAX_STATE_OF_CHARGE);
		//		EvLabel startFNode = new EvLabel(0);

		Set<EvLabel> startNodes = new HashSet<>();

		startNodes.add(startFNode);

		MulticriteriaDijkstra<EvLabel> dijkstra = new MulticriteriaDijkstra<>(searchGraph, startNodes, gc);

		dijkstra.run();

		List<Path<EvLabel>> paths = dijkstra.getPaths().get(-3);
		//		Path<EvLabel> pathToForthNode = paths.get(3);
		//		System.out.println(paths);

		paths.forEach(path -> System.out.println(path.prettyPring()));
		//		Assert.assertEquals(pathToForthNode, expectedPath);

		Path<EvLabel> fringeNodePath = paths.get(0);

		Journey journey = JourneyBuilder.buildJourney(fringeNodePath, plInstance.getExtendedEvGraph(),
				plInstance.getInitTime(), plInstance.getBatteryType(), RouterType.FAST);
		System.out.println(journey);

		Plan plan = EvResponseBuilder.buildPlan(1,
				new PlanningInstance(null, null, null, extendedGraph, null, -1, null, null, null,
						null, 0.01), journey);

		System.out.println(plan);

	}
}