package cz.cvut.felk.its.routing;

import cz.agents.basestructures.GraphStructure;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.routing.alg.MODijkstra;
import cz.cvut.felk.its.routing.structures.goalchecker.ByNodeIdsGoalChecker;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.graph.EvGraph;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import org.junit.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Tomas on 01-Aug-16.
 */
public class MODijkstraTest {
    private static GraphStructure<EvNode, EvEdge> graph;
//    private static String graphFilepath = "serialized/my-graph-v1.ser";

    @BeforeClass
    public static void runOnceBeforeClass() {
//        graph = SerializeUtil.deserializeGraph(graphFilepath);
//        System.out.println("Graph loaded: " + graph.toString());
    }

//    @BeforeClass
//    public static void runOnceBeforeClass() {
//        Supercharger s0 = new Supercharger(-1, "s0", new GPSLocation(0,0,0,0), "open");
//        Supercharger s1 = new Supercharger(-2, "s1", new GPSLocation(0,0,0,0), "open");
//        Supercharger s2 = new Supercharger(-3, "s2", new GPSLocation(0,0,0,0), "open");
//
//        GraphBuilder<EvNode, PlanningEdge> gb = new GraphBuilder<>();
//        gb.addNode(new EvNode(0, 0, 0, 0, 0, 0, 0, s0));
//        gb.addNode(new EvNode(1, 1, 0, 0, 0, 0, 0));
//        gb.addNode(new EvNode(2, 2, 0, 0, 0, 0, 0, s1));
//        gb.addNode(new EvNode(3, 3, 0, 0, 0, 0, 0));
//        gb.addNode(new EvNode(4, 4, 0, 0, 0, 0, 0, s2));
//        gb.addNode(new EvNode(5, 5, 0, 0, 0, 0, 0));
//
//
//        //        gb.addEdge(new EvEdge(0, 1, 0, 0, 3, 20));
//        //        gb.addEdge(new EvEdge(1, 3, 0, 0, 3, 20));
//        //        gb.addEdge(new EvEdge(0, 2, 0, 0, 2, 30));
//        //        gb.addEdge(new EvEdge(2, 3, 0, 0, 2, 30));
//        //        gb.addEdge(new EvEdge(0, 3, 0, 0, 5, 55));
//        //        gb.addEdge(new EvEdge(3, 4, 0, 0, 2, 50));
//
//        //        gb.addEdge(new EvEdge(0, 1, 0, 0, 3, 2000));
//        //        gb.addEdge(new EvEdge(1, 3, 0, 0, 3, 1000));
//        //        gb.addEdge(new EvEdge(0, 2, 0, 0, 2, 15000));
//        //        gb.addEdge(new EvEdge(2, 3, 0, 0, 2, 15000));
//        //        gb.addEdge(new EvEdge(0, 3, 0, 0, 5, 55000));
//        //        gb.addEdge(new EvEdge(3, 4, 0, 0, 2, 10000));
//
//        gb.addEdge(new EvEdge(0, 1, 0, 0, 10, 10));
//        gb.addEdge(new EvEdge(0, 5, 0, 0, 40, 3));
//        gb.addEdge(new EvEdge(1, 2, 0, 0, 15, 2));
//        gb.addEdge(new EvEdge(1, 4, 0, 0, 25, -3));
//        gb.addEdge(new EvEdge(2, 1, 0, 0, 18, -1));
//        gb.addEdge(new EvEdge(3, 2, 0, 0, 30, 4));
//        gb.addEdge(new EvEdge(4, 3, 0, 0, 5, 1));
//        gb.addEdge(new EvEdge(4, 2, 0, 0, 23, 4));
//        gb.addEdge(new EvEdge(5, 4, 0, 0, 11, 3));
//        //
//        gb.addEdge(new EvEdge(1, 5, 0, 0, 15, -2));
//        gb.addEdge(new EvEdge(0, 2, 0, 0, 30, 3));
//
////        graph = gb.createGraph();
////
////
//        graph = SerializeUtil.deserializeGraph("serialized/GE-bytns-to-trunk-ultra.ser");
//        System.out.println("Graph loaded: " + graph.toString());
//    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @Ignore
    @Test
    public void testMODijkstra() throws Exception {

        SearchGraph<EvLabel> sGraph = new EvGraph<>(graph);

        ByNodeIdsGoalChecker<EvLabel> gc = new ByNodeIdsGoalChecker<>(4);

        EvLabel startLabel = new EvLabel(6, (int) (ChargingModel.MAX_STATE_OF_CHARGE*0.8));
        Set<EvLabel> startLabels = new HashSet<>();
        startLabels.add(startLabel);

        MODijkstra<EvLabel> dijkstra = new MODijkstra<>(sGraph, startLabels, gc);

        dijkstra.run();

        System.out.println(dijkstra.getPaths());

    }

    @Ignore
    @Test
    public void testDominates() throws Exception {

    }

//    @Ignore
//    @Test
//    public void testComputePossibleChargingPairs2() throws Exception {
//
//
//
//
//        ConsumptionProfile consProfile = new ConsumptionProfile(100, 200, 100);
//        System.out.println(consProfile);
//        Label minLabel = new Label(1, 1000 ,consProfile, 0, ChargingModel.MAX_STATE_OF_CHARGE, null);
//
//        List<double[]> pairs = ConsumptionProfileUtils.computePossibleChargingPairsForLabel(minLabel);
//
//        double[] expectedResult = new double[]{1000, 84900};
//        System.out.println("Expected: " + Arrays.toString(expectedResult));
//        System.out.println("Result: " + Arrays.toString(pairs.get(0)));
//
//
//        Label minLabel2 = new Label(0, 1000, consProfile, 0, 0.99* ChargingModel.MAX_STATE_OF_CHARGE, null);
//        List<double[]> pairs2 = ConsumptionProfileUtils.computePossibleChargingPairsForLabel(minLabel2);
//
//        double[] expectedResult2 = new double[]{1000, 84050};
//        System.out.println("Expected: " + Arrays.toString(expectedResult2));
//        System.out.println("Result: " + Arrays.toString(pairs2.get(0)));
//        double[] expectedResult3 = new double[]{106000, 84900};
//        System.out.println("Expected: " + Arrays.toString(expectedResult3));
//        System.out.println("Result: " + Arrays.toString(pairs2.get(1)));
//
//    }
//
//    @Ignore
//    @Test //from paper Baum
//    public void testComputePossibleChargingPairs() throws Exception {
//
//        ConsumptionProfile consProfile = new ConsumptionProfile(1, 3, -1);
//        Label minLabel = new Label(0, 3, consProfile, 645616, 0.5, null);
//
//        List<double[]> pairs = ConsumptionProfileUtils.computePossibleChargingPairsForLabel(minLabel);
//
//
//        for (double[] pair : pairs) {
//            System.out.println(Arrays.toString(pair));
//        }
//
//    }
//
//    @Test
//    public void testComputePossibleChargingPairs3() {
//
//        ConsumptionProfile consProfile = new ConsumptionProfile(56246, 29254, 55746);
//        Label minLabel = new Label(0, 39221144, consProfile, 12, 500, null);
//
//        List<double[]> pairs = ConsumptionProfileUtils.computePossibleChargingPairsForLabel(minLabel);
//
//
//        for (double[] pair : pairs) {
//            System.out.println(Arrays.toString(pair));
//        }
//
//    }
}