package cz.cvut.felk.its.routing.alg;

import cz.agents.basestructures.Graph;
import cz.agents.basestructures.GraphBuilder;
import cz.cvut.felk.its.api.structures.EvResponseBuilder;
import cz.cvut.felk.its.api.structures.Plan;
import cz.cvut.felk.its.evstructures.chargers.Charger;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.evstructures.chargers.ChargingPriceRefresher;
import cz.cvut.felk.its.evstructures.chargers.Supercharger;
import cz.cvut.felk.its.routing.structures.goalchecker.AllNodesAreGoalsChecker;
import cz.cvut.felk.its.routing.structures.goalchecker.ByNodeIdsGoalChecker;
import cz.cvut.felk.its.routing.structures.routers.JourneyBuilder;
import cz.cvut.felk.its.routing.structures.routers.RouterType;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.routing.structures.PlanningInstance;
import cz.cvut.felk.its.routing.structures.graph.EvGraph;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;
import cz.cvut.felk.its.evstructures.ConsumptionProfile;
import cz.cvut.felk.its.evstructures.ConsumptionProfileUtils;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.graphs.ExtendedGraph;
import cz.cvut.felk.its.evstructures.journey.Journey;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ModifiedDijkstraNonStaticTest {

	static Graph<EvNode, EvEdge> graph;
	static Graph<EvNode, EvEdge> graphWithChargers;


	@Before
	public void setUp() throws Exception {


		buildGraph();
		buildGraphWithChargers();
	}

	private static void buildGraphWithChargers() {


		GraphBuilder<EvNode, EvEdge> gb = new GraphBuilder<>();
		EvNode n0 = new EvNode(0);
		EvNode n1 = new EvNode(1);
		EvNode n2 = new EvNode(2);
		EvNode n3 = new EvNode(3);
		EvNode n4 = new EvNode(4);

		Charger s0 = new Supercharger(-1, "s0", null, "open");
		Charger s1 = new Supercharger(-2, "s1", null, "open");

		n1.setCharger(s0);
		n3.setCharger(s1);

		gb.addNode(n0);
		gb.addNode(n1);
		gb.addNode(n2);
		gb.addNode(n3);
		gb.addNode(n4);

		gb.addEdge(new EvEdge(0, 1, 7, 7, new ConsumptionProfile(84500, 500, 84000)));
		gb.addEdge(new EvEdge(1, 2, 9, 9, new ConsumptionProfile(5500, 80000, 5000)));
		gb.addEdge(new EvEdge(2, 3, 10, 10, new ConsumptionProfile(76500, 9000, 76000)));
		gb.addEdge(new EvEdge(3, 4, 11, 11, new ConsumptionProfile(5500, 80000, 5000)));

		graphWithChargers = gb.createGraph();
		ChargingPriceRefresher.INSTANCE.refreshPrices(graphWithChargers);
	}

	private static void buildGraph() {


		GraphBuilder<EvNode, EvEdge> gb = new GraphBuilder<>();
		EvNode n0 = new EvNode(0);
		EvNode n1 = new EvNode(1);
		EvNode n2 = new EvNode(2);
		EvNode n3 = new EvNode(3);

		gb.addNode(n0);
		gb.addNode(n1);
		gb.addNode(n2);
		gb.addNode(n3);

		ConsumptionProfile cp0 = ConsumptionProfileUtils.computeConsumptionProfileBetweenTwoLocs(n0, n1, 7);

		//Special one!!
		ConsumptionProfile cp1 = new ConsumptionProfile(ChargingModel.MAX_STATE_OF_CHARGE, 0, ChargingModel.MAX_STATE_OF_CHARGE);

		ConsumptionProfile cp3 = ConsumptionProfileUtils.computeConsumptionProfileBetweenTwoLocs(n1, n2, 10);
		ConsumptionProfile cp5 = ConsumptionProfileUtils.computeConsumptionProfileBetweenTwoLocs(n2, n3, 11);

		gb.addEdge(new EvEdge(0, 1, 7, 7, cp0));
		gb.addEdge(new EvEdge(0, 2, 9, 9, cp1));
		gb.addEdge(new EvEdge(1, 2, 10, 10, cp3));
		gb.addEdge(new EvEdge(2, 3, 11, 11, cp5));

		graph = gb.createGraph();
	}

	@Ignore
	@Test
	public void testFindPathWithBatteryConstraint() throws Exception {
		SearchGraph<EvLabel> searchGraph = new EvGraph<>(graph);
		AllNodesAreGoalsChecker<EvLabel> gc = new AllNodesAreGoalsChecker<>();
		EvLabel startFNode = new EvLabel(0);

		List<EvLabel> nodes = new ArrayList<>();
		EvLabel firstStep = new EvLabel(1, new int[]{7}, startFNode, new ConsumptionProfile(501, 84999, 1), 7, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);
		EvLabel secondStep = new EvLabel(2, new int[]{17}, firstStep, new ConsumptionProfile(503, 84997, 3), 17, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);
		EvLabel thirdStep = new EvLabel(3, new int[]{28}, secondStep, new ConsumptionProfile(505, 84995, 5), 28, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);

		nodes.add(startFNode);
		nodes.add(firstStep);
		nodes.add(secondStep);
		nodes.add(thirdStep);
		Path<EvLabel> expectedPath = new Path<>(new int[] { 28 }, nodes);

		ModifiedDijkstraNonStatic<EvLabel> dijkstra = new ModifiedDijkstraNonStatic<>(searchGraph, startFNode, gc);

		dijkstra.run();


		List<Path<EvLabel>> paths = dijkstra.getPaths();
		Path<EvLabel> pathToForthNode = paths.get(3);
		System.out.println(paths);

		paths.forEach(path -> System.out.println(path.prettyPring()));
		Assert.assertEquals(pathToForthNode, expectedPath);

	}


	@Test
	public void testRunOverChargers() throws Exception {
		SearchGraph<EvLabel> searchGraph = new EvGraph<>(graphWithChargers);
		ByNodeIdsGoalChecker<EvLabel> gc = new ByNodeIdsGoalChecker<>(4);

		EvLabel startFNode = new EvLabel(0, ChargingModel.MAX_STATE_OF_CHARGE);
//		EvLabel startFNode = new EvLabel(0);

//		List<EvLabel> nodes = new ArrayList<>();
//		EvLabel firstStep = new EvLabel(1, 7, 0, startFNode,  -1, -1, null, new ConsumptionProfile(501, 84999, 1), 7, -1, ChargingModel.MAX_STATE_OF_CHARGE);
//		EvLabel secondStep = new EvLabel(2, 17, 0, firstStep,  -1, -1, null, new ConsumptionProfile(503, 84997, 3), 17, -1, ChargingModel.MAX_STATE_OF_CHARGE);
//		EvLabel thirdStep = new EvLabel(3, 28, 0, secondStep,  -1, -1, null, new ConsumptionProfile(505, 84995, 5), 28, -1, ChargingModel.MAX_STATE_OF_CHARGE);

//		nodes.add(startFNode);
//		nodes.add(firstStep);
//		nodes.add(secondStep);
//		nodes.add(thirdStep);
//		Path<EvLabel> expectedPath = new Path<>(new int[] { 28 }, nodes);

		ModifiedDijkstraNonStatic<EvLabel> dijkstra = new ModifiedDijkstraNonStatic<>(searchGraph, startFNode, gc);

		dijkstra.run();


		List<Path<EvLabel>> paths = dijkstra.getPaths();
//		Path<EvLabel> pathToForthNode = paths.get(3);
//		System.out.println(paths);

		paths.forEach(path -> System.out.println(path.prettyPring()));
//		Assert.assertEquals(pathToForthNode, expectedPath);

		Path<EvLabel> fringeNodePath = paths.get(0);

		EvNode startNode = graphWithChargers.getNode(0);
		EvNode goalNode = graphWithChargers.getNode(4);
		Map<Integer, EvEdge> startEdges = graphWithChargers.getOutEdges(startNode).stream()
				.collect(Collectors.toMap(EvEdge::getToId, e -> e));
		Map<Integer, EvEdge> goalEdges = graphWithChargers.getInEdges(goalNode).stream()
				.collect(Collectors.toMap(EvEdge::getFromId, e -> e));

		ExtendedGraph<EvNode, EvEdge> extendedGraph = new ExtendedGraph<>(graphWithChargers, startNode, goalNode,
				startEdges, goalEdges);

		PlanningInstance plInstance = new PlanningInstance(extendedGraph, ChargingModel.MAX_STATE_OF_CHARGE, 0.01);
		Journey journey = JourneyBuilder.buildJourney(fringeNodePath, plInstance.getExtendedEvGraph(), plInstance.getInitTime(),
				plInstance.getBatteryType(), RouterType.FAST);
		System.out.println(journey);

		Plan plan = EvResponseBuilder.buildPlan(1,
				new PlanningInstance(null, null, null, extendedGraph, null, -1, null, null, null,
						null, 0.01), journey);

		System.out.println(plan);

	}
}