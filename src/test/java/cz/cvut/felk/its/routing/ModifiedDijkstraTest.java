package cz.cvut.felk.its.routing;

import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.GraphBuilder;
import cz.cvut.felk.its.evstructures.chargers.Supercharger;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.PlanningEdge;
import junit.framework.TestCase;

/**
 * Created by Tomas on 07-May-16.
 */
public class ModifiedDijkstraTest extends TestCase {
    SearchGraph<EvLabel> evGraph;

    @org.junit.Before
    public void setUp() throws Exception {
        Supercharger s0 = new Supercharger(-1, "s0", new GPSLocation(0,0,0,0), "open");
        Supercharger s1 = new Supercharger(-2, "s1", new GPSLocation(0,0,0,0), "open");
        Supercharger s2 = new Supercharger(-3, "s2", new GPSLocation(0,0,0,0), "open");

        GraphBuilder<EvNode, PlanningEdge> gb = new GraphBuilder<>();
        gb.addNode(new EvNode(0, 0, 0, 0, 0, 0, 0, s0));
        gb.addNode(new EvNode(1, 1, 0, 0, 0, 0, 0));
        gb.addNode(new EvNode(2, 2, 0, 0, 0, 0, 0, s1));
        gb.addNode(new EvNode(3, 3, 0, 0, 0, 0, 0));
        gb.addNode(new EvNode(4, 4, 0, 0, 0, 0, 0, s2));
        gb.addNode(new EvNode(5, 5, 0, 0, 0, 0, 0));


//        gb.addEdge(new EvEdge(0, 1, 0, 0, 3, 20));
//        gb.addEdge(new EvEdge(1, 3, 0, 0, 3, 20));
//        gb.addEdge(new EvEdge(0, 2, 0, 0, 2, 30));
//        gb.addEdge(new EvEdge(2, 3, 0, 0, 2, 30));
//        gb.addEdge(new EvEdge(0, 3, 0, 0, 5, 55));
//        gb.addEdge(new EvEdge(3, 4, 0, 0, 2, 50));

//        gb.addEdge(new EvEdge(0, 1, 0, 0, 3, 2000));
//        gb.addEdge(new EvEdge(1, 3, 0, 0, 3, 1000));
//        gb.addEdge(new EvEdge(0, 2, 0, 0, 2, 15000));
//        gb.addEdge(new EvEdge(2, 3, 0, 0, 2, 15000));
//        gb.addEdge(new EvEdge(0, 3, 0, 0, 5, 55000));
//        gb.addEdge(new EvEdge(3, 4, 0, 0, 2, 10000));

//        gb.addEdge(new EvEdge(0, 1, 0, RoadType.UNKNOWN, 10, 10));
//        gb.addEdge(new EvEdge(0, 5, 0, RoadType.UNKNOWN, 40, 3));
//        gb.addEdge(new EvEdge(1, 2, 0, RoadType.UNKNOWN, 15, 2));
//        gb.addEdge(new EvEdge(1, 4, 0, RoadType.UNKNOWN, 25, -3));
//        gb.addEdge(new EvEdge(2, 1, 0, RoadType.UNKNOWN, 18, -1));
//        gb.addEdge(new EvEdge(3, 2, 0, RoadType.UNKNOWN, 30, 4));
//        gb.addEdge(new EvEdge(4, 3, 0, RoadType.UNKNOWN, 5, 1));
//        gb.addEdge(new EvEdge(4, 2, 0, RoadType.UNKNOWN, 23, 4));
//        gb.addEdge(new EvEdge(5, 4, 0, RoadType.UNKNOWN, 11, 3));
////
//        gb.addEdge(new EvEdge(1, 5, 0, RoadType.UNKNOWN, 15, -2));
//        gb.addEdge(new EvEdge(0, 2, 0, RoadType.UNKNOWN, 30, 3));
//
//        Graph<EvNode, PlanningEdge> g = gb.createGraph();
//
//        evGraph = new EvGraph<>(g);


    }

    @org.junit.Test
    public void testModifiedDijkstra() throws Exception {
//        GoalChecker<EvNode, PlanningEdge> gc = new ByNodeIdsGoalChecker<>(4);
//        GoalChecker<EvNode, PlanningEdge> gc2 = new AllChargersGC<>(3);
//        GoalChecker<EvNode, PlanningEdge> gc3 = new AllNodesAreGoalsChecker<>(6);
//        GoalChecker<EvNode, PlanningEdge> gc5 = new ChargerWithGoalsChecker<EvNode, PlanningEdge>(-2, 3);
//        Set<Integer> goalNodes = new HashSet<>();
//        goalNodes.add(1);
//        goalNodes.add(2);
//        goalNodes.add(3);
//        GoalChecker<EvNode, PlanningEdge> gc4 = new ByNodeIdsGoalChecker<EvNode, PlanningEdge>(goalNodes, false);
//        List<Solution> sols;
//
//        int expectedResult = 7;
//        sols = ModifiedDijkstra.modifiedDijkstra(0, gc5, evGraph, 605, 1);
//        System.out.println(sols);
//        assertEquals(expectedResult, sols.get(0).consumption);
//
//        expectedResult = 90;
//        sols = ModifiedDijkstra.modifiedDijkstra(0, gc, evGraph, 600, 1);
//        assertEquals(expectedResult, sols.get(0).consumption);
//
//
//        expectedResult = 110;
//        sols = ModifiedDijkstra.modifiedDijkstra(0, gc, evGraph, 650, 1);
//        assertEquals(expectedResult, sols.get(0).consumption);
//
//        expectedResult = 110;
//        sols = ModifiedDijkstra.modifiedDijkstra(0, gc4, evGraph, 650, 1);
//        System.out.println(sols);
//        assertEquals(expectedResult, sols.get(0).consumption);
    }

    @org.junit.Test
    public void testModifiedDijkstra1() throws Exception {

    }
//
//    @org.junit.Test
//    public void testModifiedBackwardDijkstra() throws Exception {
//        GoalChecker<EvNode, PlanningEdge> gc = new ChargersAreGoalsBackwardGC<>(6);
//        List<Solution> sols = ModifiedDijkstra.modifiedBackwardDijkstra(4, gc, evGraph, ChargingModel.MIN_STATE_OF_CHARGE);
//        System.out.println(sols);
//
//    }

    @org.junit.Test
    public void testModifiedDijkstra2() throws Exception {

    }
}