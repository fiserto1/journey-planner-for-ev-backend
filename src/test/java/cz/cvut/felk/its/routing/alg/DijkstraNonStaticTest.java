package cz.cvut.felk.its.routing.alg;

import cz.agents.basestructures.Graph;
import cz.agents.basestructures.GraphBuilder;
import cz.cvut.felk.its.evstructures.chargers.ChargingModel;
import cz.cvut.felk.its.routing.structures.goalchecker.AllNodesAreGoalsChecker;
import cz.cvut.felk.its.routing.structures.EvLabel;
import cz.cvut.felk.its.routing.structures.Path;
import cz.cvut.felk.its.routing.structures.graph.EvGraph;
import cz.cvut.felk.its.routing.structures.graph.SearchGraph;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvNode;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DijkstraNonStaticTest {

	Graph<EvNode, EvEdge> graph;


	@Before
	public void setUp() throws Exception {
//		Supercharger s0 = new Supercharger(-1, "s0", new GPSLocation(0,0,0,0), "open");
//		Supercharger s1 = new Supercharger(-2, "s1", new GPSLocation(0,0,0,0), "open");
//		Supercharger s2 = new Supercharger(-3, "s2", new GPSLocation(0,0,0,0), "open");

		GraphBuilder<EvNode, EvEdge> gb = new GraphBuilder<>();
		gb.addNode(new EvNode(0));
		gb.addNode(new EvNode(1));
		gb.addNode(new EvNode(2));
		gb.addNode(new EvNode(3));
		gb.addNode(new EvNode(4));
		gb.addNode(new EvNode(5));


		gb.addEdge(new EvEdge(0, 1, 7, null, 10));
		gb.addEdge(new EvEdge(0, 2, 9, null, 40));
		gb.addEdge(new EvEdge(0, 5, 14, null, 15));
		gb.addEdge(new EvEdge(1, 2, 10, null, 25));
		gb.addEdge(new EvEdge(1, 3, 15, null, 18));
		gb.addEdge(new EvEdge(2, 3, 11, null, 30));
		gb.addEdge(new EvEdge(2, 5, 2, null, 5));
		gb.addEdge(new EvEdge(3, 4, 6, null, 23));
		gb.addEdge(new EvEdge(4, 5, 9, null, 11));

		graph = gb.createGraph();
	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testRun() throws Exception {

		SearchGraph<EvLabel> searchGraph = new EvGraph<>(graph);
		AllNodesAreGoalsChecker<EvLabel> gc = new AllNodesAreGoalsChecker<>();
		EvLabel startFNode = new EvLabel(0);

		List<EvLabel> nodes = new ArrayList<>();
		EvLabel firstStep = new EvLabel(2, new int[]{9}, startFNode, null, 40, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);
		EvLabel secondStep = new EvLabel(3, new int[]{20}, firstStep, null, 70, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);
		EvLabel thirdStep = new EvLabel(4, new int[]{26}, secondStep, null, 93, null, ChargingModel.MAX_STATE_OF_CHARGE, 0);

		nodes.add(startFNode);
		nodes.add(firstStep);
		nodes.add(secondStep);
		nodes.add(thirdStep);
		Path<EvLabel> expectedPath = new Path<>(new int[] { 26 }, nodes);

		DijkstraNonStatic<EvLabel> dijkstra = new DijkstraNonStatic<>(searchGraph, startFNode, gc);

		dijkstra.run();


		List<Path<EvLabel>> paths = dijkstra.getPaths();
		Path<EvLabel> pathToForthNode = paths.get(5);

		paths.forEach(path -> System.out.println(path.prettyPring()));
		Assert.assertEquals(pathToForthNode, expectedPath);
	}
}