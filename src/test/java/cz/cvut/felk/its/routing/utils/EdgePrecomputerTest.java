package cz.cvut.felk.its.routing.utils;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class EdgePrecomputerTest {

	@Test
	public void testIndexes() throws Exception {
		List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
		for (int i = 5; i < 14; i++) {
			System.out.println(String.format("index %d : %d %d %d %d %d",
					i, integers.get(0), integers.get(i/4), integers.get(i/2),
					integers.get(3*i/4),integers.get( i-1)));
		}

	}
}