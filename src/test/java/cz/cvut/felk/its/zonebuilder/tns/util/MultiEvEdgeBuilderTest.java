package cz.cvut.felk.its.zonebuilder.tns.util;

import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.MultiEvEdge;
import cz.cvut.felk.its.evstructures.PlanningEdge;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Tomas on 10-May-16.
 */
public class MultiEvEdgeBuilderTest {

    EvEdge e1, e2, e3;

    @org.junit.Before
    public void setUp() throws Exception {
//        Supercharger s0 = new Supercharger(-1, "s0", new GPSLocation(0,0,0,0), "open");
//        Supercharger s1 = new Supercharger(-2, "s1", new GPSLocation(0,0,0,0), "open");
//        Supercharger s2 = new Supercharger(-3, "s2", new GPSLocation(0,0,0,0), "open");
//
//        GraphBuilder<EvNode, PlanningEdge> gb = new GraphBuilder<>();
//        gb.addNode(new EvNode(0, 0, 0, 0, 0, 0, 0, s0));
//        gb.addNode(new EvNode(1, 1, 0, 0, 0, 0, 0));
//        gb.addNode(new EvNode(2, 2, 0, 0, 0, 0, 0, s1));
//        gb.addNode(new EvNode(3, 3, 0, 0, 0, 0, 0));
//        gb.addNode(new EvNode(4, 4, 0, 0, 0, 0, 0, s2));
//
//        gb.addEdge(new EvEdge(0, 1, 0, RoadType.UNKNOWN, 3, 20));
//        gb.addEdge(new EvEdge(1, 3, 0, RoadType.UNKNOWN, 3, 20));
//        gb.addEdge(new EvEdge(0, 2, 0, RoadType.UNKNOWN, 2, 30));
//        gb.addEdge(new EvEdge(2, 3, 0, RoadType.UNKNOWN, 2, 30));
//        gb.addEdge(new EvEdge(0, 3, 0, RoadType.UNKNOWN, 5, 55));
//        gb.addEdge(new EvEdge(3, 4, 0, RoadType.UNKNOWN, 2, 50));
//        e1 = new EvEdge(0, 1, 10, RoadType.UNKNOWN, 6, 20, null, null);
//        e2 = new EvEdge(0, 1, 5, RoadType.UNKNOWN, 3, 15, null, null);
//        e3 = new EvEdge(0, 1, 5, RoadType.UNKNOWN, 3, 15, null, null);


    }

    @Test
    public void testAddEvEdge() throws Exception {
        MultiEdgeBuilder multiEdgeBuilder = new MultiEdgeBuilder();
        List<EvEdge> edges = new ArrayList<>();
        edges.add(e1);
        edges.add(e2);
        MultiEvEdge multiEvEdge = new MultiEvEdge(0, 1, 0, edges, new int[]{0,1,1,-1,-1,-1});
        Set<PlanningEdge> expectedResult = new HashSet<>();
        expectedResult.add(multiEvEdge);

        multiEdgeBuilder.addEvEdge(e1, 0);
        multiEdgeBuilder.addEvEdge(e2, 1);
        multiEdgeBuilder.addEvEdge(e3, 2);

        assertEquals(expectedResult, multiEdgeBuilder.createMultiEdges());

    }
}