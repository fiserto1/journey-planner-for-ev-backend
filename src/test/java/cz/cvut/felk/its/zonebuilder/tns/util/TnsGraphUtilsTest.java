package cz.cvut.felk.its.zonebuilder.tns.util;

import cz.agents.geotools.GPSLocationTools;
import cz.cvut.felk.its.zonebuilder.GraphTools;
import cz.cvut.felk.its.zonebuilder.tns.TnsEdge;
import cz.cvut.felk.its.zonebuilder.tns.TnsGraph;
import cz.cvut.felk.its.zonebuilder.tns.TnsNode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TnsGraphUtilsTest {

	private TnsGraph graph;

	@Before
	public void setUp() throws Exception {
		graph = new TnsGraph();

		graph.addNode(new TnsNode(1, 1, GPSLocationTools.createGPSLocation(1,1,1, GraphTools.GERMANY_SRID), null));
		graph.addNode(new TnsNode(2, 2, GPSLocationTools.createGPSLocation(1,1,1, GraphTools.GERMANY_SRID), null));
		graph.addNode(new TnsNode(3, 3, GPSLocationTools.createGPSLocation(1,1,1, GraphTools.GERMANY_SRID), null));
		graph.addNode(new TnsNode(4, 4, GPSLocationTools.createGPSLocation(1,1,1, GraphTools.GERMANY_SRID), null));
		graph.addNode(new TnsNode(5, 5, GPSLocationTools.createGPSLocation(1,1,1, GraphTools.GERMANY_SRID), null));

		Map<String, String> tags = new HashMap<>();
		List<TnsNode> via1 = new ArrayList<>();
		List<TnsNode> via2 = new ArrayList<>();
		List<TnsNode> via3 = new ArrayList<>();
		List<TnsNode> via4 = new ArrayList<>();
		graph.addEdge(new TnsEdge(4,via1,2,1,tags));
		graph.addEdge(new TnsEdge(2,via2,1,2,tags));
		graph.addEdge(new TnsEdge(1,via3,3,3,tags));
		graph.addEdge(new TnsEdge(3,via4,5,4,tags));
	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testSimplify() throws Exception {
		TnsGraphUtils.simplify(graph);

		graph.getAllEdges().forEach(System.out::println);
		graph.getAllNodes().forEach(System.out::println);
	}
}