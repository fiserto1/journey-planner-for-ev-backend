package cz.cvut.felk.its.zonebuilder;

import cz.agents.basestructures.Graph;
import cz.agents.basestructures.GraphBuilder;
import cz.cvut.felk.its.evstructures.EvNode;
import cz.cvut.felk.its.evstructures.PlanningEdge;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GraphToolsTest {

	private Graph<EvNode, PlanningEdge> graph;
	private GraphBuilder<EvNode, PlanningEdge> graphBuilder;

	@Before
	public void setUp() throws Exception {
		graphBuilder = new GraphBuilder<>();

//		graphBuilder.addNode();
//		graphBuilder.addNode();
//		graphBuilder.addNode();
//		graphBuilder.addNode();
//		graphBuilder.addNode();
//
//		graphBuilder.addEdge();
//		graphBuilder.addEdge();
//		graphBuilder.addEdge();
//		graphBuilder.addEdge();

		graph = graphBuilder.createGraph();
	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testComputeEdgeConsumption() throws Exception {

	}

	@Test
	public void testSimplifyGraph() throws Exception {
//		Graph<EvNode, PlanningEdge> simplifiedGraph = GraphTools.simplifyGraph(graph);
//		System.out.println(graph);
//		System.out.println("vs");
//		System.out.println(simplifiedGraph);
	}

	@Test
	public void testComputeNextPartConsumption() throws Exception {

	}
}