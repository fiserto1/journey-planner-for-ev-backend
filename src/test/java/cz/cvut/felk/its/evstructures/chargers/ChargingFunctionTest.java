package cz.cvut.felk.its.evstructures.chargers;

import cz.cvut.felk.its.evstructures.BatteryType;

import static junit.framework.TestCase.assertEquals;

public class ChargingFunctionTest {

	@org.junit.Test
	public void testGetChargingTime() throws Exception {
//		double[][] supportVectors = {
//				{0, ChargingModel.MIN_STATE_OF_CHARGE},
//				{2400000,0.8*ChargingModel.MAX_STATE_OF_CHARGE},
//				{4500000,ChargingModel.MAX_STATE_OF_CHARGE}
//		};

		ChargingFunction chFunction = new ChargingFunction(BatteryType.TESLA, ChargerType.SUPERCHARGER);

		double expectedResult = 0;
		double result = chFunction.getChargingTimeMillisFromMinimum(ChargingModel.MIN_STATE_OF_CHARGE);
		assertEquals(expectedResult, result);

		expectedResult = 2400000;
		result = chFunction.getChargingTimeMillisFromMinimum(0.8*ChargingModel.MAX_STATE_OF_CHARGE);
		assertEquals(expectedResult, result);

		expectedResult = 4500000;
		result = chFunction.getChargingTimeMillisFromMinimum(ChargingModel.MAX_STATE_OF_CHARGE);
		assertEquals(expectedResult, result);

		expectedResult = 4395000;
		result = chFunction.getChargingTimeMillisFromMinimum(0.99*ChargingModel.MAX_STATE_OF_CHARGE);
		assertEquals(expectedResult, result);
	}
}