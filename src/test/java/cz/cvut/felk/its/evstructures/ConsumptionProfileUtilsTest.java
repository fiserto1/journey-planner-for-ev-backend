package cz.cvut.felk.its.evstructures;

import cz.cvut.felk.its.evstructures.chargers.ChargingFunction;
import cz.cvut.felk.its.routing.structures.EvLabel;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ConsumptionProfileUtilsTest {

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testComputePossibleChargingPairs() throws Exception {
		EvLabel evLabel = new EvLabel(3, new int[]{26}, null, new ConsumptionProfile(81500,4000,81000), 26, new EvLabel(1), 500, 0);
		List<double[]> doubles = ConsumptionProfileUtils.computePossibleChargingPairs(evLabel, new ChargingFunction(), BatteryType.TESLA);

		Assert.assertEquals(4067673, doubles.get(0)[0], 0.1);
		Assert.assertEquals(500, doubles.get(0)[1], 0.1);
		Assert.assertEquals(4500026, doubles.get(1)[0], 0.1);
		Assert.assertEquals(4000, doubles.get(1)[1], 0.1);
	}
}