package cz.cvut.felk.its.evstructures;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConsumptionProfileTest {

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testGetConsumption() throws Exception {

	}

	@Test
	public void testComputeSocInB() throws Exception {

	}

	@Test
	public void testIsDominatedBy() throws Exception {
		ConsumptionProfile cp1 = new ConsumptionProfile(4588, 80912, 4088);
		ConsumptionProfile cp2 = new ConsumptionProfile(4633, 80867, 4133);
		ConsumptionProfile cp3 = new ConsumptionProfile(4633, 80867, 4133);

		Assert.assertFalse(cp1.isDominatedBy(cp2));
		Assert.assertTrue(cp2.isDominatedBy(cp1));
		Assert.assertFalse(cp2.isDominatedBy(cp3));
	}

	@Test
	public void testEquals() throws Exception {
		ConsumptionProfile cp1 = new ConsumptionProfile(4588, 80912, 4088);
		ConsumptionProfile cp2 = new ConsumptionProfile(4633, 80867, 4133);
		ConsumptionProfile cp3 = new ConsumptionProfile(4633, 80867, 4133);

		Assert.assertFalse(cp1.equals(cp2));
		Assert.assertTrue(cp2.equals(cp3));

	}

	@Test
	public void testCompareTo() throws Exception {
		ConsumptionProfile cp1 = new ConsumptionProfile(4588, 80912, 4088);
		ConsumptionProfile cp2 = new ConsumptionProfile(4633, 80867, 4133);
		ConsumptionProfile cp3 = new ConsumptionProfile(4633, 80867, 4133);

		Assert.assertTrue(cp1.compareTo(cp2) < 0);
		Assert.assertTrue(cp2.compareTo(cp1) > 0);
		Assert.assertEquals(0, cp2.compareTo(cp3));
		Assert.assertEquals(0, cp3.compareTo(cp2));

	}


	@Test
	public void testCompareToNonDominatedCP() throws Exception {
		ConsumptionProfile cp1 = new ConsumptionProfile(4588, 80812, 4188);
		ConsumptionProfile cp2 = new ConsumptionProfile(4633, 80967, 4033);

		Assert.assertFalse(cp1.compareTo(cp2) < 0);
		Assert.assertFalse(cp2.compareTo(cp1) > 0);
		Assert.assertEquals(0, cp2.compareTo(cp1));
		Assert.assertEquals(0, cp1.compareTo(cp2));

	}
}