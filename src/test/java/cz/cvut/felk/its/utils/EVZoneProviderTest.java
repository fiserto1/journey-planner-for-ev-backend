package cz.cvut.felk.its.utils;

import cz.cvut.felk.its.evstructures.EVZone;
import cz.cvut.felk.its.evstructures.EvEdge;
import cz.cvut.felk.its.evstructures.EvEdgeBetweenChargers;
import cz.cvut.felk.its.evstructures.EvNode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EVZoneProviderTest {

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testGetZone() throws Exception {
		EVZone<EvNode, EvEdge> zone = EVZoneProvider.INSTANCE.getZone();

		int counter = 0;
		for (EvEdge evEdge : zone.getGraph().getAllEdges()) {
			if (evEdge instanceof EvEdgeBetweenChargers) {
				EvEdgeBetweenChargers evBChEdge = (EvEdgeBetweenChargers) evEdge;
				counter += evBChEdge.getPaths().size();
			}
		}
		System.out.println(String.format("nodes: %d, edges: %d", zone.getNumOfChargers(),counter));

//		Set<GPSLocation> allNodes = new HashSet<>(zone.getGraph().getAllNodes());
//		zone.getGraph().getAllEdges().stream()
//				.filter(evEdge -> !(evEdge instanceof EvEdgeBetweenChargers))
//				.forEach(evEdge -> {
//					allNodes.addAll(evEdge.viaNodes.stream().collect(Collectors.toList()));
//				});
//		System.out.println(allNodes.size());
//		SerializeUtil.serializeObject(allNodes, "nod.ser");
//

//		JSONConverter jsonConverter = new JSONConverter();
//		List<GPSLocation> nodes = new ArrayList<>(zone.getGraph().getAllNodes());
//		nodes.addAll(zone.getViaNodes().keySet());
//
//		jsonConverter.convertLocationsToJSON("ge-graph-kd-tree-nodes.geojson",nodes);
//
//		List<GPSLocation> part1 = nodes.subList(0, nodes.size() / 2);
//		List<GPSLocation> part2 = nodes.subList((nodes.size() / 2), nodes.size());
//		jsonConverter.convertLocationsToJSON("ge-graph-kd-tree-nodes-part1.geojson",part1);
//		jsonConverter.convertLocationsToJSON("ge-graph-kd-tree-nodes-part2.geojson",part2);
	}
}