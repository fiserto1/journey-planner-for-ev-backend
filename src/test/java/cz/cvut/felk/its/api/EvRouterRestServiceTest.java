package cz.cvut.felk.its.api;

import cz.cvut.felk.its.api.structures.Coordinates;
import cz.cvut.felk.its.api.structures.EvRequest;
import cz.cvut.felk.its.routing.structures.routers.RouterType;
import cz.cvut.felk.its.utils.EVZoneProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class EvRouterRestServiceTest {

	@Before
	public void setUp() throws Exception {
		EVZoneProvider evZoneProvider = EVZoneProvider.INSTANCE;

	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testGetRoutes() throws Exception {
//		EvRequest evRequest = new EvRequest("test", new Coordinates(52310058, 8235696), new Coordinates(52590214, 13336881),
//				80);
//		EvRequest evRequest = new EvRequest("test", new Coordinates(51378638, 9041748), new Coordinates(52348763, 10612793),
//				60);
//		EvRequest evRequest = new EvRequest("test", new Coordinates(51611195, 8624268), new Coordinates(52529590, 12843018),
//			    60, 36);

//		EvRequest evRequest = new EvRequest("test", new Coordinates(53813626, 9777832), new Coordinates(48414619, 11370850),
//				60, 36);

//		EvRequest evRequest = new EvRequest("test", new Coordinates(52835958, 12779846), new Coordinates(53001562, 12518921),
//				40, 50);

//		EvRequest evRequest = new EvRequest("test", new Coordinates(51755303, 13679910), new Coordinates(51771930, 13686991),
//				40, 50);

//		EvRequest evRequest = new EvRequest("test", new Coordinates(52019121, 13375854), new Coordinates(51878187, 13318176),
//				50, 50);

		EvRequest evRequest = new EvRequest("test", new Coordinates(50219095, 8316650), new Coordinates(52321911, 11821289),
				50, 50);



		//50.219095, 8.316650, 52.321911, 11.821289
		List<RouterType> routerTypes = new ArrayList<>();
//		routerTypes.add(RouterType.FAST);
		routerTypes.add(RouterType.MO_FAST);
		Response routes = new EvRouterRestService().getRoutes(evRequest, routerTypes);

//		ObjectMapper mapper = new ObjectMapper();
//		mapper.writeValue(System.out, routes.getEntity());
	}
}